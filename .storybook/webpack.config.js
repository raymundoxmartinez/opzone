const path = require('path')

module.exports = ({ config }) => {
    config.resolve.extensions.push('.ts', '.tsx')
    config.resolve.alias = {
        '@src': path.join(__dirname, '../src'),
        '@components': path.join(__dirname, '../components'),
        '@pages': path.join(__dirname, '../pages'),
        '@services': path.join(__dirname, '../src/services'),
        '@hooks': path.join(__dirname, '../src/hooks'),
        '@graphql': path.join(__dirname, '../src/graphql'),
        '@redux': path.join(__dirname, '../src/redux'),
    }

    return config
}
