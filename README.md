# OmniAgora

## Description

OmniAgora is a serverless web application built with React and Amplify using AWS
cloud services. The image below depicts the architecture used to build the app.

![alt text](https://miro.medium.com/v2/resize:fit:4800/0*oTFla168vkxDc_zR)

This is a [Next.js](https://nextjs.org/) project bootstrapped with
[`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

### Services used in the architecture:

1. [DynamoDB](https://aws.amazon.com/dynamodb/) (NoSQL Database)
2. [Lambda](https://aws.amazon.com/lambda/) (Server side computing)
3. [API Gateway](https://aws.amazon.com/api-gateway/) (HTTP calls interface)
4. [Cognito](https://aws.amazon.com/cognito/) (Authentication)
5. [S3](https://aws.amazon.com/s3/) (Cloud storage)
6. [AppSync](https://aws.amazon.com/appsync/) (serverless GraphQL and Pub/Sub
   APIs)
7. [Amplify](https://aws.amazon.com/amplify/) (Client side)
8. [React](https://react.dev/) and [Next.js](https://nextjs.org/) (Client side)
9. [Storybook](https://storybook.js.org/) (Client side)

## Folder Structure

### /amplify

This directory contains the backend logic. The functions generated through the
amplify cli are found here _/amplify/backend/function_. This is also where the
data models for the the graphql resources can be edited
_/amplify/backend/api/omniagora/schema.graphql_.

### /components

This is where the components used across the pages are located.

### /pages

This is where the top level components are located. These correspond to
pages/routes in the app and each is built using components from the /components
directory.

### /src/hooks

This is where the resuable hooks are defined. There is a custom hook
corresponding to each backend resource. Use these hooks to initiate any api
requests associated with these resources.

### /src/redux

This is where the redux state slices are defined.

### /src/services

This is where the graphQl services are called from corresponding resource
functions.

### /styles

Global styles, variables and mixins can be found here.

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the
result.

## How things are wired

An overview of the current hierachy of a page component.

![alt text](https://omniagorastore124213-dev.s3.amazonaws.com/public/Untitled.png)
