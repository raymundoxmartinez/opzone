/** @type {import('next').NextConfig} */
const withPWA = require('next-pwa')({
  dest: 'public',
  swSrc: 'service-worker.js',
})
module.exports = withPWA({
  reactStrictMode: true,
  typescript: {
    ignoreBuildErrors: true,
  },
})
