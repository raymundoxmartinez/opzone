import React, { useState } from 'react'
import cryptoRandomString from 'crypto-random-string'
import { API, graphqlOperation, Auth, Storage } from 'aws-amplify'
import { useMediaQuery } from '@mui/material'

// import { createNFT } from '@src/graphql/mutations'
import { toast } from '@components/Toast'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import PageLayout from '@components/Layouts/PageLayout'
import Mobile from '@components/Layouts/NFTSLayout/Mobile'

import useSWR from 'swr'
function getNFTS() {
  return fetch('/nfts').then((res) => res.json())
}
//@ts-ignore
const NFTSManagerView = (props) => {
  const [isCreateNFTDialogOpen, setIsCreateNFTDialogOpen] = useState(false)
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))

  const { data: nfts, error } = useSWR('nfts', getNFTS)

  const handleCloseCreateNFTDialog = () => {
    setIsCreateNFTDialogOpen(false)
  }
  const handleOpenCreateNFTDialog = () => {
    setIsCreateNFTDialogOpen(true)
  }

  //   //@ts-ignore
  //   const handleCreateNFT = async ({
  //     title,
  //     description,
  //     address = '',
  //     price = 100.0,
  //     images,
  //   }: {
  //     title: string
  //     description: string
  //     address: string
  //     price: number
  //     images: any[]
  //   }) => {
  //     const { attributes, username, ...rest } =
  //       await Auth.currentAuthenticatedUser()
  //     let awsImages
  //     try {
  //       const imagePromises = images.map(async (image) => {
  //         const objKey = cryptoRandomString({ length: 10 }) + title
  //         const { key } = (await Storage.put(objKey, image, {
  //           contentType: image.type,
  //         })) as { key: string }
  //         return key
  //       })

  //       await Promise.all(imagePromises)
  //         .then((results) => {
  //           awsImages = results
  //         })
  //         .catch((err) => {
  //           throw err
  //         })
  //     } catch (error) {
  //       throw error
  //     }
  //     try {
  //       const {
  //         data: { createNFT: newNFT },
  //       } = await (API.graphql(
  //         graphqlOperation(createNFT, {
  //           input: {
  //             title,
  //             address,
  //             description,
  //             price,
  //             images: awsImages,
  //             userID: attributes.sub,
  //           },
  //         })
  //       ) as Promise<any>)
  //       const nftsWithImages = await attachImages([newNFT])

  //       //@ts-ignore
  //       setNFTS((prevState) =>
  //         prevState ? [...prevState, newNFT] : [nftsWithImages]
  //       )
  //       setIsCreateNFTDialogOpen(false)
  //       toast.success({
  //         content: `NFT ${title} was successfully created.`,
  //         timeout: toast.defaultTimeout,
  //       })
  //     } catch (error) {
  //       setIsCreateNFTDialogOpen(false)
  //       toast.error({
  //         content: `An error occurred while attempting to create nft ${title}.`,
  //         timeout: toast.defaultTimeout,
  //       })
  //     }
  //   }

  const attachImages = async (nfts: any[]): Promise<any[]> => {
    if (!Array.isArray(nfts)) return []
    const promises = nfts.map(async (nft) => {
      const imagePromises = nft.images.map(async (key: string) => {
        if (!key) return key
        const imageUrl = await Storage.get(key)
        return imageUrl
      })
      const res = await Promise.all(imagePromises)
        .then((results) => {
          nft.images = results
          return nft
        })
        .catch((err) => {
          console.log(err)
        })
      return res
    })
    const res = await Promise.all(promises)
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    return res
  }

  const nftManagerViewProps = {
    isCreateNFTDialogOpen,
    nfts,
    handleCloseCreateNFTDialog,
    // handleCreateNFT,
    handleOpenCreateNFTDialog,
  }
  return <>{matchesMobile ? <Mobile {...nftManagerViewProps} /> : null}</>
}

//@ts-ignore
NFTSManagerView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <PageLayout title="NFTS">{page}</PageLayout>
    </DashboardLayout>
  )
}
export default NFTSManagerView
