import React, { useState, useEffect, useCallback } from 'react'
import { API, graphqlOperation, Storage } from 'aws-amplify'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import { useMediaQuery } from '@mui/material'

import { toast } from '@components/Toast'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import Mobile from '@components/Layouts/ProductsLayout/Mobile'

function getNFTS() {
  return fetch('/nfts').then((res) => res.json())
}

const NFTView = (props: any) => {
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))
  const [isFundProjectDialogOpen, toggleFundProjectDialog] = useState(false)
  const [isConfirmDeleteDialogOpen, setIsConfirmDeleteDialogOpen] =
    useState(false)
  const [nft, setNFT] = useState<any>(null)
  const [showWalletToast, setShowWalletToast] = useState<any>(false)
  const router = useRouter()
  const { id: nftId } = router.query

  const { data: nfts, error } = useSWR('nfts', getNFTS)

  const handleOpenFundProjectDialog = () => {
    toggleFundProjectDialog(true)
  }

  const handlehideWalletToast = () => {
    setShowWalletToast(false)
  }

  const handleOpenConfirmDeleteDialog = () => {
    setIsConfirmDeleteDialogOpen(true)
  }
  const handleCloseConfirmDeleteDialog = () => {
    setIsConfirmDeleteDialogOpen(false)
  }

  const handleGetNFT = useCallback(
    (nftId) => {
      const filteredNFTS = nfts.filter((nft: any) => nft.id === nftId)
      setNFT(filteredNFTS[0])
    },
    [nfts]
  )

  useEffect(() => {
    if (nftId && nfts && nfts.length) handleGetNFT(nftId)
  }, [nftId, handleGetNFT, nfts])

  const nftViewProps = {
    showWalletToast,
    product: nft,
    isConfirmDeleteDialogOpen,
    handleCloseConfirmDeleteDialog,
    handleOpenConfirmDeleteDialog,
    handleOpenFundProjectDialog,
    handlehideWalletToast,
    router,
  }
  return <>{matchesMobile ? <Mobile {...nftViewProps} /> : null}</>
}
//@ts-ignore
NFTView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <section>{page}</section>
    </DashboardLayout>
  )
}
export default NFTView
