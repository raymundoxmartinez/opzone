import React, { useState, useEffect, useCallback } from 'react'
import { useMediaQuery } from '@mui/material'

import DashboardLayout from '@components/Layouts/DashboardLayout'
import PageLayout from '@components/Layouts/PageLayout'
import Mobile from '@components/Layouts/TrailsLayout/Mobile'
import {
  useTrails,
  useMapbox,
  useApp,
  useConditions,
  useTransactions,
  useLocationViews,
  useLocations,
  useRewards,
} from '@src/hooks'
import { useAssets } from '@src/hooks'

//@ts-ignore
const TrailsManagerView = (props) => {
  const {
    postTrail,
    fetchTrails,
    postTrailLocationView,
    normalizeTrail,
    trails,
  } = useTrails()
  const { handlePostAsset, fetchAssets, fetchAsset, assets } = useAssets()
  const { handlePostCondition } = useConditions()
  const { handlePostTransaction } = useTransactions()
  const { handlePostLocationView } = useLocationViews()
  const { handlePostLocation } = useLocations()
  const { postReward } = useRewards()
  const { user } = useApp()
  const { handleFetchStaticMapUrl, handleFetchAddressFromCoordinates } =
    useMapbox()

  const [isCreateDialogOpen, setIsTrailsCreateDialogOpen] = useState(false)
  const [trailsWithImageUrl, setTrailsWithImageUrl] = useState<any>([])

  const handleOnCloseTrailsCreateDialog = () => {
    setIsTrailsCreateDialogOpen(false)
  }
  const handleOnOpenTrailsCreateDialog = () => {
    setIsTrailsCreateDialogOpen(true)
  }

  const handleFetchTrails = async (force: boolean = false) => {
    const trailsResponse =
      trails && !force ? Object.values(trails) : await fetchTrails({})

    Promise.all(trailsResponse.map((trail) => normalizeTrail(trail)))
      .then((response: any) => {
        console.log('trailsWithImages', response)
        setTrailsWithImageUrl(response)
      })
      .catch((err: any) => {
        console.log(err)
      })
  }

  useEffect(() => {
    handleFetchTrails()
  }, [])
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))

  const trailManagerViewProps = {
    user,
    assets,
    trails: trailsWithImageUrl,
    isCreateDialogOpen,
    normalizeTrail,
    fetchAssets,
    handlePostTrail: postTrail,
    handleFetchStaticMapUrl,
    handleFetchAddressFromCoordinates,
    handlePostAsset,
    handlePostCondition,
    handlePostTransaction,
    handlePostLocationView,
    handlePostLocation,
    onPostTrailLocationView: postTrailLocationView,
    fetchTrails: handleFetchTrails,
    fetchAsset,
    postReward,
    OnCloseTrailsCreateDialog: handleOnCloseTrailsCreateDialog,
    onOpenTrailsCreateDialog: handleOnOpenTrailsCreateDialog,
  }
  return <>{matchesMobile ? <Mobile {...trailManagerViewProps} /> : null}</>
}

//@ts-ignore
TrailsManagerView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <PageLayout title="Trails">{page}</PageLayout>
    </DashboardLayout>
  )
}
export default TrailsManagerView
