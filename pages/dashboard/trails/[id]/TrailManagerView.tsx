import React, { useEffect, useCallback, useState } from 'react'
import { useRouter } from 'next/router'
import { useMediaQuery } from '@mui/material'

import DashboardLayout from '@components/Layouts/DashboardLayout'
import PageLayout from '@components/Layouts/PageLayout'
import Mobile from '@components/Layouts/TrailLayout/Mobile'
import {
  useTrails,
  useMapbox,
  useApp,
  useConditions,
  useTransactions,
  useLocationViews,
  useLocations,
  useTrailProgresses,
} from '@src/hooks'
import { useAssets } from '@src/hooks'

//@ts-ignore
const TrailManagerView = (props) => {
  const [trailWithImages, setTrailWithImages] = useState<any>(null)

  const {
    postTrail,
    fetchTrails,
    fetchTrail,
    postTrailLocationView,
    attachImages,
    normalizeTrail,
    deleteTrail,
    trails,
  } = useTrails()
  const { handlePostAsset, fetchAssets } = useAssets()
  const { handlePostCondition } = useConditions()
  const { handlePostTransaction, handleFetchTransaction } = useTransactions()
  const { handlePostLocationView } = useLocationViews()
  const { handlePostLocation } = useLocations()
  const { user } = useApp()
  const {
    postTrailProgress,
    deleteTrailProgress,
    fetchTrailProgress,
    fetchTrailProgresses,
    trailProgresses,
  } = useTrailProgresses()
  const { handleFetchStaticMapUrl, handleFetchAddressFromCoordinates } =
    useMapbox()
  const router = useRouter()

  const { id: trailID, inProgress } = router.query

  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))

  const handleFetchTrail = async (trailID: string, force: boolean = false) => {
    try {
      const trail =
        trails && trails[trailID] && !force
          ? trails[trailID]
          : await fetchTrail(trailID)

      const normalizedTrail = await normalizeTrail(trail)
      console.log('normalizedTrail', normalizedTrail)
      setTrailWithImages(normalizedTrail)
    } catch (error) {
      // Handle any errors appropriately (e.g., show an error message)
      console.error('Error fetching trail:', error)
    }
  }

  useEffect(() => {
    if (trailID) {
      handleFetchTrail(trailID)
    }
  }, [trailID])

  const trailManagerViewProps = {
    trails: [],
    user,
    trail: trailWithImages,
    trailProgresses,
    attachImages,
    normalizeTrail,
    fetchTrails,
    fetchTrail: handleFetchTrail,
    fetchAssets,
    deleteTrail,
    fetchTrailProgresses,
    fetchTrailProgress,
    deleteTrailProgress,
    fetchTransaction: handleFetchTransaction,
    postTrailProgress,
    handlePostTrail: postTrail,
    handleFetchStaticMapUrl,
    handleFetchAddressFromCoordinates,
    handlePostAsset,
    handlePostCondition,
    handlePostTransaction,
    handlePostLocationView,
    handlePostLocation,
    onPostTrailLocationView: postTrailLocationView,
  }
  return <>{matchesMobile ? <Mobile {...trailManagerViewProps} /> : null}</>
}

//@ts-ignore
TrailManagerView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <PageLayout>{page}</PageLayout>
    </DashboardLayout>
  )
}
export default TrailManagerView
