import React, { useState, useEffect, useCallback } from 'react'
import { useMediaQuery } from '@mui/material'

import socket from '@src/socket'
import { useUI, useXumm, useXrpl, useWallets, useApp } from '@hooks/index'
import { toast } from '@components/Toast'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import PageLayout from '@components/Layouts/PageLayout'
import Mobile from '@components/Layouts/PaymentsLayout/Mobile'
import Desktop from '@components/Layouts/PaymentsLayout/Desktop'

const PaymentsManagerView = () => {
  const [qrCode, setQRcode] = useState('')
  const [balance, setBalance] = useState(null)
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))

  const {
    xrpl,
    xrplClient,
    handleGetBalance: xrpl__handleGetBalance,
  } = useXrpl()

  const {
    payments__isPayloadDialogOpen,
    payments__isSmartContractSelectDialogOpen,
    payments__handleOpenPayloadDialog,
    payments__handleClosePayloadDialog,
    payments__handleOpenCreateSmartContractSelectDialog,
    payments__handleCloseCreateSmartContractSelectDialog,
    payments__handleOpenCreateWalletDialog,
    payments__handleCloseCreateWalletDialog,
  } = useUI()
  const {
    handlePostWallet,
    handleFetchWalletsByAddress,
    handleFetchWalletsByUserID,
  } = useWallets()
  const { user } = useApp()
  const {
    handlePostTransaction,
    handleXummSignIn,
    handleStoreXummCredentials,
    handleGetTransaction,
    xummUserAccount,
    xummUserToken,
    isTransactionLoading,
    isCredentialsLoading,
  } = useXumm()

  const handleSetQrCode = (qrCode: string) => {
    setQRcode(qrCode)
  }

  const handleCreateWallet = async ({
    title,
    description,
    address,
  }: {
    title: string
    description: string
    address: string
  }) => {
    const registeredWallet = await handleFetchWalletsByAddress({ address })
    if (registeredWallet.length) {
      payments__handleCloseCreateWalletDialog()
      toast.warning({
        content: 'Wallet is already registered.',
        timeout: toast.defaultTimeout,
      })
      return
    }
    const newWallet = await handlePostWallet({ title, description, address })
    if (newWallet) {
      handleFetchWalletsByUserID({ userID: user.id })
    }
    payments__handleCloseCreateWalletDialog()
  }

  const handleConnectWallet = async () => {
    const response = await handleXummSignIn()
    if (response && response.refs.qr_png) {
      setQRcode(response.refs.qr_png)
      socket.subscribe(response.refs.websocket_status)
      socket.on('connected', () => {
        socket.socket.onmessage = (message: Record<string, any>) => {
          console.log(message.data)
          const messageJson = JSON.parse(message.data)
          if (messageJson.signed) {
            toast.success({
              content: 'Xumm wallet connection was successful!',
              timeout: toast.defaultTimeout,
            })
            payments__handleClosePayloadDialog()

            handleStoreXummCredentials(messageJson.payload_uuidv4)
          }
        }
      })
      payments__handleOpenPayloadDialog()
    }
  }
  const handleGetBalance = useCallback(async () => {
    const balance = await xrpl__handleGetBalance(xummUserAccount)
    setBalance(balance)
  }, [xummUserAccount, xrpl__handleGetBalance])

  useEffect(() => {
    if (xrplClient && xummUserAccount) {
      handleGetBalance()
    }
  }, [xrplClient, handleGetBalance, xummUserAccount])

  const paymentsManagerViewProps = {
    xummUserAccount,
    xummUserToken,
    qrCode,
    balance,
    isTransactionLoading,
    isCredentialsLoading,
    xrpl,
    payments__isPayloadDialogOpen,
    payments__isSmartContractSelectDialogOpen,
    payments__handleClosePayloadDialog,
    payments__handleOpenCreateSmartContractSelectDialog,
    payments__handleCloseCreateSmartContractSelectDialog,
    payments__handleOpenPayloadDialog,
    payments__handleOpenCreateWalletDialog,
    payments__handleCloseCreateWalletDialog,
    handlePostTransaction,
    handleConnectWallet,
    handleGetBalance,
    handleSetQrCode,
    handlePostWallet,
    handleFetchWalletsByAddress,
    handleGetTransaction,
    handleCreateWallet,
  }
  return (
    <>
      {matchesMobile ? (
        <Mobile
          {...paymentsManagerViewProps}
          handleStoreXummCredentials={handleStoreXummCredentials}
          handleXummSignIn={handleXummSignIn}
        />
      ) : (
        <Desktop {...paymentsManagerViewProps} />
      )}
    </>
  )
}
//@ts-ignore
PaymentsManagerView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <PageLayout title="Payments">{page}</PageLayout>
    </DashboardLayout>
  )
}
export default PaymentsManagerView
