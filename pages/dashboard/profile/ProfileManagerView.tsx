import React, { useEffect, useState } from 'react'
import { useMediaQuery } from '@mui/material'
import { Storage } from 'aws-amplify'

import { BaseUser } from '@src/services/users/model'
import {
  useUsers,
  useApp,
  useUI,
  useTrailProgresses,
  useTrails,
} from '@hooks/index'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import PageLayout from '@components/Layouts/PageLayout'
import Desktop from '@components/Layouts/ProfileLayout/Desktop'
import Mobile from '@components/Layouts/ProfileLayout/Mobile'

//@ts-ignore
const ProfileManagerView = (props) => {
  const [isCreateRoleDialogOpen, setIsCreateRoleDialogOpen] = useState(false)
  const [trailsWithImageUrl, setTrailsWithImageUrl] = useState<any>([])

  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))
  const { updateUser } = useUsers()
  const { user, loading: loadingUser } = useApp()
  const { fetchTrailProgresses, trailProgresses } = useTrailProgresses()
  const { normalizeTrail } = useTrails()

  const {
    profile__isUpdateUserProfileDialogOpen,
    profile__handleOpenUpdateUserProfileDialog,
    profile__handleCloseUpdateUserProfileDialog,
  } = useUI()

  const attachImages = async (trails: any[]): Promise<any[]> => {
    if (!Array.isArray(trails)) return []
    const trailsWithImageUrlPromises = trails.map(async (trail) => {
      const { images, ...restofTrailDetails } = trail
      if (!images || (images && !images.length)) {
        return trail
      }
      const trailWithImageUrl = { ...restofTrailDetails }
      const imagePromises = images.map(
        async (file) => await Storage.get(file.key)
      )

      const imageUrls = await Promise.all(imagePromises)
        .then((results) => {
          return results
        })
        .catch((err) => {
          console.log(err)
          throw err
        })

      trailWithImageUrl.images = imageUrls

      return trailWithImageUrl
    })

    const trailsWithImageUrl = await Promise.all(trailsWithImageUrlPromises)
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    return trailsWithImageUrl
  }
  const handleUpdateUserProfile = async (vals: BaseUser) => {
    if (user) {
      updateUser(user.id, vals)
    }
  }
  const handleCloseCreateRoleDialog = () => {
    setIsCreateRoleDialogOpen(false)
  }
  const handleOpenCreateRoleDialog = () => {
    setIsCreateRoleDialogOpen(true)
  }
  const handleFetchTrailProgresses = async () => {
    const trailProgressesResponse = trailProgresses
      ? Object.values(trailProgresses)
      : await fetchTrailProgresses({ filter: { userID: { eq: user.id } } })
    console.log('BEFORE_trailsWithImages', trailProgressesResponse)
    const formattedTrails = trailProgressesResponse.reduce(
      (acc, { status, trail, id }, index) => {
        acc.push({ ...trail, status, trailProgressId: id })
        return acc
      },
      []
    )
    Promise.all(formattedTrails.map((trail) => normalizeTrail(trail)))
      .then((response: any) => {
        console.log('trailsWithImages', response)
        setTrailsWithImageUrl(response)
      })
      .catch((err: any) => {
        console.log(err)
      })
  }

  useEffect(() => {
    if (user) {
      handleFetchTrailProgresses()
    }
  }, [user])

  const profileManagerViewProps = {
    user,
    trails: trailsWithImageUrl,
    profile__isUpdateUserProfileDialogOpen,
    loadingUser,
    profile__handleOpenUpdateUserProfileDialog,
    profile__handleCloseUpdateUserProfileDialog,
    handleUpdateUserProfile,
    handleCloseCreateRoleDialog,
    handleOpenCreateRoleDialog,
    isCreateRoleDialogOpen,
  }
  return (
    <>
      {matchesMobile ? (
        <Mobile {...profileManagerViewProps} />
      ) : (
        <Desktop {...profileManagerViewProps} />
      )}
    </>
  )
}
//@ts-ignore
ProfileManagerView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <PageLayout title="Profile">{page}</PageLayout>
    </DashboardLayout>
  )
}
export default ProfileManagerView
