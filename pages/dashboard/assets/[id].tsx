import React, { useState, useEffect, useCallback } from 'react'
import { API, graphqlOperation, Storage } from 'aws-amplify'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import { useMediaQuery } from '@mui/material'

import { toast } from '@components/Toast'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import Mobile from '@components/Layouts/ProductsLayout/Mobile'

function getAssets() {
  return fetch('/assets').then((res) => res.json())
}

const AssetView = (props: any) => {
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))
  const [isFundProjectDialogOpen, toggleFundProjectDialog] = useState(false)
  const [isConfirmDeleteDialogOpen, setIsConfirmDeleteDialogOpen] =
    useState(false)
  const [asset, setAsset] = useState<any>(null)
  const [showWalletToast, setShowWalletToast] = useState<any>(false)
  const router = useRouter()
  const { id: assetId } = router.query

  const { data: assets, error } = useSWR('assets', getAssets)

  const handleOpenFundProjectDialog = () => {
    toggleFundProjectDialog(true)
  }

  const handlehideWalletToast = () => {
    setShowWalletToast(false)
  }

  const handleOpenConfirmDeleteDialog = () => {
    setIsConfirmDeleteDialogOpen(true)
  }
  const handleCloseConfirmDeleteDialog = () => {
    setIsConfirmDeleteDialogOpen(false)
  }

  const handleGetAsset = useCallback(
    (assetId) => {
      const filteredDatagetAssets = assets.filter(
        (asset: any) => asset.id === assetId
      )
      setAsset(filteredDatagetAssets[0])
    },
    [assets]
  )

  useEffect(() => {
    if (assetId && assets && assets.length) handleGetAsset(assetId)
  }, [assetId, handleGetAsset, assets])

  const assetViewProps = {
    showWalletToast,
    product: asset,
    isConfirmDeleteDialogOpen,
    handleCloseConfirmDeleteDialog,
    handleOpenConfirmDeleteDialog,
    handleOpenFundProjectDialog,
    handlehideWalletToast,
    router,
  }
  return <>{matchesMobile ? <Mobile {...assetViewProps} /> : null}</>
}
//@ts-ignore
AssetView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <section>{page}</section>
    </DashboardLayout>
  )
}
export default AssetView
