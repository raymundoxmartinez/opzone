import React, { useState, useEffect } from 'react'
import { Storage } from 'aws-amplify'
import cryptoRandomString from 'crypto-random-string'
import { useMediaQuery } from '@mui/material'

import awsmobile from '@src/aws-exports'
import { useAssets, useApp } from '@src/hooks'
import { toast } from '@components/Toast'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import PageLayout from '@components/Layouts/PageLayout'
import Mobile from '@components/Layouts/AssetsLayout/Mobile'

//@ts-ignore
const AssetsManagerView = (props) => {
  const { handlePostAsset, fetchAssets, deleteAsset, assets } = useAssets()
  const { user } = useApp()
  const [assetsWithImageUrl, setAssetsWithImageUrl] = useState<any>([])
  const attachImages = async (assets: any[]): Promise<any[]> => {
    if (!Array.isArray(assets)) return []
    const assetsWithImageUrlPromises = assets.map(async (asset) => {
      const assetWithImageUrl = { ...asset }
      assetWithImageUrl.imageUrl = await Storage.get(asset.file.key)
      return assetWithImageUrl
    })

    const assetsWithImageUrl = await Promise.all(assetsWithImageUrlPromises)
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    return assetsWithImageUrl
  }
  const handleOnChangeHandler = async (e) => {
    console.log(e)
    const file = e.target.files[0]
    if (!file) return
    const objKey = cryptoRandomString({ length: 10 }) + file.name

    console.log(file.name)
    await Storage.put(objKey, file, {
      contentType: file.type,
    })
      .then((result) => {
        console.log(result)
        const image = {
          userID: user.id,
          name: file.name,
          file: {
            bucket: awsmobile.aws_user_files_s3_bucket,
            region: awsmobile.aws_user_files_s3_bucket_region,
            key: objKey,
          },
        }
        return handlePostAsset(image)
      })
      .then(() => {
        return fetchAssets({ filter: { userID: { eq: user.id } } })
      })
      .then((response) => {
        return attachImages(response)
      })
      .then((response) => {
        setAssetsWithImageUrl(response)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const handleOnDeleteAssetClick = async (id: string) => {
    try {
      const deletedAsset = await deleteAsset(id)
      if (deletedAsset) {
        toast.success({
          content: 'Asset was successfully deleted!',
          timeout: toast.defaultTimeout,
        })
      }
      fetchAssets({ filter: { userID: { eq: user.id } } })
        .then((response: any) => {
          return attachImages(response)
        })
        .then((response: any) => {
          setAssetsWithImageUrl(response)
        })
        .catch((err: any) => {
          console.log(err)
        })
      return
    } catch (error) {
      console.log(error)
      toast.error({
        content: 'There was an error in deleting your asset.',
        timeout: toast.defaultTimeout,
      })
    }
  }

  const handleFetchAssets = async () => {
    const assetsResponse = assets
      ? Object.values(assets)
      : await fetchAssets({ filter: { userID: { eq: user.id } } })

    attachImages(assetsResponse)
      .then((response: any) => {
        setAssetsWithImageUrl(response)
      })
      .catch((err: any) => {
        console.log(err)
      })
  }

  useEffect(() => {
    if (user) {
      handleFetchAssets()
    }
  }, [user])
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))

  const assetManagerViewProps = {
    assets: assetsWithImageUrl,
    user,
    onChangeHandler: handleOnChangeHandler,
    onDeleteAssetClick: handleOnDeleteAssetClick,
    handlePostAsset,
    fetchAssets,
    deleteAsset,
  }
  return <>{matchesMobile ? <Mobile {...assetManagerViewProps} /> : null}</>
}

//@ts-ignore
AssetsManagerView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <PageLayout title="Assets">{page}</PageLayout>
    </DashboardLayout>
  )
}
export default AssetsManagerView
