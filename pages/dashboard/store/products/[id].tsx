import React, { useState, useEffect, useCallback } from 'react'
import { API, graphqlOperation, Storage } from 'aws-amplify'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import { useMediaQuery } from '@mui/material'

import { deleteProduct } from '@src/graphql/mutations'
import { toast } from '@components/Toast'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import Desktop from '@components/Layouts/ProductsLayout/Desktop'
import Mobile from '@components/Layouts/ProductsLayout/Mobile'

function getProducts() {
  return fetch('/products').then((res) => res.json())
}

const ProductView = (props: any) => {
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))
  const [isFundProjectDialogOpen, toggleFundProjectDialog] = useState(false)
  const [isConfirmDeleteDialogOpen, setIsConfirmDeleteDialogOpen] =
    useState(false)
  const [product, setProduct] = useState<any>(null)
  const [showWalletToast, setShowWalletToast] = useState<any>(false)
  const router = useRouter()
  const { id: productId } = router.query

  const { data: products, error } = useSWR('products', getProducts)

  const handleOpenFundProjectDialog = () => {
    toggleFundProjectDialog(true)
  }

  const handlehideWalletToast = () => {
    setShowWalletToast(false)
  }

  const handleOpenConfirmDeleteDialog = () => {
    setIsConfirmDeleteDialogOpen(true)
  }
  const handleCloseConfirmDeleteDialog = () => {
    setIsConfirmDeleteDialogOpen(false)
  }

  const handleOnConfirmDelete = async () => {
    try {
      const {
        data: { deleteProduct: deletedProduct },
      } = await (API.graphql(
        graphqlOperation(deleteProduct, { input: { id: productId } })
      ) as Promise<any>)

      handleCloseConfirmDeleteDialog()
      toast.success({
        content: `Product ${product.title} was successfully deleted.`,
        timeout: toast.defaultTimeout,
      })
      router.push(`/dashboard/store`)
    } catch (error) {
      handleCloseConfirmDeleteDialog()

      toast.error({
        content: `An error occurred while attempting to delete product ${product.title}.`,
        timeout: toast.defaultTimeout,
      })
    }
  }

  const handleGetProduct = useCallback(
    (productId) => {
      const filteredProducts = products.filter(
        (product: any) => product.id === productId
      )
      setProduct(filteredProducts[0])
    },
    [products]
  )

  useEffect(() => {
    if (productId && products && products.length) handleGetProduct(productId)
  }, [productId, handleGetProduct, products])

  const productViewProps = {
    showWalletToast,
    product,
    isConfirmDeleteDialogOpen,
    handleCloseConfirmDeleteDialog,
    handleOnConfirmDelete,
    handleOpenConfirmDeleteDialog,
    handleOpenFundProjectDialog,
    handlehideWalletToast,
    router,
  }
  return (
    <>
      {matchesMobile ? (
        <Mobile {...productViewProps} />
      ) : (
        <Desktop {...productViewProps} />
      )}
    </>
  )
}
//@ts-ignore
ProductView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <section>{page}</section>
    </DashboardLayout>
  )
}
export default ProductView
