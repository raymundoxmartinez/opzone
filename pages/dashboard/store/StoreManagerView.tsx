import React, { useState } from 'react'
import cryptoRandomString from 'crypto-random-string'
import { API, graphqlOperation, Auth, Storage } from 'aws-amplify'
import { useMediaQuery } from '@mui/material'

import { createProduct } from '@src/graphql/mutations'
import { toast } from '@components/Toast'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import PageLayout from '@components/Layouts/PageLayout'
import Desktop from '@components/Layouts/StoreLayout/Desktop'
import Mobile from '@components/Layouts/StoreLayout/Mobile'

import useSWR from 'swr'
function getProducts() {
  return fetch('/products').then((res) => res.json())
}
//@ts-ignore
const StoreManagerView = (props) => {
  const [isCreateProductDialogOpen, setIsCreateProductDialogOpen] =
    useState(false)
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))

  const { data: products, error } = useSWR('products', getProducts)

  const handleCloseCreateProductDialog = () => {
    setIsCreateProductDialogOpen(false)
  }
  const handleOpenCreateProductDialog = () => {
    setIsCreateProductDialogOpen(true)
  }

  //@ts-ignore
  const handleCreateProduct = async ({
    title,
    description,
    address = '',
    price = 100.0,
    images,
  }: {
    title: string
    description: string
    address: string
    price: number
    images: any[]
  }) => {
    const { attributes, username, ...rest } =
      await Auth.currentAuthenticatedUser()
    let awsImages
    try {
      const imagePromises = images.map(async (image) => {
        const objKey = cryptoRandomString({ length: 10 }) + title
        const { key } = (await Storage.put(objKey, image, {
          contentType: image.type,
        })) as { key: string }
        return key
      })

      await Promise.all(imagePromises)
        .then((results) => {
          awsImages = results
        })
        .catch((err) => {
          throw err
        })
    } catch (error) {
      throw error
    }
    try {
      const {
        data: { createProduct: newProduct },
      } = await (API.graphql(
        graphqlOperation(createProduct, {
          input: {
            title,
            address,
            description,
            price,
            images: awsImages,
            userID: attributes.sub,
          },
        })
      ) as Promise<any>)
      const productsWithImages = await attachImages([newProduct])

      //@ts-ignore
      setProducts((prevState) =>
        prevState ? [...prevState, newProduct] : [productsWithImages]
      )
      setIsCreateProductDialogOpen(false)
      toast.success({
        content: `Product ${title} was successfully created.`,
        timeout: toast.defaultTimeout,
      })
    } catch (error) {
      setIsCreateProductDialogOpen(false)
      toast.error({
        content: `An error occurred while attempting to create product ${title}.`,
        timeout: toast.defaultTimeout,
      })
    }
  }

  const attachImages = async (products: any[]): Promise<any[]> => {
    if (!Array.isArray(products)) return []
    const promises = products.map(async (product) => {
      const imagePromises = product.images.map(async (key: string) => {
        if (!key) return key
        const imageUrl = await Storage.get(key)
        return imageUrl
      })
      const res = await Promise.all(imagePromises)
        .then((results) => {
          product.images = results
          return product
        })
        .catch((err) => {
          console.log(err)
        })
      return res
    })
    const res = await Promise.all(promises)
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    return res
  }

  const storeManagerViewProps = {
    isCreateProductDialogOpen,
    products,
    handleCloseCreateProductDialog,
    handleCreateProduct,
    handleOpenCreateProductDialog,
  }
  return (
    <>
      {matchesMobile ? (
        <Mobile {...storeManagerViewProps} />
      ) : (
        <Desktop {...storeManagerViewProps} />
      )}
    </>
  )
}

//@ts-ignore
StoreManagerView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <PageLayout title="Store">{page}</PageLayout>
    </DashboardLayout>
  )
}
export default StoreManagerView
