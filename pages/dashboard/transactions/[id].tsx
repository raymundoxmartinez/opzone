import React, { useState, useEffect, useCallback } from 'react'
import moment from 'moment'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { useMediaQuery } from '@mui/material'

import socket from '@src/socket'
import {
  useTransactions,
  useXrpl,
  useMapbox,
  useXumm,
  useApp,
  useConditionProgresses,
  useAssets,
} from '@src/hooks/index'
import { toast } from '@components/Toast'
import Mobile from '@components/Layouts/TransactionsLayout/Mobile'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import awsmobile from '../../../src/aws-exports'

const TransactionView = (props: any) => {
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))
  const [transaction, setTransaction] = useState<any>(null)
  const [isVerifying, setIsVerifying] = useState<any>(false)
  const [isVerified, setIsVerified] = useState<any>(false)
  const { user } = useApp()
  const { handleFetchTransaction } = useTransactions()
  const {
    postConditionProgress,
    updateConditionProgress,
    fetchConditionProgresses,
  } = useConditionProgresses()
  const { xrpl, handleFetchXrplTransaction } = useXrpl()
  const {
    handleFetchAddressFromCoordinates,
    handleFetchStaticMapUrl,
    handleVerifyLongitudeLatitudeBounds,
  } = useMapbox()
  const {
    xummUserToken,
    handlePostEscrowFinishTransaction,
    handleGetTransaction,
  } = useXumm()
  const { handlePostAsset, fetchAssets } = useAssets()

  const { transactions, transactionById, conditionProgresses } = useSelector(
    (state) => ({
      transactions: state.transactions.items,
      transactionById: state.transactions.byId,
      conditionProgresses: state.conditionProgresses.items,
    })
  )

  const router = useRouter()
  const { id: transactionId } = router.query

  const handleFormatCondition = useCallback(
    async (condition: any) => {
      const updatedCondition = { ...condition }
      const displayAddress = await handleFetchAddressFromCoordinates(
        updatedCondition
      )
      const conditionProgress = await fetchConditionProgresses({
        filter: {
          userID: { eq: user.id },
          conditionID: { eq: updatedCondition.id },
        },
      })
      if (conditionProgress && conditionProgress.length) {
        updatedCondition.status = conditionProgress[0].status
      }
      return { ...updatedCondition, displayAddress }
    },
    [handleFetchAddressFromCoordinates, fetchConditionProgresses, user?.id]
  )

  const handleFormatTransaction = useCallback(
    async (transaction: any) => {
      let latitude, longitude
      const formattedConditions = await Promise.all(
        transaction.conditions.items.map((condition: any) => {
          if (condition.type === 'GEO') {
            latitude = condition.latitude
            longitude = condition.longitude
          }
          return handleFormatCondition(condition)
        })
      )
      let mapUrl = ''
      if (latitude && longitude) {
        mapUrl = await handleFetchStaticMapUrl({ latitude, longitude })
      }
      const formattedTransaction = {
        ...transaction,
        mapUrl,
        conditions: { ...transaction.conditions, items: formattedConditions },
      }
      setTransaction(formattedTransaction)
    },
    [handleFormatCondition, handleFetchStaticMapUrl]
  )

  const handleVerifyCondition = async () => {
    setIsVerifying(true)
    if (moment(new Date()) > moment(transaction.expiration)) {
      setIsVerifying(false)
      toast.error({
        content: `Contract has expired.`,
        timeout: toast.defaultTimeout,
      })
      return false
    }
    let latitude, longitude
    const geoCondition = transaction.conditions.items.find((condition: any) => {
      return condition.type === 'GEO'
    })
    if (geoCondition) {
      latitude = geoCondition.latitude
      longitude = geoCondition.longitude
    }
    console.log(latitude)
    console.log(longitude)
    if (latitude && longitude) {
      try {
        const isValid = await handleVerifyLongitudeLatitudeBounds({
          latitude,
          longitude,
        })
        const status = isValid ? 'APPROVED' : 'REJECTED'

        if (conditionProgresses[geoCondition.id]) {
          updateConditionProgress({
            conditionProgressID: conditionProgresses[geoCondition.id].id,
            userID: user.id,
            conditionID: geoCondition.id,
            status,
          }).then(() => {
            setIsVerified(isValid)

            if (status === 'REJECTED') {
              toast.warning({
                content: 'Condition was not met. Please try again.',
                timeout: toast.defaultTimeout,
              })
            } else {
              toast.success({
                content: 'Condition was successfully met!',
                timeout: toast.defaultTimeout,
              })
            }
          })
        } else {
          postConditionProgress({
            userID: user.id,
            conditionID: geoCondition.id,
            status,
          }).then(() => {
            setIsVerified(isValid)

            if (status === 'REJECTED') {
              toast.warning({
                content: 'Condition was not met. Please try again.',
                timeout: toast.defaultTimeout,
              })
            } else {
              toast.success({
                content: 'Condition was successfully met!',
                timeout: toast.defaultTimeout,
              })
            }
          })
        }

        setIsVerifying(false)
        return isValid
      } catch (error) {}
    }
  }

  const handleClaimTransaction = async () => {
    const transactionUpdated = await handleFetchTransaction(transaction.id)
    switch (transactionUpdated && transactionUpdated?.type) {
      case 'FILE':
        const reward = transactionUpdated?.reward
        if (!reward) {
          toast.warning({
            content: 'Asset is no longer available to claim!',
            timeout: toast.defaultTimeout,
          })
          return
        }
        const newFile = {
          name: reward.name,
          userID: user.id,
          file: { ...reward.file },
        }
        try {
          await handlePostAsset(newFile)
          toast.success({
            content: 'You have a new asset!',
            timeout: toast.defaultTimeout,
          })
        } catch (error) {}
        await fetchAssets({ filter: { userID: { eq: user.id } } })
        router.push('/dashboard/assets')
        break
      case 'WEB3_PAYMENT':
        const tran = await handleGetTransaction(transaction.xummTransactionID)
        const tran2 = await handleFetchXrplTransaction(
          tran.payload.response.txid
        )
        if (!xummUserToken) {
          toast.error({
            content: `You must connect the wallet  involved in the transaction:${transaction.toWallet.address} `,
            timeout: toast.defaultTimeout,
          })
          return
        }
        const response = await handlePostEscrowFinishTransaction({
          owner: transaction.fromWallet.address,
          xummUserToken,
          condition: transaction.condition,
          fulfillment: transaction.fulfillment,
          returnUrl: `${process.env.NEXT_PUBLIC_BASE_URL}/dashboard/payments?payload={id}`,
          offerSequence: tran2.result.Sequence,
        })

        if (response && response.next.always) {
          window.location.href = response.next.always
          socket.subscribe(response.refs.websocket_status)
          socket.on('connected', () => {
            socket.socket.onmessage = async (message: Record<string, any>) => {
              console.log(message.data)

              const messageJson = JSON.parse(message.data)
              if (messageJson.signed) {
                toast.success({
                  content: 'Escrow finish transaction was successful!',
                  timeout: toast.defaultTimeout,
                })
              } else if (messageJson.signed === false) {
                //TODO: post transaction to OA db
                toast.warning({
                  content: 'Escrow finish transaction was declined!',
                  timeout: toast.defaultTimeout,
                })
              }
            }
          })
        }
        break

      default:
        break
    }
  }

  useEffect(() => {
    if (transactionId && user) handleFetchTransaction(transactionId)
  }, [transactionId, user, handleFetchTransaction])

  useEffect(() => {
    if (transactionId && transactions && transactionId in transactions) {
      const transaction = transactions[transactionId]
      handleFormatTransaction(transaction)
    }
  }, [transactionId, transactions, handleFormatTransaction])

  useEffect(() => {
    if (
      conditionProgresses &&
      Object.values(conditionProgresses).length &&
      transaction
    ) {
      if (transaction?.conditions?.items.length) {
        const verifiedConditions = transaction?.conditions?.items.filter(
          (condition) =>
            condition.id in conditionProgresses &&
            conditionProgresses[condition.id].status === 'APPROVED'
        )
        if (
          verifiedConditions.length === transaction?.conditions?.items.length
        ) {
          setIsVerified(true)
        }
      }
    }
  }, [conditionProgresses, transaction, transactionId])

  const transactionViewProps = {
    transaction,
    user,
    xrpl,
    isVerified,
    isVerifying,
    conditionProgresses,
    handleVerifyCondition,
    handleClaimTransaction,
    onCreateConditionProgress: postConditionProgress,
    onUpdateConditionProgress: updateConditionProgress,
  }
  return <>{matchesMobile ? <Mobile {...transactionViewProps} /> : null}</>
}
//@ts-ignore
TransactionView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <section>{page}</section>
    </DashboardLayout>
  )
}
export default TransactionView
