import React, { useState } from 'react'
import { useMediaQuery } from '@mui/material'

import { toast } from '@components/Toast'
import { useWallets, useApp, useXumm } from '@src/hooks/index'
import PageLayout from '@components/Layouts/PageLayout'
import DashboardLayout from '@components/Layouts/DashboardLayout'
import Desktop from '@components/Layouts/WalletsLayout/Desktop'
import Mobile from '@components/Layouts/WalletsLayout/Mobile'

//@ts-ignore
const WalletsManagerView = (props) => {
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))

  const [isCreateWalletDialogOpen, setIsCreateWalletDialogOpen] =
    useState(false)
  const [selectedWallet, setSelectedWallet] = useState(null)
  const {
    wallets,
    loading: walletsLoading,
    handleFetchWalletsByUserID,
    handlePostWallet,
    handleFetchWalletsByAddress,
  } = useWallets()
  const { user } = useApp()
  const { handleXummSignIn, handleStoreXummCredentials, handleGetTransaction } =
    useXumm()

  const handleCloseCreateWalletDialog = () => {
    window.history.pushState({}, document.title, '/' + 'dashboard/wallets')
    setIsCreateWalletDialogOpen(false)
  }
  const handleOpenCreateWalletDialog = () => {
    setIsCreateWalletDialogOpen(true)
  }

  const handleSelectWallet = (wallet: any) => {
    setSelectedWallet(wallet)
  }

  const handleCreateWallet = async ({
    title,
    description,
    address,
  }: {
    title: string
    description: string
    address: string
  }) => {
    const registeredWallet = await handleFetchWalletsByAddress({ address })
    if (registeredWallet.length) {
      handleCloseCreateWalletDialog()
      toast.warning({
        content: 'Wallet is already registered.',
        timeout: toast.defaultTimeout,
      })
      return
    }
    const newWallet = await handlePostWallet({ title, description, address })
    if (newWallet) {
      handleFetchWalletsByUserID({ userID: user.id })
    }
    setIsCreateWalletDialogOpen(false)
  }

  const walletsManagerViewProps = {
    isCreateWalletDialogOpen,
    handleCloseCreateWalletDialog,
    handleOpenCreateWalletDialog,
    handleCreateWallet,
    handleSelectWallet,
    handleXummSignIn,
    handleGetTransaction,
    wallets,
    walletsLoading,
    selectedWallet,
  }
  return (
    <>
      {matchesMobile ? (
        <Mobile {...walletsManagerViewProps} />
      ) : (
        <Desktop {...walletsManagerViewProps} />
      )}
    </>
  )
}
//@ts-ignore
WalletsManagerView.getLayout = function getLayout(page, { user }) {
  return (
    <DashboardLayout user={user}>
      <PageLayout title="Wallets">{page}</PageLayout>
    </DashboardLayout>
  )
}
export default WalletsManagerView
