import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import Amplify from 'aws-amplify'
import {
  AmplifyAuthenticator,
  AmplifySignUp,
  AmplifySignIn,
} from '@aws-amplify/ui-react'
import { AuthState, onAuthUIStateChange } from '@aws-amplify/ui-components'
import useApp from '@hooks/app/useApp'
import awsconfig from '@src/aws-exports'

Amplify.configure(awsconfig)
//@ts-ignore
const AuthView = (props) => {
  const router = useRouter()
  const [authState, setAuthState] = useState<AuthState>()

  const { user } = useSelector((state: any) => ({
    user: state.app.user.items,
  }))

  const { fetchCurrentUser } = useApp()

  useEffect(() => {
    if (authState === AuthState.SignedIn && user) {
      router.push('/dashboard/payments')
    }

    //this is here for use of user log in through
    //the home page.
    return onAuthUIStateChange((nextAuthState, authData) => {
      setAuthState(nextAuthState)
      fetchCurrentUser()
    })
  }, [authState, router, user, fetchCurrentUser])

  useEffect(() => {
    if (authState === AuthState.SignedIn && user) {
      router.push('/dashboard/payments')
    }
  }, [authState, user, router])

  return (
    <AmplifyAuthenticator usernameAlias="email">
      <AmplifySignUp
        slot="sign-up"
        usernameAlias="email"
        formFields={[
          {
            type: 'username',
            label: 'Username',
            placeholder: 'Username',
            inputProps: { required: true, autocomplete: 'username' },
          },
          {
            type: 'email',
            label: 'Email Address',
            placeholder: 'Email Address',
            inputProps: { required: true, autocomplete: 'username' },
          },
          {
            type: 'password',
            label: 'Password',
            placeholder: 'Password',
            inputProps: { required: true, autocomplete: 'new-password' },
          },
          {
            type: 'phone_number',
            label: 'Phone Number',
            placeholder: '(555) 555-1212',
          },
        ]}
      />
      <AmplifySignIn slot="sign-in" usernameAlias="email" />
    </AmplifyAuthenticator>
  )
}
//@ts-ignore
AuthView.getLayout = function getLayout(page) {
  return <>{page}</>
}
export default AuthView
