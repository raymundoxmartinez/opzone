import React from 'react'
import 'mapbox-gl/dist/mapbox-gl.css'
import Head from 'next/head'
import { Provider } from 'react-redux'
import { Amplify } from 'aws-amplify'
import type { AppProps } from 'next/app'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { StyledEngineProvider } from '@mui/material'
import {
  AmplifyAuthenticator,
  AmplifySignUp,
  AmplifySignIn,
} from '@aws-amplify/ui-react'
import { ThemeProvider } from '@mui/material/styles'
import CssBaseline from '@mui/material/CssBaseline'
import store from '../src/redux/store'
import MainLayout from '../components/Layouts/MainLayout'
MainLayout
import withGlobalFetchCalls from '@components/hoc/withGlobalFetchCalls'
import theme from '../styles/theme'

import '../styles/globals.scss'

if (process.env.NEXT_PUBLIC_API_MOCKING === 'enabled') {
  require('../src/mocks')
}

Amplify.Logger.LOG_LEVEL =
  process.env.NODE_ENV === 'development' ? 'DEBUG' : 'INFO'

function MyApp({ Component, pageProps, router }: AppProps) {
  const WithGlobalFetchComponent = withGlobalFetchCalls(Component)

  let pageComponent = <WithGlobalFetchComponent {...pageProps} />

  if (router.route !== '/') {
    pageComponent = (
      <AmplifyAuthenticator usernameAlias="email">
        <AmplifySignUp
          slot="sign-up"
          usernameAlias="email"
          formFields={[
            {
              type: 'email',
              label: 'Custom Email Label',
              placeholder: 'Custom email placeholder',
              inputProps: { required: true, autocomplete: 'username' },
            },
            {
              type: 'password',
              label: 'Custom Password Label',
              placeholder: 'Custom password placeholder',
              inputProps: {
                required: true,
                autocomplete: 'new-password',
              },
            },
            {
              type: 'phone_number',
              label: 'Custom Phone Label',
              placeholder: 'Custom phone placeholder',
            },
          ]}
        />
        <AmplifySignIn slot="sign-in" usernameAlias="email" />
        <WithGlobalFetchComponent {...pageProps} />
      </AmplifyAuthenticator>
    )
  }
  return (
    <React.Fragment>
      <Head>
        <title>OmniAgora</title>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
        <link rel="manifest" href="/manifest.json" />
      </Head>
      <Provider store={store}>
        <DndProvider backend={HTML5Backend}>
          <StyledEngineProvider injectFirst>
            <ThemeProvider theme={theme}>
              <CssBaseline />
              {pageComponent}
            </ThemeProvider>
          </StyledEngineProvider>
        </DndProvider>
      </Provider>
    </React.Fragment>
  )
}

export default MyApp
