import React, { useState, useEffect } from 'react'
import { useMediaQuery } from '@mui/material'

import Mobile from '@components/Layouts/HomeLayout/Mobile'
import Desktop from '@components/Layouts/HomeLayout/Desktop'
import MainLayout from '@components/Layouts/MainLayout'

const Home = (props: any) => {
  const [xummUserAccount, setXummUserAccount] = useState<any>('')
  const [xummUserToken, setXummUserToken] = useState<any>('')
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))

  useEffect(() => {
    const xummUserAccount = localStorage.getItem('xummUserAccount')
    const xummUserToken = localStorage.getItem('xummUserToken')
    if (xummUserAccount && xummUserToken) {
      setXummUserAccount(xummUserAccount)
      setXummUserToken(xummUserToken)
    }
  }, [])

  const homeProps = {
    xummUserAccount,
    xummUserToken,
  }

  return (
    <>
      {matchesMobile ? (
        <Mobile {...props} {...homeProps} />
      ) : (
        <Desktop {...props} {...homeProps} />
      )}
    </>
  )
}
//@ts-ignore
Home.getLayout = function getLayout(page, { user, isLoggedIn }) {
  return (
    <MainLayout user={user} isLoggedIn={isLoggedIn}>
      {page}
    </MainLayout>
  )
}
export default Home
