import { createTheme } from '@mui/material/styles'
import { red } from '@mui/material/colors'
import variables from './variables.module.scss'

// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: variables.primaryColor,
    },
    secondary: {
      main: variables.secondaryColor,
    },
    error: {
      main: red.A400,
    },
    background: {
      default: variables.backgroundColor,
    },
  },
})

export default theme
