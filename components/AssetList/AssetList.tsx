import React from 'react'
import { useRouter } from 'next/router'
import AssetCard from '@components/AssetCard'
const AssetList = ({ assets, selectedAsset, assetProps }) => {
  const router = useRouter()
  return (
    <>
      {assets &&
        assets.map((asset: any, index: any) => (
          <AssetCard
            key={index}
            asset={asset}
            selected={selectedAsset === asset.id}
            {...assetProps}
          />
        ))}
    </>
  )
}
export default AssetList
