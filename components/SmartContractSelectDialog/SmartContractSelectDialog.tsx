import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Box,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Fade,
  IconButton,
  AppBar,
  Toolbar,
} from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import socket from '@src/socket'
import { toast } from '@components/Toast'
import { TransitionProps } from '@mui/material/transitions'
import { FormIndex } from '@components/form-components'
import {
  useXrpl,
  useXumm,
  useApp,
  useTransactions,
  useMapbox,
  useConditions,
} from '@hooks/index'

import styles from './SmartContractSelectDialog.module.scss'

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>
  },
  ref: React.Ref<unknown>
) {
  return <Fade ref={ref} {...props} />
})

const SmartContractSelectDialog = ({
  open,
  handleCloseCreateSmartContractSelectDialog,
  xummUserAccount,
  xummUserToken,
}: {
  open: any
  handleCloseCreateSmartContractSelectDialog: any
  xummUserAccount: any
  xummUserToken: any
}) => {
  const [activeStep, setActiveStep] = useState(-1)
  const [category, setCategory] = useState<string>('')
  const [skipped, setSkipped] = useState(new Set<number>())
  const [fromWalletID, setFromWalletID] = useState('')
  const { wallets } = useSelector((state: any) => ({
    wallets: state.wallets.items,
  }))
  const { handleGenerateSmartContractConditionAndFulFillment, xrpl } = useXrpl()
  const {
    handlePostEscrowTransaction,
    handleXummSignIn,
    handleStoreXummCredentials,
    handleGetTransaction,
  } = useXumm()
  const {
    handlePostTransaction,
    handleUpdateTransaction,
    handleFetchTransactionsByXummTransactionID,
  } = useTransactions()
  const { handleFetchStaticMapUrl } = useMapbox()

  const { handlePostCondition } = useConditions()

  const { user } = useApp()

  const { query } = useRouter()

  const handleOnCategoryChange = (e: any) => {
    setCategory(e.target.value)
  }
  const isStepOptional = (step: number) => {
    return step === 1
  }

  const isStepSkipped = (step: number) => {
    return skipped.has(step)
  }

  const handleNext = () => {
    let newSkipped = skipped
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values())
      newSkipped.delete(activeStep)
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1)
    setSkipped(newSkipped)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }
  const handleConnectWallet = async () => {
    const response = await handleXummSignIn({
      returnUrl: `${process.env.NEXT_PUBLIC_BASE_URL}/dashboard/payments?payload={id}&contract=GEO&register=false`,
    })
    if (response && response.next.always) {
      window.location.href = response.next.always
      socket.subscribe(response.refs.websocket_status)
      socket.on('connected', () => {
        socket.socket.onmessage = (message: Record<string, any>) => {
          console.log(message.data)
          const messageJson = JSON.parse(message.data)
          if (messageJson.signed) {
            toast.success({
              content: 'Xumm wallet connection was successful!',
              timeout: toast.defaultTimeout,
            })
            handleStoreXummCredentials(messageJson.payload_uuidv4)
          }
        }
      })
    }
  }
  const handlePostSmartContract = async (data: any) => {
    console.log(data)
    const { condition, fulfillment } =
      await handleGenerateSmartContractConditionAndFulFillment()
    const cancelAfter = xrpl!.isoTimeToRippleTime(data.cancelAfter)
    const amount = xrpl!.xrpToDrops(data.amount)
    //posting transaction to XRP ledger

    const response = await handlePostEscrowTransaction({
      ...data,
      amount,
      condition,
      cancelAfter,
      returnUrl: `${process.env.NEXT_PUBLIC_BASE_URL}/dashboard/payments?payload={id}`,
      xummUserToken,
    })

    if (response && response.next.always) {
      //TODO: post transaction to OA db
      const OATransactionResponse = await handlePostTransaction({
        status: 'pending',
        amount,
        type: 'ESCROW_GEO',
        fulfillment,
        condition,
        expiration: data.cancelAfter,
        xummTransactionID: response.uuid,
        fromWalletID: data.fromWalletID,
        toWalletID: data.toWalletID,
        receiverID: data.receiverID,
      })

      let conditions = []
      if (data.conditions.length) {
        conditions = await Promise.all(
          data.conditions.map((condition: any) =>
            handlePostCondition({
              ...condition,
              transactionID: OATransactionResponse.id,
            })
          )
        )
        const red = await handleUpdateTransaction({
          id: OATransactionResponse.id,
          conditions,
        })
      }

      window.location.href = response.next.always
      socket.subscribe(response.refs.websocket_status)
      socket.on('connected', () => {
        socket.socket.onmessage = async (message: Record<string, any>) => {
          console.log(message.data)

          const messageJson = JSON.parse(message.data)
          if (messageJson.signed) {
            toast.success({
              content: 'Escrow transaction was successful!',
              timeout: toast.defaultTimeout,
            })
          } else if (messageJson.signed === false) {
            //TODO: post transaction to OA db
            toast.warning({
              content: 'Escrow transaction was declined!',
              timeout: toast.defaultTimeout,
            })
          }
        }
      })
    }
  }

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.")
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1)
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values())
      newSkipped.add(activeStep)
      return newSkipped
    })
  }

  const handleReset = () => {
    setActiveStep(0)
  }

  useEffect(() => {
    //TODO: add transaction param to differentiate from other transactions like signin
    const payloadId = query.payload as string
    if (payloadId) {
      //here to check that creation of smart contract transaction was successful
      const response = handleFetchTransactionsByXummTransactionID({
        xummTransactionID: payloadId,
      }).then((res) => {
        return res
      })
    }
  }, [handleFetchTransactionsByXummTransactionID, query.payload])

  useEffect(() => {
    //TODO: prompt to store wallet if wallet is not stored in users directory
    if (query.payload && query.contract === 'GEO') {
      const payloadId = query.payload as string
      setActiveStep(0)
      setCategory('geo_smart_contract')
    }
  }, [])

  useEffect(() => {
    if (wallets && wallets.length && xummUserAccount) {
      const fromWalletID = wallets.reduce((acc: any, wallet: any) => {
        if (wallet.address === xummUserAccount) {
          acc = wallet.id
        }
        return acc
      }, '')
      if (fromWalletID) {
        setFromWalletID(fromWalletID)
      }
    }
  }, [wallets, xummUserAccount])

  return (
    <>
      <Dialog
        onClose={() => {
          handleCloseCreateSmartContractSelectDialog()
          setActiveStep(-1)
        }}
        aria-labelledby="customized-dialog-title"
        open={open}
        TransitionComponent={Transition}
        transitionDuration={500}
        fullScreen
      >
        {activeStep == -1 ? (
          <>
            <DialogTitle id="customized-dialog-title">
              Select Contract Type
            </DialogTitle>

            <DialogContent className={styles['dialog__content']} dividers>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  height: '100%',
                }}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-helper-label">
                    Smart Contracts
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={category}
                    label="Category"
                    onChange={handleOnCategoryChange}
                  >
                    <MenuItem value={'geo_smart_contract'}>
                      Geo Smart Contract
                    </MenuItem>
                  </Select>
                </FormControl>
              </div>
            </DialogContent>
            <DialogActions>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  pt: 2,
                  width: '100%',
                  justifyContent: 'space-between',
                }}
              >
                <Button
                  onClick={() => {
                    handleCloseCreateSmartContractSelectDialog()
                    setActiveStep(-1)
                  }}
                  style={{ color: 'red' }}
                >
                  Cancel
                </Button>
                <Button
                  onClick={() => {
                    setActiveStep(0)
                  }}
                  disabled={!category}
                >
                  Next
                </Button>
              </Box>
            </DialogActions>
          </>
        ) : category ? (
          <FormIndex
            name={category}
            activeStep={activeStep}
            isStepSkipped={isStepSkipped}
            onSubmit={handlePostSmartContract}
            onNext={handleNext}
            onBack={handleBack}
            onReset={handleReset}
            onClose={() => {
              handleCloseCreateSmartContractSelectDialog()
              setActiveStep(-1)
            }}
            handleConnectWallet={handleConnectWallet}
            handleFetchStaticMapUrl={handleFetchStaticMapUrl}
            defaultValues={{
              sender: xummUserAccount,
              destination: '',
              amount: 0,
              cancelAfter: '',
              memo: '',
              fromWalletID,
              conditions: [],
            }}
          />
        ) : null}
      </Dialog>
    </>
  )
}
export default SmartContractSelectDialog
