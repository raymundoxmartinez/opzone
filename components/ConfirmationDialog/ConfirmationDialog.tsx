import React from 'react'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import Dialog from '@mui/material/Dialog'

const ConfirmationDialog = ({
  open,
  title,
  content,
  actions,
}: {
  open: boolean
  title: string
  content: string | JSX.Element
  actions: JSX.Element
}) => {
  return (
    <Dialog open={open}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent dividers>{content}</DialogContent>
      <DialogActions>{actions}</DialogActions>
    </Dialog>
  )
}
export default ConfirmationDialog
