/**
 * Utility function to dynamically load the MapBox Search embed script at runtime.
 * @param target - typically the document.head
 * @param dataName - an identifier for the script tag
 * @returns the script tag
 */
export const importMapBoxSearchScript = (target, dataName) => {
  if (document.querySelector(` script[data-name=" ${dataName} "] `)) {
    return
  }

  const script = document.createElement('script')
  script.dataset.name = dataName
  script.id = 'search-js'
  script.src = `https://api.mapbox.com/search-js/v1.0.0-beta.17/web.js`
  script.async = true
  target.appendChild(script)
  return script
}
