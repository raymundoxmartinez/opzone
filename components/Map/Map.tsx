import React, { useState, useEffect, useRef } from 'react'
import styles from './Map.module.scss'
//@ts-ignore
import mapboxgl from '!mapbox-gl'
import { toast } from '@components/Toast'
import classNames from 'classnames'

// eslint-disable-line import/no-webpack-loader-syntax
mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCCESS_TOKEN
const Map = ({
  onLocationChange,
  locations,
  interactive = true,
  size = 'small',
}: {
  onLocationChange?: any
  locations?: any[]
  interactive?: boolean
  size?: 'small' | 'large'
}) => {
  const mapContainer = useRef(null)
  const map = useRef(null)
  const marker = useRef(null)
  const [lng, setLng] = useState(null)
  const [lat, setLat] = useState(null)
  const [zoom, setZoom] = useState(1)
  const [status, setStatus] = useState<null | string>(null)

  const getLocation = () => {
    if (!navigator.geolocation) {
      setStatus('Geolocation is not supported by your browser')
    } else {
      setStatus('Locating...')
      if (navigator.permissions && navigator.permissions.query) {
        navigator.permissions
          .query({ name: 'geolocation' })
          .then(function (result) {
            if (result.state === 'granted' || result.state === 'prompt') {
              console.log(result.state)
              //If granted then you can directly call your function here
              navigator.geolocation.getCurrentPosition(
                (position) => {
                  setStatus(null)

                  //@ts-ignore
                  setLat(position.coords.latitude)
                  //@ts-ignore
                  setLng(position.coords.longitude)
                  if (onLocationChange) {
                    onLocationChange({
                      longitude: position.coords.longitude,
                      latitude: position.coords.latitude,
                    })
                  }
                },
                () => {
                  setStatus('Unable to retrieve your location')
                },
                { maximumAge: 60000, timeout: 5000, enableHighAccuracy: true }
              )
            } else if (result.state === 'denied') {
              //If denied then you have to show instructions to enable location
              toast.error({
                content:
                  'Unable to access your location. Please make sure location tracking is enabled.',
                timeout: toast.defaultTimeout,
              })
            }
            result.onchange = function () {
              console.log(result.state)
            }
          })
      } else {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            setStatus(null)

            //@ts-ignore
            setLat(position.coords.latitude)
            //@ts-ignore
            setLng(position.coords.longitude)
            if (onLocationChange) {
              onLocationChange({
                longitude: position.coords.longitude,
                latitude: position.coords.latitude,
              })
            }
          },
          () => {
            setStatus('Unable to retrieve your location')
            toast.error({
              content:
                'Unable to access your location. Please make sure location tracking is enabled.',
              timeout: toast.defaultTimeout,
            })
          },
          { maximumAge: 60000, timeout: 5000, enableHighAccuracy: true }
        )
      }
    }
  }

  const loadMap = () => {
    if (map.current) return // initialize map only once
    if (lat && lng) {
      map.current = new mapboxgl.Map({
        //@ts-ignore
        container: mapContainer.current,
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [lng, lat],
        zoom: 9,
      })
      if (interactive) {
        marker.current = new mapboxgl.Marker({
          draggable: true,
        })
          .setLngLat([lng, lat])
          .addTo(map.current)
      }

      //@ts-ignore
      map.current.on('load', function () {
        //@ts-ignore
        map.current.resize()
      })
      //@ts-ignore
      map.current.on('move', () => {
        //@ts-ignore
        setLng(map.current.getCenter().lng.toFixed(4))
        //@ts-ignore
        setLat(map.current.getCenter().lat.toFixed(4))
        //@ts-ignore
        setZoom(map.current.getZoom().toFixed(2))
      })
      //@ts-ignore
      const currentControls = map.current._controls.filter((control) => {
        return control instanceof mapboxgl.GeolocateControl
      })
      if (!currentControls.length) {
        const geolocate = new mapboxgl.GeolocateControl({
          positionOptions: {
            enableHighAccuracy: true,
          },
          // When active the map will receive updates to the device's location as it changes.
          trackUserLocation: true,
          // Draw an arrow next to the location dot to indicate which direction the device is heading.
          showUserHeading: true,
        })
        // @ts-ignore
        map.current.addControl(geolocate)
        geolocate.on('geolocate', (position: any) => {
          console.log('A geolocate event has occurred.')
          //@ts-ignore
          setLat(position.coords.latitude)
          //@ts-ignore
          setLng(position.coords.longitude)
        })
      }
    }
  }

  const loadMarker = () => {
    if (!marker.current) return // wait for map to initialize

    //@ts-ignore
    const coordinates = document.getElementById('coordinates')
    function onDragEnd() {
      //@ts-ignore
      const lngLat = marker.current.getLngLat()
      //@ts-ignore
      coordinates.style.display = 'block'
      //@ts-ignore
      coordinates.innerHTML = `Longitude: ${lngLat.lng}<br />Latitude: ${lngLat.lat}`
      if (onLocationChange) {
        onLocationChange({ longitude: lngLat.lng, latitude: lngLat.lat })
      }
    }

    //@ts-ignore
    marker.current.on('dragend', onDragEnd)
  }
  const loadMarkers = () => {
    if (!map.current) return
    // Add markers to the map.
    if (locations && locations?.length && !interactive) {
      for (const location of locations) {
        if (location?.coordinates) {
          new mapboxgl.Marker()
            .setLngLat(location?.coordinates)
            .addTo(map.current)
        }
      }
    }
  }

  useEffect(() => {
    getLocation()
  }, [])

  useEffect(() => {
    loadMap()
    if (interactive) {
      loadMarker()
    } else {
      loadMarkers()
    }
  })

  const mapSizeCN = classNames({ [styles[`map-container-${size}`]]: true })
  return (
    <div>
      <div ref={mapContainer} className={mapSizeCN} />
      <pre id="coordinates" className={styles['coordinates']}></pre>
    </div>
  )
}

export default Map
