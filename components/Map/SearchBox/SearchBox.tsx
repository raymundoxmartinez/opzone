import React, { useState } from 'react'
import dynamic from 'next/dynamic'

const SearchBox = dynamic(
  () =>
    import('@mapbox/search-js-react').then((module) => {
      return module.SearchBox
    }),
  {
    ssr: false,
  }
)

export default function Component({ onLocationChange }) {
  const [value, setValue] = useState('')

  const handleOnChange = (value) => {
    console.log('search box value:', value)
    const feature = value?.features[0]
    const name = feature.properties.name
    setValue(name)
    if (onLocationChange) onLocationChange(feature)
  }
  return (
    <SearchBox
      accessToken={process.env.NEXT_PUBLIC_MAPBOX_ACCCESS_TOKEN}
      popoverOptions={{
        placement: 'bottom-start',
        flip: true,
        offset: 5,
      }}
      onRetrieve={handleOnChange}
      placeholder="Location Address"
      value={value}
      theme={{
        variables: {
          colorPrimary: 'myBrandRed',
        },
        cssText: '.Input:active { opacity: 0.9; }',
      }}
    />
  )
}
