import React, { useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
import { toast } from '@components/Toast'

const AddressMinimap = dynamic(
  () =>
    import('@mapbox/search-js-react').then((module) => {
      return module.AddressMinimap
    }),
  {
    ssr: false,
  }
)

const accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCCESS_TOKEN

export default function Component({ coordinates, onSaveMarkerLocation }) {
  console.log('coordinates', coordinates)
  const [status, setStatus] = useState<null | string>(null)
  const [lng, setLng] = useState(null)
  const [lat, setLat] = useState(null)

  const handleOnSaveMarkerLocation = (coordinates) => {
    onSaveMarkerLocation(coordinates)
  }
  const getLocation = () => {
    if (!navigator.geolocation) {
      setStatus('Geolocation is not supported by your browser')
    } else {
      setStatus('Locating...')
      if (navigator.permissions && navigator.permissions.query) {
        navigator.permissions
          .query({ name: 'geolocation' })
          .then(function (result) {
            if (result.state === 'granted' || result.state === 'prompt') {
              console.log(result.state)
              //If granted then you can directly call your function here
              navigator.geolocation.getCurrentPosition(
                (position) => {
                  setStatus(null)

                  //@ts-ignore
                  setLat(position.coords.latitude)
                  //@ts-ignore
                  setLng(position.coords.longitude)
                  if (onSaveMarkerLocation) {
                    onSaveMarkerLocation([
                      position.coords.longitude,
                      position.coords.latitude,
                    ])
                  }
                },
                () => {
                  setStatus('Unable to retrieve your location')
                },
                { maximumAge: 60000, timeout: 5000, enableHighAccuracy: true }
              )
            } else if (result.state === 'denied') {
              //If denied then you have to show instructions to enable location
              toast.error({
                content:
                  'Unable to access your location. Please make sure location tracking is enabled.',
                timeout: toast.defaultTimeout,
              })
            }
            result.onchange = function () {
              console.log(result.state)
            }
          })
      } else {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            setStatus(null)

            //@ts-ignore
            setLat(position.coords.latitude)
            //@ts-ignore
            setLng(position.coords.longitude)
            if (onSaveMarkerLocation) {
              onSaveMarkerLocation([
                position.coords.longitude,
                position.coords.latitude,
              ])
            }
          },
          () => {
            setStatus('Unable to retrieve your location')
            toast.error({
              content:
                'Unable to access your location. Please make sure location tracking is enabled.',
              timeout: toast.defaultTimeout,
            })
          },
          { maximumAge: 60000, timeout: 5000, enableHighAccuracy: true }
        )
      }
    }
  }
  useEffect(() => {
    getLocation()
  }, [])

  return (
    <AddressMinimap
      defaultMapStyle={['mapbox', 'outdoors-v11']}
      accessToken={accessToken}
      feature={{
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates:
            coordinates && coordinates.length ? coordinates : [lng, lat],
        },
        properties: {},
      }}
      satelliteToggle
      canAdjustMarker
      mapStyleMode="default"
      show
      footer={false}
      keepMarkerCentered
      onSaveMarkerLocation={handleOnSaveMarkerLocation}
    ></AddressMinimap>
  )
}
