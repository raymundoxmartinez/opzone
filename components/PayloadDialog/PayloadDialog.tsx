import React, { useState } from 'react'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import Alert from '@mui/material/Alert'
import CircularProgress from '@mui/material/CircularProgress'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'

const PayloadDialog = ({
  open,
  handleClosePayloadDialog,
  qrCode,
}: {
  open: any
  handleClosePayloadDialog: any
  qrCode: any
}) => {
  const [showToast, setShowToast] = useState(false)
  const handleCloseToast = () => {
    setShowToast(false)
  }
  const renderContent = () => {
    if (!qrCode) {
      return (
        <div
          style={{
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
          }}
        >
          <CircularProgress />
          <Typography variant="h4">Generating Bar Code...</Typography>
        </div>
      )
    }
    return (
      <>
        <img src={qrCode} alt="payload qr_code" />
      </>
    )
  }
  return (
    <>
      <Dialog
        onClose={handleClosePayloadDialog}
        aria-labelledby="customized-dialog-title"
        open={open}
        fullWidth
      >
        <DialogTitle id="customized-dialog-title">
          Scan code with xumm app
        </DialogTitle>
        <DialogContent
          style={{ height: 300, display: 'flex', justifyContent: 'center' }}
          dividers
        >
          {renderContent()}
          {showToast && (
            <Alert severity="success" color="info" onClose={handleCloseToast}>
              Xumm wallet has been connected!
            </Alert>
          )}
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClosePayloadDialog}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
export default PayloadDialog
