import React from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'

import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import SignOutButton from '@components/SignOutButton'

import styles from './TopNavbar.module.scss'

const TopNavbar = ({ isLoggedIn }: { isLoggedIn: any }) => {
  const router = useRouter()

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar className={styles.topBar} position="static">
        <Toolbar>
          <Link href="/" passHref legacyBehavior>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              OmniAgora
            </Typography>
          </Link>
          {!isLoggedIn ? (
            <Button onClick={() => router.push('/auth')} color="inherit">
              Login
            </Button>
          ) : (
            <SignOutButton />
          )}
        </Toolbar>
      </AppBar>
    </Box>
  )
}
export default TopNavbar
