import * as React from 'react'
import moment from 'moment'
import { useRouter } from 'next/router'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardMedia from '@mui/material/CardMedia'
import CardActions from '@mui/material/CardActions'
import CardActionArea from '@mui/material/CardActionArea'
import Avatar from '@mui/material/Avatar'
import IconButton from '@mui/material/IconButton'
import { red } from '@mui/material/colors'
import DeleteIcon from '@mui/icons-material/Delete'

import styles from './ProductCard.module.scss'

export default function ProductCard({
  product,
  onClick,
  onDeleteClick,
}: {
  product: any
  onClick?: any
  onDeleteClick: any
}) {
  const router = useRouter()
  return (
    <Card className={styles.main}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            R
          </Avatar>
        }
        title={product.title}
        subheader={moment(product.createdAt).format('MMM Do YY')}
        action={
          <IconButton
            aria-label="share"
            onClick={() => onDeleteClick(product.id)}
          >
            <DeleteIcon />
          </IconButton>
        }
      />
      <CardActionArea onClick={onClick} style={{ paddingBottom: 10 }}>
        <CardMedia
          component="img"
          height="194"
          image={product?.images?.[0] || product?.imageUrl}
          alt={product.title}
        />
      </CardActionArea>
    </Card>
  )
}
