import React from 'react'

import { Dialog, DialogTitle } from '@mui/material'
import WalletForm from '@components/form-components/WalletForm'

const CreateWalletDialog = ({
  open,
  address,
  handleCloseCreateWalletDialog,
  handleCreateWallet,
}: {
  open: any
  address: string
  handleCloseCreateWalletDialog: any
  handleCreateWallet: any
}) => {
  return (
    <>
      <Dialog
        onClose={handleCloseCreateWalletDialog}
        aria-labelledby="customized-dialog-title"
        open={open}
        fullWidth
      >
        <DialogTitle id="customized-dialog-title">Add Wallet</DialogTitle>
        <WalletForm
          defaultValues={{ address, description: '', title: '' }}
          onSubmit={handleCreateWallet}
          onClose={handleCloseCreateWalletDialog}
        />
      </Dialog>
    </>
  )
}
export default CreateWalletDialog
