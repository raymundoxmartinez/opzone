import OZFundForm from '../OZFundForm'
import OZAssetForm from '../OZAssetForm'
import CGForm from '../CGForm'
import CGStoreForm from '../CGStoreForm'
import OZInvestorForm from '../OZInvestorForm'
import GeoSmartContractForm from '../GeoSmartContractForm'
import NFTForm from '../NFTForm'
import TransactionForm from '../TransactionForm'
import LocationForm from '../LocationForm'

const FormIndex = ({
  name,
  ...props
}: {
  name: string
  [key: string]: any
}) => {
  switch (name) {
    case 'opz':
      return <OZFundForm {...props} />
    case 'opz_asset':
      return <OZAssetForm {...props} />
    case 'cg':
      return <CGForm {...props} />
    case 'store':
      return <CGStoreForm {...props} />
    case 'opz_manager':
      return <OZInvestorForm {...props} />
    case 'opz_investor':
      return <OZInvestorForm {...props} />
    case 'cg_manager':
      return <OZInvestorForm {...props} />
    case 'cg_employee':
      return <OZInvestorForm {...props} />
    case 'geo_smart_contract':
      return <GeoSmartContractForm {...props} />
    case 'nft':
      return <NFTForm {...props} />
    case 'transaction':
      return <TransactionForm {...props} />
    case 'location':
      return <LocationForm {...props} />
    default:
      return <OZFundForm {...props} />
  }
}

export default FormIndex
