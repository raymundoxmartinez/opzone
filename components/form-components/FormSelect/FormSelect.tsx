import React from 'react'
import { Select, MenuItem, InputLabel, FormControl } from '@mui/material'
import { Controller } from 'react-hook-form'
const FormInputText = ({
  //@ts-ignore
  name,
  //@ts-ignore
  control,
  //@ts-ignore
  label,
  defaultValue = '',
  //@ts-ignore
  selectOptions,
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      render={({ field: { onChange, value } }) => (
        <FormControl variant="outlined" sx={{ m: 1, minWidth: 300 }}>
          <InputLabel>{label}</InputLabel>
          <Select onChange={onChange} value={value} label={label}>
            {selectOptions.map((item: any, index: any) => (
              <MenuItem key={index} value={item.value}>
                {item.text}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      )}
    />
  )
}

export default FormInputText
