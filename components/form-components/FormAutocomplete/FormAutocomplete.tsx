import React, { useState, useEffect } from 'react'
import { TextField, Autocomplete } from '@mui/material'
import { Controller } from 'react-hook-form'
import { throttle } from 'lodash'
import useUsers from '@hooks/users/useUsers'

const FormAutocomplete = ({
  //@ts-ignore
  name,
  //@ts-ignore
  control,
  //@ts-ignore
  label,
  defaultValue = '',
  //@ts-ignore
  textFieldProps,
  //@ts-ignore
  onChange: customOnChange,
}: {
  name: string
  control: any
  label: string
  defaultValue?: string
  textFieldProps: any
  onChange?: any
}) => {
  const [inputValue, setInputValue] = useState('')
  const [options, setOptions] = useState<readonly any[]>([])
  const { fetchUsers } = useUsers()

  const handleFetchUsers = React.useMemo(
    () =>
      throttle(
        async (
          { filter }: any,
          callback: (results?: readonly any[]) => void
        ) => {
          const res = await fetchUsers({ filter })
          callback(res)
        },
        200
      ),
    [fetchUsers]
  )

  useEffect(() => {
    let active = true
    const filterQuery = [
      { username: { contains: `${inputValue}` } },
      { email: { contains: `${inputValue}` } },
    ]
    if (inputValue) {
      handleFetchUsers(
        {
          filter: { or: filterQuery },
        },
        (results?: readonly any[]) => {
          if (active) {
            let newOptions: readonly any[] = []
            if (results) {
              newOptions = results.reduce((acc, user) => {
                const temp = user.wallets.items ? [...user.wallets.items] : []
                const walletCopies: any[] = []
                temp.forEach((wallet) => {
                  const walletCopy = { ...wallet }
                  walletCopy['username'] = user.username
                  walletCopies.push(walletCopy)
                })
                acc.push(...walletCopies)
                return acc
              }, [])
            }
            setOptions(newOptions)
          }
        }
      )
    }
    return () => {
      active = false
    }
  }, [inputValue, handleFetchUsers])

  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      render={({ field: { value, onChange } }) => {
        return (
          <Autocomplete
            value={value}
            freeSolo
            inputValue={inputValue}
            fullWidth
            getOptionLabel={(option) => {
              if (typeof option === 'string') {
                return option
              } else {
                return option.title
              }
            }}
            groupBy={(option) => {
              return option.username
            }}
            filterOptions={(x) => x}
            options={options}
            autoComplete
            includeInputInList
            filterSelectedOptions
            onChange={(event, selectedWallet) => {
              if (customOnChange) {
                customOnChange(selectedWallet)
              } else {
                onChange(selectedWallet)
              }
            }}
            onInputChange={(event, newInputValue) => {
              setInputValue(newInputValue)
            }}
            renderInput={(params) => {
              return <TextField {...params} label={label} {...textFieldProps} />
            }}
          />
        )
      }}
    />
  )
}

export default FormAutocomplete
