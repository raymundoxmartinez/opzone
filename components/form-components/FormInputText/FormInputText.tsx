import { TextField } from '@mui/material'
import { Controller } from 'react-hook-form'
import React from 'react'
const FormInputText = ({
  //@ts-ignore
  name,
  //@ts-ignore
  control,
  //@ts-ignore
  label,
  defaultValue = '',
  //@ts-ignore
  textFieldProps,
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      render={({ field: { onChange, value } }) => (
        <TextField
          onChange={onChange}
          value={value}
          label={label}
          {...textFieldProps}
        />
      )}
    />
  )
}

export default FormInputText
