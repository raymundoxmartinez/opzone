import React from 'react'
import { TextField } from '@mui/material'
import { Controller } from 'react-hook-form'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker'
const FormDateTimePicker = ({
  name,
  control,
  label,
  defaultValue = '',
  textFieldProps,
  onChange: customOnChange,
}: {
  name: string
  control: any
  label: string
  defaultValue?: string
  textFieldProps: any
  onChange?: (val: any) => void
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      render={({ field: { onChange, value } }) => (
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DateTimePicker
            value={value}
            renderInput={(props) => {
              return <TextField {...props} {...textFieldProps} />
            }}
            label={label}
            onChange={(newValue) => {
              if (customOnChange) {
                customOnChange(newValue)
              } else {
                onChange(newValue)
              }
            }}
          />
        </LocalizationProvider>
      )}
    />
  )
}

export default FormDateTimePicker
