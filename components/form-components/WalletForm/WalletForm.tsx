import React from 'react'
import { yupResolver } from '@hookform/resolvers/yup'
import { DialogContent, DialogActions, Button } from '@mui/material'
import { useForm, FormProvider } from 'react-hook-form'
import { FormInputText, FormAutocomplete } from '@components/form-components'
import * as yup from 'yup'
import styles from './WalletForm.module.scss'

const schema = yup.object({
  address: yup.string().required(),
  title: yup.string().required(),
  description: yup.string(),
})

const WalletForm = ({ defaultValues, onSubmit, onClose }: any) => {
  const formMethods = useForm({
    defaultValues,
    resolver: yupResolver(schema),
  })
  return (
    <>
      <DialogContent className={styles['dialog__content']} dividers>
        <FormProvider {...formMethods}>
          <form className={styles['product-form']}>
            <FormInputText
              name="address"
              control={formMethods.control}
              label="Address"
              textFieldProps={{
                style: { margin: 5 },
                variant: 'outlined',
                type: 'string',
                fullWidth: true,
                error: formMethods.formState.errors?.address ? true : false,
                helperText: formMethods.formState.errors?.address?.message,
                disabled: true,
              }}
            />
            <FormInputText
              name="title"
              control={formMethods.control}
              label="Name"
              textFieldProps={{
                style: { margin: 5 },
                variant: 'outlined',
                type: 'string',
                fullWidth: true,
                error: formMethods.formState.errors?.title ? true : false,
                helperText: formMethods.formState.errors?.title?.message,
              }}
            />
            <FormInputText
              name="description"
              control={formMethods.control}
              label="Description"
              textFieldProps={{
                style: { margin: 5 },
                variant: 'outlined',
                type: 'string',
                multiline: true,
                rows: 5,
                fullWidth: true,
              }}
            />
          </form>
        </FormProvider>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onClose}>
          Cancel
        </Button>
        <Button autoFocus onClick={formMethods.handleSubmit(onSubmit)}>
          Create
        </Button>
      </DialogActions>
    </>
  )
}
export default WalletForm
