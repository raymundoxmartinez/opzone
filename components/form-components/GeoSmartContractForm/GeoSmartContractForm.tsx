import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useForm, FormProvider } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import AttachMoneyIcon from '@mui/icons-material/AttachMoney'
import OfflineBoltIcon from '@mui/icons-material/OfflineBolt'
import {
  Typography,
  DialogActions,
  DialogTitle,
  DialogContent,
  Stepper,
  Step,
  StepLabel,
  Box,
  Button,
  IconButton,
  Tooltip,
} from '@mui/material'
import {
  FormInputText,
  FormDateTimePicker,
  FormAutocomplete,
} from '@components/form-components'
import Map from '@components/Map'
import useXrpl from '@hooks/xrpl/useXrpl'

import styles from './GeoSmartContractForm.module.scss'

const schema = yup.object({
  sender: yup.string().required(),
  amount: yup.number().positive().integer().required(),
  destination: yup.string().required(),
  cancelAfter: yup.string().required(),
  memo: yup.string(),
  fromWalletID: yup.string(),
  toWalletID: yup.string(),
  receiverID: yup.string(),
})
const steps = ['Connect', 'Create', 'Details']
const GeoSmartContractForm = ({
  activeStep,
  isStepSkipped,
  onSubmit,
  onNext,
  onBack,
  onReset,
  defaultValues,
  handleConnectWallet,
  handleFetchStaticMapUrl,
  onClose,
}: any) => {
  const [mapUrl, setMapUrl] = useState('')
  const formMethods = useForm({
    defaultValues,
    resolver: yupResolver(schema),
  })

  const onDestinationChange = (vals: any) => {
    if (vals && vals.id) {
      formMethods.setValue('toWalletID', vals.id)
      formMethods.setValue('destination', vals.address)
      formMethods.setValue('receiverID', vals.userID)
    } else {
      formMethods.setValue('toWalletID', '')
      formMethods.setValue('destination', '')
      formMethods.setValue('receiverID', '')
    }
  }

  const onLocationChange = async (vals: any) => {
    const mapUrl = await handleFetchStaticMapUrl({
      longitude: vals.longitude,
      latitude: vals.latitude,
    })
    setMapUrl(mapUrl)

    formMethods.setValue('conditions', [
      {
        type: 'GEO',
        longitude: vals.longitude,
        latitude: vals.latitude,
        status: 'false',
      },
    ])
  }

  return (
    <>
      <DialogTitle id="customized-dialog-title">
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const stepProps: { completed?: boolean } = {}
            const labelProps: {
              optional?: React.ReactNode
            } = {}
            if (isStepSkipped(index)) {
              stepProps.completed = false
            }
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            )
          })}
        </Stepper>
      </DialogTitle>
      <DialogContent className={styles['dialog__content']} dividers>
        <FormProvider {...formMethods}>
          <form className={styles['product-form']}>
            {activeStep === 0 && (
              <>
                {formMethods.getValues('sender') ? (
                  <>
                    <Typography
                      variant="h6"
                      noWrap
                    >{`Current wallet: ${formMethods.getValues(
                      'sender'
                    )}`}</Typography>
                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: '100%',
                        flexDirection: 'column',
                      }}
                    >
                      <Typography variant="h6">Connect Another</Typography>

                      <Tooltip title="Connect Wallet">
                        <IconButton
                          className={styles['connect-wallet-btn']}
                          aria-label="connect"
                          onClick={handleConnectWallet}
                        >
                          <OfflineBoltIcon
                            style={{ width: '100%', height: '100%' }}
                          />
                        </IconButton>
                      </Tooltip>
                    </div>
                  </>
                ) : (
                  <>
                    <Typography variant="h6">Connect a wallet</Typography>
                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: '100%',
                      }}
                    >
                      <Tooltip title="Connect Wallet">
                        <IconButton
                          className={styles['connect-wallet-btn']}
                          aria-label="connect"
                          onClick={handleConnectWallet}
                        >
                          <OfflineBoltIcon
                            style={{ width: '100%', height: '100%' }}
                          />
                        </IconButton>
                      </Tooltip>
                    </div>
                  </>
                )}
              </>
            )}
            {activeStep === 1 && (
              <>
                <Typography variant="h6">
                  Adjust the location of your geo drop:
                </Typography>
                <Map onLocationChange={onLocationChange} />
                <Typography variant="h6">Select geo drop type:</Typography>
                <div>
                  <Button
                    size="large"
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      border: '1px solid green',
                    }}
                  >
                    <AttachMoneyIcon fontSize="inherit" />
                    <Typography variant="h6">XRP</Typography>
                  </Button>
                </div>
              </>
            )}
            {activeStep === 2 && (
              <>
                <img
                  src={mapUrl || ''}
                  alt={'Title'}
                  style={{
                    height: 300,
                    objectFit: 'cover',
                    width: '100%',
                  }}
                />
                <FormInputText
                  name="sender"
                  control={formMethods.control}
                  label="From Wallet"
                  textFieldProps={{
                    style: { margin: 5 },
                    variant: 'outlined',
                    type: 'string',
                    fullWidth: true,
                    error: formMethods.formState.errors?.sender ? true : false,
                    helperText: formMethods.formState.errors?.sender?.message,
                    disabled: true,
                  }}
                />

                <FormAutocomplete
                  name="destination"
                  control={formMethods.control}
                  label="To Wallet"
                  onChange={onDestinationChange}
                  textFieldProps={{
                    style: { margin: 5 },
                    variant: 'outlined',
                    type: 'string',
                    fullWidth: true,
                    error: formMethods.formState.errors?.destination
                      ? true
                      : false,
                    helperText:
                      formMethods.formState.errors?.destination?.message,
                  }}
                />
                <FormInputText
                  name="amount"
                  control={formMethods.control}
                  label="Amount to be Paid"
                  textFieldProps={{
                    style: { margin: 5 },
                    variant: 'outlined',
                    type: 'number',
                    fullWidth: true,
                    error: formMethods.formState.errors?.amount ? true : false,
                    helperText: formMethods.formState.errors?.amount?.message,
                  }}
                />
                <FormDateTimePicker
                  name="cancelAfter"
                  control={formMethods.control}
                  label="Contract Expiration Datetime"
                  textFieldProps={{
                    style: { margin: 5 },
                    variant: 'outlined',
                    InputLabelProps: {
                      shrink: true,
                    },
                    fullWidth: true,
                    error: formMethods.formState.errors?.cancelAfter
                      ? true
                      : false,
                    helperText:
                      formMethods.formState.errors?.cancelAfter?.message,
                  }}
                />

                <FormInputText
                  name="memo"
                  control={formMethods.control}
                  label="Memo"
                  textFieldProps={{
                    style: { margin: 5 },
                    variant: 'outlined',
                    type: 'string',
                    multiline: true,
                    rows: 5,
                    fullWidth: true,
                  }}
                />
              </>
            )}
          </form>
        </FormProvider>
      </DialogContent>
      <DialogActions>
        {activeStep === steps.length ? (
          <React.Fragment>
            <Typography sx={{ mt: 2, mb: 1 }}>
              All steps completed - you&apos;re finished
            </Typography>
            <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
              <Box sx={{ flex: '1 1 auto' }} />
              <Button onClick={onReset}>Reset</Button>
            </Box>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                pt: 2,
                width: '100%',
                justifyContent: 'space-between',
              }}
            >
              <Button onClick={onClose} style={{ color: 'red' }}>
                Cancel
              </Button>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                }}
              >
                <Button
                  color="inherit"
                  disabled={activeStep === -1}
                  onClick={onBack}
                  sx={{ mr: 1 }}
                >
                  Back
                </Button>
                <Box sx={{ flex: '1 1 auto' }} />
                <Button
                  onClick={
                    activeStep === steps.length - 1
                      ? formMethods.handleSubmit(onSubmit)
                      : onNext
                  }
                  // disabled={!formMethods.getValues('sender') && activeStep === 0}
                >
                  {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                </Button>
              </Box>
            </Box>
          </React.Fragment>
        )}
      </DialogActions>
    </>
  )
}
export default GeoSmartContractForm
