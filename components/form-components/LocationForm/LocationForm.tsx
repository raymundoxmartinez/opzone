import React, { useState, useEffect, lazy, Suspense } from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'

import {
  Typography,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  Box,
  Button,
  Fade,
  TextField,
} from '@mui/material'
import { TransitionProps } from '@mui/material/transitions'

import CategoryIcon from '@mui/icons-material/Category'

import { AddressMinimap, SearchBox } from '@components/Map'
import TransactionCard from '@components/TransactionCard'
import { FormIndex } from '@components/form-components'

import styles from './LocationForm.module.scss'

const schema = yup.object({
  name: yup.string().required(),
  description: yup.string(),
  userID: yup.string(),
  transactions: yup.array(),
  coordinates: yup.array(),
  address: yup.string(),
})

const steps = ['Choose Item', 'Add Conditions']
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>
  },
  ref: React.Ref<unknown>
) {
  return <Fade ref={ref} {...props} />
})
const LocationForm = ({
  activeStep,
  onSubmit,
  onReset,
  defaultValues,
  handlePostAsset,
  handleFetchStaticMapUrl,
  fetchAssets,
  handleFetchAddressFromCoordinates,
  onClose,
  trail,
  user,
}: any) => {
  const [transactionFormActiveStep, setTransactionFormActiveStep] = useState(0)
  const [mapUrl, setMapUrl] = useState('')
  const [isTransactionDialogOpen, setIsTransactionDialogOpen] = useState(false)
  const [value, setValue] = React.useState('')
  const formMethods = useForm({
    defaultValues,
    resolver: yupResolver(schema),
  })

  const handleOnCloseCreateTransactionDialog = () => {
    setIsTransactionDialogOpen(false)
  }

  const handleOnOpenCreateTransactionDialog = () => {
    setTransactionFormActiveStep(0)
    setIsTransactionDialogOpen(true)
  }

  const handleNext = () => {
    setTransactionFormActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleBack = () => {
    setTransactionFormActiveStep((prevActiveStep) => prevActiveStep - 1)
  }
  const handleReset = () => {
    setTransactionFormActiveStep(0)
  }

  const handleOnAddTransaction = (transaction) => {
    const currentTransactions = formMethods.getValues('transactions')
    formMethods.setValue('transactions', [...currentTransactions, transaction])
    setIsTransactionDialogOpen(false)
  }

  const handleOnLocationChange = async (feature: any) => {
    const { coordinates } = feature.geometry
    console.log('handleOnLocationChange', feature)
    const mapUrl = await handleFetchStaticMapUrl({
      longitude: coordinates[0],
      latitude: coordinates[1],
    })
    const displayAddress = await handleFetchAddressFromCoordinates({
      longitude: coordinates[0],
      latitude: coordinates[1],
    })
    setMapUrl(mapUrl)
    formMethods.setValue('coordinates', coordinates)
    formMethods.setValue('address', displayAddress)
    formMethods.setValue('name', feature.properties.name)
  }
  const handleOnSaveMarkerLocation = (coordinates) => {
    formMethods.setValue('coordinates', coordinates)
  }
  return (
    <>
      <div style={{ height: '100%', width: '100%' }}>
        <AddressMinimap
          coordinates={formMethods.getValues('coordinates')}
          onSaveMarkerLocation={handleOnSaveMarkerLocation}
        />
      </div>
      <DialogContent className={styles['dialog__content']} dividers>
        <FormProvider {...formMethods}>
          <form className={styles['product-form']}>
            <>
              <SearchBox onLocationChange={handleOnLocationChange} />
              <Typography variant="h6">Select geo drop type:</Typography>
              <div>
                <Button
                  size="large"
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    border: '1px solid green',
                  }}
                  disabled={!formMethods.getValues('coordinates').length}
                  onClick={handleOnOpenCreateTransactionDialog}
                >
                  <CategoryIcon fontSize="inherit" />
                  <Typography variant="h6">Item</Typography>
                </Button>
              </div>
              <div>
                {formMethods
                  .getValues('transactions')
                  .map((transaction, index) => {
                    return (
                      <TransactionCard
                        key={index}
                        transaction={transaction}
                        disabled
                      />
                    )
                  })}
              </div>
            </>
          </form>
        </FormProvider>
      </DialogContent>
      <DialogActions>
        <React.Fragment>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              pt: 2,
              width: '100%',
              justifyContent: 'space-between',
            }}
          >
            <Button onClick={onClose} style={{ color: 'red' }}>
              Cancel
            </Button>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <Box sx={{ flex: '1 1 auto' }} />
              <Button
                onClick={formMethods.handleSubmit((vals) => {
                  console.log(vals)
                  onSubmit(vals)
                  onReset()
                })}
                disabled={!formMethods.getValues('coordinates').length}
              >
                Submit
              </Button>
            </Box>
          </Box>
        </React.Fragment>
      </DialogActions>
      <Dialog
        open={isTransactionDialogOpen}
        TransitionComponent={Transition}
        transitionDuration={500}
        fullScreen
      >
        <FormIndex
          name={'transaction'}
          activeStep={transactionFormActiveStep}
          mapUrl={mapUrl}
          onSubmit={handleOnAddTransaction}
          onNext={handleNext}
          onBack={handleBack}
          onReset={handleReset}
          onClose={() => {
            handleOnCloseCreateTransactionDialog()
            setTransactionFormActiveStep(0)
          }}
          handlePostAsset={handlePostAsset}
          fetchAssets={fetchAssets}
          location={{ ...formMethods.getValues() }}
          user={user}
          defaultValues={{
            senderID: user?.id,
            conditions: [],
            assetID: '',
          }}
        />
      </Dialog>
    </>
  )
}
export default LocationForm
