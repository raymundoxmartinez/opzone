import React, { useEffect, useState } from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import { Storage } from 'aws-amplify'
import { yupResolver } from '@hookform/resolvers/yup'
import cryptoRandomString from 'crypto-random-string'
import * as yup from 'yup'
import {
  Typography,
  DialogActions,
  DialogTitle,
  DialogContent,
  Stepper,
  Step,
  StepLabel,
  Box,
  Button,
  Chip,
  TextField,
} from '@mui/material'

import awsmobile from '../../../src/aws-exports'
import ConditionList from '@components/ConditionList'
import AssetList from '@components/AssetList'

import styles from './TransactionForm.module.scss'

const schema = yup.object({
  senderID: yup.string(),
  assetID: yup.string(),
  conditions: yup.array(),
})
const steps = ['Choose Item', 'Add Conditions']
const TransactionForm = ({
  activeStep,
  onSubmit,
  onNext,
  onBack,
  onReset,
  defaultValues,
  onClose,
  handlePostAsset,
  fetchAssets,
  location,
  mapUrl,
  user,
}: any) => {
  const [imageFile, setImageFile] = useState('')
  const [assetsWithImageUrl, setAssetsWithImageUrl] = useState<any>([])
  const formMethods = useForm({
    defaultValues,
    resolver: yupResolver(schema),
  })
  formMethods.watch('assetID')
  formMethods.watch('conditions')

  const handleOnAssetClick = (id: string) => {
    formMethods.setValue('assetID', id)
  }

  const handleOnFileChange = (e: any) => {
    console.log(e)
    const file = e.target.files[0]
    if (!file) return
    const objKey = cryptoRandomString({ length: 10 }) + file.name

    console.log(file.name)
    Storage.put(objKey, file, {
      contentType: file.type,
    })
      .then((result) => {
        console.log(result)
        setImageFile(URL.createObjectURL(file))
        const image = {
          name: file.name,
          userID: user.id,
          file: {
            bucket: awsmobile.aws_user_files_s3_bucket,
            region: awsmobile.aws_user_files_s3_bucket_region,
            key: objKey,
          },
        }
        return handlePostAsset(image)
      })
      .then(() => {
        return fetchAssets({ filter: { userID: { eq: user.id } } })
      })
      .then((response) => {
        return attachImages(response)
      })
      .then((response) => {
        setAssetsWithImageUrl(response)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const attachImages = async (assets: any[]): Promise<any[]> => {
    if (!Array.isArray(assets)) return []
    const assetsWithImageUrlPromises = assets.map(async (asset) => {
      const assetWithImageUrl = { ...asset }
      assetWithImageUrl.imageUrl = await Storage.get(asset.file.key)
      return assetWithImageUrl
    })

    const assetsWithImageUrl = await Promise.all(assetsWithImageUrlPromises)
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    return assetsWithImageUrl
  }
  const handleOnConditionsChange = (conditions) => {
    formMethods.setValue('conditions', conditions)
  }

  useEffect(() => {
    formMethods.setValue('conditions', [
      {
        type: 'GEO',
        longitude: location?.coordinates?.[0],
        latitude: location?.coordinates?.[1],
        status: 'false',
        displayAddress: location?.address,
      },
    ])
  }, [])

  useEffect(() => {
    fetchAssets({ filter: { userID: { eq: user.id } } })
      .then((response: any) => {
        return attachImages(response)
      })
      .then((response: any) => {
        setAssetsWithImageUrl(response)
      })
      .catch((err: any) => {
        console.log(err)
      })
  }, [])

  return (
    <>
      <DialogTitle>
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const stepProps: { completed?: boolean } = {}
            const labelProps: {
              optional?: React.ReactNode
            } = {}
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            )
          })}
        </Stepper>
      </DialogTitle>
      <DialogContent className={styles['dialog__content']} dividers>
        <FormProvider {...formMethods}>
          <form className={styles['product-form']}>
            {activeStep === 0 && (
              <>
                <Typography variant="h6" gutterBottom>
                  Select Item To Drop
                </Typography>
                <div className={styles['product-container']}>
                  <input
                    type="file"
                    id="imageupload"
                    style={{ display: 'none' }}
                    onChange={handleOnFileChange}
                  />
                  <label
                    htmlFor="imageupload"
                    style={{ width: '100%', display: 'flex' }}
                  >
                    <Chip clickable label="Add Asset" className={styles.chip} />
                  </label>
                  <AssetList
                    assets={assetsWithImageUrl}
                    selectedAsset={formMethods.getValues('assetID')}
                    assetProps={{
                      onClick: handleOnAssetClick,
                    }}
                  />
                </div>
              </>
            )}
            {activeStep === 1 && (
              <>
                <img
                  src={mapUrl || ''}
                  alt={'Title'}
                  style={{
                    height: 300,
                    objectFit: 'cover',
                    width: '100%',
                  }}
                />
                <TextField
                  {...formMethods.register('description')}
                  label="Description"
                  variant="outlined"
                  fullWidth
                  margin="normal"
                  error={
                    formMethods.formState.errors?.description ? true : false
                  }
                  helperText={
                    formMethods.formState.errors?.description?.message
                  }
                />
                <ConditionList
                  conditions={formMethods.getValues('conditions')}
                  onConditionsChange={handleOnConditionsChange}
                />
              </>
            )}
          </form>
        </FormProvider>
      </DialogContent>
      <DialogActions>
        <React.Fragment>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              pt: 2,
              width: '100%',
              justifyContent: 'space-between',
            }}
          >
            <Button onClick={onClose} style={{ color: 'red' }}>
              Cancel
            </Button>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <Button
                color="inherit"
                disabled={activeStep === 0}
                onClick={onBack}
                sx={{ mr: 1 }}
              >
                Back
              </Button>
              <Box sx={{ flex: '1 1 auto' }} />
              <Button
                onClick={
                  activeStep === steps.length - 1
                    ? formMethods.handleSubmit((vals) => {
                        const filteredConditions = vals.conditions.filter(
                          (condition) => {
                            return Object.keys(condition).length > 1
                          }
                        )
                        console.log({
                          ...vals,
                          conditions: filteredConditions,
                        })
                        onSubmit({
                          ...vals,
                          conditions: filteredConditions,
                        })
                        onReset()
                      })
                    : onNext
                }
                disabled={
                  (!formMethods.getValues('assetID') && activeStep === 0) ||
                  (!formMethods.getValues('conditions').length &&
                    activeStep === 1)
                }
              >
                {activeStep === steps.length - 1 ? 'Add Item' : 'Next'}
              </Button>
            </Box>
          </Box>
        </React.Fragment>
      </DialogActions>
    </>
  )
}
export default TransactionForm
