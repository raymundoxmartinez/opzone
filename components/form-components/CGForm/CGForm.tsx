import React, { useState, useEffect } from 'react'
import {
  TextField,
  Typography,
  Card,
  CardContent,
  CardActionArea,
  CardMedia,
  Badge,
  DialogActions,
  DialogTitle,
  DialogContent,
  Stepper,
  Step,
  StepLabel,
  Box,
  Button,
} from '@mui/material'
import CheckIcon from '@mui/icons-material/Check'
import classNames from 'classnames'

import { FormProvider } from 'react-hook-form'
import FileUpload from '../../FileUpload'
import useSWR from 'swr'
import { useForm } from 'react-hook-form'

import styles from '../../ProductDialog/ProductDialog.module.scss'

function getProducts() {
  return fetch('/products').then((res) => res.json())
}

const steps = ['Details', 'Store', 'Contracts']
const CGForm = ({
  activeStep,
  isStepSkipped,
  onSubmit,
  onNext,
  onBack,
  onReset,
}: any) => {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [selectedProducts, setSelectedProducts] = useState<string[]>([])
  const [filteredProducts, setFilteredProducts] = useState([])

  const formMethods = useForm({
    mode: 'onBlur',
  })

  const { data: products, error } = useSWR('products', getProducts)
  const handleOnChangeTitle = (e: any) => {
    setTitle(e.target.value)
  }
  const handleOnChangeDescription = (e: any) => {
    setDescription(e.target.value)
  }

  useEffect(() => {
    if (products) {
      const filteredProducts = products.filter(
        (product: any) => product.category === 'store'
      )
      setFilteredProducts(filteredProducts)
    }
  }, [products])

  return (
    <>
      <DialogTitle id="customized-dialog-title">
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const stepProps: { completed?: boolean } = {}
            const labelProps: {
              optional?: React.ReactNode
            } = {}
            if (isStepSkipped(index)) {
              stepProps.completed = false
            }
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            )
          })}
        </Stepper>
      </DialogTitle>
      <DialogContent className={styles['dialog__content']} dividers>
        <FormProvider {...formMethods}>
          <form className={styles['product-form']}>
            {activeStep === 0 && (
              <>
                <TextField
                  {...formMethods.register('title')}
                  name="title"
                  style={{ margin: 5 }}
                  onChange={handleOnChangeTitle}
                  value={title}
                  id="outlined-basic"
                  label="Product Title"
                  variant="outlined"
                  type="string"
                  fullWidth
                />

                <TextField
                  {...formMethods.register('description')}
                  name="description"
                  style={{ margin: 5 }}
                  onChange={handleOnChangeDescription}
                  value={description}
                  id="outlined-basic"
                  label="Description"
                  variant="outlined"
                  type="string"
                  multiline
                  rows={5}
                  fullWidth
                />
                <div className={styles['file-upload']}>
                  <FileUpload
                    name="images"
                    accept="image/png, image/jpg, image/jpeg, image/gif"
                    multiple
                  />
                </div>
              </>
            )}

            {activeStep === 1 && (
              <div
                style={{
                  display: 'flex',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                }}
              >
                {filteredProducts.length &&
                  filteredProducts.map((product: any, index: any) => (
                    <Badge
                      key={index}
                      overlap="circular"
                      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                      badgeContent={
                        <CheckIcon style={{ color: 'rgb(51 162 155) ' }} />
                      }
                      invisible={!selectedProducts.includes(product.id)}
                    >
                      <Card
                        className={classNames({
                          [styles['product-card']]: true,
                          [styles['selected']]: selectedProducts.includes(
                            product.id
                          ),
                        })}
                      >
                        <CardActionArea
                          onClick={(e) => {
                            e.preventDefault()
                            setSelectedProducts((prevProducts) => {
                              if (prevProducts.includes(product.id)) {
                                return prevProducts.filter(
                                  (productId) => productId !== product.id
                                )
                              } else {
                                return [...prevProducts, product.id]
                              }
                            })
                          }}
                        >
                          <CardMedia
                            component="img"
                            height="100"
                            image={product.images[0]}
                            alt={product.title}
                          />
                          <CardContent
                            className={styles['product-card__content ']}
                          >
                            <Typography sx={{ fontSize: 14 }} gutterBottom>
                              {product.title}
                            </Typography>
                            {/* <Typography variant="body2" color="text.secondary">
                              {product.description}
                            </Typography> */}
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </Badge>
                  ))}
              </div>
            )}
            {activeStep == 2 && (
              <>
                <Typography variant="h6">Select Contracts To Apply</Typography>
                <div
                  style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'center',
                    height: 'calc(100% - 100px)',
                  }}
                >
                  {Array(15)
                    .fill(1)
                    .map((num, index) => {
                      return (
                        <div
                          key={index}
                          style={{
                            display: 'flex',
                            justifyContent: 'center',
                            flexDirection: 'column',
                            alignItems: 'center',
                          }}
                        >
                          <Card
                            sx={{ maxWidth: 345 }}
                            style={{
                              borderRadius: '50%',
                              height: 75,
                              margin: 20,
                              width: 75,
                            }}
                          >
                            <CardActionArea onClick={() => {}}>
                              <CardMedia
                                component="img"
                                height="75"
                                image="/images/contract.png"
                                alt="smart contract"
                              />
                            </CardActionArea>
                          </Card>
                          <Typography variant="body2">OZ Contract</Typography>
                        </div>
                      )
                    })}
                </div>
              </>
            )}
          </form>
        </FormProvider>
      </DialogContent>
      <DialogActions>
        {activeStep === steps.length ? (
          <React.Fragment>
            <Typography sx={{ mt: 2, mb: 1 }}>
              All steps completed - you&apos;re finished
            </Typography>
            <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
              <Box sx={{ flex: '1 1 auto' }} />
              <Button onClick={onReset}>Reset</Button>
            </Box>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
              <Button
                color="inherit"
                disabled={activeStep === -1}
                onClick={onBack}
                sx={{ mr: 1 }}
              >
                Back
              </Button>
              <Box sx={{ flex: '1 1 auto' }} />
              <Button
                onClick={
                  activeStep === steps.length - 1
                    ? formMethods.handleSubmit(onSubmit)
                    : onNext
                }
              >
                {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
              </Button>
            </Box>
          </React.Fragment>
        )}
      </DialogActions>
    </>
  )
}
export default CGForm
