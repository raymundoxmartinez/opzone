import React, { useState, useEffect } from 'react'
import {
  Typography,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  Box,
  Button,
  Fade,
  TextField,
} from '@mui/material'
import { TransitionProps } from '@mui/material/transitions'

import ConditionList from '@components/ConditionList'

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>
  },
  ref: React.Ref<unknown>
) {
  return <Fade ref={ref} {...props} />
})
const TransactionDialog = ({
  transactionID,
  isTransactionDialogOpen,
  onCloseTransactionDialog,
  fetchTransaction,
}) => {
  const [transaction, setTransaction] = useState(null)

  const handleFetchTransaction = async () => {
    const transaction = await fetchTransaction(transactionID)
    setTransaction(transaction)
  }

  useEffect(() => {
    handleFetchTransaction()
  }, [transactionID])

  if (!transaction) return null
  return (
    <Dialog
      open={isTransactionDialogOpen}
      TransitionComponent={Transition}
      transitionDuration={500}
      fullScreen
    >
      <ConditionList
        conditions={transaction?.conditions?.items}
        onConditionsChange={() => {}}
      />
      <DialogActions>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            pt: 2,
            width: '100%',
            justifyContent: 'space-between',
          }}
        >
          <Button onClick={onCloseTransactionDialog} style={{ color: 'red' }}>
            Close
          </Button>
        </Box>
      </DialogActions>
    </Dialog>
  )
}
export default TransactionDialog
