import React from 'react'
import { useRef } from 'react'
import { useDrag, useDrop } from 'react-dnd'
import type { Identifier, XYCoord } from 'dnd-core'
import Avatar from '@mui/material/Avatar'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'

import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Divider from '@mui/material/Divider'
import CategoryIcon from '@mui/icons-material/Category'
import Typography from '@mui/material/Typography'
import { red, purple } from '@mui/material/colors'
import PlaceIcon from '@mui/icons-material/Place'

import styles from './LocationCardWithTransactions.module.scss'

interface DragItem {
  index: number
  id: string
  type: string
}
const LocationCardWithTransactions = ({
  item: { name, address, description, transactions },
  index,
  moveCard,
  id,
  onTransactionClick,
}) => {
  const ref = useRef<HTMLDivElement>(null)
  const [{ handlerId }, drop] = useDrop<
    DragItem,
    void,
    { handlerId: Identifier | null }
  >({
    accept: 'card',
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      }
    },
    hover(item: DragItem, monitor) {
      if (!ref.current) {
        return
      }
      const dragIndex = item.index
      const hoverIndex = index

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }

      // Determine rectangle on screen
      const hoverBoundingRect = ref.current?.getBoundingClientRect()

      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

      // Determine mouse position
      const clientOffset = monitor.getClientOffset()

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%

      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }

      // Time to actually perform the action
      moveCard(dragIndex, hoverIndex)

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex
    },
  })

  const [{ isDragging }, drag] = useDrag({
    type: 'card',
    item: () => {
      return { id, index }
    },
    collect: (monitor: any) => ({
      isDragging: monitor.isDragging(),
    }),
  })

  const opacity = isDragging ? 0 : 1
  drag(drop(ref))
  return (
    <Accordion className={styles.main}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        ref={ref}
        classes={{ content: styles['accordion__summary__content'] }}
        style={{
          display: 'flex',
          alignItems: 'center',
          opacity,
          width: '100%',
          overflow: 'hidden',
        }}
      >
        <Avatar
          sx={{
            width: 40,
            height: 40,
            bgcolor: red[500],
            margin: '10px',
          }}
          aria-label="recipe"
        >
          <PlaceIcon />
        </Avatar>

        <div className={styles.content}>
          <Typography noWrap variant="h6">
            {name}
          </Typography>
          <Typography noWrap gutterBottom variant="subtitle1">
            {description}
          </Typography>
          <div style={{ display: 'flex' }}>
            <Typography variant="subtitle1" noWrap>
              {address}
            </Typography>
          </div>
        </div>
      </AccordionSummary>
      <AccordionDetails>
        <List>
          {transactions &&
            transactions?.items?.map((transaction, index) => (
              <ListItem key={index} disablePadding>
                <ListItemButton
                  onClick={() => onTransactionClick(transaction?.id)}
                >
                  <ListItemIcon>
                    <CategoryIcon />
                  </ListItemIcon>
                  <ListItemText primary={transaction.description} />
                </ListItemButton>
              </ListItem>
            ))}
        </List>
      </AccordionDetails>
    </Accordion>
  )
}
export default LocationCardWithTransactions
