import { ComponentMeta } from '@storybook/react'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import LocationCardWithTransactions from './LocationCardWithTransactions'

export default {
  component: LocationCardWithTransactions,
  title: 'LocationCardWithTransactions',
  decorators: [
    (story) => <DndProvider backend={HTML5Backend}>{story()}</DndProvider>,
  ],
  tags: ['autodocs'],
} as ComponentMeta<typeof LocationCardWithTransactions>

export const Default = {
  args: {
    item: {
      id: '1',
      name: 'Jurassic Park location',
      description:
        'Jurassic Park fdf fsd  fsd fds  fds  fds , f, sa,,asd d,dsa dsa,dsad ,dasd',
      address: 'Los Angeles',
      transactions: [
        { description: 'get coupon', conditions: [{ type: 'GEO' }] },
        { description: 'get coupon', conditions: [{ type: 'GEO' }] },
        { description: 'get coupon', conditions: [{ type: 'GEO' }] },
      ],
    },
    disabled: false,
  },
}

export const Disabled = {
  args: {
    item: {
      ...Default.args.item,
    },
    disabled: true,
  },
}
