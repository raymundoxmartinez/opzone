import React, { useState } from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import * as yup from 'yup'
import Backdrop from '@mui/material/Backdrop'
import CircularProgress from '@mui/material/CircularProgress'
import cryptoRandomString from 'crypto-random-string'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Box,
  TextField,
  Fade,
} from '@mui/material'
import { toast } from '@components/Toast'
import { TransitionProps } from '@mui/material/transitions'
import { FormIndex } from '@components/form-components'
import { useMapbox, useAssets } from '@hooks/index'
import DraggableListContainer from '@components/DraggableListContainer'
import LocationCard from '@components/LocationCard'
import FileUpload from '@components/FileUpload'
import { Storage } from 'aws-amplify'
import awsmobile from '@src/aws-exports'

import DirectionsMap from '@components/DirectionsMap'

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>
  },
  ref: React.Ref<unknown>
) {
  return <Fade ref={ref} {...props} />
})

const schema = yup.object({
  name: yup.string().required(),
  description: yup.string(),
  type: yup.string(),
  images: yup.array(),
  locations: yup.array().required(),
})

const TrailsCreateDialog = ({
  open,
  user,
  assets,
  onCloseTrailsCreateDialog,
  onPostTrail,
  onPostTransaction,
  onPostCondition,
  onPostLocationView,
  onPostLocation,
  onPostTrailLocationView,
  fetchTrails,
  fetchAsset,
  postReward,
}: any) => {
  const [activeStep, setActiveStep] = useState(0)
  const [isLocationDialogOpen, setIsLocationDialogOpen] = useState(false)

  const { handleFetchStaticMapUrl, handleFetchAddressFromCoordinates } =
    useMapbox()
  const { handlePostAsset, fetchAssets } = useAssets()

  const formMethods = useForm({
    defaultValues: {
      name: '',
      description: '',
      type: '',
      images: [],
      locations: [],
    },
    resolver: yupResolver(schema),
  })

  const handleReset = () => {
    setActiveStep(0)
  }

  const handleOnCloseCreateLocationDialog = () => {
    setIsLocationDialogOpen(false)
    setActiveStep(0)
  }

  const handleOnOpenCreateLocationDialog = () => {
    setIsLocationDialogOpen(true)
  }

  const handleOnAddLocation = (location) => {
    const currentLocations = formMethods.getValues('locations')
    formMethods.setValue('locations', [...currentLocations, location])
    setIsLocationDialogOpen(false)
  }
  const handleCloseCreateTrailDialog = () => {
    onCloseTrailsCreateDialog()
    setActiveStep(-1)
    formMethods.reset()
  }
  const handleOnPostTrail = async (vals) => {
    //create conditions
    if (formMethods?.formState?.errors?.locations) {
      toast.warning({
        content: formMethods?.formState?.errors?.locations?.message,
        timeout: toast.defaultTimeout,
      })
      return
    }
    const currentLocations = formMethods.getValues('locations')
    let locationViewIDs = []
    for (let location of currentLocations) {
      const locationRes = await onPostLocation(location)
      const locationViewRes = await onPostLocationView({
        ...location,
        locationID: locationRes.id,
      })
      locationViewIDs.push(locationViewRes.id)
      for (let transaction of location?.transactions) {
        let transactionRequest = {
          ...transaction,
          locationViewID: locationViewRes.id,
        }
        if (transaction?.assetID) {
          const asset =
            assets[transaction.assetID] ||
            (await fetchAsset(transaction?.assetID))
          const file = asset.file
          const name = asset.name
          const reward = await postReward({ name, file })
          transactionRequest.rewardID = reward.id
        }
        delete transactionRequest.assetID
        const transactionPost = await onPostTransaction(transactionRequest)
        const conditions = await Promise.all(
          transaction.conditions.map(async (condition) => {
            return await onPostCondition({
              ...condition,
              transactionID: transactionPost.id,
            })
          })
        )
      }
    }

    if (vals.images && vals.images.length) {
      const updatedImages = await Promise.all(
        vals.images.map(async (file) => {
          const objKey = cryptoRandomString({ length: 10 }) + file.name
          const result = await Storage.put(objKey, file, {
            contentType: file.type,
          })
          console.log(result)
          const image = {
            bucket: awsmobile.aws_user_files_s3_bucket,
            region: awsmobile.aws_user_files_s3_bucket_region,
            key: objKey,
          }
          return image
        })
      ).then((response) => {
        return response
      })

      if (updatedImages.length) {
        vals.images = updatedImages
      }
    }
    const newTrail = await onPostTrail({ ...vals, userID: user.id })
    await Promise.all(
      locationViewIDs.map((id) =>
        onPostTrailLocationView({
          trailID: newTrail.id,
          locationViewID: id,
        })
      )
    )
    await fetchTrails(true)

    handleCloseCreateTrailDialog()
  }

  return (
    <>
      <Dialog
        onClose={handleCloseCreateTrailDialog}
        open={open}
        TransitionComponent={Transition}
        transitionDuration={500}
        fullScreen
      >
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={formMethods.formState.isSubmitting}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <>
          <DirectionsMap
            size={'small'}
            locations={formMethods.getValues('locations')}
            loading={formMethods.formState.isSubmitting}
          />
          <DialogContent dividers>
            <TextField
              {...formMethods.register('name')}
              label="Name"
              variant="outlined"
              fullWidth
              margin="normal"
              error={formMethods.formState.errors?.name ? true : false}
              helperText={formMethods.formState.errors?.name?.message}
            />
            <TextField
              {...formMethods.register('description')}
              label="Description"
              variant="outlined"
              fullWidth
              margin="normal"
              multiline
              error={formMethods.formState.errors?.description ? true : false}
              helperText={formMethods.formState.errors?.description?.message}
            />
            <FormProvider {...formMethods}>
              <FileUpload
                name="images"
                accept="image/png, image/jpg, image/jpeg, image/gif"
                multiple
              />
            </FormProvider>
            <DraggableListContainer
              cardRenderer={LocationCard}
              items={formMethods.getValues('locations')}
            />
          </DialogContent>
          <DialogActions>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                pt: 2,
                width: '100%',
                justifyContent: 'space-between',
              }}
            >
              <Button
                onClick={handleCloseCreateTrailDialog}
                style={{ color: 'red' }}
              >
                Cancel
              </Button>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                }}
              >
                <Button
                  color="inherit"
                  onClick={handleOnOpenCreateLocationDialog}
                  sx={{ mr: 1 }}
                >
                  Add Location
                </Button>
                <Box sx={{ flex: '1 1 auto' }} />
                <Button
                  onClick={formMethods.handleSubmit(handleOnPostTrail)}
                  disabled={!formMethods.getValues('locations').length}
                >
                  Finish
                </Button>
              </Box>
            </Box>
          </DialogActions>
        </>
      </Dialog>
      <Dialog
        open={isLocationDialogOpen}
        TransitionComponent={Transition}
        transitionDuration={500}
        fullScreen
      >
        <FormIndex
          name={'location'}
          user={user}
          activeStep={activeStep}
          onSubmit={handleOnAddLocation}
          onReset={handleReset}
          onClose={handleOnCloseCreateLocationDialog}
          handleFetchStaticMapUrl={handleFetchStaticMapUrl}
          handleFetchAddressFromCoordinates={handleFetchAddressFromCoordinates}
          handlePostAsset={handlePostAsset}
          fetchAssets={fetchAssets}
          trail={{ ...formMethods.getValues() }}
          defaultValues={{
            name: '',
            description: '',
            userID: user?.id,
            transactions: [],
            address: '',
            coordinates: [],
          }}
        />
      </Dialog>
    </>
  )
}
export default TrailsCreateDialog
