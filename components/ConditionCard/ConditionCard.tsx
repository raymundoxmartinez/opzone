import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import Button from '@mui/material/Button'
import Select from '@mui/material/Select'
import Avatar from '@mui/material/Avatar'
import Typography from '@mui/material/Typography'
import TextField from '@mui/material/TextField'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import { red, purple, green, yellow } from '@mui/material/colors'
import PlaceIcon from '@mui/icons-material/Place'
import QuestionMarkIcon from '@mui/icons-material/QuestionMark'
import VerifiedIcon from '@mui/icons-material/Verified'
import Fab from '@mui/material/Fab'
import { toast } from '@components/Toast'

import styles from './ConditionCard.module.scss'

export default function ConditionCard({
  condition,
  conditionIndex,
  inProgress,
  user,
  conditionProgresses,
  onConditionChange,
  onCreateConditionProgress,
  onUpdateConditionProgress,
  onVerifyCondition,
}: {
  user: any
  condition: any
  conditionIndex: number
  inProgress?: boolean
  conditionProgresses?: Record<string, any>
  onCreateConditionProgress?: any
  onUpdateConditionProgress?: any
  onVerifyCondition?: any
  onConditionChange: ({
    conditionIndex,
    value,
    conditionType,
    isNewField,
  }: {
    conditionIndex?: number
    value?: any
    conditionType?: string
    isNewField?: boolean
  }) => void
}) {
  const [question, setQuestion] = useState('')
  const [answer, setAnswer] = useState('')
  const [questionError, setQuestionError] = useState(false)
  const [answerError, setAnswerError] = useState(false)

  const handleOnConditionChange = ({ target }) => {
    onConditionChange({ conditionType: target.value, isNewField: true })
  }

  const handleOnRemoveCondition = ({ target }) => {
    onConditionChange({ conditionIndex })
  }

  const handleOnQuestionChange = ({ target }) => {
    setQuestion(target.value)
    if (questionError) {
      setQuestionError(false)
    }
  }
  const handleOnAnswerChange = ({ target }) => {
    setAnswer(target.value)
    if (answerError) {
      setQuestionError(false)
    }
  }

  const handleOnAnswerSubmit = () => {
    let status = 'REJECTED'
    if (
      answer &&
      answer.toLowerCase() === condition?.choices[0].toLowerCase()
    ) {
      status = 'APPROVED'
    }
    if (conditionProgresses[condition.id]) {
      onUpdateConditionProgress({
        conditionProgressID: conditionProgresses[condition.id].id,
        userID: user.id,
        conditionID: condition.id,
        status,
      }).then(() => {
        if (status === 'REJECTED') {
          toast.warning({
            content: 'Condition was not met. Please try again.',
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.success({
            content: 'Condition was successfully met!',
            timeout: toast.defaultTimeout,
          })
        }
      })
    } else {
      onCreateConditionProgress({
        userID: user.id,
        conditionID: condition.id,
        status,
      }).then(() => {
        if (status === 'REJECTED') {
          toast.warning({
            content: 'Condition was not met. Please try again.',
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.success({
            content: 'Condition was successfully met!',
            timeout: toast.defaultTimeout,
          })
        }
      })
    }
  }

  const handleOnQuestionSubmit = () => {
    if (!question || !answer) {
      if (!question) {
        setQuestionError(true)
      }
      if (!answer) {
        setAnswerError(true)
      }
      return
    }
    const updatedCondition = {
      ...condition,
      question,
      choices: [answer],
      answerIndex: 0,
    }
    console.log('updatedCondition', updatedCondition)
    onConditionChange({ conditionIndex, value: updatedCondition })
  }
  const renderQuestion = () => {
    let questionContent

    if (
      inProgress &&
      condition.question &&
      conditionProgresses &&
      conditionProgresses[condition.id]?.status === 'APPROVED'
    ) {
      questionContent = (
        <CardContent className={styles['content-answer']}>
          <Avatar sx={{ bgcolor: purple[500] }} aria-label="recipe">
            <QuestionMarkIcon />
          </Avatar>
          <div className={styles['fields-container']}>
            <Typography variant="body2" color="text.secondary" margin={2}>
              {condition.question}
            </Typography>
            <Typography variant="body2" color="text.secondary" margin={2}>
              {condition.choices[0]}
            </Typography>
          </div>
          <Avatar
            sx={{ bgcolor: green[500], width: 56, height: 56 }}
            aria-label="recipe"
          >
            <VerifiedIcon />
          </Avatar>
        </CardContent>
      )
    } else if (condition.question && inProgress) {
      questionContent = (
        <>
          <CardContent className={styles['content-answer']}>
            <Avatar sx={{ bgcolor: purple[500] }} aria-label="recipe">
              <QuestionMarkIcon />
            </Avatar>
            <div className={styles['fields-container']}>
              <Typography variant="body2" color="text.secondary" margin={2}>
                {condition.question}
              </Typography>
              <TextField
                label="Answer"
                variant="outlined"
                fullWidth
                value={answer}
                onChange={handleOnAnswerChange}
                error={answerError}
                helperText={
                  answerError
                    ? 'Please provide an answer to the question.'
                    : null
                }
              />
            </div>
          </CardContent>
          <CardActions style={{ float: 'right' }}>
            <Button
              size="small"
              variant="contained"
              onClick={handleOnAnswerSubmit}
            >
              Submit
            </Button>
          </CardActions>
        </>
      )
    } else if (condition.question && !inProgress) {
      questionContent = (
        <CardContent className={styles.content}>
          <Avatar sx={{ bgcolor: purple[500] }} aria-label="recipe">
            <QuestionMarkIcon />
          </Avatar>
          <Typography variant="body2" color="text.secondary" margin={2}>
            {condition.question}
          </Typography>
        </CardContent>
      )
    } else {
      questionContent = (
        <>
          <CardContent className={styles['content-edit']}>
            <Avatar sx={{ bgcolor: purple[500] }} aria-label="recipe">
              <QuestionMarkIcon />
            </Avatar>
            <div className={styles['fields-container']}>
              <TextField
                label="Question"
                variant="outlined"
                fullWidth
                margin="normal"
                value={question}
                onChange={handleOnQuestionChange}
                error={questionError}
                helperText={questionError ? 'Please provide a question.' : null}
              />
              <TextField
                label="Answer"
                variant="outlined"
                fullWidth
                value={answer}
                onChange={handleOnAnswerChange}
                error={answerError}
                helperText={
                  answerError
                    ? 'Please provide an answer to the question.'
                    : null
                }
              />
            </div>
          </CardContent>
          <CardActions style={{ float: 'right' }}>
            <Button size="small" onClick={handleOnRemoveCondition}>
              Cancel
            </Button>
            <Button
              size="small"
              variant="contained"
              onClick={handleOnQuestionSubmit}
            >
              Add
            </Button>
          </CardActions>
        </>
      )
    }

    return <Card className={styles.main}>{questionContent}</Card>
  }

  const renderCondition = () => {
    if ('TEMP' in condition) {
      return null
    }
    switch (condition.type) {
      case 'GEO':
        return (
          <Card className={styles.main}>
            <CardContent className={styles['content-answer']}>
              <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                <PlaceIcon />
              </Avatar>
              <div style={{ display: 'flex', overflow: 'hidden' }}>
                <Typography
                  variant="body2"
                  color="text.secondary"
                  margin={2}
                  noWrap
                >
                  {condition.displayAddress}
                </Typography>
              </div>
              {inProgress &&
              conditionProgresses &&
              conditionProgresses[condition.id]?.status === 'APPROVED' ? (
                <Avatar
                  sx={{ bgcolor: green[500], width: 56, height: 56 }}
                  aria-label="recipe"
                >
                  <VerifiedIcon />
                </Avatar>
              ) : inProgress ? (
                <CardActions style={{ float: 'right' }}>
                  <Fab
                    size="small"
                    sx={{
                      bgcolor: yellow[500],
                      color: 'white',
                      width: 56,
                      height: 56,
                    }}
                    onClick={onVerifyCondition}
                  >
                    <VerifiedIcon />
                  </Fab>
                </CardActions>
              ) : null}
            </CardContent>
          </Card>
        )
      case 'QUESTION':
        return <>{renderQuestion()}</>
      default:
        return
    }
  }
  return (
    <>
      {!('TEMP' in condition) ? null : (
        <div className="dropdown_container">
          <FormControl variant="outlined" sx={{ width: '100%' }}>
            <InputLabel id="demo-simple-select-standard-label">
              Select Condition Type
            </InputLabel>
            <Select
              labelId="demo-simple-select-standard-label"
              id="demo-simple-select-standard"
              label="Select Condition Type"
              onChange={handleOnConditionChange}
              value={''}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={'QUESTION'}>Question</MenuItem>
            </Select>
          </FormControl>
        </div>
      )}
      {renderCondition()}
    </>
  )
}
