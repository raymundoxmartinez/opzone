import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import ConditionCard from './index'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Example/ConditionCard',
  component: ConditionCard,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof ConditionCard>

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof ConditionCard> = (args: any) => (
  <ConditionCard {...args} />
)

export const Primary = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  primary: true,
  label: 'ConditionCard',
}

export const Secondary = Template.bind({})
Secondary.args = {
  label: 'ConditionCard',
}

export const Large = Template.bind({})
Large.args = {
  size: 'large',
  label: 'ConditionCard',
}

export const Small = Template.bind({})
Small.args = {
  size: 'small',
  label: 'ConditionCard',
}
