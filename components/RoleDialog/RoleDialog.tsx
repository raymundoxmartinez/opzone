import React, { useState } from 'react'

import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Box,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Fade,
} from '@mui/material'
import { TransitionProps } from '@mui/material/transitions'
import { FormIndex } from '@components/form-components'

import styles from './RoleDialog.module.scss'

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>
  },
  ref: React.Ref<unknown>
) {
  return <Fade ref={ref} {...props} />
})
const RoleDialog = ({
  open,
  handleCloseCreateRoleDialog,
  handleCreateRole,
}: {
  open: any
  handleCloseCreateRoleDialog: any
  handleCreateRole: any
}) => {
  const [activeStep, setActiveStep] = useState(-1)
  const [category, setCategory] = useState<string>('')
  const [skipped, setSkipped] = useState(new Set<number>())

  const handleOnCategoryChange = (e: any) => {
    setCategory(e.target.value)
  }

  const isStepOptional = (step: number) => {
    return step === 1
  }

  const isStepSkipped = (step: number) => {
    return skipped.has(step)
  }

  const handleNext = () => {
    let newSkipped = skipped
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values())
      newSkipped.delete(activeStep)
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1)
    setSkipped(newSkipped)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.")
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1)
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values())
      newSkipped.add(activeStep)
      return newSkipped
    })
  }

  const handleReset = () => {
    setActiveStep(0)
  }

  return (
    <>
      <Dialog
        onClose={handleCloseCreateRoleDialog}
        aria-labelledby="customized-dialog-title"
        open={open}
        TransitionComponent={Transition}
        transitionDuration={500}
        fullScreen
      >
        {activeStep == -1 ? (
          <>
            <DialogTitle id="customized-dialog-title">
              Select Role Type
            </DialogTitle>
            <DialogContent className={styles['dialog__content']} dividers>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  height: '100%',
                }}
              >
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-helper-label">
                    Role Type
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={category}
                    label="Category"
                    onChange={handleOnCategoryChange}
                  >
                    <MenuItem value={'opz_investor'}>
                      Opportunity Zone Investor
                    </MenuItem>
                    <MenuItem value={'opz_manager'}>
                      Opportunity Zone Fund Manager
                    </MenuItem>
                    <MenuItem value={'cg_manager'}>
                      Consumer Good Store Manager
                    </MenuItem>
                    <MenuItem value={'cg_employee'}>
                      Consumer Good Store Employee
                    </MenuItem>
                  </Select>
                </FormControl>
              </div>
            </DialogContent>
            <DialogActions>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  pt: 2,
                  width: '100%',
                  justifyContent: 'space-between',
                }}
              >
                <Button
                  onClick={handleCloseCreateRoleDialog}
                  style={{ color: 'red' }}
                >
                  Cancel
                </Button>
                <Button
                  onClick={() => {
                    setActiveStep(0)
                  }}
                  disabled={!category}
                >
                  Next
                </Button>
              </Box>
            </DialogActions>
          </>
        ) : category ? (
          <FormIndex
            name={category}
            activeStep={activeStep}
            isStepSkipped={isStepSkipped}
            onSubmit={handleCreateRole}
            onNext={handleNext}
            onBack={handleBack}
            onReset={handleReset}
          />
        ) : null}
      </Dialog>
    </>
  )
}
export default RoleDialog
