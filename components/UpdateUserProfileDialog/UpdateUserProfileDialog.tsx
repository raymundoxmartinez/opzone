import React from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Box,
} from '@mui/material'
import FileUpload from '../FileUpload'
import { FormInputText } from '@components/form-components'

import styles from './UpdateUserProfileDialog.module.scss'

const UpdateUserProfileDialog = ({
  user,
  open,
  handleCloseUpdateUserProfileDialog,
  handleUpdateUserProfile,
}: {
  user: any
  open: any
  handleCloseUpdateUserProfileDialog: any
  handleUpdateUserProfile: any
}) => {
  const formMethods = useForm({
    defaultValues: {
      images: user?.images && user.images.length ? user.images[0] : '',
      username: user?.username,
      email: user?.email,
      location: user?.location,
      bio: user?.bio,
    },
  })

  const renderContent = () => {
    //using FormProvider because FileUpload needs access
    //to the form context
    return (
      <>
        <FormProvider {...formMethods}>
          <form>
            <div className={styles['file-upload__container']}>
              <FileUpload
                avatar
                name="images"
                accept="image/png, image/jpg, image/jpeg, image/gif"
                src={user?.images && user.images.length ? user.images[0] : ''}
              />
            </div>
            <FormInputText
              name="username"
              control={formMethods.control}
              defaultValue={user?.username}
              label="Username"
              textFieldProps={{
                style: { margin: 5 },
                variant: 'outlined',
                type: 'string',
                fullWidth: true,
              }}
            />
            <FormInputText
              name="email"
              control={formMethods.control}
              defaultValue={user?.email}
              label="email"
              textFieldProps={{
                style: { margin: 5 },
                variant: 'outlined',
                type: 'string',
                fullWidth: true,
              }}
            />
            <FormInputText
              name="location"
              control={formMethods.control}
              defaultValue={user?.location}
              label="location"
              textFieldProps={{
                style: { margin: 5 },
                variant: 'outlined',
                type: 'string',
                fullWidth: true,
              }}
            />
            <FormInputText
              name="bio"
              control={formMethods.control}
              defaultValue={user?.bio}
              label="bio"
              textFieldProps={{
                style: { margin: 5 },
                variant: 'outlined',
                type: 'string',
                fullWidth: true,
                multiline: true,
                rows: 5,
              }}
            />
          </form>
        </FormProvider>
      </>
    )
  }
  return (
    <>
      <Dialog
        onClose={handleCloseUpdateUserProfileDialog}
        aria-labelledby="customized-dialog-title"
        open={open}
        fullScreen
      >
        <DialogTitle id="customized-dialog-title">
          Update User Profile
        </DialogTitle>
        <DialogContent className={styles['dialog__content']} dividers>
          {renderContent()}
        </DialogContent>
        <DialogActions>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              pt: 2,
              width: '100%',
              justifyContent: 'space-between',
            }}
          >
            <Button
              style={{ color: 'red' }}
              autoFocus
              onClick={handleCloseUpdateUserProfileDialog}
            >
              Cancel
            </Button>
            <Button
              autoFocus
              onClick={formMethods.handleSubmit(handleUpdateUserProfile)}
            >
              Update
            </Button>
          </Box>
        </DialogActions>
      </Dialog>
    </>
  )
}
export default UpdateUserProfileDialog
