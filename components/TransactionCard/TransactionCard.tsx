import * as React from 'react'
import { useRouter } from 'next/router'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import CardActionArea from '@mui/material/CardActionArea'
import Typography from '@mui/material/Typography'
import CategoryIcon from '@mui/icons-material/Category'

import styles from './TransactionCard.module.scss'

export default function TransactionCard({
  transaction,
  disabled,
  inProgress,
}: {
  transaction: any
  disabled: boolean
  inProgress: boolean
}) {
  const router = useRouter()

  //@ts-ignore
  const formatContractType = (type) => {
    switch (type) {
      case 'ESCROW_GEO':
        return 'Geo Contract'
      default:
        break
    }
  }
  const displayAddress = transaction.conditions.find(
    (condition) => condition.type === 'GEO'
  ).displayAddress
  return (
    <Card className={styles.main}>
      <CardActionArea
        onClick={(e) => {
          const params = inProgress ? '?inProgress=true' : ''

          e.preventDefault()
          router.push(`/dashboard/transactions/${transaction.id}${params}`)
        }}
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
        }}
        disabled={disabled}
      >
        <CardContent className={styles.content}>
          <CategoryIcon />
          <Typography style={{ marginLeft: 5 }}>
            {transaction.description}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}
