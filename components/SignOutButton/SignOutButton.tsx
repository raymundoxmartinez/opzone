import React from 'react'
import { Button } from '@mui/material'
import useApp from '@src/hooks/app/useApp'
//@ts-ignore
const SignOutButton = (props) => {
  const { signOutUser } = useApp()

  const signOut = async (e) => {
    if (props.onClick) {
      props.onClick()
    }
    e.preventDefault()
    localStorage.removeItem('xummUserAccount')
    localStorage.removeItem('xummUserToken')
    localStorage.removeItem('xummSessionId')
    setTimeout(async () => {
      signOutUser()
    }, 2000)
  }
  return (
    <Button style={{ color: 'white', marginLeft: 20 }} onClick={signOut}>
      Sign Out
    </Button>
  )
}
export default SignOutButton
