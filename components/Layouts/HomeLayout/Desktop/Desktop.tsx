import React from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'

import { Carousel } from 'react-responsive-carousel'

import { Button } from '@mui/material'

import styles from './Desktop.module.scss'
import 'react-responsive-carousel/lib/styles/carousel.min.css'

const HomeDesktop = (props: any) => {
  const router = useRouter()
  const renderButton = () => {
    let button = (
      <Button
        className={styles.actionBtn}
        variant="contained"
        onClick={() => {
          router.push('/auth')
        }}
      >
        Get Started
      </Button>
    )
    if (props.isLoggedIn) {
      button = (
        <Button
          variant="contained"
          className={styles.actionBtn}
          onClick={() => {
            router.push('/dashboard/payments')
          }}
        >
          Go to Dashboard
        </Button>
      )
    }
    return button
  }

  return (
    <div>
      <header className={styles.window}>
        <div className={styles.headerTextContainer}>
          <div className={styles.headerTextWrapper}>
            <h2 className={styles.header}>
              Buy, sell, and trade value all in one place.
            </h2>
            <h1 style={{ color: '#3f6060' }}>
              Get the tools you need to begin investing in all the things you
              like: Opportunity Zone Funds, art, NFTS...
            </h1>
            <div style={{ justifyContent: 'center', display: 'flex' }}>
              {renderButton()}
            </div>
          </div>
        </div>
      </header>
      <div className={styles.headerImagery}>
        <div className={styles.headerImageMarket}>
          <img src={'/images/market2.png'} alt="market" width={500} />
        </div>
      </div>
      <div className={styles.diagonalYellow}>
        <svg
          className={styles.trap}
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 100 100"
          preserveAspectRatio="none"
        >
          <polygon fill="#3f6060" points="0,100 100,0 100,100"></polygon>
        </svg>
      </div>
      <div style={{ position: 'relative', marginTop: '230px' }}>
        <div className={styles.benefitsHeader}>
          <h2 className={styles.benefitsTitle}>Your member benefits</h2>
          <p className={styles.benefitsParagraph}>
            Shop, create and invest in NFTs using the XRP ledger.
          </p>
        </div>
        <Carousel
          infiniteLoop
          centerSlidePercentage={60}
          className={styles.carouselComp}
          showThumbs={false}
          showArrows={true}
          width={'100%'}
          centerMode
        >
          <div>
            <img
              style={{
                objectFit: 'cover',
                width: '98%',
                height: 500,
                borderRadius: 6,
              }}
              src={'/images/carousel_0.jpeg'}
              alt="invest"
            />
          </div>
          <div>
            <img
              style={{
                objectFit: 'cover',
                width: '98%',
                height: 500,
                borderRadius: 6,
              }}
              src={'/images/investing.jpeg'}
              alt="invest"
            />
          </div>
          <div>
            <img
              style={{
                objectFit: 'cover',
                width: '98%',
                height: 500,
                borderRadius: 6,
              }}
              src={'/images/create.jpeg'}
              alt="invest"
            />
          </div>
        </Carousel>
      </div>
      <div
        style={{
          position: 'relative',
          marginTop: 0,
          backgroundColor: '#dff9f3',
          height: '100%',
          paddingTop: 32,
        }}
      >
        <div
        // className={styles.benefitsHeader}
        >
          <h2 className={styles.benefitsTitle}>How it works</h2>
          <div style={{ width: 887, margin: 'auto' }}>
            <div className={styles.howToStep}>
              <div className={styles.stepNumber}>1</div>
              <Link href="/auth" passHref className={styles.stepDescription}>
                Create an account
              </Link>
            </div>
            <div className={styles.howToStep}>
              <div className={styles.stepNumber}>2</div>
              <p className={styles.stepDescription}>Download Xumm wallet</p>
            </div>
            <a
              href="https://play.google.com/store/apps/details?id=com.xrpllabs.xumm&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"
              style={{
                display: 'inline-block',
                overflow: 'hidden',
                borderRadius: 13,
                width: 250,
                height: 83,
              }}
            >
              <img
                alt="Get it on Google Play"
                src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png"
                style={{ borderRadius: 13, width: 250, height: 83 }}
              />
            </a>
            <a
              href="https://apps.apple.com/us/app/xumm-xrp-wallet/id1492302343?itsct=apps_box_badge&amp;itscg=30200"
              style={{
                display: 'inline-block',
                overflow: 'hidden',
                borderRadius: 13,
                width: 250,
                height: 83,
              }}
            >
              <img
                src="https://tools.applemediaservices.com/api/badges/download-on-the-app-store/black/en-us?size=250x83&amp;releaseDate=1585526400&h=b785ee8ab19ac787de3c09ed8c2889f1"
                alt="Download on the App Store"
                style={{ borderRadius: 13, width: 250, height: 83 }}
              />
            </a>
            <div className={styles.howToStep}>
              <div className={styles.stepNumber}>3</div>
              <Link href="/dashboard" passHref className={styles.stepDescription}>
                
                  Go to the dashboard and connect your xumm wallet
                
              </Link>
            </div>

            <div className={styles.howToStep}>
              <div className={styles.stepNumber}>4</div>
              <p className={styles.stepDescription}>
                Scan QR code using xumm app to verify transactions
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomeDesktop
