import React from 'react'
import { useRouter } from 'next/router'
import { Button } from '@mui/material'
import dynamic from 'next/dynamic'

const Globe = dynamic(import('react-globe.gl'), {
  ssr: false,
})
import styles from './Mobile.module.scss'
import 'react-responsive-carousel/lib/styles/carousel.min.css'

const HomeMobile = (props: any) => {
  const router = useRouter()

  const renderButton = () => {
    let button = (
      <Button
        className={styles.actionBtn}
        variant="contained"
        onClick={() => {
          router.push('/auth')
        }}
      >
        Get Started
      </Button>
    )
    if (props.isLoggedIn) {
      button = (
        <Button
          variant="contained"
          className={styles.actionBtn}
          onClick={() => {
            router.push('/dashboard/payments')
          }}
        >
          Go to Dashboard
        </Button>
      )
    }
    return button
  }
  const markerSvg = `<svg viewBox="-4 0 36 36">
  <path fill="currentColor" d="M14,0 C21.732,0 28,5.641 28,12.6 C28,23.963 14,36 14,36 C14,36 0,24.064 0,12.6 C0,5.641 6.268,0 14,0 Z"></path>
  <circle fill="black" cx="14" cy="14" r="7"></circle>
</svg>`
  const N = 30
  const gData = [...Array(N).keys()].map(() => ({
    lat: (Math.random() - 0.5) * 180,
    lng: (Math.random() - 0.5) * 360,
    size: 7 + Math.random() * 30,
    color: ['red', 'white', 'blue', 'green'][Math.round(Math.random() * 3)],
  }))
  return (
    <>
      <div style={{ position: 'relative' }}>
        <header className={styles.window}>
          <div className={styles.headerTextContainer}>
            <div className={styles.headerTextWrapper}>
              <h2 className={styles.header}>
                Buy, sell, and trade value all in one place.
              </h2>
              <h1 className={styles.subHeader}>
                Get the tools you need to create smart, geo-based transactions.
              </h1>
            </div>
            <div style={{ marginTop: 75 }}>{renderButton()}</div>
          </div>
        </header>
        <Globe
          globeImageUrl="//unpkg.com/three-globe/example/img/earth-dark.jpg"
          htmlElementsData={gData}
          htmlElement={(d) => {
            const el = document.createElement('div')
            el.innerHTML = markerSvg
            el.style.color = d.color
            el.style.width = `${d.size}px`

            el.style['pointer-events'] = 'auto'
            el.style.cursor = 'pointer'
            el.onclick = () => console.info(d)
            return el
          }}
        />
      </div>
    </>
  )
}

export default HomeMobile
