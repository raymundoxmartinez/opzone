import React, { Fragment } from 'react'
import TopNavbar from '../../TopNavbar'
const MainLayout = ({
  children,
  user,
  isLoggedIn,
}: {
  children: any
  user: any
  isLoggedIn: any
}) => {
  return (
    <Fragment>
      <TopNavbar isLoggedIn={isLoggedIn} />
      <main>{children}</main>
    </Fragment>
  )
}
export default MainLayout
