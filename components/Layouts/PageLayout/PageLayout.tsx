import React, { Fragment } from 'react'
import { Typography } from '@mui/material'
import styles from './PageLayout.module.scss'
const PageLayout = ({
  title,
  extraHeader = null,
  children,
}: {
  title?: string
  children: any
  extraHeader?: any
}) => {
  return (
    <Fragment>
      {title && (
        <header className={styles['page-title']}>
          <Typography variant="h5">{title}</Typography>
          {extraHeader}
        </header>
      )}
      <section className={styles['page-section']}>{children}</section>
    </Fragment>
  )
}
export default PageLayout
