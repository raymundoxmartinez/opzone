import React, { useEffect, useState } from 'react'
import { Button } from '@mui/material'

import TrailsCreateDialog from '@components/TrailsCreateDialog'
import TrailCard from '@components/TrailCard/TrailCard'

//@ts-ignore
const Mobile = ({
  user,
  handlePostTrail,
  handlePostCondition,
  handlePostTransaction,
  handlePostLocationView,
  handlePostLocation,
  fetchTrails,
  onPostTrailLocationView,
  normalizeTrail,
  fetchAsset,
  assets,
  postReward,
  trails,
  isCreateDialogOpen,
  OnCloseTrailsCreateDialog,
  onOpenTrailsCreateDialog,
}: any) => {
  return (
    <>
      <TrailsCreateDialog
        open={isCreateDialogOpen}
        user={user}
        onCloseTrailsCreateDialog={OnCloseTrailsCreateDialog}
        onPostTrail={handlePostTrail}
        onPostTransaction={handlePostTransaction}
        onPostCondition={handlePostCondition}
        onPostLocationView={handlePostLocationView}
        onPostLocation={handlePostLocation}
        onPostTrailLocationView={onPostTrailLocationView}
        fetchTrails={fetchTrails}
        fetchAsset={fetchAsset}
        assets={assets}
        postReward={postReward}
      />
      <Button onClick={onOpenTrailsCreateDialog}>+ Create</Button>
      {trails.map((trail, index) => (
        <TrailCard key={index} trail={trail} />
      ))}
    </>
  )
}

export default Mobile
