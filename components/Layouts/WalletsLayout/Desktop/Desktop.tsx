import React from 'react'

import { toast } from '@components/Toast'

import { Chip, Stack, Paper, Typography } from '@mui/material'
import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import CreateWalletDialog from '@components/CreateWalletDialog'

//@ts-ignore
const Desktop = ({
  isCreateWalletDialogOpen,
  handleCloseCreateWalletDialog,
  handleCreateWallet,
  wallets,
  walletsLoading,
  selectedWallet,
  handleSelectWallet,
  handleOpenCreateWalletDialog,
}: any) => {
  return (
    <>
      <CreateWalletDialog
        open={isCreateWalletDialogOpen}
        handleCloseCreateWalletDialog={handleCloseCreateWalletDialog}
        handleCreateWallet={handleCreateWallet}
      />
      <Stack direction="row" alignItems="center" spacing={4}>
        <Stack direction="column" alignItems="flex-start" spacing={1}>
          {wallets &&
            wallets.map((wallet: any, index: any) => (
              <Chip
                clickable
                onClick={() => {
                  //@ts-ignore
                  navigator.clipboard.writeText(wallet.address)
                  handleSelectWallet(wallet)
                  toast.success({
                    //@ts-ignore
                    content: `${wallet.title} was copied to clipboard.`,
                    timeout: toast.defaultTimeout,
                  })
                }}
                icon={<ContentCopyIcon />}
                key={index}
                //@ts-ignore
                label={wallet.title}
                color="primary"
                style={{
                  minWidth: 300,
                  height: 50,
                }}
              />
            ))}
          <Chip
            clickable
            onClick={() => handleOpenCreateWalletDialog()}
            label="Add Wallet"
            style={{
              border: '2px dashed #39ba88',
              minWidth: 300,
              height: 50,
            }}
          />
        </Stack>
        {selectedWallet && (
          <Paper
            style={{
              height: 500,
              width: '40%',
              backgroundColor: '#3f6060',
              color: 'white',
              padding: 20,
              borderRadius: 10,
            }}
          >
            <Typography variant="h4" gutterBottom>
              {/* @ts-ignore */}
              {selectedWallet.title}
            </Typography>
            <Typography variant="h6" gutterBottom>
              {/* @ts-ignore */}
              Address: {selectedWallet.address}
            </Typography>
            <Typography variant="h6">
              {/* @ts-ignore */}
              Description: {selectedWallet.description}
            </Typography>
          </Paper>
        )}
      </Stack>
    </>
  )
}

export default Desktop
