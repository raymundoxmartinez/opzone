import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { toast } from '@components/Toast'
import { API } from 'aws-amplify'
import { Chip, Stack, Paper, Typography } from '@mui/material'
import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import CreateWalletDialog from '@components/CreateWalletDialog'
import OfflineBoltIcon from '@mui/icons-material/OfflineBolt'
import socket from '@src/socket'

//@ts-ignore
const Mobile = ({
  isCreateWalletDialogOpen,
  handleCloseCreateWalletDialog,
  handleCreateWallet,
  wallets,
  walletsLoading,
  selectedWallet,
  handleSelectWallet,
  handleOpenCreateWalletDialog,
  handleStoreXummCredentials,
  handleXummSignIn,
  handleGetTransaction,
}: any) => {
  const [address, setAddress] = useState('')
  const { query } = useRouter()

  const handleConnectWallet = async () => {
    const response = await handleXummSignIn({
      returnUrl: `${process.env.NEXT_PUBLIC_BASE_URL}/dashboard/wallets?payload={id}&register=false`,
    })
    if (response && response.next.always) {
      window.location.href = response.next.always
      socket.subscribe(response.refs.websocket_status)
      socket.on('connected', () => {
        socket.socket.onmessage = (message: Record<string, any>) => {
          console.log(message.data)
          const messageJson = JSON.parse(message.data)
          if (messageJson.signed) {
            toast.success({
              content: 'Xumm wallet connection was successful!',
              timeout: toast.defaultTimeout,
            })
            handleStoreXummCredentials(messageJson.payload_uuidv4)
          }
        }
      })
    }
  }

  useEffect(() => {
    if (query.payload && query.register === 'false') {
      handleGetTransaction(query.payload)
        .then((response: any) => {
          const xummUserAccount = response.payload.response.account
          setAddress(xummUserAccount)
          window.history.pushState(
            {},
            document.title,
            '/' + 'dashboard/wallets'
          )
          handleOpenCreateWalletDialog()
        })
        .catch((error: any) => {
          window.history.pushState(
            {},
            document.title,
            '/' + 'dashboard/wallets'
          )
        })
    }
  }, [])

  return (
    <>
      <CreateWalletDialog
        open={isCreateWalletDialogOpen}
        address={address}
        handleCloseCreateWalletDialog={handleCloseCreateWalletDialog}
        handleCreateWallet={handleCreateWallet}
      />
      <Stack direction="column" alignItems="center" spacing={4}>
        {selectedWallet && (
          <Paper
            style={{
              backgroundColor: '#3f6060',
              color: 'white',
              borderRadius: 10,
              padding: 20,
              width: '100%',
            }}
          >
            <Typography variant="h4" gutterBottom noWrap>
              {/* @ts-ignore */}
              {selectedWallet.title}
            </Typography>
            <Typography variant="h6" gutterBottom noWrap>
              {/* @ts-ignore */}
              Address: {selectedWallet.address}
            </Typography>
            <Typography variant="h6">
              {/* @ts-ignore */}
              Description: {selectedWallet.description}
            </Typography>
          </Paper>
        )}
        <Stack direction="column" alignItems="flex-start" spacing={1}>
          {wallets &&
            wallets.map((wallet: any, index: any) => (
              <Chip
                clickable
                onClick={() => {
                  //@ts-ignore
                  navigator.clipboard.writeText(wallet.address)
                  handleSelectWallet(wallet)
                  toast.success({
                    //@ts-ignore
                    content: `${wallet.title} was copied to clipboard.`,
                    timeout: toast.defaultTimeout,
                  })
                }}
                icon={<ContentCopyIcon />}
                key={index}
                //@ts-ignore
                label={wallet.title}
                color="primary"
                style={{
                  minWidth: 300,
                  height: 50,
                }}
              />
            ))}
          <Chip
            clickable
            onClick={handleConnectWallet}
            icon={
              <OfflineBoltIcon style={{ color: 'rgba(57, 186, 136, 1)' }} />
            }
            label="Add Wallet"
            style={{
              border: '2px dashed #39ba88',
              minWidth: 300,
              height: 50,
            }}
          />
        </Stack>
      </Stack>
    </>
  )
}

export default Mobile
