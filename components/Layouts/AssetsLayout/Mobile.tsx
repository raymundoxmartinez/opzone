import React from 'react'
import { Chip } from '@mui/material'
import ProductCard from '@components/ProductCard'
import styles from './Mobile.module.scss'

//@ts-ignore
const Mobile = ({ assets, onChangeHandler, onDeleteAssetClick }: any) => {
  return (
    <>
      <div className={styles['product-container']}>
        <input
          type="file"
          id="imageupload"
          style={{ display: 'none' }}
          onChange={onChangeHandler}
        />
        <label htmlFor="imageupload" style={{ width: '100%', display: 'flex' }}>
          <Chip clickable label="Add Asset" className={styles.chip} />
        </label>
        {assets &&
          assets.map((asset: any, index: any) => (
            <div key={index} style={{ marginLeft: 10, width: '100%' }}>
              <ProductCard product={asset} onDeleteClick={onDeleteAssetClick} />
            </div>
          ))}
      </div>
    </>
  )
}

export default Mobile
