import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Auth } from 'aws-amplify'
import { useRouter } from 'next/router'
import { API } from 'aws-amplify'

import { useMediaQuery } from '@mui/material'
import { useTheme } from '@mui/material/styles'
import { toast } from '@components/Toast'
import services from '@services/index'
import socket from '@src/socket'
import Mobile from './Mobile'
import Desktop from './Desktop'

const DashboardLayout = ({ children, user }: { children: any; user: any }) => {
  //@ts-ignore
  const matchesMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'))
  const theme = useTheme()
  const router = useRouter()

  const [isDrawerOpen, setIsDrawerOpen] = useState<boolean>(false)
  const [isWalletStoreDialogOpen, setIsWalletStoreDialogOpen] =
    useState<boolean>(false)
  const [xummUserAccount, setXummUserAccount] = useState<any>('')
  const [xummUserToken, setXummUserToken] = useState<any>('')
  const [loadingXummSignIn, setLoadingXummSignIn] = useState<boolean>(false)
  const [qrCode, setQRcode] = useState('')
  const [isPayloadDialogOpen, setIsPayloadDialogOpen] = useState(false)

  const { transactionsBySenderID, transactionsByReceiverID } = useSelector(
    (state) => ({
      transactionsBySenderID: state.transactions.bySenderId.idList,
      transactionsByReceiverID: state.transactions.byReceiverId.idList,
    })
  )

  const handleDrawerOpen = () => {
    setIsDrawerOpen(true)
  }
  const handleDrawerClose = () => {
    setIsDrawerOpen(false)
  }

  const handleClosePayloadDialog = () => {
    setIsPayloadDialogOpen(false)
  }
  const handleCloseSaveWalletDialog = () => {
    setIsWalletStoreDialogOpen(false)
  }

  const handleGetSigInPayload = async (payloadId: string) => {
    const response = await API.get('xumm', `/payload`, {
      queryStringParameters: {
        id: payloadId,
      },
    })

    if (response) {
      const xummUserAccount = response.response.account
      const xummUserToken = response.application.issued_user_token
      //TODO: store in backend on user model
      localStorage.setItem('xummUserAccount', xummUserAccount)
      localStorage.setItem('xummUserToken', xummUserToken)

      setXummUserAccount(xummUserAccount)
      setXummUserToken(xummUserToken)
      setIsWalletStoreDialogOpen(true)
    }
  }

  const handleSaveWallet = async () => {
    const { attributes, username, ...rest } =
      await Auth.currentAuthenticatedUser()
    const updateUserResponse = await services.userService.updateUser(
      attributes.sub,
      //@ts-ignore
      { wallets: [xummUserAccount] }
    )
    return updateUserResponse
  }

  const handleXummSignIn = async () => {
    setLoadingXummSignIn(true)
    const response = await API.post('xumm', '/payload', {
      body: {
        options: {
          submit: false,
          expire: 240,
          return_url: {
            web: 'http://localhost:3000/?payload={id}',
          },
        },
        txjson: {
          TransactionType: 'SignIn',
        },
      },
    })
    setLoadingXummSignIn(false)
    if (response && response.refs.qr_png) {
      setQRcode(response.refs.qr_png)
      socket.subscribe(response.refs.websocket_status)
      socket.on('connected', () => {
        socket.socket.onmessage = (message: Record<string, any>) => {
          console.log(message.data)
          const messageJson = JSON.parse(message.data)
          if (messageJson.signed) {
            toast.success({
              content: 'Xumm wallet connection was successful!',
              timeout: toast.defaultTimeout,
            })
            setIsPayloadDialogOpen(false)

            handleGetSigInPayload(messageJson.payload_uuidv4)
          }
        }
      })
      setIsPayloadDialogOpen(true)
    }
  }

  const dashboardLayoutProps = {
    theme,
    router,
    isDrawerOpen,
    isWalletStoreDialogOpen,
    qrCode,
    isPayloadDialogOpen,
    children,
    user,
    transactionsBySenderID,
    transactionsByReceiverID,
    handleDrawerOpen,
    handleDrawerClose,
    handleCloseSaveWalletDialog,
    handleClosePayloadDialog,
    handleSaveWallet,
  }
  return (
    <>
      {matchesMobile ? (
        <Mobile {...dashboardLayoutProps} />
      ) : (
        <Desktop {...dashboardLayoutProps} />
      )}
    </>
  )
}

export default DashboardLayout
