import * as React from 'react'
import Link from 'next/link'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import CssBaseline from '@mui/material/CssBaseline'
import Divider from '@mui/material/Divider'
import Drawer from '@mui/material/Drawer'
import IconButton from '@mui/material/IconButton'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Button from '@mui/material/Button'
import MenuIcon from '@mui/icons-material/Menu'
import Toolbar from '@mui/material/Toolbar'
import Badge from '@mui/material/Badge'
import Typography from '@mui/material/Typography'
import { dashboardNavLinks } from '@src/configs/constants'
import { ToastMessageHost } from '@components/Toast'
import SignOutButton from '@components/SignOutButton'
import PayloadDialog from '@components/PayloadDialog'
import ConfirmationDialog from '@components/ConfirmationDialog'

import styles from './Mobile.module.scss'

const drawerWidth = 240

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window
}
const navLinks = ['profile', 'trails', 'assets', 'wallets', 'payments']
export default function ResponsiveDrawer({
  theme,
  router,
  isDrawerOpen,
  isWalletStoreDialogOpen,
  qrCode,
  isPayloadDialogOpen,
  children,
  user,
  handleDrawerOpen,
  handleDrawerClose,
  handleCloseSaveWalletDialog,
  handleClosePayloadDialog,
  handleSaveWallet,
  transactionsBySenderID,
  transactionsByReceiverID,
  ...props
}: any) {
  const { window } = props
  const [mobileOpen, setMobileOpen] = React.useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  const drawer = (
    <>
      <Toolbar />
      <Divider />
      <List className={styles.sidebar}>
        {dashboardNavLinks
          .filter((link) => navLinks.includes(link.key))
          .map((link) =>
            link.href ? (
              <Link href={link.href} passHref key={link.key} legacyBehavior>
                <ListItemButton
                  classes={{ selected: styles.selected }}
                  selected={router.pathname === link.href}
                  key={link.key}
                >
                  <ListItemIcon>
                    {link.key === 'Profile' ? (
                      <Badge
                        badgeContent={transactionsByReceiverID.length}
                        color="secondary"
                      >
                        {link.icon}
                      </Badge>
                    ) : (
                      <>{link.icon}</>
                    )}
                  </ListItemIcon>
                  <ListItemText primary={link.tooltip} />
                </ListItemButton>
              </Link>
            ) : (
              <ListItem button key={link.key} disabled>
                <ListItemIcon>{link.icon}</ListItemIcon>
                <ListItemText primary={link.tooltip} />
              </ListItem>
            )
          )}
      </List>
    </>
  )

  const container =
    window !== undefined ? () => window().document.body : undefined

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          ml: { sm: `${drawerWidth}px` },
        }}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <Badge
              badgeContent={transactionsByReceiverID.length}
              color="secondary"
            >
              <MenuIcon />
            </Badge>
          </IconButton>
          <Link href="/" passHref legacyBehavior>
            <Typography variant="h6" noWrap component="div">
              OmniAgora
            </Typography>
          </Link>
        </Toolbar>
      </AppBar>
      <Box
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
        aria-label="mailbox folders"
      >
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          classes={{ paper: styles.sidebar }}
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
            },
          }}
        >
          {drawer}
          <div style={{ marginTop: 100 }}>
            <SignOutButton onClick={handleDrawerToggle} />
          </div>
        </Drawer>
        <Drawer
          classes={{ paper: styles.sidebar }}
          variant="permanent"
          sx={{
            display: { xs: 'none', sm: 'block' },
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
            },
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>
      <Box
        component="main"
        id="main-content"
        style={{ width: '100%', height: '100vh' }}
        sx={{
          flexGrow: 1,
          width: { sm: `calc(100% - ${drawerWidth}px)` },
        }}
      >
        <ToastMessageHost />
        <PayloadDialog
          qrCode={qrCode}
          handleClosePayloadDialog={handleClosePayloadDialog}
          open={isPayloadDialogOpen}
        />
        <ConfirmationDialog
          open={isWalletStoreDialogOpen}
          title="Confirm"
          actions={
            <div style={{ display: 'flex' }}>
              <Button onClick={handleCloseSaveWalletDialog}>
                Don&apos;t Save
              </Button>
              <Button onClick={() => handleSaveWallet()}>Save</Button>
            </div>
          }
          content="Would you like to store this wallet address so other users can find you here?"
        />
        <div className={styles['page-content']}>{children}</div>
      </Box>
    </Box>
  )
}
