import React from 'react'
import { styled, Theme, CSSObject } from '@mui/material/styles'
import Link from 'next/link'
import Box from '@mui/material/Box'
import MuiDrawer from '@mui/material/Drawer'
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import List from '@mui/material/List'
import CssBaseline from '@mui/material/CssBaseline'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import IconButton from '@mui/material/IconButton'
import Button from '@mui/material/Button'
import MenuIcon from '@mui/icons-material/Menu'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import SignOutButton from '@components/SignOutButton'
import PayloadDialog from '@components/PayloadDialog'
import { ToastMessageHost } from '@components/Toast'
import ConfirmationDialog from '@components/ConfirmationDialog'
import { dashboardNavLinks } from '@src/configs/constants'
import styles from './Desktop.module.scss'

const drawerWidth = 240

const openedMixin = (theme: Theme): CSSObject => ({
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
})

const closedMixin = (theme: Theme): CSSObject => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(9)} + 1px)`,
  },
})

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}))

interface AppBarProps extends MuiAppBarProps {
  open?: boolean
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}))

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: 'nowrap',
  boxSizing: 'border-box',
  ...(open && {
    ...openedMixin(theme),
    '& .MuiDrawer-paper': openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    '& .MuiDrawer-paper': closedMixin(theme),
  }),
}))

export default function MiniDrawer({
  theme,
  router,
  isDrawerOpen,
  isWalletStoreDialogOpen,
  qrCode,
  isPayloadDialogOpen,
  children,
  user,
  handleDrawerOpen,
  handleDrawerClose,
  handleCloseSaveWalletDialog,
  handleClosePayloadDialog,
  handleSaveWallet,
}: any) {
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar className={styles.topBar} position="fixed" open={isDrawerOpen}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: '36px',
              ...(isDrawerOpen && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Link href="/" passHref legacyBehavior>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              OmniAgora
            </Typography>
          </Link>
          <Typography variant="h6" noWrap component="div">
            {user && user.username ? user.username : ''}
          </Typography>
          <SignOutButton />
        </Toolbar>
      </AppBar>
      <Drawer
        classes={{ paper: styles.sidebar }}
        variant="permanent"
        open={isDrawerOpen}
      >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List className={styles.sidebar}>
          {dashboardNavLinks.map((link) =>
            link.href ? (
              <Link href={link.href} passHref key={link.key} legacyBehavior>
                <ListItemButton
                  classes={{ selected: styles.selected }}
                  selected={router.pathname === link.href}
                  key={link.key}
                >
                  <ListItemIcon>{link.icon}</ListItemIcon>
                  <ListItemText primary={link.tooltip} />
                </ListItemButton>
              </Link>
            ) : (
              <ListItem button key={link.key} disabled>
                <ListItemIcon>{link.icon}</ListItemIcon>
                <ListItemText primary={link.tooltip} />
              </ListItem>
            )
          )}
        </List>
      </Drawer>
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <DrawerHeader />
        <ToastMessageHost />
        <PayloadDialog
          qrCode={qrCode}
          handleClosePayloadDialog={handleClosePayloadDialog}
          open={isPayloadDialogOpen}
        />
        <ConfirmationDialog
          open={isWalletStoreDialogOpen}
          title="Confirm"
          actions={
            <div style={{ display: 'flex' }}>
              <Button onClick={handleCloseSaveWalletDialog}>
                Don&apos;t Save
              </Button>
              <Button onClick={() => handleSaveWallet()}>Save</Button>
            </div>
          }
          content="Would you like to store this wallet address so other users can find you here?"
        />
        {children}
      </Box>
    </Box>
  );
}
