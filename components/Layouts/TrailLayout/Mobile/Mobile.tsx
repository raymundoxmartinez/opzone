import React, { useEffect, useState, useCallback } from 'react'
import useLocation, { useRouter } from 'next/router'
import { Carousel } from 'react-responsive-carousel'
import Backdrop from '@mui/material/Backdrop'
import CircularProgress from '@mui/material/CircularProgress'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import PauseIcon from '@mui/icons-material/Pause'
import {
  Typography,
  AppBar,
  Toolbar,
  Button,
  Tabs,
  Tab,
  Fab,
  MenuItem,
  Menu,
} from '@mui/material'
import PlayArrowIcon from '@mui/icons-material/PlayArrow'
import DeleteIcon from '@mui/icons-material/Delete'
import { toast } from '@components/Toast'
import Map from '@components/Map'
import LocationCardWithTransactions from '@components/LocationCardWithTransactions'
import DraggableListContainer from '@components/DraggableListContainer'
import TransactionDialog from '@components/TransactionDialog'

import styles from './Mobile.module.scss'
import 'react-responsive-carousel/lib/styles/carousel.min.css'

const Mobile = ({
  fetchTrail,
  trailProgresses,
  fetchTrailProgresses,
  fetchTransaction,
  attachImages,
  normalizeTrail,
  deleteTrail,
  deleteTrailProgress,
  fetchTrails,
  user,
  postTrailProgress,
  trail,
  fetchTrailProgress,
}) => {
  const router = useRouter()
  const { id: trailID, inProgress } = router.query

  const [value, setValue] = useState('locations')
  const [expanded, setExpanded] = useState(false)
  const [isSubmitting, setIsSubmitting] = useState(false)

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)

  const handleOpenOptionsMenu = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    setAnchorEl(event.currentTarget)
  }

  const handleCloseOptionsMenu = () => {
    setAnchorEl(null)
  }

  const handleExpand = () => {
    setExpanded(true)
  }

  const handleOnTransactionClick = (transactionID) => {
    const params = inProgress ? `?inProgress=${inProgress}` : ''

    router.push(`/dashboard/transactions/${transactionID}${params}`)
  }

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleOnTrailPlayClick = async () => {
    const trailProgressResponse =
      (trailProgresses &&
        Object.values(trailProgresses).find((trailProgress) => {
          return trailProgress.trailID === trailID
        })) ||
      (await fetchTrailProgresses({
        filter: { userID: { eq: user.id }, trailID: { eq: trailID } },
      }))
    if (
      !trailProgressResponse ||
      (trailProgressResponse &&
        Array.isArray(trailProgressResponse) &&
        !trailProgressResponse.length)
    ) {
      try {
        const trailProgressRequest = {
          userID: user.id,
          status: 'ACTIVE',
          trailID,
        }
        const response = await postTrailProgress(trailProgressRequest)
        const params = `?inProgress=${response.id}`
        router.push(`/dashboard/trails/${trailID}${params}`)
        return
      } catch (error) {
        console.log(error)
      }
    } else {
      toast.warning({
        content: `You are already participating in this trail. View in Profile.`,
        timeout: toast.defaultTimeout,
      })
      return
    }
  }

  const handleOnTrailPauseClick = async () => {
    try {
      const trailProgressRequest = {
        // id,
        status: 'PAUSED',
      }
      const response = await postTrailProgress(trailProgressRequest)
      return
    } catch (error) {
      console.log(error)
    }
  }

  const handleOnDeleteTrailClick = async () => {
    setIsSubmitting(true)
    try {
      const deleteRes = await deleteTrail(trailID)
      await fetchTrails()
      router.push(`/dashboard/trails`)
      setIsSubmitting(false)
    } catch (error) {
      console.log(error)
      setIsSubmitting(false)
    }
  }

  const handleOnQuitTrailClick = async () => {
    setIsSubmitting(true)
    try {
      const deleteRes = await deleteTrailProgress(inProgress)
      router.push(`/dashboard/profile`)
      setIsSubmitting(false)
    } catch (error) {
      console.log(error)
      setIsSubmitting(false)
    }
  }

  return (
    <>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isSubmitting}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <></>
      <Carousel
        infiniteLoop
        centerSlidePercentage={80}
        className={styles.carouselComp}
        showThumbs={false}
        showArrows={true}
        width={'100%'}
        centerMode
      >
        {trail?.images && trail?.images.length ? (
          trail?.images.map((imageUrl, index) => (
            <div key={index}>
              <img
                style={{
                  objectFit: 'cover',
                  width: '98%',
                  height: 300,
                  borderRadius: 6,
                }}
                src={imageUrl}
                alt="invest"
              />
            </div>
          ))
        ) : (
          <div>
            <img
              style={{
                objectFit: 'cover',
                width: '98%',
                height: 300,
                borderRadius: 6,
              }}
              src={'/images/create.jpeg'}
              alt="invest"
            />
          </div>
        )}
      </Carousel>
      <Typography variant="h5" gutterBottom>
        {trail?.name}
      </Typography>
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 3 }}>
          <Typography variant="body2" gutterBottom>
            {expanded
              ? trail?.description
              : `${trail?.description?.substring(0, 100)}...`}
          </Typography>
          {!expanded && (
            <Button color="primary" onClick={handleExpand}>
              Show more
            </Button>
          )}
        </div>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
          }}
        >
          {inProgress ? (
            <Fab color="primary" aria-label="add" onClick={() => {}}>
              <PauseIcon />
            </Fab>
          ) : (
            <Fab
              color="primary"
              aria-label="add"
              onClick={handleOnTrailPlayClick}
            >
              <PlayArrowIcon />
            </Fab>
          )}
        </div>
      </div>
      <div style={{ display: 'flex' }}>
        <Tabs
          value={value}
          textColor="primary"
          indicatorColor="primary"
          onChange={handleChange}
          centered
          className={styles.trailTabs}
        >
          <Tab label="Locations" value="locations" disableRipple />
          <Tab label="Map" value="map" disableRipple />
        </Tabs>

        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
          }}
        >
          {(trail?.userID == user?.id || inProgress) && (
            <Button
              id="basic-button"
              aria-controls={open ? 'basic-menu' : undefined}
              aria-haspopup="true"
              aria-expanded={open ? 'true' : undefined}
              onClick={handleOpenOptionsMenu}
              endIcon={<KeyboardArrowDownIcon />}
            >
              Options
            </Button>
          )}

          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleCloseOptionsMenu}
            MenuListProps={{
              'aria-labelledby': 'basic-button',
            }}
          >
            {inProgress ? (
              <MenuItem onClick={handleOnQuitTrailClick}>
                Quit Trail
                <DeleteIcon />
              </MenuItem>
            ) : (
              <MenuItem onClick={handleOnDeleteTrailClick}>
                Delete
                <DeleteIcon />
              </MenuItem>
            )}
          </Menu>
        </div>
      </div>
      {value === 'locations' && trail && (
        <DraggableListContainer
          cardRenderer={LocationCardWithTransactions}
          items={trail?.locations}
          itemProps={{
            onTransactionClick: handleOnTransactionClick,
          }}
        />
      )}

      {value === 'map' && trail && (
        <Map interactive={false} size={'small'} locations={trail?.locations} />
      )}
    </>
  )
}

const MemoizedMobile = React.memo(Mobile)
export default MemoizedMobile
