import React from 'react'

import {
  Typography,
  Avatar,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  IconButton,
  Button,
  Skeleton,
} from '@mui/material'
import EditIcon from '@mui/icons-material/Edit'
import UpdateUserProfileDialog from '@components/UpdateUserProfileDialog'
import CreateRoleDialog from '@components/RoleDialog'
import CircleIcon from '@mui/icons-material/Circle'
import styles from './Desktop.module.scss'

//@ts-ignore
const Desktop = ({
  user,
  profile__isUpdateUserProfileDialogOpen,
  profile__handleCloseUpdateUserProfileDialog,
  profile__handleOpenUpdateUserProfileDialog,
  isCreateRoleDialogOpen,
  loadingUser,
  handleUpdateUserProfile,
  handleCreateRole = () => {},
  handleCloseCreateRoleDialog,
  handleOpenCreateRoleDialog,
}: any) => {
  return (
    <>
      <UpdateUserProfileDialog
        user={user}
        open={profile__isUpdateUserProfileDialogOpen}
        handleCloseUpdateUserProfileDialog={
          profile__handleCloseUpdateUserProfileDialog
        }
        handleUpdateUserProfile={handleUpdateUserProfile}
      />
      <CreateRoleDialog
        open={isCreateRoleDialogOpen}
        handleCloseCreateRoleDialog={handleCloseCreateRoleDialog}
        handleCreateRole={handleCreateRole}
      />
      {loadingUser === 'idle' ? (
        <div className={styles['profile-manager__container']}>
          <div className={styles['profile-manager__edit-button-container']}>
            <IconButton onClick={profile__handleOpenUpdateUserProfileDialog}>
              <EditIcon />
            </IconButton>
          </div>
          <div className={styles['profile-manager__user-details-container']}>
            <Avatar
              alt="user profile"
              sx={{ width: 150, height: 150 }}
              src={user?.images && user.images.length ? user.images[0] : ''}
            />
            <div
              className={
                styles['profile-manager__user-details-container__details']
              }
            >
              <Typography>
                <strong>Username:</strong> {user?.username}
              </Typography>
              <Typography>
                <strong>Email:</strong> {user?.email}
              </Typography>
              <Typography>
                <strong>Phone Number:</strong> {user?.phone_number}
              </Typography>
              <Typography>
                <strong>Bio:</strong> {user?.bio}
              </Typography>
            </div>
          </div>
          <div className={styles['profile-manager__roles-container']}>
            <Typography variant="h6">Active Roles</Typography>
            <List
              sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}
              component="nav"
              aria-labelledby="nested-list-subheader"
            >
              <ListItem>
                <ListItemIcon>
                  <CircleIcon />
                </ListItemIcon>
                <ListItemText primary="Manager" />
              </ListItem>
            </List>
          </div>
          <div className={styles['profile-manager__action-buttons-container']}>
            <Button onClick={handleOpenCreateRoleDialog}>Add Role</Button>
          </div>
        </div>
      ) : (
        <div className={styles['profile-manager__container']}>
          <div className={styles['profile-manager__user-details-container']}>
            <Skeleton variant="circular" width={150} height={150} />
            <div
              className={
                styles['profile-manager__user-details-container__details']
              }
            >
              <Skeleton width={300} variant="text" />
              <Skeleton width={300} variant="text" />
              <Skeleton width={300} variant="text" />
            </div>
          </div>
          <div className={styles['profile-manager__roles-container']}>
            <Skeleton variant="rectangular" width={300} height={118} />
          </div>
        </div>
      )}
    </>
  )
}
export default Desktop
