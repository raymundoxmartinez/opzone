import React from 'react'
import { useRouter } from 'next/router'

import { Chip } from '@mui/material'
import CreateProductDialog from '@components/ProductDialog'
import ProductCard from '@components/ProductCard'
import styles from './Mobile.module.scss'

//@ts-ignore
const Mobile = ({
  isCreateProductDialogOpen,
  handleCloseCreateProductDialog,
  handleCreateProduct,
  products,
  handleOpenCreateProductDialog,
}: any) => {
  const router = useRouter()

  return (
    <>
      <CreateProductDialog
        open={isCreateProductDialogOpen}
        handleCloseCreateProductDialog={handleCloseCreateProductDialog}
        handleCreateProduct={handleCreateProduct}
      />
      <div className={styles['product-container']}>
        <Chip
          clickable
          onClick={() => handleOpenCreateProductDialog(true)}
          label="Add Product"
          className={styles.chip}
        />
        {products &&
          products.map((product: any, index: any) => (
            <ProductCard
              key={index}
              product={product}
              onClick={(e: any) => {
                e.preventDefault()
                router.push(`/dashboard/store/products/${product.id}`)
              }}
            />
          ))}
      </div>
    </>
  )
}

export default Mobile
