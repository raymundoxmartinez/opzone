import React from 'react'
import { Chip } from '@mui/material'
import CreateProductDialog from '@components/ProductDialog'
import ProductCard from '@components/ProductCard'

//@ts-ignore
const Desktop = ({
  isCreateProductDialogOpen,
  handleCloseCreateProductDialog,
  handleCreateProduct,
  products,
  handleOpenCreateProductDialog,
}: any) => {
  return (
    <>
      <CreateProductDialog
        open={isCreateProductDialogOpen}
        handleCloseCreateProductDialog={handleCloseCreateProductDialog}
        handleCreateProduct={handleCreateProduct}
      />

      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip
          clickable
          onClick={() => handleOpenCreateProductDialog(true)}
          label="Add Product"
          style={{
            textAlign: 'center',
            border: '2px dashed #39ba88',
            height: 322,
            width: 300,
          }}
        />
        {products &&
          products.map((product: any, index: any) => (
            <ProductCard key={index} product={product} />
          ))}
      </div>
    </>
  )
}

export default Desktop
