import React from 'react'

import ConfirmationDialog from '@components/ConfirmationDialog'

import { Button, Paper, Typography, Alert } from '@mui/material'
import PieChartIcon from '@mui/icons-material/PieChart'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import PeopleIcon from '@mui/icons-material/People'
import styles from './Desktop.module.scss'

const Desktop = ({
  showWalletToast,
  product,
  isConfirmDeleteDialogOpen,
  handleCloseConfirmDeleteDialog,
  handleOnConfirmDelete,
  handleOpenConfirmDeleteDialog,
  handleOpenFundProjectDialog,
  handlehideWalletToast,
  router,
}: any) => {
  if (!product) return <div>Loading...</div>
  return (
    <>
      <div>
        {showWalletToast && (
          <Alert
            severity="error"
            onClose={() => {
              handlehideWalletToast()
            }}
            onClick={() => router.push(`/`)}
          >
            Click Get Started On The Home Page And Create A Wallet First.
          </Alert>
        )}
        <ConfirmationDialog
          open={isConfirmDeleteDialogOpen}
          title="Confirm Product Delete"
          actions={
            <div style={{ display: 'flex' }}>
              <Button onClick={handleCloseConfirmDeleteDialog}>Cancel</Button>
              <Button onClick={handleOnConfirmDelete}>Delete</Button>
            </div>
          }
          content={`Are you sure you want to delete product ${product.title}?`}
        />
        <div
          // className={styles.window}
          style={{
            height: 600,
            width: '100%',
          }}
        >
          <div
            style={{
              display: 'flex',
              paddingLeft: 20,
              height: 70,
            }}
          >
            <Typography variant="h2" style={{ color: 'black' }}>
              {product.title}
            </Typography>

            {/* <Button
              variant="contained"
              color="error"
              style={{ position: 'absolute', right: 40 }}
              onClick={handleOpenConfirmDeleteDialog}
            >
              Delete
            </Button> */}
          </div>
          <div style={{ display: 'flex', height: '100%', padding: 20 }}>
            <img
              src={product.images[0]}
              alt={product.title}
              style={{
                height: '100%',
                width: '75%',
                margin: '0px 10px 0px 0px',
                objectFit: 'cover',
              }}
            />
            {product.category === 'fund' || product.category === 'asset' ? (
              <Paper
                style={{
                  backgroundColor: '#e0faf4',
                  padding: 10,
                  width: '40%',
                  height: 250,
                }}
              >
                <div
                  style={{
                    marginLeft: 30,
                    height: '80%',
                  }}
                >
                  <div
                    style={{
                      fontSize: '20px',
                      display: 'flex',
                      alignItems: 'center',
                      height: 50,
                    }}
                  >
                    <MonetizationOnIcon />
                    <span style={{ marginLeft: 20 }}>Goal: {100}</span>
                  </div>
                  <div
                    style={{
                      fontSize: '20px',
                      display: 'flex',
                      alignItems: 'center',
                      height: 50,
                    }}
                  >
                    <PieChartIcon />
                    <span style={{ marginLeft: 20 }}>
                      Total raised: {10000}
                    </span>
                  </div>
                  <div
                    style={{
                      fontSize: '20px',
                      display: 'flex',
                      alignItems: 'center',
                      height: 50,
                    }}
                  >
                    <PeopleIcon />
                    <span style={{ marginLeft: 20 }}>Contributions: {11}</span>
                  </div>
                </div>

                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                  }}
                >
                  <Button
                    variant="contained"
                    onClick={handleOpenFundProjectDialog}
                  >
                    Back This Project
                  </Button>
                  {/* <FundProjectDialog
                projectAddress={product.address}
                open={isFundProjectDialogOpen}
                handleCloseFundProjectDialog={handleCloseFundProjectDialog}
                handleShowWalletToast={handleShowWalletToast}
              /> */}
                </div>
              </Paper>
            ) : null}
          </div>
        </div>
        <div
          // className={styles.paper}
          style={{
            padding: 20,
            display: 'flex',
            margin: 20,
            //   backgroundColor: '#e0faf4',
          }}
        >
          <section
            style={{
              marginTop: 10,
              height: 600,
              width: '75%',
            }}
          >
            <Typography style={{ color: 'black' }} variant="h3">
              {' '}
              Description
            </Typography>
            <Typography style={{ color: 'black' }} variant="body1">
              {product.description}
            </Typography>
          </section>
        </div>
      </div>
    </>
  )
}

export default Desktop
