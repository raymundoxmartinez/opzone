import React from 'react'
import { Button, Paper, Typography, Alert, Tabs, Tab, Box } from '@mui/material'

import ConfirmationDialog from '@components/ConfirmationDialog'

interface TabPanelProps {
  children?: React.ReactNode
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}
function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}
const Mobile = ({
  showWalletToast,
  product,
  isConfirmDeleteDialogOpen,
  handleCloseConfirmDeleteDialog,
  handleOnConfirmDelete,
  handleOpenConfirmDeleteDialog,
  handleOpenFundProjectDialog,
  handlehideWalletToast,
  router,
}: any) => {
  const [value, setValue] = React.useState(0)

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue)
  }
  if (!product) return <div>Loading...</div>
  return (
    <>
      {showWalletToast && (
        <Alert
          severity="error"
          onClose={() => {
            handlehideWalletToast()
          }}
          onClick={() => router.push(`/`)}
        >
          Click Get Started On The Home Page And Create A Wallet First.
        </Alert>
      )}
      <ConfirmationDialog
        open={isConfirmDeleteDialogOpen}
        title="Confirm Product Delete"
        actions={
          <div style={{ display: 'flex' }}>
            <Button onClick={handleCloseConfirmDeleteDialog}>Cancel</Button>
            <Button onClick={handleOnConfirmDelete}>Delete</Button>
          </div>
        }
        content={`Are you sure you want to delete product ${product.title}?`}
      />
      <img
        src={product.images[0]}
        alt={product.title}
        style={{
          height: 300,
          objectFit: 'cover',
          width: '100%',
        }}
      />
      <Paper
        style={{
          position: 'relative',
          height: '100vh',
        }}
      >
        <div
          style={{
            borderRadius: '50% 50% 0 0',
            position: 'absolute',
            height: 25,
            width: '100%',
            backgroundColor: 'white',
            top: -25,
          }}
        />
        <div
          style={{
            padding: 10,
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Typography variant="h6">{product.title}</Typography>
          <Typography variant="h6">{product.price || '$1000'}</Typography>
        </div>

        <Box sx={{ width: '100%' }}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
            >
              <Tab label="Description" {...a11yProps(0)} />
            </Tabs>
          </Box>
          <TabPanel value={value} index={0}>
            {product.description}
          </TabPanel>
        </Box>
      </Paper>
    </>
  )
}

export default Mobile
