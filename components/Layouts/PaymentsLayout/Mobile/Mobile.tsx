import React, { Fragment, useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import * as yup from 'yup'
import { useRouter } from 'next/router'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form'
import socket from '@src/socket'
import { toast } from '@components/Toast'
import {
  Button,
  Typography,
  Card,
  CardMedia,
  CardActionArea,
  Paper,
  IconButton,
  CircularProgress,
  Tooltip,
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { LoadingButton } from '@mui/lab'
import PayloadDialog from '@components/PayloadDialog'
import SmartContractSelectDialog from '@components/SmartContractSelectDialog'
import { FormInputText, FormAutocomplete } from '@components/form-components'
import OfflineBoltIcon from '@mui/icons-material/OfflineBolt'
import CreateWalletDialog from '@components/CreateWalletDialog'

import styles from './Mobile.module.scss'

const schema = yup.object({
  sender: yup.string().required(),
  amount: yup.number().positive().integer().required(),
  destination: yup.string().required(),
  memo: yup.string(),
  fromWalletID: yup.string(),
  toWalletID: yup.string(),
  receiverID: yup.string(),
})
const Mobile = ({
  xummUserAccount,
  xummUserToken,
  qrCode,
  balance,
  isTransactionLoading,
  isCredentialsLoading,
  payments__isPayloadDialogOpen,
  payments__isSmartContractSelectDialogOpen,
  payments__handleOpenCreateSmartContractSelectDialog,
  payments__handleCloseCreateSmartContractSelectDialog,
  payments__handleClosePayloadDialog,
  payments__handleOpenPayloadDialog,
  payments__handleOpenCreateWalletDialog,
  payments__handleCloseCreateWalletDialog,
  handleGetBalance,
  handlePostTransaction,
  handleSetQrCode,
  handleStoreXummCredentials,
  handleXummSignIn,
  handleGetTransaction,
  xrpl,
  handleCreateWallet,
  handleFetchWalletsByAddress,
}: any) => {
  const [expanded, setExpanded] = useState<string>('simplePay')
  const [address, setAddress] = useState('')
  const [fromWalletID, setFromWalletID] = useState('')

  const { isCreateWalletDialogOpen, wallets } = useSelector((state: any) => ({
    isCreateWalletDialogOpen: state.ui.payments.isCreateWalletDialogOpen,
    wallets: state.wallets.items,
  }))
  const { query } = useRouter()
  const {
    control: paymentFormControl,
    handleSubmit,
    formState: { errors: paymentFormErrors },
    reset: resetPaymentForm,
    ...formMethods
  } = useForm({
    defaultValues: {
      sender: xummUserAccount,
      amount: 0,
      destination: '',
      memo: '',
      fromWalletID,
      toWalletID: '',
      receiverID: '',
    },
    resolver: yupResolver(schema),
  })
  const handleConnectWallet = async () => {
    const response = await handleXummSignIn()
    if (response && response.next.always) {
      window.location.href = response.next.always
      socket.subscribe(response.refs.websocket_status)
      socket.on('connected', () => {
        socket.socket.onmessage = (message: Record<string, any>) => {
          console.log(message.data)
          const messageJson = JSON.parse(message.data)
          if (messageJson.signed) {
            toast.success({
              content: 'Xumm wallet connection was successful!',
              timeout: toast.defaultTimeout,
            })
            handleStoreXummCredentials(messageJson.payload_uuidv4)
          }
        }
      })
    }
  }
  const onDestinationChange = (vals: any) => {
    if (vals && vals.id) {
      formMethods.setValue('toWalletID', vals.id)
      formMethods.setValue('destination', vals.address)
      formMethods.setValue('receiverID', vals.userID)
    } else {
      formMethods.setValue('toWalletID', '')
      formMethods.setValue('destination', '')
      formMethods.setValue('receiverID', '')
    }
  }
  //TODO: move this to a usePayments hook
  const handleSendPayment = async ({
    amount,
    destination,
    memo,
    receiverID,
    toWalletID,
  }: {
    amount: number
    destination: string
    memo: string
    receiverID: string
    toWalletID: string
  }) => {
    const response = await handlePostTransaction({
      returnUrl: `${process.env.NEXT_PUBLIC_BASE_URL}/dashboard/payments?payload={id}`,
      xummUserToken,
      amount: xrpl!.xrpToDrops(amount),
      memo,
      destination,
      type: 'Payment',
      fromWalletID,
      receiverID,
      toWalletID,
    })
    if (response && response.next.always) {
      window.location.href = response.next.always
      socket.subscribe(response.refs.websocket_status)
      socket.on('connected', () => {
        socket.socket.onmessage = (message: Record<string, any>) => {
          console.log(message.data)
          const messageJson = JSON.parse(message.data)
          if (messageJson.signed) {
            toast.success({
              content: 'Transaction was successful!',
              timeout: toast.defaultTimeout,
            })
            resetPaymentForm()
            handleGetBalance()
          } else if (messageJson.signed === false) {
            toast.warning({
              content: 'Transaction was declined!',
              timeout: toast.defaultTimeout,
            })
            resetPaymentForm()
            handleGetBalance()
          }
        }
      })
    }
  }
  const handleChange =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      switch (panel) {
        case 'simplePay':
          if (isExpanded) {
            setExpanded(panel)
          } else {
            setExpanded('smartPay')
          }
          break
        case 'smartPay':
          if (isExpanded) {
            setExpanded(panel)
          } else {
            setExpanded('simplePay')
          }
          break
        default:
          setExpanded('simplePay')
          return
      }
    }

  useEffect(() => {
    const payloadId = query.payload
    if (payloadId) {
      toast.success({
        content: 'Xumm wallet connection was successful!',
        timeout: toast.defaultTimeout,
      })
      handleStoreXummCredentials(payloadId)
    }
  }, [handleStoreXummCredentials, query.payload])

  useEffect(() => {
    if (
      query.payload &&
      query.register === 'false' &&
      query.contract === 'GEO'
    ) {
      handleGetTransaction(query.payload)
        .then(async (response: any) => {
          const xummUserAccount = response.payload.response.account
          const res = await handleFetchWalletsByAddress({
            address: xummUserAccount,
          })
          setAddress(xummUserAccount)

          window.history.pushState(
            {},
            document.title,
            '/' + 'dashboard/payments'
          )
          if (res && !res.length) {
            payments__handleOpenCreateWalletDialog()
          } else {
            payments__handleOpenCreateSmartContractSelectDialog()
          }
        })
        .catch((error: any) => {
          window.history.pushState(
            {},
            document.title,
            '/' + 'dashboard/payments'
          )
        })
    }
  }, [])

  useEffect(() => {
    if (wallets && wallets.length && xummUserAccount) {
      const fromWalletID = wallets.reduce((acc: any, wallet: any) => {
        if (wallet.address === xummUserAccount) {
          acc = wallet.id
        }
        return acc
      }, '')
      if (fromWalletID) {
        setFromWalletID(fromWalletID)
      }
    }
  }, [wallets, xummUserAccount])

  return (
    <Fragment>
      <PayloadDialog
        qrCode={qrCode}
        handleClosePayloadDialog={payments__handleClosePayloadDialog}
        open={payments__isPayloadDialogOpen}
      />
      <CreateWalletDialog
        open={isCreateWalletDialogOpen}
        address={address}
        handleCloseCreateWalletDialog={payments__handleCloseCreateWalletDialog}
        handleCreateWallet={handleCreateWallet}
      />
      <SmartContractSelectDialog
        xummUserToken={xummUserToken}
        xummUserAccount={xummUserAccount}
        open={payments__isSmartContractSelectDialogOpen}
        handleCloseCreateSmartContractSelectDialog={
          payments__handleCloseCreateSmartContractSelectDialog
        }
      />
      <div className={styles['container']}>
        <Accordion
          classes={{
            root:
              expanded === 'simplePay'
                ? styles['accordion-expanded']
                : styles['accordion'],
          }}
          expanded={expanded === 'simplePay'}
          onChange={handleChange('simplePay')}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            Simple Pay
          </AccordionSummary>
          <AccordionDetails>
            <div className={styles['left-container']}>
              <div className={styles['left-container__payment-details']}>
                {xummUserAccount ? (
                  <Paper
                    elevation={4}
                    className={styles['account-details__container']}
                  >
                    {isTransactionLoading === 'pending' ? (
                      <CircularProgress
                        className={styles['loading-spinner']}
                        color="inherit"
                        size={35}
                      />
                    ) : (
                      <>
                        <Tooltip title="Change Wallet">
                          <IconButton
                            className={styles['connect-wallet-btn']}
                            aria-label="connect"
                            onClick={handleConnectWallet}
                          >
                            <OfflineBoltIcon fontSize="inherit" />
                          </IconButton>
                        </Tooltip>
                        <Typography component={'h1'} noWrap>
                          <strong>Balance: </strong>
                          {balance ? `${balance} XRP` : 'N/A'}
                        </Typography>
                        <Typography component={'h1'} noWrap>
                          <strong>Account: </strong> {xummUserAccount}
                        </Typography>
                      </>
                    )}
                  </Paper>
                ) : (
                  <>
                    <LoadingButton
                      className={styles['connect-wallet-loading-btn']}
                      aria-label="connect"
                      onClick={handleConnectWallet}
                      variant="contained"
                      loading={isCredentialsLoading === 'pending'}
                      loadingIndicator={
                        <CircularProgress
                          className={styles['loading-spinner']}
                          color="inherit"
                          size={35}
                        />
                      }
                      endIcon={
                        isCredentialsLoading === 'pending' ? null : (
                          <OfflineBoltIcon
                            className={styles['connect-icon']}
                            fontSize="inherit"
                          />
                        )
                      }
                    >
                      {isCredentialsLoading === 'pending'
                        ? ''
                        : 'Connect Wallet'}
                    </LoadingButton>
                    {paymentFormErrors?.sender ? (
                      <span style={{ color: 'red', textAlign: 'center' }}>
                        You must connect a wallet before continuing.
                      </span>
                    ) : null}
                  </>
                )}

                <div
                  className={
                    styles['left-container__payment-details__form-container']
                  }
                >
                  <form className={styles['payment-form']}>
                    <div className={styles['amount-container']}>
                      <FormInputText
                        label="XRP Amount"
                        name="amount"
                        control={paymentFormControl}
                        textFieldProps={{
                          variant: 'standard',
                          type: 'number',
                          placeholder: '0',
                          style: { margin: 15, width: '50%' },
                          inputProps: { style: { fontSize: 60 } },
                          error: paymentFormErrors?.amount ? true : false,
                          helperText: paymentFormErrors?.amount?.message,
                        }}
                      />
                    </div>
                    <FormAutocomplete
                      name="destination"
                      control={paymentFormControl}
                      onChange={onDestinationChange}
                      label="Send to"
                      textFieldProps={{
                        fullWidth: true,
                        error: paymentFormErrors?.destination ? true : false,
                        helperText: paymentFormErrors?.destination?.message,
                      }}
                    />
                    <FormInputText
                      name="memo"
                      label="Memo"
                      control={paymentFormControl}
                      textFieldProps={{
                        style: { margin: 15 },
                        variant: 'outlined',
                        type: 'string',
                        fullWidth: true,
                        multiline: true,
                        rows: 4,
                        error: paymentFormErrors?.memo ? true : false,
                        helperText: paymentFormErrors?.memo?.message,
                      }}
                    />
                    <div
                      className={
                        styles[
                          'left-container__payment-details__form-container__button-container'
                        ]
                      }
                    >
                      <Button
                        className={
                          styles[
                            'left-container__payment-details__form-container__button-container__button'
                          ]
                        }
                        autoFocus
                        onClick={handleSubmit(handleSendPayment)}
                        size="large"
                      >
                        Pay
                      </Button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </AccordionDetails>
        </Accordion>
        <Accordion
          classes={{
            root:
              expanded === 'smartPay'
                ? styles['accordion-expanded']
                : styles['accordion'],
          }}
          expanded={expanded === 'smartPay'}
          onChange={handleChange('smartPay')}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            Smart Pay
          </AccordionSummary>
          <AccordionDetails>
            <div className={styles['right-container']}>
              <div className={styles['right-container__smart-contract']}>
                <div
                  className={
                    styles['right-container__smart-contract__title-container']
                  }
                >
                  <Typography component={'h1'}>
                    Select Smart Contract
                  </Typography>
                </div>
                <div
                  className={
                    styles['right-container__smart-contract__card-container']
                  }
                >
                  <Card
                    sx={{ maxWidth: 345 }}
                    className={
                      styles[
                        'right-container__smart-contract__card-container__card'
                      ]
                    }
                  >
                    <CardActionArea
                      onClick={
                        payments__handleOpenCreateSmartContractSelectDialog
                      }
                    >
                      <CardMedia
                        component="img"
                        image="/images/contract.png"
                        alt="smart contract"
                      />
                    </CardActionArea>
                  </Card>
                </div>
              </div>
            </div>
          </AccordionDetails>
        </Accordion>
      </div>
    </Fragment>
  )
}
export default Mobile
