import React, { Fragment } from 'react'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form'
import {
  Button,
  Typography,
  Divider,
  Card,
  CardMedia,
  CardActionArea,
  Paper,
  IconButton,
  CircularProgress,
  Tooltip,
} from '@mui/material'
import socket from '@src/socket'
import { toast } from '@components/Toast'
import { LoadingButton } from '@mui/lab'
import PayloadDialog from '@components/PayloadDialog'
import SmartContractSelectDialog from '@components/SmartContractSelectDialog'
import { FormInputText, FormAutocomplete } from '@components/form-components'
import OfflineBoltIcon from '@mui/icons-material/OfflineBolt'

import styles from './Desktop.module.scss'
const schema = yup.object({
  amount: yup.number().positive().integer().required(),
  destination: yup.string().required(),
  memo: yup.string(),
})
const Desktop = ({
  xummUserAccount,
  xummUserToken,
  qrCode,
  balance,
  isTransactionLoading,
  isCredentialsLoading,
  payments__isPayloadDialogOpen,
  payments__isSmartContractSelectDialogOpen,
  payments__handleOpenCreateSmartContractSelectDialog,
  payments__handleCloseCreateSmartContractSelectDialog,
  payments__handleClosePayloadDialog,
  payments__handleOpenPayloadDialog,
  handleGetBalance,
  handlePostTransaction,
  handleConnectWallet,
  handleSetQrCode,
  xrpl,
}: any) => {
  const {
    control: paymentFormControl,
    handleSubmit: handlePaymentFormSubmit,
    formState: { errors: paymentFormErrors },
    reset: resetPaymentForm,
  } = useForm({
    defaultValues: {
      amount: 0,
      destination: '',
      memo: '',
    },
    resolver: yupResolver(schema),
  })

  // /TODO: move this to a usePayments hook
  const handleSendPayment = async ({
    amount,
    destination,
    memo,
  }: {
    amount: number
    destination: string
    memo: string
  }) => {
    const response = await handlePostTransaction({
      returnUrl: `${process.env.NEXT_PUBLIC_BASE_URL}/dashboard/payments?payload={id}`,
      xummUserToken,
      amount: xrpl!.xrpToDrops(amount),
      memo,
      destination,
      type: 'Payment',
    })
    if (response && response.refs.qr_png) {
      handleSetQrCode(response.refs.qr_png)
      socket.subscribe(response.refs.websocket_status)
      socket.on('connected', () => {
        socket.socket.onmessage = (message: Record<string, any>) => {
          console.log(message.data)
          const messageJson = JSON.parse(message.data)
          if (messageJson.signed) {
            toast.success({
              content: 'Transaction was successful!',
              timeout: toast.defaultTimeout,
            })
            payments__handleClosePayloadDialog()
            resetPaymentForm()
            handleGetBalance()
          } else if (messageJson.signed === false) {
            toast.warning({
              content: 'Transaction was declined!',
              timeout: toast.defaultTimeout,
            })
            payments__handleClosePayloadDialog()
            resetPaymentForm()
            handleGetBalance()
          }
        }
      })
      payments__handleOpenPayloadDialog()
    }
  }
  return (
    <Fragment>
      <PayloadDialog
        qrCode={qrCode}
        handleClosePayloadDialog={payments__handleClosePayloadDialog}
        open={payments__isPayloadDialogOpen}
      />
      <SmartContractSelectDialog
        xummUserAccount={xummUserAccount}
        open={payments__isSmartContractSelectDialogOpen}
        handleCloseCreateSmartContractSelectDialog={
          payments__handleCloseCreateSmartContractSelectDialog
        }
      />
      <div className={styles['container']}>
        <div className={styles['left-container']}>
          <div className={styles['left-container__payment-details']}>
            {xummUserAccount ? (
              <Paper
                elevation={4}
                className={styles['account-details__container']}
              >
                {isTransactionLoading === 'pending' ? (
                  <CircularProgress
                    className={styles['loading-spinner']}
                    color="inherit"
                    size={35}
                  />
                ) : (
                  <>
                    <Tooltip title="Change Wallet">
                      <IconButton
                        className={styles['connect-wallet-btn']}
                        aria-label="connect"
                        onClick={handleConnectWallet}
                      >
                        <OfflineBoltIcon fontSize="inherit" />
                      </IconButton>
                    </Tooltip>
                    <Typography component={'h1'}>
                      <strong>Balance: </strong>
                      {balance}
                    </Typography>
                    <Typography component={'h1'}>
                      <strong>Account: </strong> {xummUserAccount}
                    </Typography>
                  </>
                )}
              </Paper>
            ) : (
              <LoadingButton
                className={styles['connect-wallet-loading-btn']}
                aria-label="connect"
                onClick={handleConnectWallet}
                variant="contained"
                loading={isCredentialsLoading === 'pending'}
                loadingIndicator={
                  <CircularProgress
                    className={styles['loading-spinner']}
                    color="inherit"
                    size={35}
                  />
                }
                endIcon={
                  isCredentialsLoading === 'pending' ? null : (
                    <OfflineBoltIcon
                      className={styles['connect-icon']}
                      fontSize="inherit"
                    />
                  )
                }
              >
                {isCredentialsLoading === 'pending' ? '' : 'Connect Wallet'}
              </LoadingButton>
            )}
            <div
              className={
                styles['left-container__payment-details__title-container']
              }
            >
              <Typography component={'h1'}>Pay</Typography>
            </div>

            <div
              className={
                styles['left-container__payment-details__form-container']
              }
            >
              <form>
                <div className={styles['amount-container']}>
                  <FormInputText
                    label="XRP Amount"
                    name="amount"
                    control={paymentFormControl}
                    textFieldProps={{
                      variant: 'standard',
                      type: 'number',
                      placeholder: '0',
                      style: { margin: 15, width: '50%' },
                      inputProps: { style: { fontSize: 60 } },
                      error: paymentFormErrors?.amount ? true : false,
                      helperText: paymentFormErrors?.amount?.message,
                    }}
                  />
                </div>
                <FormAutocomplete
                  name="destination"
                  control={paymentFormControl}
                  label="Send to"
                  textFieldProps={{
                    fullWidth: true,
                    error: paymentFormErrors?.destination ? true : false,
                    helperText: paymentFormErrors?.destination?.message,
                  }}
                />
                <FormInputText
                  name="memo"
                  label="Memo"
                  control={paymentFormControl}
                  textFieldProps={{
                    style: { margin: 15 },
                    variant: 'outlined',
                    type: 'string',
                    fullWidth: true,
                    multiline: true,
                    rows: 4,
                    error: paymentFormErrors?.memo ? true : false,
                    helperText: paymentFormErrors?.memo?.message,
                  }}
                />
                <div
                  className={
                    styles[
                      'left-container__payment-details__form-container__button-container'
                    ]
                  }
                >
                  <Button
                    className={
                      styles[
                        'left-container__payment-details__form-container__button-container__button'
                      ]
                    }
                    autoFocus
                    onClick={
                      handlePaymentFormSubmit &&
                      handlePaymentFormSubmit(handleSendPayment)
                    }
                    size="large"
                  >
                    Pay
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <Divider orientation="vertical" flexItem>
          Or
        </Divider>
        <div className={styles['right-container']}>
          <div className={styles['right-container__smart-contract']}>
            <div
              className={
                styles['right-container__smart-contract__title-container']
              }
            >
              <Typography component={'h1'}>Use a Smart Contract</Typography>
            </div>
            <div
              className={
                styles['right-container__smart-contract__card-container']
              }
            >
              <Card
                sx={{ maxWidth: 345 }}
                className={
                  styles[
                    'right-container__smart-contract__card-container__card'
                  ]
                }
              >
                <CardActionArea
                  onClick={payments__handleOpenCreateSmartContractSelectDialog}
                >
                  <CardMedia
                    component="img"
                    height="200"
                    image="/images/contract.png"
                    alt="smart contract"
                  />
                </CardActionArea>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}
//@ts-ignore

export default Desktop
