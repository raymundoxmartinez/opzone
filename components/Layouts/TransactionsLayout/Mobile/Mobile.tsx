import React from 'react'
import moment from 'moment'
import useLocation, { useRouter } from 'next/router'

import {
  Paper,
  Typography,
  Tabs,
  Tab,
  Box,
  Button,
  CircularProgress,
  Backdrop,
} from '@mui/material'
import VerifiedIcon from '@mui/icons-material/Verified'
import ConditionCard from '@components/ConditionCard'

interface TabPanelProps {
  children?: React.ReactNode
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  )
}
function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}
const Mobile = ({
  transaction,
  handleVerifyCondition,
  handleClaimTransaction,
  xrpl,
  isVerified,
  isVerifying,
  user,
  conditionProgresses,
  onCreateConditionProgress,
  onUpdateConditionProgress,
}: any) => {
  const [value, setValue] = React.useState(1)
  const router = useRouter()
  const { inProgress } = router.query
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue)
  }
  if (!transaction)
    return (
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100vh',
        }}
      >
        <CircularProgress />
      </Box>
    )
  const breadcrumbsPaths = [
    { label: 'Home', url: '/' },
    { label: 'Products', url: '/products' },
    { label: 'Category', url: '/products/category' },
    { label: 'Product Details', url: '/products/category/product-details' },
  ]
  return (
    <>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isVerifying}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <img
        src={transaction?.mapUrl || ''}
        alt={'Title'}
        style={{
          height: 300,
          objectFit: 'cover',
          width: '100%',
        }}
      />
      <>
        <div
          style={{
            borderRadius: '50% 50% 0 0',
            position: 'absolute',
            height: 25,
            width: '100%',
            backgroundColor: 'white',
            top: -25,
          }}
        />
        <div
          style={{
            padding: 10,
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Typography variant="h6">{transaction?.type}</Typography>
          {/* <Typography variant="h6">
            {xrpl &&
              transaction?.amount &&
              xrpl!.dropsToXrp(transaction?.amount)}{' '}
            XRP
          </Typography> */}
        </div>

        {/* <div style={{ padding: 10 }}>
          <Typography variant="subtitle1">{`Expires: ${moment(
            transaction?.expiration
          ).format('MMM Do YY h:mm A')}`}</Typography>
          <Typography variant="subtitle1">{`From: ${
            transaction?.sender?.username || 'N/A'
          }`}</Typography>

          <Typography variant="subtitle1">{`To: ${transaction?.toWallet?.title}-${transaction?.toWallet?.address}`}</Typography>
        </div> */}
        {inProgress && (
          <div
            style={{
              padding: '0px 10px 10px 10px',
              display: 'flex',
            }}
          >
            {/* <Button
              size="small"
              style={{
                display: 'flex',
                flexDirection: 'column',
                border: '1px solid green',
                margin: 5,
              }}
              onClick={handleVerifyCondition}
            >
              <VerifiedIcon fontSize="inherit" />
              <Typography variant="h6">Verify</Typography>
            </Button> */}
            <Button
              size="small"
              style={{
                display: 'flex',
                flexDirection: 'column',
                border: '1px solid green',
                margin: 5,
              }}
              disabled={!isVerified}
              onClick={handleClaimTransaction}
            >
              <VerifiedIcon fontSize="inherit" />
              <Typography variant="h6">Claim</Typography>
            </Button>
          </div>
        )}

        <Box sx={{ borderBottom: 1, borderColor: 'divider', marginBottom: 1 }}>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            <Tab label="Description" {...a11yProps(0)} />
            <Tab label="Conditions" {...a11yProps(0)} />
          </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
          {transaction?.description}
        </TabPanel>
        <TabPanel value={value} index={1}>
          {transaction.conditions?.items.map((condition: any, index: any) => (
            <ConditionCard
              key={condition.id}
              condition={condition}
              inProgress={inProgress}
              user={user}
              conditionProgresses={conditionProgresses}
              onCreateConditionProgress={onCreateConditionProgress}
              onUpdateConditionProgress={onUpdateConditionProgress}
              onVerifyCondition={handleVerifyCondition}
            />
          ))}
        </TabPanel>
      </>
    </>
  )
}

export default Mobile
