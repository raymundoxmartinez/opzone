import React from 'react'
import { useRouter } from 'next/router'
import { Chip } from '@mui/material'
import CreateProductDialog from '@components/ProductDialog'
import ProductCard from '@components/ProductCard'
import styles from './Mobile.module.scss'

//@ts-ignore
const Mobile = ({
  isCreateNFTDialogOpen,
  handleCloseCreateNFTDialog,
  handleCreateProduct,
  nfts,
  handleOpenCreateNFTDialog,
}: any) => {
  const router = useRouter()

  return (
    <>
      <CreateProductDialog
        open={isCreateNFTDialogOpen}
        handleCloseCreateProductDialog={handleCloseCreateNFTDialog}
        handleCreateProduct={handleCreateProduct}
        initialCategory={'nft'}
      />
      <div className={styles['product-container']}>
        <Chip
          clickable
          onClick={() => handleOpenCreateNFTDialog(true)}
          label="Add NFT"
          className={styles.chip}
        />
        {nfts &&
          nfts.map((nft: any, index: any) => (
            <ProductCard
              key={index}
              product={nft}
              onClick={(e: any) => {
                e.preventDefault()
                router.push(`/dashboard/nfts/${nft.id}`)
              }}
            />
          ))}
      </div>
    </>
  )
}

export default Mobile
