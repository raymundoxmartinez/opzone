import React, { useState, useEffect, useRef } from 'react'
import styles from './DirectionsMap.module.scss'
//@ts-ignore
import mapboxgl from '!mapbox-gl'
import { toast } from '@components/Toast'
import classNames from 'classnames'

// eslint-disable-line import/no-webpack-loader-syntax
mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCCESS_TOKEN
const DirectionsMap = ({
  locations,
  size = 'small',
  loading = false,
}: {
  locations: any[]
  size?: 'small' | 'large'
  loading?: boolean
}) => {
  console.log('DirectionMap locations:', locations)
  const mapContainer = useRef(null)
  const map = useRef(null)
  const [lng, setLng] = useState(null)
  const [lat, setLat] = useState(null)
  const [status, setStatus] = useState<null | string>(null)

  const getLocation = () => {
    if (locations.length) {
      setLng(locations[0].coordinates[0])
      setLat(locations[0].coordinates[1])
      return
    }
    if (!navigator.geolocation) {
      setStatus('Geolocation is not supported by your browser')
    } else {
      setStatus('Locating...')
      if (navigator.permissions && navigator.permissions.query) {
        navigator.permissions
          .query({ name: 'geolocation' })
          .then(function (result) {
            if (result.state === 'granted' || result.state === 'prompt') {
              console.log(result.state)
              //If granted then you can directly call your function here
              navigator.geolocation.getCurrentPosition(
                (position) => {
                  setStatus(null)

                  //@ts-ignore
                  setLat(position.coords.latitude)
                  //@ts-ignore
                  setLng(position.coords.longitude)
                },
                () => {
                  setStatus('Unable to retrieve your location')
                },
                { maximumAge: 60000, timeout: 5000, enableHighAccuracy: true }
              )
            } else if (result.state === 'denied') {
              //If denied then you have to show instructions to enable location
              toast.error({
                content:
                  'Unable to access your location. Please make sure location tracking is enabled.',
                timeout: toast.defaultTimeout,
              })
            }
            result.onchange = function () {
              console.log(result.state)
            }
          })
      } else {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            setStatus(null)

            //@ts-ignore
            setLat(position.coords.latitude)
            //@ts-ignore
            setLng(position.coords.longitude)
          },
          () => {
            setStatus('Unable to retrieve your location')
            toast.error({
              content:
                'Unable to access your location. Please make sure location tracking is enabled.',
              timeout: toast.defaultTimeout,
            })
          },
          { maximumAge: 60000, timeout: 5000, enableHighAccuracy: true }
        )
      }
    }
  }

  // create a function to make a directions request
  const getRoute = async () => {
    // make a directions request using cycling profile
    // an arbitrary start will always be the same
    // only the end or destination will change
    let locationsCoordinates = `${lng},${lat};${lng},${lat}`

    if (locations.length) {
      if (locations.length === 1) {
        const coordinates = locations[0].coordinates
        locationsCoordinates = `${coordinates[0]},${coordinates[1]};${coordinates[0]},${coordinates[1]}`
      } else {
        locationsCoordinates = locations.reduce(
          (acc, { coordinates }, index) => {
            acc += coordinates.join()
            if (index !== locations.length - 1) {
              acc += ';'
            }
            return acc
          },
          ''
        )
      }
    }

    const query = await fetch(
      `https://api.mapbox.com/directions/v5/mapbox/driving/${locationsCoordinates}?steps=true&geometries=geojson&access_token=${mapboxgl.accessToken}`,
      { method: 'GET' }
    )
    const json = await query.json()
    const data = json.routes[0]
    const route = data.geometry.coordinates
    const geojson = {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'LineString',
        coordinates: route,
      },
    }
    // if the route already exists on the map, we'll reset it using setData
    //@ts-ignore
    if (map.current.getSource('route')) {
      //@ts-ignore
      map.current.getSource('route').setData(geojson)
    }
    // otherwise, we'll make a new request
    else {
      //@ts-ignore
      map.current.addLayer({
        id: 'route',
        type: 'line',
        source: {
          type: 'geojson',
          data: geojson,
        },
        layout: {
          'line-join': 'round',
          'line-cap': 'round',
        },
        paint: {
          'line-color': '#3887be',
          'line-width': 5,
          'line-opacity': 0.75,
        },
      })
    }

    if (locations.length) {
      const coordinates = locations[0].coordinates
      //@ts-ignore
      map.current.flyTo({
        center: coordinates,
      })
    } else {
      //@ts-ignore
      map.current.flyTo({
        center: [lng, lat],
      })
    }
  }

  const loadMap = () => {
    if (lat && lng) {
      map.current = new mapboxgl.Map({
        //@ts-ignore
        container: mapContainer.current,
        style: 'mapbox://styles/mapbox/streets-v12',
        center: [lng, lat],
        zoom: 12,
      })

      //@ts-ignore
      map.current.on('load', function () {
        // Add starting point to the map
        if (locations.length) {
          getRoute()
          locations.forEach(({ coordinates }, index) => {
            if (index !== locations.length - 1) {
              // create the popup
              const popup = new mapboxgl.Popup({ offset: 25 }).setText(
                `${locations[index].name}`
              )

              // create DOM element for the marker
              const el = document.createElement('div')
              el.id = `marker_${index}`

              // create the marker
              new mapboxgl.Marker()
                .setLngLat(coordinates)
                .setPopup(popup) // sets a popup on this marker
                .addTo(map.current)
            } else {
              // create the popup
              const popup = new mapboxgl.Popup({ offset: 25 }).setText(
                `${locations[index].name}`
              )

              // create DOM element for the marker
              const el = document.createElement('div')
              el.id = `marker_${index}`

              // create the marker
              new mapboxgl.Marker()
                .setLngLat(coordinates)
                .setPopup(popup) // sets a popup on this marker
                .addTo(map.current)
            }
          })
        } else {
          getRoute()

          // create the popup
          const popup = new mapboxgl.Popup({ offset: 25 }).setText(
            'Construction on the Washington Monument began in 1848.'
          )

          // create DOM element for the marker
          const el = document.createElement('div')
          el.id = `marker`

          // create the marker
          new mapboxgl.Marker()
            .setLngLat([lng, lat])
            .setPopup(popup) // sets a popup on this marker
            .addTo(map.current)
        }
      })
    }
  }

  useEffect(() => {
    getLocation()
  }, [])

  useEffect(() => {
    if (!loading) {
      loadMap()
    }
  })

  const mapSizeCN = classNames({ [styles[`map-container-${size}`]]: true })
  return (
    <div>
      <div ref={mapContainer} className={mapSizeCN} />
    </div>
  )
}

export default DirectionsMap
