import React from 'react'

export interface FileUploadProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  label?: string
  src?: string
  name: string
  avatar?: boolean
}
