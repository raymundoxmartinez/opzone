import React, { useCallback, useEffect } from 'react'
import { useFormContext } from 'react-hook-form'
import { useDropzone, DropzoneOptions } from 'react-dropzone'
import { Paper, Avatar, Button } from '@mui/material'

import { FileUploadProps } from './types'
import styles from './FileUpload.module.scss'

const FileUpload = (props: FileUploadProps) => {
  const { name, label = name, avatar, src } = props
  const { register, unregister, setValue, watch } = useFormContext()
  const files: File[] = watch(name)
  //@ts-ignore
  const onDrop = useCallback<DropzoneOptions['onDrop']>(
    (droppedFiles) => {
      setValue(name, droppedFiles, { shouldValidate: true })
    },
    [setValue, name]
  )
  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: props.accept,
  })

  useEffect(() => {
    if (src) {
      setValue(name, [src], { shouldValidate: true })
    }
  }, [src, name, setValue])

  useEffect(() => {
    register(name)
  }, [register, name])
  const { ref, ...rootProps } = getRootProps()

  const avatarComponent = () => {
    return (
      <>
        <input {...props} id={name} {...getInputProps()} />
        <div {...rootProps}>
          {typeof files !== 'string' && files?.length ? (
            files.map((file) => {
              return (
                <Avatar
                  style={{ width: 152, height: 152 }}
                  src={
                    typeof file === 'string' ? file : URL.createObjectURL(file)
                  }
                  alt={file.name}
                  key={file.name}
                />
              )
            })
          ) : (
            //TODO: change to be able to accept string[]
            <Avatar
              style={{ width: 152, height: 152 }}
              // @ts-ignore
              src={files}
              alt="default"
            />
          )}
        </div>
      </>
    )
  }

  const defaultComponent = () => {
    return (
      <Paper className={styles.paper} {...rootProps}>
        <input {...props} id={name} {...getInputProps()} />
        {files?.length ? (
          <div className={styles.imagesContainer}>
            {files.map((file) => {
              return (
                <div className={styles.imageContainer} key={file.name}>
                  <img
                    src={URL.createObjectURL(file)}
                    alt={file.name}
                    style={{ width: 100, height: 100 }}
                  />
                </div>
              )
            })}
          </div>
        ) : (
          <p>Drag n drop an image/video here, or click to select files</p>
        )}
      </Paper>
    )
  }
  return (
    <Button ref={ref} className={styles.fileButton}>
      {avatar ? avatarComponent() : defaultComponent()}
    </Button>
  )
}

export default FileUpload
