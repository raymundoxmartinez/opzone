import * as React from 'react'
import moment from 'moment'
import { useRouter } from 'next/router'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardMedia from '@mui/material/CardMedia'
import CardActions from '@mui/material/CardActions'
import CardActionArea from '@mui/material/CardActionArea'
import IconButton from '@mui/material/IconButton'
import FavoriteIcon from '@mui/icons-material/Favorite'
import classNames from 'classnames'

import styles from './AssetCard.module.scss'

export default function AssetCard({
  asset,
  onClick,
  selected,
}: {
  asset: any
  onClick: any
  selected: boolean
}) {
  const router = useRouter()
  const handleOnClick = (e) => {
    onClick(asset.id)
  }
  const cardCN = classNames({
    [styles.selected]: selected,
    [styles.main]: true,
  })
  return (
    <Card className={cardCN}>
      <CardHeader
        title={asset.title}
        subheader={moment(asset.createdAt).format('MMM Do YY')}
      />
      <CardActionArea onClick={handleOnClick}>
        <CardMedia
          component="img"
          height="194"
          image={asset?.images?.[0] || asset?.imageUrl}
          alt={asset.title}
        />
      </CardActionArea>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
      </CardActions>
    </Card>
  )
}
