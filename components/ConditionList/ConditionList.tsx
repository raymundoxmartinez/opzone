import React, { useState, useEffect } from 'react'
import { Button, Fab } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import ConditionCard from '@components/ConditionCard'

const ConditionList = ({ conditions, onConditionsChange }) => {
  const [conditionFields, setConditionFields] = useState<any>([])
  const [conditionFieldsMap, setConditionFieldsMap] = useState({})
  const [selectNewConditionField, setSelectNewConditionField] = useState(false)

  const handleOnConditionChange = ({
    conditionIndex,
    value,
    conditionType,
    isNewField = false,
  }: {
    conditionIndex?: number
    value?: any
    conditionType?: string
    isNewField?: boolean
  }) => {
    const updatedConditionFields: { [key: number]: any } = {
      ...conditionFieldsMap,
    }

    if (value && conditionIndex) {
      updatedConditionFields[conditionIndex] = value
    } else if (isNewField) {
      const currentConditionFields = [...conditionFields]
      const newField = currentConditionFields.pop()
      newField.type = conditionType

      //removes temp status of field
      if ('TEMP' in newField) delete newField.TEMP
      updatedConditionFields[conditionFields.length - 1] = newField
      setSelectNewConditionField(false)
    } else if (conditionIndex) {
      const indexOfItemToBeDeleted = conditionIndex
      if (indexOfItemToBeDeleted === 0 && conditionFields[1]) {
        //remove the operator of the filter next in line if there is one
        conditionFields[1].operator = ''
      }
      delete updatedConditionFields[conditionIndex]
    }
    onConditionsChange(Object.values(updatedConditionFields))
  }
  const handleOnAddConditionClick = (e) => {
    //handling addition of new filter by setting a
    //temporary object.
    const updatedCompoundFilters = [...conditionFields, { TEMP: {} }]
    setConditionFields(updatedCompoundFilters)
    setSelectNewConditionField(false)
  }

  useEffect(() => {
    if (conditions) {
      const conditionFieldsMap = conditions.reduce(
        (acc: any, condition: any, index: any) => {
          acc[index] = condition
          return acc
        },
        {}
      )
      setConditionFieldsMap(conditionFieldsMap)

      if (Object.values(conditionFieldsMap).length) {
        setConditionFields(Object.values(conditionFieldsMap))

        const hasTempVal = Object.values(conditionFieldsMap).find(
          (item: any) => {
            return 'TEMP' in item
          }
        )
        if (hasTempVal) {
          setSelectNewConditionField(false)
        } else {
          setSelectNewConditionField(true)
        }
      } else {
        //sets temp object used to display empty state
        setConditionFields([{ TEMP: {} }])
        setSelectNewConditionField(false)
      }
    }
  }, [conditions])
  return (
    <>
      {conditionFields.length
        ? conditionFields.map((condition: any, index: any) => (
            <ConditionCard
              key={index}
              condition={condition}
              conditionIndex={index}
              onConditionChange={handleOnConditionChange}
            />
          ))
        : null}

      {selectNewConditionField && (
        <div className="operator_container">
          <Fab color="primary" onClick={handleOnAddConditionClick}>
            <AddIcon />
          </Fab>
        </div>
      )}
    </>
  )
}
export default ConditionList
