import DraggableListContainer from './DraggableListContainer'
import LocationCard from '../LocationCard'
import { ComponentMeta } from '@storybook/react'
import * as LocationStories from '../LocationCard/LocationCard.stories'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'

export default {
  component: DraggableListContainer,
  title: 'DraggableListContainer',
  decorators: [
    (story) => <DndProvider backend={HTML5Backend}>{story()}</DndProvider>,
  ],
  tags: ['autodocs'],
} as ComponentMeta<typeof DraggableListContainer>

export const Default = {
  args: {
    // Shaping the stories through args composition.
    // The data was inherited from the Default story in Task.stories.jsx.
    cardRenderer: LocationCard,
    items: [
      { ...LocationStories.Default.args.item, id: '1', name: 'Location 1' },
      { ...LocationStories.Default.args.item, id: '2', name: 'Location 2' },
      { ...LocationStories.Default.args.item, id: '3', name: 'Location 3' },
      { ...LocationStories.Default.args.item, id: '4', name: 'Location 4' },
      { ...LocationStories.Default.args.item, id: '5', name: 'Location 5' },
      { ...LocationStories.Default.args.item, id: '6', name: 'Location 6' },
    ],
  },
}
