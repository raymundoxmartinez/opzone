import { useCallback, useState, useEffect } from 'react'
import Box from '@mui/material/Box'
import update from 'immutability-helper'
import type { FC } from 'react'

const DraggableListContainer: FC = ({
  cardRenderer,
  items,
  itemProps,
}: any) => {
  const [cards, setCards] = useState<any[]>([])

  const moveCard = useCallback((dragIndex: number, hoverIndex: number) => {
    setCards((prevCards: any[]) =>
      update(prevCards, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, prevCards[dragIndex] as any],
        ],
      })
    )
  }, [])

  const renderCard = useCallback((card: any, index: number) => {
    const Card = cardRenderer
    return (
      <Card
        key={index}
        index={index}
        id={index}
        item={card}
        moveCard={moveCard}
        {...itemProps}
      />
    )
  }, [])

  useEffect(() => {
    setCards(items)
  }, [items])

  return (
    <>
      {cards &&
        Array.isArray(cards) &&
        cards.map((card, i) => renderCard(card, i))}
    </>
  )
}

export default DraggableListContainer
