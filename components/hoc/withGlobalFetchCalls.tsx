import React from 'react'
import { Amplify } from 'aws-amplify'
import { connect } from 'react-redux'

import awsconfig from '@src/aws-exports'
import { fetchWalletsByUserID } from '@redux/wallets/walletsSlice'
import { fetchCurrentUser } from '@redux/app/appSlice'
import {
  fetchTransactionsByReceiverID,
  fetchTransactionsBySenderID,
} from '@redux/transactions/transactionsSlice'
import { AuthState, onAuthUIStateChange } from '@aws-amplify/ui-components'
import { toast } from '@components/Toast'

Amplify.configure(awsconfig)

// This function takes a component...
function getDisplayName(WrappedComponent: any) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component'
}

// This function takes a component...
function withGlobalFetchCalls(WrappedComponent: any) {
  const mapStateToProps = (state: any, props: any) => {
    return {
      user: state.app.user.items,
      isLoggedIn: state.app.isLoggedIn,
    }
  }
  const mapDispatchToProps = (dispatch: any, props: any) => {
    return {
      handleFetchWalletsByUserID: async (request: Record<string, any>) => {
        const resultAction = await dispatch(fetchWalletsByUserID(request))
        const wallets = resultAction.payload
        if (fetchWalletsByUserID.fulfilled.match(resultAction)) {
          return wallets
        } else {
          if (resultAction.payload) {
            toast.error({
              content: `Get failed: ${resultAction.payload.errorMessage}`,
              timeout: toast.defaultTimeout,
            })
          } else {
            toast.error({
              content: `Get failed: ${resultAction.error.message}`,
              timeout: toast.defaultTimeout,
            })
          }
        }
      },
      handleFetchTransactionsByReceiverID: async (
        request: Record<string, any>
      ) => {
        const resultAction = await dispatch(
          fetchTransactionsByReceiverID(request)
        )
        const transactions = resultAction.payload
        if (fetchTransactionsByReceiverID.fulfilled.match(resultAction)) {
          return transactions
        } else {
          if (resultAction.payload) {
            toast.error({
              content: `Get failed: ${resultAction.payload.errorMessage}`,
              timeout: toast.defaultTimeout,
            })
          } else {
            toast.error({
              content: `Get failed: ${resultAction.error.message}`,
              timeout: toast.defaultTimeout,
            })
          }
        }
      },
      handleFetchTransactionsBySenderID: async (
        request: Record<string, any>
      ) => {
        const resultAction = await dispatch(
          fetchTransactionsBySenderID(request)
        )
        const transactions = resultAction.payload
        if (fetchTransactionsBySenderID.fulfilled.match(resultAction)) {
          return transactions
        } else {
          if (resultAction.payload) {
            toast.error({
              content: `Get failed: ${resultAction.payload.errorMessage}`,
              timeout: toast.defaultTimeout,
            })
          } else {
            toast.error({
              content: `Get failed: ${resultAction.error.message}`,
              timeout: toast.defaultTimeout,
            })
          }
        }
      },
      handleFetchCurrentUser: async () => {
        const resultAction = await dispatch(fetchCurrentUser())
        const user = resultAction.payload
        if (fetchCurrentUser.fulfilled.match(resultAction)) {
          return user
        } else {
          if (resultAction.payload) {
            toast.error({
              content: `Get failed: ${resultAction.payload.errorMessage}`,
              timeout: toast.defaultTimeout,
            })
          } else {
            toast.error({
              content: `Get failed: ${resultAction.error.message}`,
              timeout: toast.defaultTimeout,
            })
          }
        }
      },
    }
  }

  class withGlobalFetchCalls extends React.Component {
    _isMounted = false
    constructor(props: any) {
      super(props)
      this.state = {
        isMounted: false,
      }
    }
    componentDidMount() {
      //@ts-ignore
      const { handleFetchCurrentUser, user } = this.props
      this._isMounted = true

      //used to handle case where user navigates by
      //editing the browser url, which doesn't trigger the
      //onAuthStateUIStaetChange function below
      if (!user) {
        handleFetchCurrentUser()
      }
      //in other when user reloads the page this function
      //is triggered
      onAuthUIStateChange((nextAuthState, authData) => {
        if (this._isMounted) {
          if (nextAuthState === AuthState.SignedIn && !user) {
            handleFetchCurrentUser()
          }
        }
      })
    }

    componentWillUnmount() {
      this._isMounted = false
    }

    componentDidUpdate(
      prevProps: Readonly<{ user: any }>,
      prevState: Readonly<{}>,
      snapshot?: any
    ): void {
      const {
        //@ts-ignore
        handleFetchWalletsByUserID,
        //@ts-ignore
        handleFetchTransactionsByReceiverID,
        //@ts-ignore
        user,
      } = this.props

      if (
        (user && user.id && prevProps.user && prevProps.user.id !== user.id) ||
        (user && user.id && !prevProps.user)
      ) {
        // handleFetchWalletsByUserID({ userID: user.id })
        // handleFetchTransactionsByReceiverID({ receiverID: user.id })
      }
    }
    // Use the layout defined at the page level, if available
    //@ts-ignore
    getLayout = WrappedComponent.getLayout || ((page: any) => page)

    render() {
      return (
        <>
          {this.getLayout(
            <WrappedComponent {...this.props} {...this.state} />,
            {
              //@ts-ignore
              user: this.props.user,
              //@ts-ignore
              isLoggedIn: this.props.isLoggedIn,
            }
          )}
        </>
      )
    }
  }
  //@ts-ignore
  withGlobalFetchCalls.displayName = `WithSubscription(${getDisplayName(
    WrappedComponent
  )})`
  return connect(mapStateToProps, mapDispatchToProps)(withGlobalFetchCalls)
}

export default withGlobalFetchCalls
