import React from 'react'
import { useRef } from 'react'
import { useDrag, useDrop } from 'react-dnd'
import type { Identifier, XYCoord } from 'dnd-core'
import Avatar from '@mui/material/Avatar'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import { red, purple } from '@mui/material/colors'
import PlaceIcon from '@mui/icons-material/Place'

import styles from './LocationCard.module.scss'

interface DragItem {
  index: number
  id: string
  type: string
}
const LocationCard = ({
  item: { name, address, description },
  index,
  moveCard,
  id,
}) => {
  const ref = useRef<HTMLDivElement>(null)
  const [{ handlerId }, drop] = useDrop<
    DragItem,
    void,
    { handlerId: Identifier | null }
  >({
    accept: 'card',
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      }
    },
    hover(item: DragItem, monitor) {
      if (!ref.current) {
        return
      }
      const dragIndex = item.index
      const hoverIndex = index

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }

      // Determine rectangle on screen
      const hoverBoundingRect = ref.current?.getBoundingClientRect()

      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

      // Determine mouse position
      const clientOffset = monitor.getClientOffset()

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%

      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }

      // Time to actually perform the action
      moveCard(dragIndex, hoverIndex)

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex
    },
  })

  const [{ isDragging }, drag] = useDrag({
    type: 'card',
    item: () => {
      return { id, index }
    },
    collect: (monitor: any) => ({
      isDragging: monitor.isDragging(),
    }),
  })

  const opacity = isDragging ? 0 : 1
  drag(drop(ref))
  return (
    <Card
      ref={ref}
      className={styles.main}
      style={{ display: 'flex', alignItems: 'center', opacity }}
    >
      <Avatar
        sx={{
          width: 40,
          height: 40,
          bgcolor: red[500],
          margin: '10px',
        }}
        aria-label="recipe"
      >
        <PlaceIcon />
      </Avatar>

      <CardContent className={styles.content}>
        <Typography noWrap variant="h6">
          {name}
        </Typography>
        <Typography noWrap gutterBottom variant="subtitle1">
          {description}
        </Typography>
        <div style={{ display: 'flex' }}>
          <Typography variant="subtitle1" noWrap>
            {address}
          </Typography>
        </div>
      </CardContent>
    </Card>
  )
}
export default LocationCard
