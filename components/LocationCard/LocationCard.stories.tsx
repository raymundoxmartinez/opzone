import { ComponentMeta } from '@storybook/react'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import LocationCard from './LocationCard'

export default {
  component: LocationCard,
  title: 'LocationCard',
  decorators: [
    (story) => <DndProvider backend={HTML5Backend}>{story()}</DndProvider>,
  ],
  tags: ['autodocs'],
} as ComponentMeta<typeof LocationCard>

export const Default = {
  args: {
    item: {
      id: '1',
      name: 'Jurassic Park location',
      description:
        'Jurassic Park fdf fsd  fsd fds  fds  fds , f, sa,,asd d,dsa dsa,dsad ,dasd',
      address: 'Los Angeles',
    },
    disabled: false,
  },
}

export const Disabled = {
  args: {
    item: {
      ...Default.args.item,
    },
    disabled: true,
  },
}
