const eventManager = {
  list: new Map(),

  //@ts-ignore
  on(event, callback) {
    this.list.has(event) || this.list.set(event, [])
    this.list.get(event).push(callback)
    return this
  },
  //@ts-ignore
  off(event) {
    this.list.delete(event)
    return this
  },
  //@ts-ignore
  emit(event, ...args) {
    this.list.has(event) &&
      //@ts-ignore
      this.list.get(event).forEach((callback) =>
        setTimeout(() => {
          callback(...args)
        }, 0)
      )
  },
}

export default eventManager
