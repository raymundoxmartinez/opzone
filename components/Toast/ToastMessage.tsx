import React from 'react'
import { Alert, AlertTitle } from '@mui/material'
import { TOAST_COLOR, TOAST_TYPE } from './constants'

interface TOAST_PROP_TYPES {
  toastId: string | number
  header: typeof React.Component
  content: typeof React.Component
  icon: typeof React.Component | string
  type: typeof TOAST_TYPE
  color: TOAST_COLOR
  timeout: number
  dismissable: boolean
}

interface TOAST_MESSAGE_PROP_TYPES {
  toast: TOAST_PROP_TYPES
  removeToast: (props: any) => any
}

class ToastMessage extends React.PureComponent<TOAST_MESSAGE_PROP_TYPES> {
  public timeoutTimer: ReturnType<typeof setTimeout> | undefined
  //@ts-ignore
  constructor(props) {
    super(props)
    this.dismissToast = this.dismissToast.bind(this)
  }
  componentDidMount() {
    const {
      toast: { timeout },
    } = this.props
    if (timeout) {
      this.timeoutTimer = setTimeout(this.dismissToast, timeout)
    }
  }

  componentWillUnmount() {
    this.clearTimer()
  }

  clearTimer = () => {
    if (this.timeoutTimer) {
      clearTimeout(this.timeoutTimer)
      delete this.timeoutTimer
    }
  }

  public dismissToast = () => {
    const { toast, removeToast } = this.props
    removeToast(toast.toastId)
    this.clearTimer()
  }

  public renderMessageContent(toast: TOAST_PROP_TYPES) {
    return toast.header ? (
      <>
        <AlertTitle>{toast.header}</AlertTitle>
        {toast.content}
      </>
    ) : (
      <>{toast.content}</>
    )
  }

  render() {
    const { toast } = this.props
    const messageProps: {
      color: TOAST_COLOR
      onClose: (() => any) | undefined
    } = {
      color: toast.color,
      onClose: toast.dismissable ? this.dismissToast : undefined,
    }
    if (!!toast.type) {
      //@ts-ignore
      messageProps['severity'] = toast.type.toLowerCase()
    }
    return (
      <Alert style={{ width: '100%' }} {...messageProps}>
        {this.renderMessageContent(toast)}
      </Alert>
    )
  }
}
export default ToastMessage
