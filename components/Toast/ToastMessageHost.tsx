import React from 'react'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import Box from '@mui/material/Box'
import { Transition } from 'react-transition-group'
import ToastMessage from './ToastMessage'
import eventManager from './eventManager'
import update from 'immutability-helper'
import { ACTION } from './constants'

import styles from './ToastMessageHost.module.scss'

const TOAST_ANIMATION_DURATION_MS = 300

class ToastMessageHost extends React.PureComponent {
  //@ts-ignore
  constructor(props) {
    super(props)
    this.state = {
      toastIds: [],
      toastOptions: {},
    }
  }

  componentDidMount() {
    eventManager
      .on(ACTION.SHOW, this.showToast)
      .on(ACTION.UPDATE, this.updateToast)
      //@ts-ignore
      .on(ACTION.CLEAR, (toastId) =>
        toastId === null ? this.clear() : this.removeToast(toastId)
      )
  }

  componentWillUnmount() {
    eventManager.off(ACTION.SHOW).off(ACTION.UPDATE).off(ACTION.CLEAR)
  }
  //@ts-ignore
  isToastActive = (toastIds) => this.state.toastIds.indexOf(toastIds) !== -1
  //@ts-ignore
  showToast = (toastOptions) => {
    const { toastId } = toastOptions
    //@ts-ignore
    if (this.state.toastIds.includes(toastId)) {
      this.updateToast(toastOptions)
    } else {
      this.setState((prevState) =>
        update(prevState, {
          toastOptions: { [toastId]: { $set: toastOptions } },
          toastIds: { $push: [toastOptions.toastId] },
        })
      )
    }
  }
  //@ts-ignore
  updateToast = (toastOptions) => {
    const { toastId, ...newToastOptions } = toastOptions
    this.setState((prevState) =>
      update(prevState, {
        toastOptions: {
          [toastId]: {
            //@ts-ignore
            $set: { ...prevState.toastOptions[toastId], ...newToastOptions },
          },
        },
      })
    )
  }
  //@ts-ignore
  removeToast = (toastId) => {
    this.setState((prevState) =>
      update(prevState, {
        toastOptions: { $unset: [toastId] },
        toastIds: {
          //@ts-ignore
          $splice: [[prevState.toastIds.findIndex((id) => id === toastId), 1]],
        },
      })
    )
  }

  clear = () => {
    this.setState({ toastOptions: {}, toastIds: [] })
  }

  render() {
    //@ts-ignore
    const { toastIds, toastOptions } = this.state
    return (
      <Box className={styles['toast-host']}>
        <Transition timeout={TOAST_ANIMATION_DURATION_MS}>
          <List>
            {/* @ts-ignore */}
            {toastIds.map((toastId) => {
              const toast = toastOptions[toastId]
              return (
                <ListItem key={toastId}>
                  <ToastMessage toast={toast} removeToast={this.removeToast} />
                </ListItem>
              )
            })}
          </List>
        </Transition>
      </Box>
    )
  }
}

export default ToastMessageHost
