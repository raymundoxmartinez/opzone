// Ref: https://react.semantic-ui.com/collections/message
export type TOAST_COLOR = 'info' | 'success' | 'error' | undefined

export const ACTION = {
  SHOW: 0,
  UPDATE: 1,
  CLEAR: 2,
}

export const TOAST_TYPE = {
  INFO: 'info',
  SUCCESS: 'success',
  WARNING: 'warning',
  ERROR: 'error',
} as const
