import eventManager from './eventManager'
import { ACTION, TOAST_TYPE } from './constants'

const toastDefaults = {
  dismissable: true,
}

export type ToastOptions = {
  toastId?: any
  type: any
  icon: string
  content: any
  timeout: number
}

type ToastTypes = 'info' | 'success' | 'warning' | 'error'

/**
 * Generate a random toastId
 */
function generateToastId() {
  return (Math.random().toString(36) + Date.now().toString(36)).substr(2, 10)
}

/**
 * Generate a toastId or use the one provided
 */
function getToastId(toastOptions: ToastOptions) {
  if (
    toastOptions &&
    (typeof toastOptions.toastId === 'string' ||
      (typeof toastOptions.toastId === 'number' &&
        !isNaN(toastOptions.toastId)))
  ) {
    return toastOptions.toastId
  }

  return generateToastId()
}

function dispatchToast(toastOptions: ToastOptions) {
  const toastId = getToastId(toastOptions)
  eventManager.emit(ACTION.SHOW, { ...toastDefaults, ...toastOptions, toastId })
  return toastId
}

const toast = (toastOptions: ToastOptions) => dispatchToast(toastOptions)

/**
 * For each available type create a shortcut
 */
for (const t in TOAST_TYPE) {
  ;(toast as any)[t.toLowerCase()] = (toastOptions: any) =>
    dispatchToast({ ...toastOptions, type: t })
}

/**
 * Remove toast programmaticaly
 */
toast.dismiss = (toastId = null) => {
  eventManager.emit(ACTION.CLEAR, toastId)
}

/**
 * Update a toast by Id
 */
toast.update = (toastId: any, toastOptions = {}) => {
  eventManager.emit(ACTION.UPDATE, { ...toastOptions, toastId })
}

toast.defaultTimeout = 3000
export default toast as typeof toast & {
  [key in ToastTypes]: (
    toastOptions: Omit<ToastOptions, 'type' | 'icon'>
  ) => void
}
