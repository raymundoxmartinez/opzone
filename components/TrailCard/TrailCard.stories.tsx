import { ComponentMeta } from '@storybook/react'
import TrailCard from './TrailCard'

export default {
  component: TrailCard,
  title: 'TrailCard',
  tags: ['autodocs'],
} as ComponentMeta<typeof TrailCard>

export const Default = {
  args: {
    trail: {
      id: '1',
      name: 'Jurassic Park trail',
      description:
        'Jurassic Park fdf fsd  fsd fds  fds  fds , f, sa,,asd d,dsa dsa,dsad ,dasd',
      address: 'Los Angeles',
    },
    disabled: false,
  },
}

export const Disabled = {
  args: {
    trail: {
      ...Default.args.trail,
    },
    disabled: true,
  },
}
