import React from 'react'
import { useRouter } from 'next/router'
import Avatar from '@mui/material/Avatar'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import { Carousel } from 'react-responsive-carousel'

import CardActionArea from '@mui/material/CardActionArea'
import Typography from '@mui/material/Typography'
import PlaceIcon from '@mui/icons-material/Place'
import { red } from '@mui/material/colors'

import styles from './TrailCard.module.scss'
import 'react-responsive-carousel/lib/styles/carousel.min.css'

const TrailCard = ({
  trail: {
    name,
    description,
    address,
    id,
    images,
    trailProgressId = null,
    locations,
  },
  disabled = false,
  inProgress,
}) => {
  const router = useRouter()

  return (
    <Card className={styles.main}>
      <Carousel
        infiniteLoop
        centerSlidePercentage={80}
        className={styles.carouselComp}
        showThumbs={false}
        showArrows={true}
        width={'100%'}
        centerMode
      >
        {images && images.length ? (
          images.map((imageUrl, index) => (
            <div key={index}>
              <img
                style={{
                  objectFit: 'cover',
                  width: '98%',
                  height: 300,
                  borderRadius: 6,
                }}
                src={imageUrl}
                alt="trail"
              />
            </div>
          ))
        ) : (
          <div>
            <img
              style={{
                objectFit: 'cover',
                width: '98%',
                height: 300,
                borderRadius: 6,
              }}
              src={'/images/create.jpeg'}
              alt="trail"
            />
          </div>
        )}
      </Carousel>
      <CardActionArea
        onClick={(e) => {
          const params = inProgress ? `?inProgress=${trailProgressId}` : ''
          e.preventDefault()
          router.push(`/dashboard/trails/${id}${params}`)
        }}
        disabled={disabled}
      >
        <CardContent className={styles.content}>
          <Typography noWrap variant="h6">
            {name}
          </Typography>
          <Typography noWrap gutterBottom variant="subtitle1">
            {description}
          </Typography>
          <div style={{ display: 'flex' }}>
            <Avatar
              sx={{
                width: 24,
                height: 24,
                bgcolor: red[500],
                marginRight: '5px',
              }}
              aria-label="recipe"
            >
              <PlaceIcon fontSize="inherit" />
            </Avatar>
            <Typography noWrap variant="subtitle1">
              {locations && locations.length ? locations[0].address : ''}
            </Typography>
          </div>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}
export default TrailCard
