import { Logger } from 'aws-amplify'

const logger = new Logger('OA LOGGER')

export default logger
