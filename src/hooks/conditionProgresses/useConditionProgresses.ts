import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Storage } from 'aws-amplify'
import {
  fetchConditionProgress as fetchConditionProgressRedux,
  fetchConditionProgresses as fetchConditionProgressesRedux,
  postConditionProgress as postConditionProgressRedux,
  updateConditionProgress as updateConditionProgressRedux,
} from '@redux/conditionProgresses/conditionProgressesSlice'

import { toast } from '@components/Toast'

const useConditionProgresses = () => {
  const reduxDispatch = useDispatch()
  const { conditionProgresses, loading } = useSelector(
    (state: Record<string, any>) => ({
      conditionProgresses: state.conditionProgresses.items,
      loading: state.conditionProgresses.loading,
    })
  )

  const postConditionProgress = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(
      postConditionProgressRedux(request)
    )
    if (postConditionProgressRedux.fulfilled.match(resultAction)) {
      return resultAction.payload
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const updateConditionProgress = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(
      updateConditionProgressRedux(request)
    )
    if (updateConditionProgressRedux.fulfilled.match(resultAction)) {
      return resultAction.payload
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const fetchConditionProgresses = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(
        fetchConditionProgressesRedux(request)
      )
      const conditionProgresses = resultAction.payload
      if (fetchConditionProgressesRedux.fulfilled.match(resultAction)) {
        return conditionProgresses
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const fetchConditionProgress = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(
        fetchConditionProgressRedux(request)
      )
      const conditionProgress = resultAction.payload
      if (fetchConditionProgressRedux.fulfilled.match(resultAction)) {
        return conditionProgress
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const attachImages = async (conditionProgresses: any[]): Promise<any[]> => {
    if (!Array.isArray(conditionProgresses)) return []
    const conditionsWithImageUrlPromises = conditionProgresses.map(
      async (conditionProgress) => {
        const { images, ...restofConditionDetails } = conditionProgress
        if (!images || (images && !images.length)) {
          return conditionProgress
        }
        const conditionWithImageUrl = { ...restofConditionDetails }
        const imagePromises = images.map(
          async (file) => await Storage.get(file.key)
        )

        const imageUrls = await Promise.all(imagePromises)
          .then((results) => {
            return results
          })
          .catch((err) => {
            console.log(err)
            throw err
          })

        conditionWithImageUrl.images = imageUrls

        return conditionWithImageUrl
      }
    )

    const conditionsWithImageUrl = await Promise.all(
      conditionsWithImageUrlPromises
    )
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    return conditionsWithImageUrl
  }

  const normalizeConditionProgress = async (
    conditionProgress
  ): Promise<any[]> => {
    const { images, ...restofConditionDetails } = conditionProgress
    if (!images || (images && !images.length)) {
      return conditionProgress
    }
    const normalizedCondition = { ...restofConditionDetails }
    const imagePromises = images.map(
      async (file) => await Storage.get(file.key)
    )

    const imageUrls = await Promise.all(imagePromises)
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    normalizedCondition.images = imageUrls

    if (normalizedCondition?.locations?.items) {
      normalizedCondition.locations = normalizedCondition?.locations?.items.map(
        (item) => item.locationView
      )
    }

    return normalizedCondition
  }
  return {
    conditionProgresses,
    loading,
    postConditionProgress,
    updateConditionProgress,
    fetchConditionProgress,
    fetchConditionProgresses,
    attachImages,
    normalizeConditionProgress,
  }
}

export default useConditionProgresses
