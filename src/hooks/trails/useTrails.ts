import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Storage } from 'aws-amplify'
import {
  fetchTrail as fetchTrailRedux,
  fetchTrails as fetchTrailsRedux,
  postTrail as postTrailRedux,
  deleteTrail as deleteTrailRedux,
} from '@redux/trails/trailsSlice'
import services from '@services/index'

import { toast } from '@components/Toast'

const useTrails = () => {
  const reduxDispatch = useDispatch()
  const { trails, loading } = useSelector((state: Record<string, any>) => ({
    trails: state.trails.items,
    loading: state.trails.loading,
  }))

  const postTrailLocationView = async (request: Record<string, any>) => {
    try {
      const resultAction = await services.trailsService.createTrailLocationView(
        request
      )
      return resultAction.payload
    } catch (error) {
      toast.error({
        content: `Post failed: ${error}`,
        timeout: toast.defaultTimeout,
      })
    }
  }

  const postTrail = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(postTrailRedux(request))
    if (postTrailRedux.fulfilled.match(resultAction)) {
      return resultAction.payload
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const fetchTrails = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchTrailsRedux(request))
      const trails = resultAction.payload
      if (fetchTrailsRedux.fulfilled.match(resultAction)) {
        return trails
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const fetchTrail = useCallback(
    async (request: string) => {
      const resultAction = await reduxDispatch(fetchTrailRedux(request))
      const trail = resultAction.payload
      if (fetchTrailRedux.fulfilled.match(resultAction)) {
        return trail
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const deleteTrail = useCallback(
    async (request: string) => {
      const resultAction = await reduxDispatch(deleteTrailRedux(request))
      const trail = resultAction.payload
      if (deleteTrailRedux.fulfilled.match(resultAction)) {
        return trail
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const attachImages = async (trails: any[]): Promise<any[]> => {
    if (!Array.isArray(trails)) return []
    const trailsWithImageUrlPromises = trails.map(async (trail) => {
      const { images, ...restofTrailDetails } = trail
      if (!images || (images && !images.length)) {
        return trail
      }
      const trailWithImageUrl = { ...restofTrailDetails }
      const imagePromises = images.map(
        async (file) => await Storage.get(file.key)
      )

      const imageUrls = await Promise.all(imagePromises)
        .then((results) => {
          return results
        })
        .catch((err) => {
          console.log(err)
          throw err
        })

      trailWithImageUrl.images = imageUrls

      return trailWithImageUrl
    })

    const trailsWithImageUrl = await Promise.all(trailsWithImageUrlPromises)
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    return trailsWithImageUrl
  }

  const normalizeTrail = async (trail): Promise<any[]> => {
    const { images, ...restofTrailDetails } = trail
    const normalizedTrail = { ...restofTrailDetails }
    if (images && images.length) {
      const imagePromises = images.map(
        async (file) => await Storage.get(file.key)
      )
      const imageUrls = await Promise.all(imagePromises)
        .then((results) => {
          return results
        })
        .catch((err) => {
          console.log(err)
          throw err
        })

      normalizedTrail.images = imageUrls
    } else {
      normalizedTrail.images = []
    }

    if (normalizedTrail?.locations?.items) {
      normalizedTrail.locations = normalizedTrail?.locations?.items.map(
        (item) => item.locationView
      )
    }

    return normalizedTrail
  }
  return {
    trails,
    loading,
    postTrail,
    fetchTrail,
    fetchTrails,
    deleteTrail,
    postTrailLocationView,
    attachImages,
    normalizeTrail,
  }
}

export default useTrails
