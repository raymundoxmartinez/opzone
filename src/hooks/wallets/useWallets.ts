import { useCallback } from 'react'
import {
  fetchWallets as fetchWalletsRedux,
  postWallet as postWalletRedux,
  fetchWalletsByUserID as fetchWalletsByUserIDRedux,
  fetchWalletsByAddress,
} from '../../redux/wallets/walletsSlice'
import { toast } from '@components/Toast'

import { useSelector, useDispatch } from 'react-redux'

const useWallets = () => {
  const reduxDispatch = useDispatch()
  const { wallets, loading } = useSelector((state: Record<string, any>) => ({
    wallets: state.wallets.items,
    loading: state.wallets.loading,
  }))

  const handlePostWallet = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(postWalletRedux(request))
    if (postWalletRedux.fulfilled.match(resultAction)) {
      return wallets
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const handleFetchWallets = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchWalletsRedux(request))
      const wallets = resultAction.payload
      if (fetchWalletsRedux.fulfilled.match(resultAction)) {
        return wallets
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  const handleFetchWalletsByUserID = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(
        fetchWalletsByUserIDRedux(request)
      )
      const wallets = resultAction.payload
      if (fetchWalletsByUserIDRedux.fulfilled.match(resultAction)) {
        return wallets
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  const handleFetchWalletsByAddress = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchWalletsByAddress(request))
      const wallets = resultAction.payload
      if (fetchWalletsByAddress.fulfilled.match(resultAction)) {
        return wallets
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  return {
    wallets,
    loading,
    handleFetchWallets,
    handlePostWallet,
    handleFetchWalletsByUserID,
    handleFetchWalletsByAddress,
  }
}

export default useWallets
