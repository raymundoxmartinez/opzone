import { useCallback } from 'react'
import {
  updateUser as updateUserRedux,
  fetchUsers as fetchUsersRedux,
} from '../../redux/users/usersSlice'
import { fetchCurrentUser } from '../../redux/app/appSlice'
import { profile__handleCloseUpdateUserProfileDialog } from '../../redux/ui/uiSlice'
import { toast } from '@components/Toast'

import { useSelector, useDispatch } from 'react-redux'
import { BaseUser } from '../../../src/services/users/model'

const useUsers = () => {
  const reduxDispatch = useDispatch()
  const { users, loading } = useSelector((state: Record<string, any>) => ({
    users: state.users.items,
    loading: state.users.loading,
  }))

  const updateUser = useCallback(
    async (id: string, userDetails: BaseUser) => {
      const resultAction = await reduxDispatch(
        updateUserRedux({ id, ...userDetails })
      )
      const updatedUser = resultAction.payload
      if (updateUserRedux.fulfilled.match(resultAction)) {
        reduxDispatch(fetchCurrentUser())
        reduxDispatch(profile__handleCloseUpdateUserProfileDialog())
        toast.success({
          content: `User ${updatedUser.username} was successfully updated.`,
          timeout: toast.defaultTimeout,
        })
      } else {
        reduxDispatch(profile__handleCloseUpdateUserProfileDialog())
        if (resultAction.payload) {
          toast.error({
            content: `Update failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Update failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const fetchUsers = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchUsersRedux(request))
      const users = resultAction.payload
      if (fetchUsersRedux.fulfilled.match(resultAction)) {
        return users
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  return {
    users,
    loading,
    updateUser,
    fetchUsers,
  }
}

export default useUsers
