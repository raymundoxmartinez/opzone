import { useCallback } from 'react'
import {
  fetchLocationViews as fetchLocationViewsRedux,
  postLocationView as postLocationViewRedux,
} from '../../redux/locationViews/locationViewsSlice'
import { toast } from '@components/Toast'

import { useSelector, useDispatch } from 'react-redux'

const useLocationViews = () => {
  const reduxDispatch = useDispatch()
  const { locationViews, loading } = useSelector(
    (state: Record<string, any>) => ({
      locationViews: state.locationViews.items,
      loading: state.locationViews.loading,
    })
  )

  const handlePostLocationView = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(postLocationViewRedux(request))
    const locationView = resultAction.payload
    if (postLocationViewRedux.fulfilled.match(resultAction)) {
      return locationView
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }

  const handleFetchLocationViews = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchLocationViewsRedux(request))
      const locationViews = resultAction.payload
      if (fetchLocationViewsRedux.fulfilled.match(resultAction)) {
        return locationViews
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  return {
    locationViews,
    loading,
    handleFetchLocationViews,
    handlePostLocationView,
  }
}

export default useLocationViews
