import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  fetchRewards as fetchRewardsRedux,
  fetchReward as fetchRewardRedux,
  postReward as postRewardRedux,
  deleteReward as deleteRewardRedux,
} from '@redux/rewards/rewardsSlice'
import { toast } from '@components/Toast'

const useRewards = () => {
  const reduxDispatch = useDispatch()
  const { rewards, loading } = useSelector((state: Record<string, any>) => ({
    rewards: state.rewards.items,
    loading: state.rewards.loading,
  }))

  const postReward = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(postRewardRedux(request))
    const reward = resultAction.payload

    if (postRewardRedux.fulfilled.match(resultAction)) {
      return reward
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const fetchRewards = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchRewardsRedux(request))
      const rewards = resultAction.payload
      if (fetchRewardsRedux.fulfilled.match(resultAction)) {
        return rewards
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  const fetchReward = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchRewardRedux(request))
      const reward = resultAction.payload
      if (fetchRewardRedux.fulfilled.match(resultAction)) {
        return reward
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const deleteReward = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(deleteRewardRedux(request))
      const reward = resultAction.payload
      if (deleteRewardRedux.fulfilled.match(resultAction)) {
        return reward
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  return {
    rewards,
    loading,
    postReward,
    fetchRewards,
    deleteReward,
    fetchReward,
  }
}

export default useRewards
