import { useCallback } from 'react'
import {
  fetchConditions as fetchConditionsRedux,
  postCondition as postConditionRedux,
  fetchCondition,
} from '../../redux/conditions/conditionsSlice'
import { toast } from '@components/Toast'

import { useSelector, useDispatch } from 'react-redux'

const useConditions = () => {
  const reduxDispatch = useDispatch()
  const { conditions, loading } = useSelector((state: Record<string, any>) => ({
    conditions: state.conditions.items,
    loading: state.conditions.loading,
  }))

  const handlePostCondition = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(postConditionRedux(request))
    const condition = resultAction.payload
    if (postConditionRedux.fulfilled.match(resultAction)) {
      return condition
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const handleFetchCondition = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchCondition(request))
      const condition = resultAction.payload
      if (fetchCondition.fulfilled.match(resultAction)) {
        return condition
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  const handleFetchConditions = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchConditionsRedux(request))
      const conditions = resultAction.payload
      if (fetchConditionsRedux.fulfilled.match(resultAction)) {
        return conditions
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  return {
    conditions,
    loading,
    handleFetchConditions,
    handleFetchCondition,
    handlePostCondition,
  }
}

export default useConditions
