import { useCallback, useEffect, useState } from 'react'
import services from '@services/index'
import useScript from '../useScript'

const useXrpl = () => {
  const status = useScript(
    'https://unpkg.com/xrpl@2.0.0/build/xrpl-latest-min.js'
  )
  const [xrpl, setXrpl] = useState<any>(null)
  const [xrplClient, setXrplClient] = useState<any>(null)

  const handleGetBalance = useCallback(
    async (xummUserAccount: string) => {
      //@ts-ignore
      const [currency, ...rest] = await xrplClient.getBalances(xummUserAccount)
      return currency?.value
    },
    [xrplClient]
  )

  const handleConnectXrplClient = async () => {
    //@ts-ignore
    const client = new window.xrpl.Client(process.env.NEXT_PUBLIC_XRPL_URL)
    await client.connect()
    setXrplClient(client)
    //@ts-ignore
    setXrpl(window.xrpl)
  }

  const handleGenerateSmartContractConditionAndFulFillment =
    useCallback(async () => {
      const response = await services.xummService.getConditionAndFulfillment()
      return response
    }, [])

  const handleCalculateReleaseOrCancelTime = useCallback((dateTime) => {
    const rippleOffset = 946684800
    const CancelAfter =
      Math.floor(Date.now() / 1000) + 24 * 60 * 60 - rippleOffset
    console.log(CancelAfter)
    return CancelAfter
  }, [])

  const handleFetchXrplTransaction = async (id: string) => {
    const transaction = await xrplClient.request({
      command: 'tx',
      transaction: id,
    })
    return transaction
  }

  useEffect(() => {
    if (status === 'ready') {
      handleConnectXrplClient()
    }
  }, [status])

  return {
    handleGetBalance,
    handleGenerateSmartContractConditionAndFulFillment,
    handleCalculateReleaseOrCancelTime,
    handleFetchXrplTransaction,
    xrpl,
    xrplClient,
  }
}

export default useXrpl
