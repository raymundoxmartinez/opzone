import { useSelector, useDispatch } from 'react-redux'
import {
  profile__handleOpenUpdateUserProfileDialog as profile__handleOpenUpdateUserProfileDialogRedux,
  profile__handleCloseUpdateUserProfileDialog as profile__handleCloseUpdateUserProfileDialogRedux,
  payments__handleOpenPayloadDialog as payments__handleOpenPayloadDialogRedux,
  payments__handleClosePayloadDialog as payments__handleClosePayloadDialogRedux,
  payments__handleOpenCreateSmartContractSelectDialog as payments__handleOpenCreateSmartContractSelectDialogRedux,
  payments__handleCloseCreateSmartContractSelectDialog as payments__handleCloseCreateSmartContractSelectDialogRedux,
  payments__handleOpenCreateWalletDialog as payments__handleOpenCreateWalletDialogRedux,
  payments__handleCloseCreateWalletDialog as payments__handleCloseCreateWalletDialogRedux,
} from '@redux/ui/uiSlice'

const useUI = () => {
  const reduxDispatch = useDispatch()

  const {
    profile__isUpdateUserProfileDialogOpen,
    payments__isPayloadDialogOpen,
    payments__isSmartContractSelectDialogOpen,
  } = useSelector((state: Record<string, any>) => ({
    profile__isUpdateUserProfileDialogOpen:
      state.ui.profile.isUpdateUserProfileDialogOpen,
    payments__isPayloadDialogOpen: state.ui.payments.isPayloadDialogOpen,
    payments__isSmartContractSelectDialogOpen:
      state.ui.payments.isSmartContractSelectDialogOpen,
  }))

  const profile__handleOpenUpdateUserProfileDialog = () => {
    reduxDispatch(profile__handleOpenUpdateUserProfileDialogRedux())
  }
  const profile__handleCloseUpdateUserProfileDialog = () => {
    reduxDispatch(profile__handleCloseUpdateUserProfileDialogRedux())
  }
  const payments__handleOpenPayloadDialog = () => {
    reduxDispatch(payments__handleOpenPayloadDialogRedux())
  }
  const payments__handleClosePayloadDialog = () => {
    reduxDispatch(payments__handleClosePayloadDialogRedux())
  }
  const payments__handleOpenCreateSmartContractSelectDialog = () => {
    reduxDispatch(payments__handleOpenCreateSmartContractSelectDialogRedux())
  }
  const payments__handleCloseCreateSmartContractSelectDialog = () => {
    reduxDispatch(payments__handleCloseCreateSmartContractSelectDialogRedux())
  }
  const payments__handleOpenCreateWalletDialog = () => {
    reduxDispatch(payments__handleOpenCreateWalletDialogRedux())
  }
  const payments__handleCloseCreateWalletDialog = () => {
    reduxDispatch(payments__handleCloseCreateWalletDialogRedux())
  }

  return {
    profile__isUpdateUserProfileDialogOpen,
    payments__isPayloadDialogOpen,
    payments__isSmartContractSelectDialogOpen,
    profile__handleOpenUpdateUserProfileDialog,
    profile__handleCloseUpdateUserProfileDialog,
    payments__handleOpenPayloadDialog,
    payments__handleClosePayloadDialog,
    payments__handleOpenCreateSmartContractSelectDialog,
    payments__handleCloseCreateSmartContractSelectDialog,
    payments__handleOpenCreateWalletDialog,
    payments__handleCloseCreateWalletDialog,
  }
}

export default useUI
