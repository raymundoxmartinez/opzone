import { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Transaction, EscrowTransaction } from '@services/xumm/types'
import { toast } from '@components/Toast'
import {
  fetchTransaction,
  storeXummCredentials,
  signIn,
  postTransaction,
  postEscrowTransaction,
  removeXummCredentials,
  postEscrowFinishTransaction,
} from '@redux/xumm/xummSlice'

const useXumm = () => {
  const reduxDispatch = useDispatch()
  const {
    xummUserToken,
    xummUserAccount,
    isTransactionLoading,
    isCredentialsLoading,
  } = useSelector((state: any) => ({
    xummUserToken: state.xumm.credentials.items.xummUserToken,
    xummUserAccount: state.xumm.credentials.items.xummUserAccount,
    isTransactionLoading: state.xumm.transactions.loading,
    isCredentialsLoading: state.xumm.credentials.loading,
  }))

  const handlePostTransaction = useCallback(
    async ({
      returnUrl,
      xummUserToken,
      amount,
      memo,
      type,
      destination,
    }: Transaction) => {
      const response = await reduxDispatch(
        postTransaction({
          returnUrl,
          xummUserToken,
          amount,
          memo,
          type,
          destination,
        })
      )
      if (postTransaction.fulfilled.match(response)) {
        return response.payload
      } else {
        if (response.payload) {
          toast.error({
            content: `Payment failed: ${response.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Payment failed: ${response.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  const handlePostEscrowTransaction = useCallback(
    async ({
      returnUrl,
      xummUserToken,
      amount,
      memo,
      destination,
      condition,
      cancelAfter,
    }: EscrowTransaction) => {
      const response = await reduxDispatch(
        postEscrowTransaction({
          returnUrl,
          xummUserToken,
          amount,
          memo,
          destination,
          condition,
          cancelAfter,
        })
      )
      if (postEscrowTransaction.fulfilled.match(response)) {
        return response.payload
      } else {
        if (response.payload) {
          toast.error({
            content: `Payment failed: ${response.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Payment failed: ${response.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  const handlePostEscrowFinishTransaction = useCallback(
    async ({
      returnUrl,
      xummUserToken,
      condition,
      fulfillment,
      owner,
      memo,
      fee,
      offerSequence,
    }: any) => {
      const response = await reduxDispatch(
        postEscrowFinishTransaction({
          returnUrl,
          xummUserToken,
          condition,
          fulfillment,
          owner,
          memo,
          fee,
          offerSequence,
        })
      )
      if (postEscrowFinishTransaction.fulfilled.match(response)) {
        return response.payload
      } else {
        if (response.payload) {
          toast.error({
            content: `Claim failed: ${response.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Claim failed: ${response.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const handleXummSignIn = useCallback(
    async (
      { returnUrl } = {
        returnUrl: `${process.env.NEXT_PUBLIC_BASE_URL}/dashboard/payments?payload={id}`,
      }
    ) => {
      const response = await reduxDispatch(signIn({ returnUrl }))
      if (signIn.fulfilled.match(response)) {
        return response.payload
      } else {
        if (response.payload) {
          toast.error({
            content: `SignIn failed: ${response.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `SignIn failed: ${response.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const handleGetTransaction = useCallback(
    async (id: string) => {
      const response = await reduxDispatch(fetchTransaction(id))
      return response
    },
    [reduxDispatch]
  )

  const handleStoreXummCredentials = useCallback(
    async (id: string) => {
      const response = await reduxDispatch(storeXummCredentials(id))
      return response
    },
    [reduxDispatch]
  )
  const handleRemoveXummCredentials = useCallback(async () => {
    const response = await reduxDispatch(removeXummCredentials())
    return response
  }, [reduxDispatch])

  return {
    xummUserToken,
    xummUserAccount,
    isTransactionLoading,
    isCredentialsLoading,
    handlePostTransaction,
    handleXummSignIn,
    handleStoreXummCredentials,
    handleGetTransaction,
    handlePostEscrowTransaction,
    handleRemoveXummCredentials,
    handlePostEscrowFinishTransaction,
  }
}

export default useXumm
