import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  fetchAssets as fetchAssetsRedux,
  fetchAsset as fetchAssetRedux,
  postAsset as postAssetRedux,
  deleteAsset as deleteAssetRedux,
} from '@redux/assets/assetsSlice'
import { toast } from '@components/Toast'

const useAssets = () => {
  const reduxDispatch = useDispatch()
  const { assets, loading } = useSelector((state: Record<string, any>) => ({
    assets: state.assets.items,
    loading: state.assets.loading,
  }))

  const handlePostAsset = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(postAssetRedux(request))
    if (postAssetRedux.fulfilled.match(resultAction)) {
      return assets
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const fetchAssets = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchAssetsRedux(request))
      const assets = resultAction.payload
      if (fetchAssetsRedux.fulfilled.match(resultAction)) {
        return assets
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  const fetchAsset = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchAssetRedux(request))
      const asset = resultAction.payload
      if (fetchAssetRedux.fulfilled.match(resultAction)) {
        return asset
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const deleteAsset = useCallback(
    async (request: string) => {
      const resultAction = await reduxDispatch(deleteAssetRedux(request))
      const asset = resultAction.payload
      if (deleteAssetRedux.fulfilled.match(resultAction)) {
        return asset
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  return {
    assets,
    loading,
    handlePostAsset,
    fetchAssets,
    deleteAsset,
    fetchAsset,
  }
}

export default useAssets
