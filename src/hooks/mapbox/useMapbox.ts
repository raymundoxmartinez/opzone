import { useCallback } from 'react'
import services from '@services/index'
import { toast } from '@components/Toast'

const useMapbox = () => {
  const handleFetchAddressFromCoordinates = useCallback(
    async ({
      latitude,
      longitude,
    }: {
      latitude: string
      longitude: string
    }) => {
      const placeName = await services.mapboxService.getAddressFromCoordinates({
        latitude,
        longitude,
      })
      return placeName
    },
    []
  )
  const handleFetchStaticMapUrl = useCallback(
    async ({
      latitude,
      longitude,
    }: {
      latitude: string
      longitude: string
    }) => {
      const mapUrl = await services.mapboxService.getStaticMapUrl({
        latitude,
        longitude,
      })
      return mapUrl
    },
    []
  )
  const handleVerifyLongitudeLatitudeBounds = useCallback(
    async ({
      latitude,
      longitude,
    }: {
      latitude: string
      longitude: string
    }) => {
      try {
        const isValid =
          await services.mapboxService.verifyLongitudeLatitudeBounds({
            latitude,
            longitude,
          })
        return isValid
      } catch (error) {
        toast.error({
          content:
            'Unable to access your location. Please make sure location tracking is enabled.',
          timeout: toast.defaultTimeout,
        })
      }
    },
    []
  )
  return {
    handleFetchAddressFromCoordinates,
    handleFetchStaticMapUrl,
    handleVerifyLongitudeLatitudeBounds,
  }
}

export default useMapbox
