import { useSelector, useDispatch } from 'react-redux'
import { toast } from '@components/Toast'
import {
  fetchCurrentUser as fetchCurrentUserRedux,
  signOut,
  resetReduxState,
} from '@redux/app/appSlice'
import { useCallback } from 'react'

const useUsers = () => {
  const reduxDispatch = useDispatch()
  const { user, loading } = useSelector((state: Record<string, any>) => ({
    user: state.app.user.items,
    loading: state.app.user.loading,
  }))

  const fetchCurrentUser = useCallback(async () => {
    const resultAction = await reduxDispatch(fetchCurrentUserRedux())
    if (fetchCurrentUserRedux.fulfilled.match(resultAction)) {
      return resultAction
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Get Current User failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Get Current User failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }, [reduxDispatch])

  const signOutUser = async () => {
    const resultAction = await reduxDispatch(signOut())
    if (signOut.fulfilled.match(resultAction)) {
      reduxDispatch(resetReduxState())
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `SignOut failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `SignOut failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  return {
    user,
    loading,
    fetchCurrentUser,
    signOutUser,
  }
}

export default useUsers
