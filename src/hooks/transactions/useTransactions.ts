import { useCallback } from 'react'
import {
  fetchTransactions as fetchTransactionsRedux,
  postTransaction as postTransactionRedux,
  updateTransaction as updateTransactionRedux,
  fetchTransactionsByXummTransactionID as fetchTransactionsByXummTransactionIDRedux,
  fetchTransaction,
} from '../../redux/transactions/transactionsSlice'
import { toast } from '@components/Toast'

import { useSelector, useDispatch } from 'react-redux'

const useTransactions = () => {
  const reduxDispatch = useDispatch()
  const { transactions, loading } = useSelector(
    (state: Record<string, any>) => ({
      transactions: state.transactions.items,
      loading: state.transactions.loading,
    })
  )

  const handlePostTransaction = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(postTransactionRedux(request))
    const transaction = resultAction.payload
    if (postTransactionRedux.fulfilled.match(resultAction)) {
      return transaction
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const handleUpdateTransaction = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(updateTransactionRedux(request))
    const transaction = resultAction.payload
    if (updateTransactionRedux.fulfilled.match(resultAction)) {
      return transaction
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Update failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Update failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const handleFetchTransaction = useCallback(
    async (request: string) => {
      const resultAction = await reduxDispatch(fetchTransaction(request))
      const transaction = resultAction.payload
      if (fetchTransaction.fulfilled.match(resultAction)) {
        return transaction
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  const handleFetchTransactions = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchTransactionsRedux(request))
      const transactions = resultAction.payload
      if (fetchTransactionsRedux.fulfilled.match(resultAction)) {
        return transactions
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  const handleFetchTransactionsByXummTransactionID = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(
        fetchTransactionsByXummTransactionIDRedux(request)
      )
      const transactions = resultAction.payload
      if (
        fetchTransactionsByXummTransactionIDRedux.fulfilled.match(resultAction)
      ) {
        return transactions
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  return {
    transactions,
    loading,
    handleFetchTransactions,
    handleFetchTransaction,
    handlePostTransaction,
    handleUpdateTransaction,
    handleFetchTransactionsByXummTransactionID,
  }
}

export default useTransactions
