import { useCallback } from 'react'
import {
  fetchLocations as fetchLocationsRedux,
  postLocation as postLocationRedux,
} from '../../redux/locations/locationsSlice'
import { toast } from '@components/Toast'

import { useSelector, useDispatch } from 'react-redux'

const useLocations = () => {
  const reduxDispatch = useDispatch()
  const { locations, loading } = useSelector((state: Record<string, any>) => ({
    locations: state.locations.items,
    loading: state.locations.loading,
  }))

  const handlePostLocation = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(postLocationRedux(request))
    const location = resultAction.payload
    if (postLocationRedux.fulfilled.match(resultAction)) {
      return location
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }

  const handleFetchLocations = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(fetchLocationsRedux(request))
      const locations = resultAction.payload
      if (fetchLocationsRedux.fulfilled.match(resultAction)) {
        return locations
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  return {
    locations,
    loading,
    handleFetchLocations,
    handlePostLocation,
  }
}

export default useLocations
