import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Storage } from 'aws-amplify'
import {
  fetchTrailProgress as fetchTrailProgressRedux,
  fetchTrailProgresses as fetchTrailProgressesRedux,
  postTrailProgress as postTrailProgressRedux,
  deleteTrailProgress as deleteTrailProgressRedux,
} from '@redux/trailProgresses/trailProgressesSlice'
import services from '@services/index'

import { toast } from '@components/Toast'

const useTrailProgresses = () => {
  const reduxDispatch = useDispatch()
  const { trailProgresses, loading } = useSelector(
    (state: Record<string, any>) => ({
      trailProgresses: state.trailProgresses.items,
      loading: state.trailProgresses.loading,
    })
  )

  const postTrailProgressLocationView = async (
    request: Record<string, any>
  ) => {
    try {
      const resultAction =
        await services.trailProgressesService.createTrailProgressLocationView(
          request
        )
      return resultAction.payload
    } catch (error) {
      toast.error({
        content: `Post failed: ${error}`,
        timeout: toast.defaultTimeout,
      })
    }
  }

  const postTrailProgress = async (request: Record<string, any>) => {
    const resultAction = await reduxDispatch(postTrailProgressRedux(request))
    if (postTrailProgressRedux.fulfilled.match(resultAction)) {
      return resultAction.payload
    } else {
      if (resultAction.payload) {
        toast.error({
          content: `Post failed: ${resultAction.payload.errorMessage}`,
          timeout: toast.defaultTimeout,
        })
      } else {
        toast.error({
          content: `Post failed: ${resultAction.error.message}`,
          timeout: toast.defaultTimeout,
        })
      }
    }
  }
  const fetchTrailProgresses = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(
        fetchTrailProgressesRedux(request)
      )
      const trailProgresses = resultAction.payload
      if (fetchTrailProgressesRedux.fulfilled.match(resultAction)) {
        return trailProgresses
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const fetchTrailProgress = useCallback(
    async (request: string) => {
      const resultAction = await reduxDispatch(fetchTrailProgressRedux(request))
      const trailProgress = resultAction.payload
      if (fetchTrailProgressRedux.fulfilled.match(resultAction)) {
        return trailProgress
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )

  const attachImages = async (trailProgresses: any[]): Promise<any[]> => {
    if (!Array.isArray(trailProgresses)) return []
    const trailsWithImageUrlPromises = trailProgresses.map(
      async (trailProgress) => {
        const { images, ...restofTrailDetails } = trailProgress
        if (!images || (images && !images.length)) {
          return trailProgress
        }
        const trailWithImageUrl = { ...restofTrailDetails }
        const imagePromises = images.map(
          async (file) => await Storage.get(file.key)
        )

        const imageUrls = await Promise.all(imagePromises)
          .then((results) => {
            return results
          })
          .catch((err) => {
            console.log(err)
            throw err
          })

        trailWithImageUrl.images = imageUrls

        return trailWithImageUrl
      }
    )

    const trailsWithImageUrl = await Promise.all(trailsWithImageUrlPromises)
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    return trailsWithImageUrl
  }

  const normalizeTrailProgress = async (trailProgress): Promise<any[]> => {
    const { images, ...restofTrailDetails } = trailProgress
    if (!images || (images && !images.length)) {
      return trailProgress
    }
    const normalizedTrail = { ...restofTrailDetails }
    const imagePromises = images.map(
      async (file) => await Storage.get(file.key)
    )

    const imageUrls = await Promise.all(imagePromises)
      .then((results) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        throw err
      })

    normalizedTrail.images = imageUrls

    if (normalizedTrail?.locations?.items) {
      normalizedTrail.locations = normalizedTrail?.locations?.items.map(
        (item) => item.locationView
      )
    }

    return normalizedTrail
  }

  const deleteTrailProgress = useCallback(
    async (request: Record<string, any>) => {
      const resultAction = await reduxDispatch(
        deleteTrailProgressRedux(request)
      )
      const trailProgress = resultAction.payload
      if (deleteTrailProgressRedux.fulfilled.match(resultAction)) {
        return trailProgress
      } else {
        if (resultAction.payload) {
          toast.error({
            content: `Get failed: ${resultAction.payload.errorMessage}`,
            timeout: toast.defaultTimeout,
          })
        } else {
          toast.error({
            content: `Get failed: ${resultAction.error.message}`,
            timeout: toast.defaultTimeout,
          })
        }
      }
    },
    [reduxDispatch]
  )
  return {
    trailProgresses,
    loading,
    postTrailProgress,
    fetchTrailProgress,
    fetchTrailProgresses,
    deleteTrailProgress,
    postTrailProgressLocationView,
    attachImages,
    normalizeTrailProgress,
  }
}

export default useTrailProgresses
