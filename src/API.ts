/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateUserInput = {
  id?: string | null,
  username: string,
  email: string,
  bio?: string | null,
  location?: string | null,
  images?: Array< S3ObjectInput | null > | null,
  backgroundImage?: string | null,
};

export type S3ObjectInput = {
  bucket: string,
  region: string,
  key: string,
};

export type ModelUserConditionInput = {
  username?: ModelStringInput | null,
  email?: ModelStringInput | null,
  bio?: ModelStringInput | null,
  location?: ModelStringInput | null,
  backgroundImage?: ModelStringInput | null,
  and?: Array< ModelUserConditionInput | null > | null,
  or?: Array< ModelUserConditionInput | null > | null,
  not?: ModelUserConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type User = {
  __typename: "User",
  id: string,
  username: string,
  email: string,
  bio?: string | null,
  location?: string | null,
  images?:  Array<S3Object | null > | null,
  backgroundImage?: string | null,
  wallets?: ModelWalletConnection | null,
  createdAt: string,
  updatedAt: string,
};

export type S3Object = {
  __typename: "S3Object",
  bucket: string,
  region: string,
  key: string,
};

export type ModelWalletConnection = {
  __typename: "ModelWalletConnection",
  items:  Array<Wallet | null >,
  nextToken?: string | null,
};

export type Wallet = {
  __typename: "Wallet",
  id: string,
  title: string,
  images?: Array< string | null > | null,
  description?: string | null,
  userID: string,
  address: string,
  user?: User | null,
  createdAt: string,
  updatedAt: string,
};

export type UpdateUserInput = {
  id: string,
  username?: string | null,
  email?: string | null,
  bio?: string | null,
  location?: string | null,
  images?: Array< S3ObjectInput | null > | null,
  backgroundImage?: string | null,
};

export type DeleteUserInput = {
  id: string,
};

export type CreateWalletInput = {
  id?: string | null,
  title: string,
  images?: Array< string | null > | null,
  description?: string | null,
  userID: string,
  address: string,
};

export type ModelWalletConditionInput = {
  title?: ModelStringInput | null,
  images?: ModelStringInput | null,
  description?: ModelStringInput | null,
  userID?: ModelIDInput | null,
  address?: ModelStringInput | null,
  and?: Array< ModelWalletConditionInput | null > | null,
  or?: Array< ModelWalletConditionInput | null > | null,
  not?: ModelWalletConditionInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type UpdateWalletInput = {
  id: string,
  title?: string | null,
  images?: Array< string | null > | null,
  description?: string | null,
  userID?: string | null,
  address?: string | null,
};

export type DeleteWalletInput = {
  id: string,
};

export type CreateProductInput = {
  id?: string | null,
  title: string,
  images?: Array< S3ObjectInput | null > | null,
  description?: string | null,
  price: number,
  category?: string | null,
  userID: string,
  address: string,
  nft?: string | null,
};

export type ModelProductConditionInput = {
  title?: ModelStringInput | null,
  description?: ModelStringInput | null,
  price?: ModelFloatInput | null,
  category?: ModelStringInput | null,
  userID?: ModelIDInput | null,
  address?: ModelStringInput | null,
  nft?: ModelStringInput | null,
  and?: Array< ModelProductConditionInput | null > | null,
  or?: Array< ModelProductConditionInput | null > | null,
  not?: ModelProductConditionInput | null,
};

export type ModelFloatInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type Product = {
  __typename: "Product",
  id: string,
  title: string,
  images?:  Array<S3Object | null > | null,
  description?: string | null,
  price: number,
  category?: string | null,
  userID: string,
  address: string,
  user?: User | null,
  nft?: string | null,
  createdAt: string,
  updatedAt: string,
};

export type UpdateProductInput = {
  id: string,
  title?: string | null,
  images?: Array< S3ObjectInput | null > | null,
  description?: string | null,
  price?: number | null,
  category?: string | null,
  userID?: string | null,
  address?: string | null,
  nft?: string | null,
};

export type DeleteProductInput = {
  id: string,
};

export type CreateTrailInput = {
  id?: string | null,
  name?: string | null,
  description?: string | null,
  duration?: string | null,
  type?: TrailType | null,
  images?: Array< S3ObjectInput | null > | null,
  order?: Array< string | null > | null,
  userID: string,
};

export enum TrailType {
  ENTERTAINMENT = "ENTERTAINMENT",
  HISTORICAL = "HISTORICAL",
  EXERCISE = "EXERCISE",
  SOCIAL = "SOCIAL",
}


export type ModelTrailConditionInput = {
  name?: ModelStringInput | null,
  description?: ModelStringInput | null,
  duration?: ModelStringInput | null,
  type?: ModelTrailTypeInput | null,
  order?: ModelIDInput | null,
  userID?: ModelIDInput | null,
  and?: Array< ModelTrailConditionInput | null > | null,
  or?: Array< ModelTrailConditionInput | null > | null,
  not?: ModelTrailConditionInput | null,
};

export type ModelTrailTypeInput = {
  eq?: TrailType | null,
  ne?: TrailType | null,
};

export type Trail = {
  __typename: "Trail",
  id: string,
  name?: string | null,
  description?: string | null,
  duration?: string | null,
  type?: TrailType | null,
  images?:  Array<S3Object | null > | null,
  locations?: ModelTrailLocationViewsConnection | null,
  order?: Array< string | null > | null,
  trailProgress?: ModelTrailProgressConnection | null,
  userID: string,
  user?: User | null,
  createdAt: string,
  updatedAt: string,
};

export type ModelTrailLocationViewsConnection = {
  __typename: "ModelTrailLocationViewsConnection",
  items:  Array<TrailLocationViews | null >,
  nextToken?: string | null,
};

export type TrailLocationViews = {
  __typename: "TrailLocationViews",
  id: string,
  trailID: string,
  locationViewID: string,
  trail: Trail,
  locationView: LocationView,
  createdAt: string,
  updatedAt: string,
};

export type LocationView = {
  __typename: "LocationView",
  id: string,
  name?: string | null,
  address?: string | null,
  description?: string | null,
  coordinates?: Array< number | null > | null,
  locationID: string,
  location?: Location | null,
  userID: string,
  user?: User | null,
  transactions?: ModelTransactionConnection | null,
  trails?: ModelTrailLocationViewsConnection | null,
  createdAt: string,
  updatedAt: string,
};

export type Location = {
  __typename: "Location",
  id: string,
  name?: string | null,
  address?: string | null,
  description?: string | null,
  coordinates?: Array< number | null > | null,
  userID: string,
  user?: User | null,
  createdAt: string,
  updatedAt: string,
};

export type ModelTransactionConnection = {
  __typename: "ModelTransactionConnection",
  items:  Array<Transaction | null >,
  nextToken?: string | null,
};

export type Transaction = {
  __typename: "Transaction",
  id: string,
  status: string,
  amount?: string | null,
  description?: string | null,
  type: TransactionType,
  senderID: string,
  verifierID?: string | null,
  receiverID?: string | null,
  conditions?: ModelConditionConnection | null,
  expiration?: string | null,
  sender?: User | null,
  verifier?: User | null,
  receiver?: User | null,
  fulfillment?: string | null,
  condition?: string | null,
  fromWalletID?: string | null,
  toWalletID?: string | null,
  toWallet?: Wallet | null,
  fromWallet?: Wallet | null,
  xummTransactionID?: string | null,
  locationViewID?: string | null,
  rewardID?: string | null,
  reward?: Reward | null,
  transactionProgress?: ModelTransactionProgressConnection | null,
  createdAt: string,
  updatedAt: string,
};

export enum TransactionType {
  PAYMENT = "PAYMENT",
  FILE = "FILE",
  MESSAGE = "MESSAGE",
  WEB3_PAYMENT = "WEB3_PAYMENT",
}


export type ModelConditionConnection = {
  __typename: "ModelConditionConnection",
  items:  Array<Condition | null >,
  nextToken?: string | null,
};

export type Condition = {
  __typename: "Condition",
  id: string,
  type: ConditionType,
  latitude?: string | null,
  longitude?: string | null,
  question?: string | null,
  choices?: Array< string | null > | null,
  answerIndex?: number | null,
  status: string,
  transactionID: string,
  createdAt: string,
  updatedAt: string,
};

export enum ConditionType {
  GEO = "GEO",
  QUESTION = "QUESTION",
  TIME = "TIME",
}


export type Reward = {
  __typename: "Reward",
  id: string,
  name?: string | null,
  file?: S3Object | null,
  createdAt: string,
  updatedAt: string,
};

export type ModelTransactionProgressConnection = {
  __typename: "ModelTransactionProgressConnection",
  items:  Array<TransactionProgress | null >,
  nextToken?: string | null,
};

export type TransactionProgress = {
  __typename: "TransactionProgress",
  id: string,
  userID: string,
  transactionID: string,
  user?: User | null,
  transaction?: Transaction | null,
  status: TransactionProgressStatus,
  completedConditions?: ModelConditionProgressConnection | null,
  createdAt: string,
  updatedAt: string,
  transactionTransactionProgressId?: string | null,
};

export enum TransactionProgressStatus {
  PENDING = "PENDING",
  IN_PROGRESS = "IN_PROGRESS",
  COMPLETED = "COMPLETED",
  FAILED = "FAILED",
  CANCELLED = "CANCELLED",
  REFUNDED = "REFUNDED",
  EXPIRED = "EXPIRED",
}


export type ModelConditionProgressConnection = {
  __typename: "ModelConditionProgressConnection",
  items:  Array<ConditionProgress | null >,
  nextToken?: string | null,
};

export type ConditionProgress = {
  __typename: "ConditionProgress",
  id: string,
  userID: string,
  conditionID: string,
  user?: User | null,
  condition?: Condition | null,
  status: ConditionProgressStatus,
  createdAt: string,
  updatedAt: string,
  trailProgressCompletedConditionsId?: string | null,
  transactionProgressCompletedConditionsId?: string | null,
};

export enum ConditionProgressStatus {
  NOT_STARTED = "NOT_STARTED",
  IN_PROGRESS = "IN_PROGRESS",
  COMPLETED = "COMPLETED",
  SKIPPED = "SKIPPED",
  PENDING_REVIEW = "PENDING_REVIEW",
  APPROVED = "APPROVED",
  REJECTED = "REJECTED",
  EXPIRED = "EXPIRED",
}


export type ModelTrailProgressConnection = {
  __typename: "ModelTrailProgressConnection",
  items:  Array<TrailProgress | null >,
  nextToken?: string | null,
};

export type TrailProgress = {
  __typename: "TrailProgress",
  id: string,
  name?: string | null,
  userID: string,
  user?: User | null,
  status: TrailProgressStatus,
  trailID: string,
  trail?: Trail | null,
  order?: Array< string | null > | null,
  completedConditions?: ModelConditionProgressConnection | null,
  createdAt: string,
  updatedAt: string,
  trailTrailProgressId?: string | null,
};

export enum TrailProgressStatus {
  DRAFT = "DRAFT",
  ACTIVE = "ACTIVE",
  INACTIVE = "INACTIVE",
  COMPLETED = "COMPLETED",
  PAUSED = "PAUSED",
  ARCHIVED = "ARCHIVED",
  UNDER_REVIEW = "UNDER_REVIEW",
  REJECTED = "REJECTED",
  DELETED = "DELETED",
}


export type UpdateTrailInput = {
  id: string,
  name?: string | null,
  description?: string | null,
  duration?: string | null,
  type?: TrailType | null,
  images?: Array< S3ObjectInput | null > | null,
  order?: Array< string | null > | null,
  userID?: string | null,
};

export type DeleteTrailInput = {
  id: string,
};

export type CreateTrailProgressInput = {
  id?: string | null,
  name?: string | null,
  userID: string,
  status: TrailProgressStatus,
  trailID: string,
  order?: Array< string | null > | null,
  trailTrailProgressId?: string | null,
};

export type ModelTrailProgressConditionInput = {
  name?: ModelStringInput | null,
  userID?: ModelIDInput | null,
  status?: ModelTrailProgressStatusInput | null,
  trailID?: ModelIDInput | null,
  order?: ModelIDInput | null,
  and?: Array< ModelTrailProgressConditionInput | null > | null,
  or?: Array< ModelTrailProgressConditionInput | null > | null,
  not?: ModelTrailProgressConditionInput | null,
  trailTrailProgressId?: ModelIDInput | null,
};

export type ModelTrailProgressStatusInput = {
  eq?: TrailProgressStatus | null,
  ne?: TrailProgressStatus | null,
};

export type UpdateTrailProgressInput = {
  id: string,
  name?: string | null,
  userID?: string | null,
  status?: TrailProgressStatus | null,
  trailID?: string | null,
  order?: Array< string | null > | null,
  trailTrailProgressId?: string | null,
};

export type DeleteTrailProgressInput = {
  id: string,
};

export type CreateLocationInput = {
  id?: string | null,
  name?: string | null,
  address?: string | null,
  description?: string | null,
  coordinates?: Array< number | null > | null,
  userID: string,
};

export type ModelLocationConditionInput = {
  name?: ModelStringInput | null,
  address?: ModelStringInput | null,
  description?: ModelStringInput | null,
  coordinates?: ModelFloatInput | null,
  userID?: ModelIDInput | null,
  and?: Array< ModelLocationConditionInput | null > | null,
  or?: Array< ModelLocationConditionInput | null > | null,
  not?: ModelLocationConditionInput | null,
};

export type UpdateLocationInput = {
  id: string,
  name?: string | null,
  address?: string | null,
  description?: string | null,
  coordinates?: Array< number | null > | null,
  userID?: string | null,
};

export type DeleteLocationInput = {
  id: string,
};

export type CreateLocationViewInput = {
  id?: string | null,
  name?: string | null,
  address?: string | null,
  description?: string | null,
  coordinates?: Array< number | null > | null,
  locationID: string,
  userID: string,
};

export type ModelLocationViewConditionInput = {
  name?: ModelStringInput | null,
  address?: ModelStringInput | null,
  description?: ModelStringInput | null,
  coordinates?: ModelFloatInput | null,
  locationID?: ModelIDInput | null,
  userID?: ModelIDInput | null,
  and?: Array< ModelLocationViewConditionInput | null > | null,
  or?: Array< ModelLocationViewConditionInput | null > | null,
  not?: ModelLocationViewConditionInput | null,
};

export type UpdateLocationViewInput = {
  id: string,
  name?: string | null,
  address?: string | null,
  description?: string | null,
  coordinates?: Array< number | null > | null,
  locationID?: string | null,
  userID?: string | null,
};

export type DeleteLocationViewInput = {
  id: string,
};

export type CreateTransactionInput = {
  id?: string | null,
  status: string,
  amount?: string | null,
  description?: string | null,
  type: TransactionType,
  senderID: string,
  verifierID?: string | null,
  receiverID?: string | null,
  expiration?: string | null,
  fulfillment?: string | null,
  condition?: string | null,
  fromWalletID?: string | null,
  toWalletID?: string | null,
  xummTransactionID?: string | null,
  locationViewID?: string | null,
  rewardID?: string | null,
};

export type ModelTransactionConditionInput = {
  status?: ModelStringInput | null,
  amount?: ModelStringInput | null,
  description?: ModelStringInput | null,
  type?: ModelTransactionTypeInput | null,
  senderID?: ModelIDInput | null,
  verifierID?: ModelIDInput | null,
  receiverID?: ModelIDInput | null,
  expiration?: ModelStringInput | null,
  fulfillment?: ModelStringInput | null,
  condition?: ModelStringInput | null,
  fromWalletID?: ModelIDInput | null,
  toWalletID?: ModelIDInput | null,
  xummTransactionID?: ModelStringInput | null,
  locationViewID?: ModelIDInput | null,
  rewardID?: ModelIDInput | null,
  and?: Array< ModelTransactionConditionInput | null > | null,
  or?: Array< ModelTransactionConditionInput | null > | null,
  not?: ModelTransactionConditionInput | null,
};

export type ModelTransactionTypeInput = {
  eq?: TransactionType | null,
  ne?: TransactionType | null,
};

export type UpdateTransactionInput = {
  id: string,
  status?: string | null,
  amount?: string | null,
  description?: string | null,
  type?: TransactionType | null,
  senderID?: string | null,
  verifierID?: string | null,
  receiverID?: string | null,
  expiration?: string | null,
  fulfillment?: string | null,
  condition?: string | null,
  fromWalletID?: string | null,
  toWalletID?: string | null,
  xummTransactionID?: string | null,
  locationViewID?: string | null,
  rewardID?: string | null,
};

export type DeleteTransactionInput = {
  id: string,
};

export type CreateTransactionProgressInput = {
  id?: string | null,
  userID: string,
  transactionID: string,
  status: TransactionProgressStatus,
  transactionTransactionProgressId?: string | null,
};

export type ModelTransactionProgressConditionInput = {
  userID?: ModelIDInput | null,
  transactionID?: ModelIDInput | null,
  status?: ModelTransactionProgressStatusInput | null,
  and?: Array< ModelTransactionProgressConditionInput | null > | null,
  or?: Array< ModelTransactionProgressConditionInput | null > | null,
  not?: ModelTransactionProgressConditionInput | null,
  transactionTransactionProgressId?: ModelIDInput | null,
};

export type ModelTransactionProgressStatusInput = {
  eq?: TransactionProgressStatus | null,
  ne?: TransactionProgressStatus | null,
};

export type UpdateTransactionProgressInput = {
  id: string,
  userID?: string | null,
  transactionID?: string | null,
  status?: TransactionProgressStatus | null,
  transactionTransactionProgressId?: string | null,
};

export type DeleteTransactionProgressInput = {
  id: string,
};

export type CreateConditionInput = {
  id?: string | null,
  type: ConditionType,
  latitude?: string | null,
  longitude?: string | null,
  question?: string | null,
  choices?: Array< string | null > | null,
  answerIndex?: number | null,
  status: string,
  transactionID: string,
};

export type ModelConditionConditionInput = {
  type?: ModelConditionTypeInput | null,
  latitude?: ModelStringInput | null,
  longitude?: ModelStringInput | null,
  question?: ModelStringInput | null,
  choices?: ModelStringInput | null,
  answerIndex?: ModelIntInput | null,
  status?: ModelStringInput | null,
  transactionID?: ModelIDInput | null,
  and?: Array< ModelConditionConditionInput | null > | null,
  or?: Array< ModelConditionConditionInput | null > | null,
  not?: ModelConditionConditionInput | null,
};

export type ModelConditionTypeInput = {
  eq?: ConditionType | null,
  ne?: ConditionType | null,
};

export type ModelIntInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type UpdateConditionInput = {
  id: string,
  type?: ConditionType | null,
  latitude?: string | null,
  longitude?: string | null,
  question?: string | null,
  choices?: Array< string | null > | null,
  answerIndex?: number | null,
  status?: string | null,
  transactionID?: string | null,
};

export type DeleteConditionInput = {
  id: string,
};

export type CreateConditionProgressInput = {
  id?: string | null,
  userID: string,
  conditionID: string,
  status: ConditionProgressStatus,
  trailProgressCompletedConditionsId?: string | null,
  transactionProgressCompletedConditionsId?: string | null,
};

export type ModelConditionProgressConditionInput = {
  userID?: ModelIDInput | null,
  conditionID?: ModelIDInput | null,
  status?: ModelConditionProgressStatusInput | null,
  and?: Array< ModelConditionProgressConditionInput | null > | null,
  or?: Array< ModelConditionProgressConditionInput | null > | null,
  not?: ModelConditionProgressConditionInput | null,
  trailProgressCompletedConditionsId?: ModelIDInput | null,
  transactionProgressCompletedConditionsId?: ModelIDInput | null,
};

export type ModelConditionProgressStatusInput = {
  eq?: ConditionProgressStatus | null,
  ne?: ConditionProgressStatus | null,
};

export type UpdateConditionProgressInput = {
  id: string,
  userID?: string | null,
  conditionID?: string | null,
  status?: ConditionProgressStatus | null,
  trailProgressCompletedConditionsId?: string | null,
  transactionProgressCompletedConditionsId?: string | null,
};

export type DeleteConditionProgressInput = {
  id: string,
};

export type CreateAssetInput = {
  id?: string | null,
  name?: string | null,
  file?: S3ObjectInput | null,
  userID: string,
};

export type ModelAssetConditionInput = {
  name?: ModelStringInput | null,
  userID?: ModelIDInput | null,
  and?: Array< ModelAssetConditionInput | null > | null,
  or?: Array< ModelAssetConditionInput | null > | null,
  not?: ModelAssetConditionInput | null,
};

export type Asset = {
  __typename: "Asset",
  id: string,
  name?: string | null,
  file?: S3Object | null,
  userID: string,
  owner?: User | null,
  createdAt: string,
  updatedAt: string,
};

export type UpdateAssetInput = {
  id: string,
  name?: string | null,
  file?: S3ObjectInput | null,
  userID?: string | null,
};

export type DeleteAssetInput = {
  id: string,
};

export type CreateRewardInput = {
  id?: string | null,
  name?: string | null,
  file?: S3ObjectInput | null,
};

export type ModelRewardConditionInput = {
  name?: ModelStringInput | null,
  and?: Array< ModelRewardConditionInput | null > | null,
  or?: Array< ModelRewardConditionInput | null > | null,
  not?: ModelRewardConditionInput | null,
};

export type UpdateRewardInput = {
  id: string,
  name?: string | null,
  file?: S3ObjectInput | null,
};

export type DeleteRewardInput = {
  id: string,
};

export type CreateTrailLocationViewsInput = {
  id?: string | null,
  trailID: string,
  locationViewID: string,
};

export type ModelTrailLocationViewsConditionInput = {
  trailID?: ModelIDInput | null,
  locationViewID?: ModelIDInput | null,
  and?: Array< ModelTrailLocationViewsConditionInput | null > | null,
  or?: Array< ModelTrailLocationViewsConditionInput | null > | null,
  not?: ModelTrailLocationViewsConditionInput | null,
};

export type UpdateTrailLocationViewsInput = {
  id: string,
  trailID?: string | null,
  locationViewID?: string | null,
};

export type DeleteTrailLocationViewsInput = {
  id: string,
};

export type ModelUserFilterInput = {
  id?: ModelIDInput | null,
  username?: ModelStringInput | null,
  email?: ModelStringInput | null,
  bio?: ModelStringInput | null,
  location?: ModelStringInput | null,
  backgroundImage?: ModelStringInput | null,
  and?: Array< ModelUserFilterInput | null > | null,
  or?: Array< ModelUserFilterInput | null > | null,
  not?: ModelUserFilterInput | null,
};

export type ModelUserConnection = {
  __typename: "ModelUserConnection",
  items:  Array<User | null >,
  nextToken?: string | null,
};

export type ModelWalletFilterInput = {
  id?: ModelIDInput | null,
  title?: ModelStringInput | null,
  images?: ModelStringInput | null,
  description?: ModelStringInput | null,
  userID?: ModelIDInput | null,
  address?: ModelStringInput | null,
  and?: Array< ModelWalletFilterInput | null > | null,
  or?: Array< ModelWalletFilterInput | null > | null,
  not?: ModelWalletFilterInput | null,
};

export type ModelProductFilterInput = {
  id?: ModelIDInput | null,
  title?: ModelStringInput | null,
  description?: ModelStringInput | null,
  price?: ModelFloatInput | null,
  category?: ModelStringInput | null,
  userID?: ModelIDInput | null,
  address?: ModelStringInput | null,
  nft?: ModelStringInput | null,
  and?: Array< ModelProductFilterInput | null > | null,
  or?: Array< ModelProductFilterInput | null > | null,
  not?: ModelProductFilterInput | null,
};

export type ModelProductConnection = {
  __typename: "ModelProductConnection",
  items:  Array<Product | null >,
  nextToken?: string | null,
};

export type ModelTrailFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  description?: ModelStringInput | null,
  duration?: ModelStringInput | null,
  type?: ModelTrailTypeInput | null,
  order?: ModelIDInput | null,
  userID?: ModelIDInput | null,
  and?: Array< ModelTrailFilterInput | null > | null,
  or?: Array< ModelTrailFilterInput | null > | null,
  not?: ModelTrailFilterInput | null,
};

export type ModelTrailConnection = {
  __typename: "ModelTrailConnection",
  items:  Array<Trail | null >,
  nextToken?: string | null,
};

export type ModelTrailProgressFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  userID?: ModelIDInput | null,
  status?: ModelTrailProgressStatusInput | null,
  trailID?: ModelIDInput | null,
  order?: ModelIDInput | null,
  and?: Array< ModelTrailProgressFilterInput | null > | null,
  or?: Array< ModelTrailProgressFilterInput | null > | null,
  not?: ModelTrailProgressFilterInput | null,
  trailTrailProgressId?: ModelIDInput | null,
};

export type ModelLocationFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  address?: ModelStringInput | null,
  description?: ModelStringInput | null,
  coordinates?: ModelFloatInput | null,
  userID?: ModelIDInput | null,
  and?: Array< ModelLocationFilterInput | null > | null,
  or?: Array< ModelLocationFilterInput | null > | null,
  not?: ModelLocationFilterInput | null,
};

export type ModelLocationConnection = {
  __typename: "ModelLocationConnection",
  items:  Array<Location | null >,
  nextToken?: string | null,
};

export type ModelLocationViewFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  address?: ModelStringInput | null,
  description?: ModelStringInput | null,
  coordinates?: ModelFloatInput | null,
  locationID?: ModelIDInput | null,
  userID?: ModelIDInput | null,
  and?: Array< ModelLocationViewFilterInput | null > | null,
  or?: Array< ModelLocationViewFilterInput | null > | null,
  not?: ModelLocationViewFilterInput | null,
};

export type ModelLocationViewConnection = {
  __typename: "ModelLocationViewConnection",
  items:  Array<LocationView | null >,
  nextToken?: string | null,
};

export type ModelTransactionFilterInput = {
  id?: ModelIDInput | null,
  status?: ModelStringInput | null,
  amount?: ModelStringInput | null,
  description?: ModelStringInput | null,
  type?: ModelTransactionTypeInput | null,
  senderID?: ModelIDInput | null,
  verifierID?: ModelIDInput | null,
  receiverID?: ModelIDInput | null,
  expiration?: ModelStringInput | null,
  fulfillment?: ModelStringInput | null,
  condition?: ModelStringInput | null,
  fromWalletID?: ModelIDInput | null,
  toWalletID?: ModelIDInput | null,
  xummTransactionID?: ModelStringInput | null,
  locationViewID?: ModelIDInput | null,
  rewardID?: ModelIDInput | null,
  and?: Array< ModelTransactionFilterInput | null > | null,
  or?: Array< ModelTransactionFilterInput | null > | null,
  not?: ModelTransactionFilterInput | null,
};

export type ModelTransactionProgressFilterInput = {
  id?: ModelIDInput | null,
  userID?: ModelIDInput | null,
  transactionID?: ModelIDInput | null,
  status?: ModelTransactionProgressStatusInput | null,
  and?: Array< ModelTransactionProgressFilterInput | null > | null,
  or?: Array< ModelTransactionProgressFilterInput | null > | null,
  not?: ModelTransactionProgressFilterInput | null,
  transactionTransactionProgressId?: ModelIDInput | null,
};

export type ModelConditionFilterInput = {
  id?: ModelIDInput | null,
  type?: ModelConditionTypeInput | null,
  latitude?: ModelStringInput | null,
  longitude?: ModelStringInput | null,
  question?: ModelStringInput | null,
  choices?: ModelStringInput | null,
  answerIndex?: ModelIntInput | null,
  status?: ModelStringInput | null,
  transactionID?: ModelIDInput | null,
  and?: Array< ModelConditionFilterInput | null > | null,
  or?: Array< ModelConditionFilterInput | null > | null,
  not?: ModelConditionFilterInput | null,
};

export type ModelConditionProgressFilterInput = {
  id?: ModelIDInput | null,
  userID?: ModelIDInput | null,
  conditionID?: ModelIDInput | null,
  status?: ModelConditionProgressStatusInput | null,
  and?: Array< ModelConditionProgressFilterInput | null > | null,
  or?: Array< ModelConditionProgressFilterInput | null > | null,
  not?: ModelConditionProgressFilterInput | null,
  trailProgressCompletedConditionsId?: ModelIDInput | null,
  transactionProgressCompletedConditionsId?: ModelIDInput | null,
};

export type ModelAssetFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  userID?: ModelIDInput | null,
  and?: Array< ModelAssetFilterInput | null > | null,
  or?: Array< ModelAssetFilterInput | null > | null,
  not?: ModelAssetFilterInput | null,
};

export type ModelAssetConnection = {
  __typename: "ModelAssetConnection",
  items:  Array<Asset | null >,
  nextToken?: string | null,
};

export type ModelRewardFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  and?: Array< ModelRewardFilterInput | null > | null,
  or?: Array< ModelRewardFilterInput | null > | null,
  not?: ModelRewardFilterInput | null,
};

export type ModelRewardConnection = {
  __typename: "ModelRewardConnection",
  items:  Array<Reward | null >,
  nextToken?: string | null,
};

export type ModelTrailLocationViewsFilterInput = {
  id?: ModelIDInput | null,
  trailID?: ModelIDInput | null,
  locationViewID?: ModelIDInput | null,
  and?: Array< ModelTrailLocationViewsFilterInput | null > | null,
  or?: Array< ModelTrailLocationViewsFilterInput | null > | null,
  not?: ModelTrailLocationViewsFilterInput | null,
};

export type ModelIDKeyConditionInput = {
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC",
}


export type ModelSubscriptionUserFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  username?: ModelSubscriptionStringInput | null,
  email?: ModelSubscriptionStringInput | null,
  bio?: ModelSubscriptionStringInput | null,
  location?: ModelSubscriptionStringInput | null,
  backgroundImage?: ModelSubscriptionStringInput | null,
  and?: Array< ModelSubscriptionUserFilterInput | null > | null,
  or?: Array< ModelSubscriptionUserFilterInput | null > | null,
};

export type ModelSubscriptionIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  in?: Array< string | null > | null,
  notIn?: Array< string | null > | null,
};

export type ModelSubscriptionStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  in?: Array< string | null > | null,
  notIn?: Array< string | null > | null,
};

export type ModelSubscriptionWalletFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  title?: ModelSubscriptionStringInput | null,
  images?: ModelSubscriptionStringInput | null,
  description?: ModelSubscriptionStringInput | null,
  userID?: ModelSubscriptionIDInput | null,
  address?: ModelSubscriptionStringInput | null,
  and?: Array< ModelSubscriptionWalletFilterInput | null > | null,
  or?: Array< ModelSubscriptionWalletFilterInput | null > | null,
};

export type ModelSubscriptionProductFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  title?: ModelSubscriptionStringInput | null,
  description?: ModelSubscriptionStringInput | null,
  price?: ModelSubscriptionFloatInput | null,
  category?: ModelSubscriptionStringInput | null,
  userID?: ModelSubscriptionIDInput | null,
  address?: ModelSubscriptionStringInput | null,
  nft?: ModelSubscriptionStringInput | null,
  and?: Array< ModelSubscriptionProductFilterInput | null > | null,
  or?: Array< ModelSubscriptionProductFilterInput | null > | null,
};

export type ModelSubscriptionFloatInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  in?: Array< number | null > | null,
  notIn?: Array< number | null > | null,
};

export type ModelSubscriptionTrailFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  name?: ModelSubscriptionStringInput | null,
  description?: ModelSubscriptionStringInput | null,
  duration?: ModelSubscriptionStringInput | null,
  type?: ModelSubscriptionStringInput | null,
  order?: ModelSubscriptionIDInput | null,
  userID?: ModelSubscriptionIDInput | null,
  and?: Array< ModelSubscriptionTrailFilterInput | null > | null,
  or?: Array< ModelSubscriptionTrailFilterInput | null > | null,
};

export type ModelSubscriptionTrailProgressFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  name?: ModelSubscriptionStringInput | null,
  userID?: ModelSubscriptionIDInput | null,
  status?: ModelSubscriptionStringInput | null,
  trailID?: ModelSubscriptionIDInput | null,
  order?: ModelSubscriptionIDInput | null,
  and?: Array< ModelSubscriptionTrailProgressFilterInput | null > | null,
  or?: Array< ModelSubscriptionTrailProgressFilterInput | null > | null,
};

export type ModelSubscriptionLocationFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  name?: ModelSubscriptionStringInput | null,
  address?: ModelSubscriptionStringInput | null,
  description?: ModelSubscriptionStringInput | null,
  coordinates?: ModelSubscriptionFloatInput | null,
  userID?: ModelSubscriptionIDInput | null,
  and?: Array< ModelSubscriptionLocationFilterInput | null > | null,
  or?: Array< ModelSubscriptionLocationFilterInput | null > | null,
};

export type ModelSubscriptionLocationViewFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  name?: ModelSubscriptionStringInput | null,
  address?: ModelSubscriptionStringInput | null,
  description?: ModelSubscriptionStringInput | null,
  coordinates?: ModelSubscriptionFloatInput | null,
  locationID?: ModelSubscriptionIDInput | null,
  userID?: ModelSubscriptionIDInput | null,
  and?: Array< ModelSubscriptionLocationViewFilterInput | null > | null,
  or?: Array< ModelSubscriptionLocationViewFilterInput | null > | null,
};

export type ModelSubscriptionTransactionFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  status?: ModelSubscriptionStringInput | null,
  amount?: ModelSubscriptionStringInput | null,
  description?: ModelSubscriptionStringInput | null,
  type?: ModelSubscriptionStringInput | null,
  senderID?: ModelSubscriptionIDInput | null,
  verifierID?: ModelSubscriptionIDInput | null,
  receiverID?: ModelSubscriptionIDInput | null,
  expiration?: ModelSubscriptionStringInput | null,
  fulfillment?: ModelSubscriptionStringInput | null,
  condition?: ModelSubscriptionStringInput | null,
  fromWalletID?: ModelSubscriptionIDInput | null,
  toWalletID?: ModelSubscriptionIDInput | null,
  xummTransactionID?: ModelSubscriptionStringInput | null,
  locationViewID?: ModelSubscriptionIDInput | null,
  rewardID?: ModelSubscriptionIDInput | null,
  and?: Array< ModelSubscriptionTransactionFilterInput | null > | null,
  or?: Array< ModelSubscriptionTransactionFilterInput | null > | null,
};

export type ModelSubscriptionTransactionProgressFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  userID?: ModelSubscriptionIDInput | null,
  transactionID?: ModelSubscriptionIDInput | null,
  status?: ModelSubscriptionStringInput | null,
  and?: Array< ModelSubscriptionTransactionProgressFilterInput | null > | null,
  or?: Array< ModelSubscriptionTransactionProgressFilterInput | null > | null,
};

export type ModelSubscriptionConditionFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  type?: ModelSubscriptionStringInput | null,
  latitude?: ModelSubscriptionStringInput | null,
  longitude?: ModelSubscriptionStringInput | null,
  question?: ModelSubscriptionStringInput | null,
  choices?: ModelSubscriptionStringInput | null,
  answerIndex?: ModelSubscriptionIntInput | null,
  status?: ModelSubscriptionStringInput | null,
  transactionID?: ModelSubscriptionIDInput | null,
  and?: Array< ModelSubscriptionConditionFilterInput | null > | null,
  or?: Array< ModelSubscriptionConditionFilterInput | null > | null,
};

export type ModelSubscriptionIntInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  in?: Array< number | null > | null,
  notIn?: Array< number | null > | null,
};

export type ModelSubscriptionConditionProgressFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  userID?: ModelSubscriptionIDInput | null,
  conditionID?: ModelSubscriptionIDInput | null,
  status?: ModelSubscriptionStringInput | null,
  and?: Array< ModelSubscriptionConditionProgressFilterInput | null > | null,
  or?: Array< ModelSubscriptionConditionProgressFilterInput | null > | null,
};

export type ModelSubscriptionAssetFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  name?: ModelSubscriptionStringInput | null,
  userID?: ModelSubscriptionIDInput | null,
  and?: Array< ModelSubscriptionAssetFilterInput | null > | null,
  or?: Array< ModelSubscriptionAssetFilterInput | null > | null,
};

export type ModelSubscriptionRewardFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  name?: ModelSubscriptionStringInput | null,
  and?: Array< ModelSubscriptionRewardFilterInput | null > | null,
  or?: Array< ModelSubscriptionRewardFilterInput | null > | null,
};

export type ModelSubscriptionTrailLocationViewsFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  trailID?: ModelSubscriptionIDInput | null,
  locationViewID?: ModelSubscriptionIDInput | null,
  and?: Array< ModelSubscriptionTrailLocationViewsFilterInput | null > | null,
  or?: Array< ModelSubscriptionTrailLocationViewsFilterInput | null > | null,
};

export type CreateUserMutationVariables = {
  input: CreateUserInput,
  condition?: ModelUserConditionInput | null,
};

export type CreateUserMutation = {
  createUser?:  {
    __typename: "User",
    id: string,
    username: string,
    email: string,
    bio?: string | null,
    location?: string | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    backgroundImage?: string | null,
    wallets?:  {
      __typename: "ModelWalletConnection",
      items:  Array< {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateUserMutationVariables = {
  input: UpdateUserInput,
  condition?: ModelUserConditionInput | null,
};

export type UpdateUserMutation = {
  updateUser?:  {
    __typename: "User",
    id: string,
    username: string,
    email: string,
    bio?: string | null,
    location?: string | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    backgroundImage?: string | null,
    wallets?:  {
      __typename: "ModelWalletConnection",
      items:  Array< {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteUserMutationVariables = {
  input: DeleteUserInput,
  condition?: ModelUserConditionInput | null,
};

export type DeleteUserMutation = {
  deleteUser?:  {
    __typename: "User",
    id: string,
    username: string,
    email: string,
    bio?: string | null,
    location?: string | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    backgroundImage?: string | null,
    wallets?:  {
      __typename: "ModelWalletConnection",
      items:  Array< {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateWalletMutationVariables = {
  input: CreateWalletInput,
  condition?: ModelWalletConditionInput | null,
};

export type CreateWalletMutation = {
  createWallet?:  {
    __typename: "Wallet",
    id: string,
    title: string,
    images?: Array< string | null > | null,
    description?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateWalletMutationVariables = {
  input: UpdateWalletInput,
  condition?: ModelWalletConditionInput | null,
};

export type UpdateWalletMutation = {
  updateWallet?:  {
    __typename: "Wallet",
    id: string,
    title: string,
    images?: Array< string | null > | null,
    description?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteWalletMutationVariables = {
  input: DeleteWalletInput,
  condition?: ModelWalletConditionInput | null,
};

export type DeleteWalletMutation = {
  deleteWallet?:  {
    __typename: "Wallet",
    id: string,
    title: string,
    images?: Array< string | null > | null,
    description?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateProductMutationVariables = {
  input: CreateProductInput,
  condition?: ModelProductConditionInput | null,
};

export type CreateProductMutation = {
  createProduct?:  {
    __typename: "Product",
    id: string,
    title: string,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    description?: string | null,
    price: number,
    category?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    nft?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateProductMutationVariables = {
  input: UpdateProductInput,
  condition?: ModelProductConditionInput | null,
};

export type UpdateProductMutation = {
  updateProduct?:  {
    __typename: "Product",
    id: string,
    title: string,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    description?: string | null,
    price: number,
    category?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    nft?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteProductMutationVariables = {
  input: DeleteProductInput,
  condition?: ModelProductConditionInput | null,
};

export type DeleteProductMutation = {
  deleteProduct?:  {
    __typename: "Product",
    id: string,
    title: string,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    description?: string | null,
    price: number,
    category?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    nft?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateTrailMutationVariables = {
  input: CreateTrailInput,
  condition?: ModelTrailConditionInput | null,
};

export type CreateTrailMutation = {
  createTrail?:  {
    __typename: "Trail",
    id: string,
    name?: string | null,
    description?: string | null,
    duration?: string | null,
    type?: TrailType | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    locations?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    order?: Array< string | null > | null,
    trailProgress?:  {
      __typename: "ModelTrailProgressConnection",
      items:  Array< {
        __typename: "TrailProgress",
        id: string,
        name?: string | null,
        userID: string,
        status: TrailProgressStatus,
        trailID: string,
        order?: Array< string | null > | null,
        createdAt: string,
        updatedAt: string,
        trailTrailProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateTrailMutationVariables = {
  input: UpdateTrailInput,
  condition?: ModelTrailConditionInput | null,
};

export type UpdateTrailMutation = {
  updateTrail?:  {
    __typename: "Trail",
    id: string,
    name?: string | null,
    description?: string | null,
    duration?: string | null,
    type?: TrailType | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    locations?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    order?: Array< string | null > | null,
    trailProgress?:  {
      __typename: "ModelTrailProgressConnection",
      items:  Array< {
        __typename: "TrailProgress",
        id: string,
        name?: string | null,
        userID: string,
        status: TrailProgressStatus,
        trailID: string,
        order?: Array< string | null > | null,
        createdAt: string,
        updatedAt: string,
        trailTrailProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteTrailMutationVariables = {
  input: DeleteTrailInput,
  condition?: ModelTrailConditionInput | null,
};

export type DeleteTrailMutation = {
  deleteTrail?:  {
    __typename: "Trail",
    id: string,
    name?: string | null,
    description?: string | null,
    duration?: string | null,
    type?: TrailType | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    locations?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    order?: Array< string | null > | null,
    trailProgress?:  {
      __typename: "ModelTrailProgressConnection",
      items:  Array< {
        __typename: "TrailProgress",
        id: string,
        name?: string | null,
        userID: string,
        status: TrailProgressStatus,
        trailID: string,
        order?: Array< string | null > | null,
        createdAt: string,
        updatedAt: string,
        trailTrailProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateTrailProgressMutationVariables = {
  input: CreateTrailProgressInput,
  condition?: ModelTrailProgressConditionInput | null,
};

export type CreateTrailProgressMutation = {
  createTrailProgress?:  {
    __typename: "TrailProgress",
    id: string,
    name?: string | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TrailProgressStatus,
    trailID: string,
    trail?:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    order?: Array< string | null > | null,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    trailTrailProgressId?: string | null,
  } | null,
};

export type UpdateTrailProgressMutationVariables = {
  input: UpdateTrailProgressInput,
  condition?: ModelTrailProgressConditionInput | null,
};

export type UpdateTrailProgressMutation = {
  updateTrailProgress?:  {
    __typename: "TrailProgress",
    id: string,
    name?: string | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TrailProgressStatus,
    trailID: string,
    trail?:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    order?: Array< string | null > | null,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    trailTrailProgressId?: string | null,
  } | null,
};

export type DeleteTrailProgressMutationVariables = {
  input: DeleteTrailProgressInput,
  condition?: ModelTrailProgressConditionInput | null,
};

export type DeleteTrailProgressMutation = {
  deleteTrailProgress?:  {
    __typename: "TrailProgress",
    id: string,
    name?: string | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TrailProgressStatus,
    trailID: string,
    trail?:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    order?: Array< string | null > | null,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    trailTrailProgressId?: string | null,
  } | null,
};

export type CreateLocationMutationVariables = {
  input: CreateLocationInput,
  condition?: ModelLocationConditionInput | null,
};

export type CreateLocationMutation = {
  createLocation?:  {
    __typename: "Location",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLocationMutationVariables = {
  input: UpdateLocationInput,
  condition?: ModelLocationConditionInput | null,
};

export type UpdateLocationMutation = {
  updateLocation?:  {
    __typename: "Location",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLocationMutationVariables = {
  input: DeleteLocationInput,
  condition?: ModelLocationConditionInput | null,
};

export type DeleteLocationMutation = {
  deleteLocation?:  {
    __typename: "Location",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLocationViewMutationVariables = {
  input: CreateLocationViewInput,
  condition?: ModelLocationViewConditionInput | null,
};

export type CreateLocationViewMutation = {
  createLocationView?:  {
    __typename: "LocationView",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    locationID: string,
    location?:  {
      __typename: "Location",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactions?:  {
      __typename: "ModelTransactionConnection",
      items:  Array< {
        __typename: "Transaction",
        id: string,
        status: string,
        amount?: string | null,
        description?: string | null,
        type: TransactionType,
        senderID: string,
        verifierID?: string | null,
        receiverID?: string | null,
        expiration?: string | null,
        fulfillment?: string | null,
        condition?: string | null,
        fromWalletID?: string | null,
        toWalletID?: string | null,
        xummTransactionID?: string | null,
        locationViewID?: string | null,
        rewardID?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    trails?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLocationViewMutationVariables = {
  input: UpdateLocationViewInput,
  condition?: ModelLocationViewConditionInput | null,
};

export type UpdateLocationViewMutation = {
  updateLocationView?:  {
    __typename: "LocationView",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    locationID: string,
    location?:  {
      __typename: "Location",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactions?:  {
      __typename: "ModelTransactionConnection",
      items:  Array< {
        __typename: "Transaction",
        id: string,
        status: string,
        amount?: string | null,
        description?: string | null,
        type: TransactionType,
        senderID: string,
        verifierID?: string | null,
        receiverID?: string | null,
        expiration?: string | null,
        fulfillment?: string | null,
        condition?: string | null,
        fromWalletID?: string | null,
        toWalletID?: string | null,
        xummTransactionID?: string | null,
        locationViewID?: string | null,
        rewardID?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    trails?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLocationViewMutationVariables = {
  input: DeleteLocationViewInput,
  condition?: ModelLocationViewConditionInput | null,
};

export type DeleteLocationViewMutation = {
  deleteLocationView?:  {
    __typename: "LocationView",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    locationID: string,
    location?:  {
      __typename: "Location",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactions?:  {
      __typename: "ModelTransactionConnection",
      items:  Array< {
        __typename: "Transaction",
        id: string,
        status: string,
        amount?: string | null,
        description?: string | null,
        type: TransactionType,
        senderID: string,
        verifierID?: string | null,
        receiverID?: string | null,
        expiration?: string | null,
        fulfillment?: string | null,
        condition?: string | null,
        fromWalletID?: string | null,
        toWalletID?: string | null,
        xummTransactionID?: string | null,
        locationViewID?: string | null,
        rewardID?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    trails?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateTransactionMutationVariables = {
  input: CreateTransactionInput,
  condition?: ModelTransactionConditionInput | null,
};

export type CreateTransactionMutation = {
  createTransaction?:  {
    __typename: "Transaction",
    id: string,
    status: string,
    amount?: string | null,
    description?: string | null,
    type: TransactionType,
    senderID: string,
    verifierID?: string | null,
    receiverID?: string | null,
    conditions?:  {
      __typename: "ModelConditionConnection",
      items:  Array< {
        __typename: "Condition",
        id: string,
        type: ConditionType,
        latitude?: string | null,
        longitude?: string | null,
        question?: string | null,
        choices?: Array< string | null > | null,
        answerIndex?: number | null,
        status: string,
        transactionID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    expiration?: string | null,
    sender?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    verifier?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    receiver?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fulfillment?: string | null,
    condition?: string | null,
    fromWalletID?: string | null,
    toWalletID?: string | null,
    toWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fromWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    xummTransactionID?: string | null,
    locationViewID?: string | null,
    rewardID?: string | null,
    reward?:  {
      __typename: "Reward",
      id: string,
      name?: string | null,
      file?:  {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactionProgress?:  {
      __typename: "ModelTransactionProgressConnection",
      items:  Array< {
        __typename: "TransactionProgress",
        id: string,
        userID: string,
        transactionID: string,
        status: TransactionProgressStatus,
        createdAt: string,
        updatedAt: string,
        transactionTransactionProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateTransactionMutationVariables = {
  input: UpdateTransactionInput,
  condition?: ModelTransactionConditionInput | null,
};

export type UpdateTransactionMutation = {
  updateTransaction?:  {
    __typename: "Transaction",
    id: string,
    status: string,
    amount?: string | null,
    description?: string | null,
    type: TransactionType,
    senderID: string,
    verifierID?: string | null,
    receiverID?: string | null,
    conditions?:  {
      __typename: "ModelConditionConnection",
      items:  Array< {
        __typename: "Condition",
        id: string,
        type: ConditionType,
        latitude?: string | null,
        longitude?: string | null,
        question?: string | null,
        choices?: Array< string | null > | null,
        answerIndex?: number | null,
        status: string,
        transactionID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    expiration?: string | null,
    sender?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    verifier?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    receiver?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fulfillment?: string | null,
    condition?: string | null,
    fromWalletID?: string | null,
    toWalletID?: string | null,
    toWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fromWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    xummTransactionID?: string | null,
    locationViewID?: string | null,
    rewardID?: string | null,
    reward?:  {
      __typename: "Reward",
      id: string,
      name?: string | null,
      file?:  {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactionProgress?:  {
      __typename: "ModelTransactionProgressConnection",
      items:  Array< {
        __typename: "TransactionProgress",
        id: string,
        userID: string,
        transactionID: string,
        status: TransactionProgressStatus,
        createdAt: string,
        updatedAt: string,
        transactionTransactionProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteTransactionMutationVariables = {
  input: DeleteTransactionInput,
  condition?: ModelTransactionConditionInput | null,
};

export type DeleteTransactionMutation = {
  deleteTransaction?:  {
    __typename: "Transaction",
    id: string,
    status: string,
    amount?: string | null,
    description?: string | null,
    type: TransactionType,
    senderID: string,
    verifierID?: string | null,
    receiverID?: string | null,
    conditions?:  {
      __typename: "ModelConditionConnection",
      items:  Array< {
        __typename: "Condition",
        id: string,
        type: ConditionType,
        latitude?: string | null,
        longitude?: string | null,
        question?: string | null,
        choices?: Array< string | null > | null,
        answerIndex?: number | null,
        status: string,
        transactionID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    expiration?: string | null,
    sender?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    verifier?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    receiver?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fulfillment?: string | null,
    condition?: string | null,
    fromWalletID?: string | null,
    toWalletID?: string | null,
    toWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fromWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    xummTransactionID?: string | null,
    locationViewID?: string | null,
    rewardID?: string | null,
    reward?:  {
      __typename: "Reward",
      id: string,
      name?: string | null,
      file?:  {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactionProgress?:  {
      __typename: "ModelTransactionProgressConnection",
      items:  Array< {
        __typename: "TransactionProgress",
        id: string,
        userID: string,
        transactionID: string,
        status: TransactionProgressStatus,
        createdAt: string,
        updatedAt: string,
        transactionTransactionProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateTransactionProgressMutationVariables = {
  input: CreateTransactionProgressInput,
  condition?: ModelTransactionProgressConditionInput | null,
};

export type CreateTransactionProgressMutation = {
  createTransactionProgress?:  {
    __typename: "TransactionProgress",
    id: string,
    userID: string,
    transactionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transaction?:  {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TransactionProgressStatus,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    transactionTransactionProgressId?: string | null,
  } | null,
};

export type UpdateTransactionProgressMutationVariables = {
  input: UpdateTransactionProgressInput,
  condition?: ModelTransactionProgressConditionInput | null,
};

export type UpdateTransactionProgressMutation = {
  updateTransactionProgress?:  {
    __typename: "TransactionProgress",
    id: string,
    userID: string,
    transactionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transaction?:  {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TransactionProgressStatus,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    transactionTransactionProgressId?: string | null,
  } | null,
};

export type DeleteTransactionProgressMutationVariables = {
  input: DeleteTransactionProgressInput,
  condition?: ModelTransactionProgressConditionInput | null,
};

export type DeleteTransactionProgressMutation = {
  deleteTransactionProgress?:  {
    __typename: "TransactionProgress",
    id: string,
    userID: string,
    transactionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transaction?:  {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TransactionProgressStatus,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    transactionTransactionProgressId?: string | null,
  } | null,
};

export type CreateConditionMutationVariables = {
  input: CreateConditionInput,
  condition?: ModelConditionConditionInput | null,
};

export type CreateConditionMutation = {
  createCondition?:  {
    __typename: "Condition",
    id: string,
    type: ConditionType,
    latitude?: string | null,
    longitude?: string | null,
    question?: string | null,
    choices?: Array< string | null > | null,
    answerIndex?: number | null,
    status: string,
    transactionID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateConditionMutationVariables = {
  input: UpdateConditionInput,
  condition?: ModelConditionConditionInput | null,
};

export type UpdateConditionMutation = {
  updateCondition?:  {
    __typename: "Condition",
    id: string,
    type: ConditionType,
    latitude?: string | null,
    longitude?: string | null,
    question?: string | null,
    choices?: Array< string | null > | null,
    answerIndex?: number | null,
    status: string,
    transactionID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteConditionMutationVariables = {
  input: DeleteConditionInput,
  condition?: ModelConditionConditionInput | null,
};

export type DeleteConditionMutation = {
  deleteCondition?:  {
    __typename: "Condition",
    id: string,
    type: ConditionType,
    latitude?: string | null,
    longitude?: string | null,
    question?: string | null,
    choices?: Array< string | null > | null,
    answerIndex?: number | null,
    status: string,
    transactionID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateConditionProgressMutationVariables = {
  input: CreateConditionProgressInput,
  condition?: ModelConditionProgressConditionInput | null,
};

export type CreateConditionProgressMutation = {
  createConditionProgress?:  {
    __typename: "ConditionProgress",
    id: string,
    userID: string,
    conditionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    condition?:  {
      __typename: "Condition",
      id: string,
      type: ConditionType,
      latitude?: string | null,
      longitude?: string | null,
      question?: string | null,
      choices?: Array< string | null > | null,
      answerIndex?: number | null,
      status: string,
      transactionID: string,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: ConditionProgressStatus,
    createdAt: string,
    updatedAt: string,
    trailProgressCompletedConditionsId?: string | null,
    transactionProgressCompletedConditionsId?: string | null,
  } | null,
};

export type UpdateConditionProgressMutationVariables = {
  input: UpdateConditionProgressInput,
  condition?: ModelConditionProgressConditionInput | null,
};

export type UpdateConditionProgressMutation = {
  updateConditionProgress?:  {
    __typename: "ConditionProgress",
    id: string,
    userID: string,
    conditionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    condition?:  {
      __typename: "Condition",
      id: string,
      type: ConditionType,
      latitude?: string | null,
      longitude?: string | null,
      question?: string | null,
      choices?: Array< string | null > | null,
      answerIndex?: number | null,
      status: string,
      transactionID: string,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: ConditionProgressStatus,
    createdAt: string,
    updatedAt: string,
    trailProgressCompletedConditionsId?: string | null,
    transactionProgressCompletedConditionsId?: string | null,
  } | null,
};

export type DeleteConditionProgressMutationVariables = {
  input: DeleteConditionProgressInput,
  condition?: ModelConditionProgressConditionInput | null,
};

export type DeleteConditionProgressMutation = {
  deleteConditionProgress?:  {
    __typename: "ConditionProgress",
    id: string,
    userID: string,
    conditionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    condition?:  {
      __typename: "Condition",
      id: string,
      type: ConditionType,
      latitude?: string | null,
      longitude?: string | null,
      question?: string | null,
      choices?: Array< string | null > | null,
      answerIndex?: number | null,
      status: string,
      transactionID: string,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: ConditionProgressStatus,
    createdAt: string,
    updatedAt: string,
    trailProgressCompletedConditionsId?: string | null,
    transactionProgressCompletedConditionsId?: string | null,
  } | null,
};

export type CreateAssetMutationVariables = {
  input: CreateAssetInput,
  condition?: ModelAssetConditionInput | null,
};

export type CreateAssetMutation = {
  createAsset?:  {
    __typename: "Asset",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    userID: string,
    owner?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateAssetMutationVariables = {
  input: UpdateAssetInput,
  condition?: ModelAssetConditionInput | null,
};

export type UpdateAssetMutation = {
  updateAsset?:  {
    __typename: "Asset",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    userID: string,
    owner?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteAssetMutationVariables = {
  input: DeleteAssetInput,
  condition?: ModelAssetConditionInput | null,
};

export type DeleteAssetMutation = {
  deleteAsset?:  {
    __typename: "Asset",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    userID: string,
    owner?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateRewardMutationVariables = {
  input: CreateRewardInput,
  condition?: ModelRewardConditionInput | null,
};

export type CreateRewardMutation = {
  createReward?:  {
    __typename: "Reward",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateRewardMutationVariables = {
  input: UpdateRewardInput,
  condition?: ModelRewardConditionInput | null,
};

export type UpdateRewardMutation = {
  updateReward?:  {
    __typename: "Reward",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteRewardMutationVariables = {
  input: DeleteRewardInput,
  condition?: ModelRewardConditionInput | null,
};

export type DeleteRewardMutation = {
  deleteReward?:  {
    __typename: "Reward",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateTrailLocationViewsMutationVariables = {
  input: CreateTrailLocationViewsInput,
  condition?: ModelTrailLocationViewsConditionInput | null,
};

export type CreateTrailLocationViewsMutation = {
  createTrailLocationViews?:  {
    __typename: "TrailLocationViews",
    id: string,
    trailID: string,
    locationViewID: string,
    trail:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    locationView:  {
      __typename: "LocationView",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      locationID: string,
      location?:  {
        __typename: "Location",
        id: string,
        name?: string | null,
        address?: string | null,
        description?: string | null,
        coordinates?: Array< number | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactions?:  {
        __typename: "ModelTransactionConnection",
        nextToken?: string | null,
      } | null,
      trails?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateTrailLocationViewsMutationVariables = {
  input: UpdateTrailLocationViewsInput,
  condition?: ModelTrailLocationViewsConditionInput | null,
};

export type UpdateTrailLocationViewsMutation = {
  updateTrailLocationViews?:  {
    __typename: "TrailLocationViews",
    id: string,
    trailID: string,
    locationViewID: string,
    trail:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    locationView:  {
      __typename: "LocationView",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      locationID: string,
      location?:  {
        __typename: "Location",
        id: string,
        name?: string | null,
        address?: string | null,
        description?: string | null,
        coordinates?: Array< number | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactions?:  {
        __typename: "ModelTransactionConnection",
        nextToken?: string | null,
      } | null,
      trails?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteTrailLocationViewsMutationVariables = {
  input: DeleteTrailLocationViewsInput,
  condition?: ModelTrailLocationViewsConditionInput | null,
};

export type DeleteTrailLocationViewsMutation = {
  deleteTrailLocationViews?:  {
    __typename: "TrailLocationViews",
    id: string,
    trailID: string,
    locationViewID: string,
    trail:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    locationView:  {
      __typename: "LocationView",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      locationID: string,
      location?:  {
        __typename: "Location",
        id: string,
        name?: string | null,
        address?: string | null,
        description?: string | null,
        coordinates?: Array< number | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactions?:  {
        __typename: "ModelTransactionConnection",
        nextToken?: string | null,
      } | null,
      trails?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type GetUserQueryVariables = {
  id: string,
};

export type GetUserQuery = {
  getUser?:  {
    __typename: "User",
    id: string,
    username: string,
    email: string,
    bio?: string | null,
    location?: string | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    backgroundImage?: string | null,
    wallets?:  {
      __typename: "ModelWalletConnection",
      items:  Array< {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListUsersQueryVariables = {
  filter?: ModelUserFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListUsersQuery = {
  listUsers?:  {
    __typename: "ModelUserConnection",
    items:  Array< {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetWalletQueryVariables = {
  id: string,
};

export type GetWalletQuery = {
  getWallet?:  {
    __typename: "Wallet",
    id: string,
    title: string,
    images?: Array< string | null > | null,
    description?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListWalletsQueryVariables = {
  filter?: ModelWalletFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListWalletsQuery = {
  listWallets?:  {
    __typename: "ModelWalletConnection",
    items:  Array< {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetProductQueryVariables = {
  id: string,
};

export type GetProductQuery = {
  getProduct?:  {
    __typename: "Product",
    id: string,
    title: string,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    description?: string | null,
    price: number,
    category?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    nft?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListProductsQueryVariables = {
  filter?: ModelProductFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListProductsQuery = {
  listProducts?:  {
    __typename: "ModelProductConnection",
    items:  Array< {
      __typename: "Product",
      id: string,
      title: string,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      description?: string | null,
      price: number,
      category?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      nft?: string | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetTrailQueryVariables = {
  id: string,
};

export type GetTrailQuery = {
  getTrail?:  {
    __typename: "Trail",
    id: string,
    name?: string | null,
    description?: string | null,
    duration?: string | null,
    type?: TrailType | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    locations?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    order?: Array< string | null > | null,
    trailProgress?:  {
      __typename: "ModelTrailProgressConnection",
      items:  Array< {
        __typename: "TrailProgress",
        id: string,
        name?: string | null,
        userID: string,
        status: TrailProgressStatus,
        trailID: string,
        order?: Array< string | null > | null,
        createdAt: string,
        updatedAt: string,
        trailTrailProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListTrailsQueryVariables = {
  filter?: ModelTrailFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListTrailsQuery = {
  listTrails?:  {
    __typename: "ModelTrailConnection",
    items:  Array< {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetTrailProgressQueryVariables = {
  id: string,
};

export type GetTrailProgressQuery = {
  getTrailProgress?:  {
    __typename: "TrailProgress",
    id: string,
    name?: string | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TrailProgressStatus,
    trailID: string,
    trail?:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    order?: Array< string | null > | null,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    trailTrailProgressId?: string | null,
  } | null,
};

export type ListTrailProgressesQueryVariables = {
  filter?: ModelTrailProgressFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListTrailProgressesQuery = {
  listTrailProgresses?:  {
    __typename: "ModelTrailProgressConnection",
    items:  Array< {
      __typename: "TrailProgress",
      id: string,
      name?: string | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      status: TrailProgressStatus,
      trailID: string,
      trail?:  {
        __typename: "Trail",
        id: string,
        name?: string | null,
        description?: string | null,
        duration?: string | null,
        type?: TrailType | null,
        order?: Array< string | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      order?: Array< string | null > | null,
      completedConditions?:  {
        __typename: "ModelConditionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
      trailTrailProgressId?: string | null,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetLocationQueryVariables = {
  id: string,
};

export type GetLocationQuery = {
  getLocation?:  {
    __typename: "Location",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLocationsQueryVariables = {
  filter?: ModelLocationFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLocationsQuery = {
  listLocations?:  {
    __typename: "ModelLocationConnection",
    items:  Array< {
      __typename: "Location",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetLocationViewQueryVariables = {
  id: string,
};

export type GetLocationViewQuery = {
  getLocationView?:  {
    __typename: "LocationView",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    locationID: string,
    location?:  {
      __typename: "Location",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactions?:  {
      __typename: "ModelTransactionConnection",
      items:  Array< {
        __typename: "Transaction",
        id: string,
        status: string,
        amount?: string | null,
        description?: string | null,
        type: TransactionType,
        senderID: string,
        verifierID?: string | null,
        receiverID?: string | null,
        expiration?: string | null,
        fulfillment?: string | null,
        condition?: string | null,
        fromWalletID?: string | null,
        toWalletID?: string | null,
        xummTransactionID?: string | null,
        locationViewID?: string | null,
        rewardID?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    trails?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLocationViewsQueryVariables = {
  filter?: ModelLocationViewFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLocationViewsQuery = {
  listLocationViews?:  {
    __typename: "ModelLocationViewConnection",
    items:  Array< {
      __typename: "LocationView",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      locationID: string,
      location?:  {
        __typename: "Location",
        id: string,
        name?: string | null,
        address?: string | null,
        description?: string | null,
        coordinates?: Array< number | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactions?:  {
        __typename: "ModelTransactionConnection",
        nextToken?: string | null,
      } | null,
      trails?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetTransactionQueryVariables = {
  id: string,
};

export type GetTransactionQuery = {
  getTransaction?:  {
    __typename: "Transaction",
    id: string,
    status: string,
    amount?: string | null,
    description?: string | null,
    type: TransactionType,
    senderID: string,
    verifierID?: string | null,
    receiverID?: string | null,
    conditions?:  {
      __typename: "ModelConditionConnection",
      items:  Array< {
        __typename: "Condition",
        id: string,
        type: ConditionType,
        latitude?: string | null,
        longitude?: string | null,
        question?: string | null,
        choices?: Array< string | null > | null,
        answerIndex?: number | null,
        status: string,
        transactionID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    expiration?: string | null,
    sender?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    verifier?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    receiver?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fulfillment?: string | null,
    condition?: string | null,
    fromWalletID?: string | null,
    toWalletID?: string | null,
    toWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fromWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    xummTransactionID?: string | null,
    locationViewID?: string | null,
    rewardID?: string | null,
    reward?:  {
      __typename: "Reward",
      id: string,
      name?: string | null,
      file?:  {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactionProgress?:  {
      __typename: "ModelTransactionProgressConnection",
      items:  Array< {
        __typename: "TransactionProgress",
        id: string,
        userID: string,
        transactionID: string,
        status: TransactionProgressStatus,
        createdAt: string,
        updatedAt: string,
        transactionTransactionProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListTransactionsQueryVariables = {
  filter?: ModelTransactionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListTransactionsQuery = {
  listTransactions?:  {
    __typename: "ModelTransactionConnection",
    items:  Array< {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetTransactionProgressQueryVariables = {
  id: string,
};

export type GetTransactionProgressQuery = {
  getTransactionProgress?:  {
    __typename: "TransactionProgress",
    id: string,
    userID: string,
    transactionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transaction?:  {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TransactionProgressStatus,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    transactionTransactionProgressId?: string | null,
  } | null,
};

export type ListTransactionProgressesQueryVariables = {
  filter?: ModelTransactionProgressFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListTransactionProgressesQuery = {
  listTransactionProgresses?:  {
    __typename: "ModelTransactionProgressConnection",
    items:  Array< {
      __typename: "TransactionProgress",
      id: string,
      userID: string,
      transactionID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transaction?:  {
        __typename: "Transaction",
        id: string,
        status: string,
        amount?: string | null,
        description?: string | null,
        type: TransactionType,
        senderID: string,
        verifierID?: string | null,
        receiverID?: string | null,
        expiration?: string | null,
        fulfillment?: string | null,
        condition?: string | null,
        fromWalletID?: string | null,
        toWalletID?: string | null,
        xummTransactionID?: string | null,
        locationViewID?: string | null,
        rewardID?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      status: TransactionProgressStatus,
      completedConditions?:  {
        __typename: "ModelConditionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
      transactionTransactionProgressId?: string | null,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetConditionQueryVariables = {
  id: string,
};

export type GetConditionQuery = {
  getCondition?:  {
    __typename: "Condition",
    id: string,
    type: ConditionType,
    latitude?: string | null,
    longitude?: string | null,
    question?: string | null,
    choices?: Array< string | null > | null,
    answerIndex?: number | null,
    status: string,
    transactionID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListConditionsQueryVariables = {
  filter?: ModelConditionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListConditionsQuery = {
  listConditions?:  {
    __typename: "ModelConditionConnection",
    items:  Array< {
      __typename: "Condition",
      id: string,
      type: ConditionType,
      latitude?: string | null,
      longitude?: string | null,
      question?: string | null,
      choices?: Array< string | null > | null,
      answerIndex?: number | null,
      status: string,
      transactionID: string,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetConditionProgressQueryVariables = {
  id: string,
};

export type GetConditionProgressQuery = {
  getConditionProgress?:  {
    __typename: "ConditionProgress",
    id: string,
    userID: string,
    conditionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    condition?:  {
      __typename: "Condition",
      id: string,
      type: ConditionType,
      latitude?: string | null,
      longitude?: string | null,
      question?: string | null,
      choices?: Array< string | null > | null,
      answerIndex?: number | null,
      status: string,
      transactionID: string,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: ConditionProgressStatus,
    createdAt: string,
    updatedAt: string,
    trailProgressCompletedConditionsId?: string | null,
    transactionProgressCompletedConditionsId?: string | null,
  } | null,
};

export type ListConditionProgressesQueryVariables = {
  filter?: ModelConditionProgressFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListConditionProgressesQuery = {
  listConditionProgresses?:  {
    __typename: "ModelConditionProgressConnection",
    items:  Array< {
      __typename: "ConditionProgress",
      id: string,
      userID: string,
      conditionID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      condition?:  {
        __typename: "Condition",
        id: string,
        type: ConditionType,
        latitude?: string | null,
        longitude?: string | null,
        question?: string | null,
        choices?: Array< string | null > | null,
        answerIndex?: number | null,
        status: string,
        transactionID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      status: ConditionProgressStatus,
      createdAt: string,
      updatedAt: string,
      trailProgressCompletedConditionsId?: string | null,
      transactionProgressCompletedConditionsId?: string | null,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetAssetQueryVariables = {
  id: string,
};

export type GetAssetQuery = {
  getAsset?:  {
    __typename: "Asset",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    userID: string,
    owner?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListAssetsQueryVariables = {
  filter?: ModelAssetFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListAssetsQuery = {
  listAssets?:  {
    __typename: "ModelAssetConnection",
    items:  Array< {
      __typename: "Asset",
      id: string,
      name?: string | null,
      file?:  {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null,
      userID: string,
      owner?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetRewardQueryVariables = {
  id: string,
};

export type GetRewardQuery = {
  getReward?:  {
    __typename: "Reward",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListRewardsQueryVariables = {
  filter?: ModelRewardFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListRewardsQuery = {
  listRewards?:  {
    __typename: "ModelRewardConnection",
    items:  Array< {
      __typename: "Reward",
      id: string,
      name?: string | null,
      file?:  {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetTrailLocationViewsQueryVariables = {
  id: string,
};

export type GetTrailLocationViewsQuery = {
  getTrailLocationViews?:  {
    __typename: "TrailLocationViews",
    id: string,
    trailID: string,
    locationViewID: string,
    trail:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    locationView:  {
      __typename: "LocationView",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      locationID: string,
      location?:  {
        __typename: "Location",
        id: string,
        name?: string | null,
        address?: string | null,
        description?: string | null,
        coordinates?: Array< number | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactions?:  {
        __typename: "ModelTransactionConnection",
        nextToken?: string | null,
      } | null,
      trails?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListTrailLocationViewsQueryVariables = {
  filter?: ModelTrailLocationViewsFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListTrailLocationViewsQuery = {
  listTrailLocationViews?:  {
    __typename: "ModelTrailLocationViewsConnection",
    items:  Array< {
      __typename: "TrailLocationViews",
      id: string,
      trailID: string,
      locationViewID: string,
      trail:  {
        __typename: "Trail",
        id: string,
        name?: string | null,
        description?: string | null,
        duration?: string | null,
        type?: TrailType | null,
        order?: Array< string | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      },
      locationView:  {
        __typename: "LocationView",
        id: string,
        name?: string | null,
        address?: string | null,
        description?: string | null,
        coordinates?: Array< number | null > | null,
        locationID: string,
        userID: string,
        createdAt: string,
        updatedAt: string,
      },
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type WalletsByUserQueryVariables = {
  userID: string,
  id?: ModelIDKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelWalletFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type WalletsByUserQuery = {
  walletsByUser?:  {
    __typename: "ModelWalletConnection",
    items:  Array< {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type WalletsByAddressQueryVariables = {
  address: string,
  id?: ModelIDKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelWalletFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type WalletsByAddressQuery = {
  walletsByAddress?:  {
    __typename: "ModelWalletConnection",
    items:  Array< {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type TransactionsBySenderIDQueryVariables = {
  senderID: string,
  id?: ModelIDKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelTransactionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type TransactionsBySenderIDQuery = {
  transactionsBySenderID?:  {
    __typename: "ModelTransactionConnection",
    items:  Array< {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type TransactionsByVerifierIDQueryVariables = {
  verifierID: string,
  id?: ModelIDKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelTransactionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type TransactionsByVerifierIDQuery = {
  transactionsByVerifierID?:  {
    __typename: "ModelTransactionConnection",
    items:  Array< {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type TransactionsByReceiverIDQueryVariables = {
  receiverID: string,
  id?: ModelIDKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelTransactionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type TransactionsByReceiverIDQuery = {
  transactionsByReceiverID?:  {
    __typename: "ModelTransactionConnection",
    items:  Array< {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type TransactionsByFromToWalletIDQueryVariables = {
  fromWalletID: string,
  id?: ModelIDKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelTransactionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type TransactionsByFromToWalletIDQuery = {
  transactionsByFromToWalletID?:  {
    __typename: "ModelTransactionConnection",
    items:  Array< {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type TransactionsByToWalletIDQueryVariables = {
  toWalletID: string,
  id?: ModelIDKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelTransactionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type TransactionsByToWalletIDQuery = {
  transactionsByToWalletID?:  {
    __typename: "ModelTransactionConnection",
    items:  Array< {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type TransactionsByXummTransactionIDQueryVariables = {
  xummTransactionID: string,
  id?: ModelIDKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelTransactionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type TransactionsByXummTransactionIDQuery = {
  transactionsByXummTransactionID?:  {
    __typename: "ModelTransactionConnection",
    items:  Array< {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type OnCreateUserSubscriptionVariables = {
  filter?: ModelSubscriptionUserFilterInput | null,
};

export type OnCreateUserSubscription = {
  onCreateUser?:  {
    __typename: "User",
    id: string,
    username: string,
    email: string,
    bio?: string | null,
    location?: string | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    backgroundImage?: string | null,
    wallets?:  {
      __typename: "ModelWalletConnection",
      items:  Array< {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateUserSubscriptionVariables = {
  filter?: ModelSubscriptionUserFilterInput | null,
};

export type OnUpdateUserSubscription = {
  onUpdateUser?:  {
    __typename: "User",
    id: string,
    username: string,
    email: string,
    bio?: string | null,
    location?: string | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    backgroundImage?: string | null,
    wallets?:  {
      __typename: "ModelWalletConnection",
      items:  Array< {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteUserSubscriptionVariables = {
  filter?: ModelSubscriptionUserFilterInput | null,
};

export type OnDeleteUserSubscription = {
  onDeleteUser?:  {
    __typename: "User",
    id: string,
    username: string,
    email: string,
    bio?: string | null,
    location?: string | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    backgroundImage?: string | null,
    wallets?:  {
      __typename: "ModelWalletConnection",
      items:  Array< {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateWalletSubscriptionVariables = {
  filter?: ModelSubscriptionWalletFilterInput | null,
};

export type OnCreateWalletSubscription = {
  onCreateWallet?:  {
    __typename: "Wallet",
    id: string,
    title: string,
    images?: Array< string | null > | null,
    description?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateWalletSubscriptionVariables = {
  filter?: ModelSubscriptionWalletFilterInput | null,
};

export type OnUpdateWalletSubscription = {
  onUpdateWallet?:  {
    __typename: "Wallet",
    id: string,
    title: string,
    images?: Array< string | null > | null,
    description?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteWalletSubscriptionVariables = {
  filter?: ModelSubscriptionWalletFilterInput | null,
};

export type OnDeleteWalletSubscription = {
  onDeleteWallet?:  {
    __typename: "Wallet",
    id: string,
    title: string,
    images?: Array< string | null > | null,
    description?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateProductSubscriptionVariables = {
  filter?: ModelSubscriptionProductFilterInput | null,
};

export type OnCreateProductSubscription = {
  onCreateProduct?:  {
    __typename: "Product",
    id: string,
    title: string,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    description?: string | null,
    price: number,
    category?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    nft?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateProductSubscriptionVariables = {
  filter?: ModelSubscriptionProductFilterInput | null,
};

export type OnUpdateProductSubscription = {
  onUpdateProduct?:  {
    __typename: "Product",
    id: string,
    title: string,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    description?: string | null,
    price: number,
    category?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    nft?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteProductSubscriptionVariables = {
  filter?: ModelSubscriptionProductFilterInput | null,
};

export type OnDeleteProductSubscription = {
  onDeleteProduct?:  {
    __typename: "Product",
    id: string,
    title: string,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    description?: string | null,
    price: number,
    category?: string | null,
    userID: string,
    address: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    nft?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateTrailSubscriptionVariables = {
  filter?: ModelSubscriptionTrailFilterInput | null,
};

export type OnCreateTrailSubscription = {
  onCreateTrail?:  {
    __typename: "Trail",
    id: string,
    name?: string | null,
    description?: string | null,
    duration?: string | null,
    type?: TrailType | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    locations?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    order?: Array< string | null > | null,
    trailProgress?:  {
      __typename: "ModelTrailProgressConnection",
      items:  Array< {
        __typename: "TrailProgress",
        id: string,
        name?: string | null,
        userID: string,
        status: TrailProgressStatus,
        trailID: string,
        order?: Array< string | null > | null,
        createdAt: string,
        updatedAt: string,
        trailTrailProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateTrailSubscriptionVariables = {
  filter?: ModelSubscriptionTrailFilterInput | null,
};

export type OnUpdateTrailSubscription = {
  onUpdateTrail?:  {
    __typename: "Trail",
    id: string,
    name?: string | null,
    description?: string | null,
    duration?: string | null,
    type?: TrailType | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    locations?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    order?: Array< string | null > | null,
    trailProgress?:  {
      __typename: "ModelTrailProgressConnection",
      items:  Array< {
        __typename: "TrailProgress",
        id: string,
        name?: string | null,
        userID: string,
        status: TrailProgressStatus,
        trailID: string,
        order?: Array< string | null > | null,
        createdAt: string,
        updatedAt: string,
        trailTrailProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteTrailSubscriptionVariables = {
  filter?: ModelSubscriptionTrailFilterInput | null,
};

export type OnDeleteTrailSubscription = {
  onDeleteTrail?:  {
    __typename: "Trail",
    id: string,
    name?: string | null,
    description?: string | null,
    duration?: string | null,
    type?: TrailType | null,
    images?:  Array< {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null > | null,
    locations?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    order?: Array< string | null > | null,
    trailProgress?:  {
      __typename: "ModelTrailProgressConnection",
      items:  Array< {
        __typename: "TrailProgress",
        id: string,
        name?: string | null,
        userID: string,
        status: TrailProgressStatus,
        trailID: string,
        order?: Array< string | null > | null,
        createdAt: string,
        updatedAt: string,
        trailTrailProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateTrailProgressSubscriptionVariables = {
  filter?: ModelSubscriptionTrailProgressFilterInput | null,
};

export type OnCreateTrailProgressSubscription = {
  onCreateTrailProgress?:  {
    __typename: "TrailProgress",
    id: string,
    name?: string | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TrailProgressStatus,
    trailID: string,
    trail?:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    order?: Array< string | null > | null,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    trailTrailProgressId?: string | null,
  } | null,
};

export type OnUpdateTrailProgressSubscriptionVariables = {
  filter?: ModelSubscriptionTrailProgressFilterInput | null,
};

export type OnUpdateTrailProgressSubscription = {
  onUpdateTrailProgress?:  {
    __typename: "TrailProgress",
    id: string,
    name?: string | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TrailProgressStatus,
    trailID: string,
    trail?:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    order?: Array< string | null > | null,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    trailTrailProgressId?: string | null,
  } | null,
};

export type OnDeleteTrailProgressSubscriptionVariables = {
  filter?: ModelSubscriptionTrailProgressFilterInput | null,
};

export type OnDeleteTrailProgressSubscription = {
  onDeleteTrailProgress?:  {
    __typename: "TrailProgress",
    id: string,
    name?: string | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TrailProgressStatus,
    trailID: string,
    trail?:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    order?: Array< string | null > | null,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    trailTrailProgressId?: string | null,
  } | null,
};

export type OnCreateLocationSubscriptionVariables = {
  filter?: ModelSubscriptionLocationFilterInput | null,
};

export type OnCreateLocationSubscription = {
  onCreateLocation?:  {
    __typename: "Location",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateLocationSubscriptionVariables = {
  filter?: ModelSubscriptionLocationFilterInput | null,
};

export type OnUpdateLocationSubscription = {
  onUpdateLocation?:  {
    __typename: "Location",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteLocationSubscriptionVariables = {
  filter?: ModelSubscriptionLocationFilterInput | null,
};

export type OnDeleteLocationSubscription = {
  onDeleteLocation?:  {
    __typename: "Location",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateLocationViewSubscriptionVariables = {
  filter?: ModelSubscriptionLocationViewFilterInput | null,
};

export type OnCreateLocationViewSubscription = {
  onCreateLocationView?:  {
    __typename: "LocationView",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    locationID: string,
    location?:  {
      __typename: "Location",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactions?:  {
      __typename: "ModelTransactionConnection",
      items:  Array< {
        __typename: "Transaction",
        id: string,
        status: string,
        amount?: string | null,
        description?: string | null,
        type: TransactionType,
        senderID: string,
        verifierID?: string | null,
        receiverID?: string | null,
        expiration?: string | null,
        fulfillment?: string | null,
        condition?: string | null,
        fromWalletID?: string | null,
        toWalletID?: string | null,
        xummTransactionID?: string | null,
        locationViewID?: string | null,
        rewardID?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    trails?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateLocationViewSubscriptionVariables = {
  filter?: ModelSubscriptionLocationViewFilterInput | null,
};

export type OnUpdateLocationViewSubscription = {
  onUpdateLocationView?:  {
    __typename: "LocationView",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    locationID: string,
    location?:  {
      __typename: "Location",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactions?:  {
      __typename: "ModelTransactionConnection",
      items:  Array< {
        __typename: "Transaction",
        id: string,
        status: string,
        amount?: string | null,
        description?: string | null,
        type: TransactionType,
        senderID: string,
        verifierID?: string | null,
        receiverID?: string | null,
        expiration?: string | null,
        fulfillment?: string | null,
        condition?: string | null,
        fromWalletID?: string | null,
        toWalletID?: string | null,
        xummTransactionID?: string | null,
        locationViewID?: string | null,
        rewardID?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    trails?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteLocationViewSubscriptionVariables = {
  filter?: ModelSubscriptionLocationViewFilterInput | null,
};

export type OnDeleteLocationViewSubscription = {
  onDeleteLocationView?:  {
    __typename: "LocationView",
    id: string,
    name?: string | null,
    address?: string | null,
    description?: string | null,
    coordinates?: Array< number | null > | null,
    locationID: string,
    location?:  {
      __typename: "Location",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    userID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactions?:  {
      __typename: "ModelTransactionConnection",
      items:  Array< {
        __typename: "Transaction",
        id: string,
        status: string,
        amount?: string | null,
        description?: string | null,
        type: TransactionType,
        senderID: string,
        verifierID?: string | null,
        receiverID?: string | null,
        expiration?: string | null,
        fulfillment?: string | null,
        condition?: string | null,
        fromWalletID?: string | null,
        toWalletID?: string | null,
        xummTransactionID?: string | null,
        locationViewID?: string | null,
        rewardID?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    trails?:  {
      __typename: "ModelTrailLocationViewsConnection",
      items:  Array< {
        __typename: "TrailLocationViews",
        id: string,
        trailID: string,
        locationViewID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateTransactionSubscriptionVariables = {
  filter?: ModelSubscriptionTransactionFilterInput | null,
};

export type OnCreateTransactionSubscription = {
  onCreateTransaction?:  {
    __typename: "Transaction",
    id: string,
    status: string,
    amount?: string | null,
    description?: string | null,
    type: TransactionType,
    senderID: string,
    verifierID?: string | null,
    receiverID?: string | null,
    conditions?:  {
      __typename: "ModelConditionConnection",
      items:  Array< {
        __typename: "Condition",
        id: string,
        type: ConditionType,
        latitude?: string | null,
        longitude?: string | null,
        question?: string | null,
        choices?: Array< string | null > | null,
        answerIndex?: number | null,
        status: string,
        transactionID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    expiration?: string | null,
    sender?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    verifier?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    receiver?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fulfillment?: string | null,
    condition?: string | null,
    fromWalletID?: string | null,
    toWalletID?: string | null,
    toWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fromWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    xummTransactionID?: string | null,
    locationViewID?: string | null,
    rewardID?: string | null,
    reward?:  {
      __typename: "Reward",
      id: string,
      name?: string | null,
      file?:  {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactionProgress?:  {
      __typename: "ModelTransactionProgressConnection",
      items:  Array< {
        __typename: "TransactionProgress",
        id: string,
        userID: string,
        transactionID: string,
        status: TransactionProgressStatus,
        createdAt: string,
        updatedAt: string,
        transactionTransactionProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateTransactionSubscriptionVariables = {
  filter?: ModelSubscriptionTransactionFilterInput | null,
};

export type OnUpdateTransactionSubscription = {
  onUpdateTransaction?:  {
    __typename: "Transaction",
    id: string,
    status: string,
    amount?: string | null,
    description?: string | null,
    type: TransactionType,
    senderID: string,
    verifierID?: string | null,
    receiverID?: string | null,
    conditions?:  {
      __typename: "ModelConditionConnection",
      items:  Array< {
        __typename: "Condition",
        id: string,
        type: ConditionType,
        latitude?: string | null,
        longitude?: string | null,
        question?: string | null,
        choices?: Array< string | null > | null,
        answerIndex?: number | null,
        status: string,
        transactionID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    expiration?: string | null,
    sender?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    verifier?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    receiver?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fulfillment?: string | null,
    condition?: string | null,
    fromWalletID?: string | null,
    toWalletID?: string | null,
    toWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fromWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    xummTransactionID?: string | null,
    locationViewID?: string | null,
    rewardID?: string | null,
    reward?:  {
      __typename: "Reward",
      id: string,
      name?: string | null,
      file?:  {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactionProgress?:  {
      __typename: "ModelTransactionProgressConnection",
      items:  Array< {
        __typename: "TransactionProgress",
        id: string,
        userID: string,
        transactionID: string,
        status: TransactionProgressStatus,
        createdAt: string,
        updatedAt: string,
        transactionTransactionProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteTransactionSubscriptionVariables = {
  filter?: ModelSubscriptionTransactionFilterInput | null,
};

export type OnDeleteTransactionSubscription = {
  onDeleteTransaction?:  {
    __typename: "Transaction",
    id: string,
    status: string,
    amount?: string | null,
    description?: string | null,
    type: TransactionType,
    senderID: string,
    verifierID?: string | null,
    receiverID?: string | null,
    conditions?:  {
      __typename: "ModelConditionConnection",
      items:  Array< {
        __typename: "Condition",
        id: string,
        type: ConditionType,
        latitude?: string | null,
        longitude?: string | null,
        question?: string | null,
        choices?: Array< string | null > | null,
        answerIndex?: number | null,
        status: string,
        transactionID: string,
        createdAt: string,
        updatedAt: string,
      } | null >,
      nextToken?: string | null,
    } | null,
    expiration?: string | null,
    sender?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    verifier?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    receiver?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fulfillment?: string | null,
    condition?: string | null,
    fromWalletID?: string | null,
    toWalletID?: string | null,
    toWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    fromWallet?:  {
      __typename: "Wallet",
      id: string,
      title: string,
      images?: Array< string | null > | null,
      description?: string | null,
      userID: string,
      address: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    xummTransactionID?: string | null,
    locationViewID?: string | null,
    rewardID?: string | null,
    reward?:  {
      __typename: "Reward",
      id: string,
      name?: string | null,
      file?:  {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transactionProgress?:  {
      __typename: "ModelTransactionProgressConnection",
      items:  Array< {
        __typename: "TransactionProgress",
        id: string,
        userID: string,
        transactionID: string,
        status: TransactionProgressStatus,
        createdAt: string,
        updatedAt: string,
        transactionTransactionProgressId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateTransactionProgressSubscriptionVariables = {
  filter?: ModelSubscriptionTransactionProgressFilterInput | null,
};

export type OnCreateTransactionProgressSubscription = {
  onCreateTransactionProgress?:  {
    __typename: "TransactionProgress",
    id: string,
    userID: string,
    transactionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transaction?:  {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TransactionProgressStatus,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    transactionTransactionProgressId?: string | null,
  } | null,
};

export type OnUpdateTransactionProgressSubscriptionVariables = {
  filter?: ModelSubscriptionTransactionProgressFilterInput | null,
};

export type OnUpdateTransactionProgressSubscription = {
  onUpdateTransactionProgress?:  {
    __typename: "TransactionProgress",
    id: string,
    userID: string,
    transactionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transaction?:  {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TransactionProgressStatus,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    transactionTransactionProgressId?: string | null,
  } | null,
};

export type OnDeleteTransactionProgressSubscriptionVariables = {
  filter?: ModelSubscriptionTransactionProgressFilterInput | null,
};

export type OnDeleteTransactionProgressSubscription = {
  onDeleteTransactionProgress?:  {
    __typename: "TransactionProgress",
    id: string,
    userID: string,
    transactionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    transaction?:  {
      __typename: "Transaction",
      id: string,
      status: string,
      amount?: string | null,
      description?: string | null,
      type: TransactionType,
      senderID: string,
      verifierID?: string | null,
      receiverID?: string | null,
      conditions?:  {
        __typename: "ModelConditionConnection",
        nextToken?: string | null,
      } | null,
      expiration?: string | null,
      sender?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      verifier?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      receiver?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      fulfillment?: string | null,
      condition?: string | null,
      fromWalletID?: string | null,
      toWalletID?: string | null,
      toWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      fromWallet?:  {
        __typename: "Wallet",
        id: string,
        title: string,
        images?: Array< string | null > | null,
        description?: string | null,
        userID: string,
        address: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      xummTransactionID?: string | null,
      locationViewID?: string | null,
      rewardID?: string | null,
      reward?:  {
        __typename: "Reward",
        id: string,
        name?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactionProgress?:  {
        __typename: "ModelTransactionProgressConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: TransactionProgressStatus,
    completedConditions?:  {
      __typename: "ModelConditionProgressConnection",
      items:  Array< {
        __typename: "ConditionProgress",
        id: string,
        userID: string,
        conditionID: string,
        status: ConditionProgressStatus,
        createdAt: string,
        updatedAt: string,
        trailProgressCompletedConditionsId?: string | null,
        transactionProgressCompletedConditionsId?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
    transactionTransactionProgressId?: string | null,
  } | null,
};

export type OnCreateConditionSubscriptionVariables = {
  filter?: ModelSubscriptionConditionFilterInput | null,
};

export type OnCreateConditionSubscription = {
  onCreateCondition?:  {
    __typename: "Condition",
    id: string,
    type: ConditionType,
    latitude?: string | null,
    longitude?: string | null,
    question?: string | null,
    choices?: Array< string | null > | null,
    answerIndex?: number | null,
    status: string,
    transactionID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateConditionSubscriptionVariables = {
  filter?: ModelSubscriptionConditionFilterInput | null,
};

export type OnUpdateConditionSubscription = {
  onUpdateCondition?:  {
    __typename: "Condition",
    id: string,
    type: ConditionType,
    latitude?: string | null,
    longitude?: string | null,
    question?: string | null,
    choices?: Array< string | null > | null,
    answerIndex?: number | null,
    status: string,
    transactionID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteConditionSubscriptionVariables = {
  filter?: ModelSubscriptionConditionFilterInput | null,
};

export type OnDeleteConditionSubscription = {
  onDeleteCondition?:  {
    __typename: "Condition",
    id: string,
    type: ConditionType,
    latitude?: string | null,
    longitude?: string | null,
    question?: string | null,
    choices?: Array< string | null > | null,
    answerIndex?: number | null,
    status: string,
    transactionID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateConditionProgressSubscriptionVariables = {
  filter?: ModelSubscriptionConditionProgressFilterInput | null,
};

export type OnCreateConditionProgressSubscription = {
  onCreateConditionProgress?:  {
    __typename: "ConditionProgress",
    id: string,
    userID: string,
    conditionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    condition?:  {
      __typename: "Condition",
      id: string,
      type: ConditionType,
      latitude?: string | null,
      longitude?: string | null,
      question?: string | null,
      choices?: Array< string | null > | null,
      answerIndex?: number | null,
      status: string,
      transactionID: string,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: ConditionProgressStatus,
    createdAt: string,
    updatedAt: string,
    trailProgressCompletedConditionsId?: string | null,
    transactionProgressCompletedConditionsId?: string | null,
  } | null,
};

export type OnUpdateConditionProgressSubscriptionVariables = {
  filter?: ModelSubscriptionConditionProgressFilterInput | null,
};

export type OnUpdateConditionProgressSubscription = {
  onUpdateConditionProgress?:  {
    __typename: "ConditionProgress",
    id: string,
    userID: string,
    conditionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    condition?:  {
      __typename: "Condition",
      id: string,
      type: ConditionType,
      latitude?: string | null,
      longitude?: string | null,
      question?: string | null,
      choices?: Array< string | null > | null,
      answerIndex?: number | null,
      status: string,
      transactionID: string,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: ConditionProgressStatus,
    createdAt: string,
    updatedAt: string,
    trailProgressCompletedConditionsId?: string | null,
    transactionProgressCompletedConditionsId?: string | null,
  } | null,
};

export type OnDeleteConditionProgressSubscriptionVariables = {
  filter?: ModelSubscriptionConditionProgressFilterInput | null,
};

export type OnDeleteConditionProgressSubscription = {
  onDeleteConditionProgress?:  {
    __typename: "ConditionProgress",
    id: string,
    userID: string,
    conditionID: string,
    user?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    condition?:  {
      __typename: "Condition",
      id: string,
      type: ConditionType,
      latitude?: string | null,
      longitude?: string | null,
      question?: string | null,
      choices?: Array< string | null > | null,
      answerIndex?: number | null,
      status: string,
      transactionID: string,
      createdAt: string,
      updatedAt: string,
    } | null,
    status: ConditionProgressStatus,
    createdAt: string,
    updatedAt: string,
    trailProgressCompletedConditionsId?: string | null,
    transactionProgressCompletedConditionsId?: string | null,
  } | null,
};

export type OnCreateAssetSubscriptionVariables = {
  filter?: ModelSubscriptionAssetFilterInput | null,
};

export type OnCreateAssetSubscription = {
  onCreateAsset?:  {
    __typename: "Asset",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    userID: string,
    owner?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateAssetSubscriptionVariables = {
  filter?: ModelSubscriptionAssetFilterInput | null,
};

export type OnUpdateAssetSubscription = {
  onUpdateAsset?:  {
    __typename: "Asset",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    userID: string,
    owner?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteAssetSubscriptionVariables = {
  filter?: ModelSubscriptionAssetFilterInput | null,
};

export type OnDeleteAssetSubscription = {
  onDeleteAsset?:  {
    __typename: "Asset",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    userID: string,
    owner?:  {
      __typename: "User",
      id: string,
      username: string,
      email: string,
      bio?: string | null,
      location?: string | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      backgroundImage?: string | null,
      wallets?:  {
        __typename: "ModelWalletConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateRewardSubscriptionVariables = {
  filter?: ModelSubscriptionRewardFilterInput | null,
};

export type OnCreateRewardSubscription = {
  onCreateReward?:  {
    __typename: "Reward",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateRewardSubscriptionVariables = {
  filter?: ModelSubscriptionRewardFilterInput | null,
};

export type OnUpdateRewardSubscription = {
  onUpdateReward?:  {
    __typename: "Reward",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteRewardSubscriptionVariables = {
  filter?: ModelSubscriptionRewardFilterInput | null,
};

export type OnDeleteRewardSubscription = {
  onDeleteReward?:  {
    __typename: "Reward",
    id: string,
    name?: string | null,
    file?:  {
      __typename: "S3Object",
      bucket: string,
      region: string,
      key: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateTrailLocationViewsSubscriptionVariables = {
  filter?: ModelSubscriptionTrailLocationViewsFilterInput | null,
};

export type OnCreateTrailLocationViewsSubscription = {
  onCreateTrailLocationViews?:  {
    __typename: "TrailLocationViews",
    id: string,
    trailID: string,
    locationViewID: string,
    trail:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    locationView:  {
      __typename: "LocationView",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      locationID: string,
      location?:  {
        __typename: "Location",
        id: string,
        name?: string | null,
        address?: string | null,
        description?: string | null,
        coordinates?: Array< number | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactions?:  {
        __typename: "ModelTransactionConnection",
        nextToken?: string | null,
      } | null,
      trails?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateTrailLocationViewsSubscriptionVariables = {
  filter?: ModelSubscriptionTrailLocationViewsFilterInput | null,
};

export type OnUpdateTrailLocationViewsSubscription = {
  onUpdateTrailLocationViews?:  {
    __typename: "TrailLocationViews",
    id: string,
    trailID: string,
    locationViewID: string,
    trail:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    locationView:  {
      __typename: "LocationView",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      locationID: string,
      location?:  {
        __typename: "Location",
        id: string,
        name?: string | null,
        address?: string | null,
        description?: string | null,
        coordinates?: Array< number | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactions?:  {
        __typename: "ModelTransactionConnection",
        nextToken?: string | null,
      } | null,
      trails?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteTrailLocationViewsSubscriptionVariables = {
  filter?: ModelSubscriptionTrailLocationViewsFilterInput | null,
};

export type OnDeleteTrailLocationViewsSubscription = {
  onDeleteTrailLocationViews?:  {
    __typename: "TrailLocationViews",
    id: string,
    trailID: string,
    locationViewID: string,
    trail:  {
      __typename: "Trail",
      id: string,
      name?: string | null,
      description?: string | null,
      duration?: string | null,
      type?: TrailType | null,
      images?:  Array< {
        __typename: "S3Object",
        bucket: string,
        region: string,
        key: string,
      } | null > | null,
      locations?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      order?: Array< string | null > | null,
      trailProgress?:  {
        __typename: "ModelTrailProgressConnection",
        nextToken?: string | null,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    locationView:  {
      __typename: "LocationView",
      id: string,
      name?: string | null,
      address?: string | null,
      description?: string | null,
      coordinates?: Array< number | null > | null,
      locationID: string,
      location?:  {
        __typename: "Location",
        id: string,
        name?: string | null,
        address?: string | null,
        description?: string | null,
        coordinates?: Array< number | null > | null,
        userID: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      userID: string,
      user?:  {
        __typename: "User",
        id: string,
        username: string,
        email: string,
        bio?: string | null,
        location?: string | null,
        backgroundImage?: string | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      transactions?:  {
        __typename: "ModelTransactionConnection",
        nextToken?: string | null,
      } | null,
      trails?:  {
        __typename: "ModelTrailLocationViewsConnection",
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    },
    createdAt: string,
    updatedAt: string,
  } | null,
};
