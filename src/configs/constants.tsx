import Home from '@mui/icons-material/Home'
import ShowChart from '@mui/icons-material/ShowChart'
import AccountBalanceWallet from '@mui/icons-material/AccountBalanceWallet'
import People from '@mui/icons-material/People'
import Storefront from '@mui/icons-material/Storefront'
import Settings from '@mui/icons-material/Settings'
import MonetizationOn from '@mui/icons-material/MonetizationOn'
import TokenIcon from '@mui/icons-material/Token'
import CategoryIcon from '@mui/icons-material/Category'
import RouteIcon from '@mui/icons-material/Route'

export const dashboardNavLinks = [
  { href: '/dashboard', key: 'Home', icon: <Home />, tooltip: 'Home' },
  {
    href: '/dashboard/payments',
    key: 'payments',
    icon: <MonetizationOn />,
    tooltip: 'Payments',
  },
  {
    href: '/dashboard/wallets',
    key: 'wallets',
    icon: <AccountBalanceWallet />,
    tooltip: 'Wallets',
  },
  {
    href: '/dashboard/assets',
    key: 'assets',
    icon: <CategoryIcon />,
    tooltip: 'Assets',
  },
  {
    href: '/dashboard/trails',
    key: 'trails',
    icon: <RouteIcon />,
    tooltip: 'Trails',
  },

  {
    href: '/dashboard/nfts',
    key: 'NFTs',
    icon: <TokenIcon />,
    tooltip: 'NFTs',
  },
  {
    href: '/dashboard/store',
    key: 'Store',
    icon: <Storefront />,
    tooltip: 'Store',
  },

  {
    href: '',
    disabled: true,
    key: 'Investments',
    icon: <ShowChart />,
    tooltip: 'Investments',
  },
  {
    href: '',
    disabled: true,
    key: 'Network',
    icon: <People />,
    tooltip: 'Network',
  },

  {
    href: '/dashboard/profile',
    key: 'profile',
    icon: <Settings />,
    tooltip: 'Profile',
  },
]
