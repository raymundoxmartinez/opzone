import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'

export interface AppState {
  error: null | string | undefined
  items: Record<string, any> | null | undefined
  loading: 'idle' | 'pending'
  currentRequestId: string | undefined
  idList: any[] | undefined
}
export const initialState: AppState = {
  error: null,
  items: null,
  loading: 'idle',
  currentRequestId: undefined,
  idList: [],
}

export const postTrail = createAsyncThunk<any, any>(
  'trails/post',
  async (request, { rejectWithValue }) => {
    try {
      const response = await services.trailsService.createTrail({
        ...request,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchTrails = createAsyncThunk<any, any>(
  'trails/getAll',
  async ({ filter, input }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.trailsService.fetchTrails({
        filter,
        input,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchTrail = createAsyncThunk<any, any>(
  'trails/get',
  async (id: string, { rejectWithValue }) => {
    try {
      const response = await services.trailsService.fetchTrail(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const deleteTrail = createAsyncThunk<any, any>(
  'trails/delete',
  async (id: string, { rejectWithValue }) => {
    try {
      const response = await services.trailsService.deleteTrail(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

const appSlice = createSlice({
  name: 'trails',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postTrail.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(postTrail.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      state.loading = 'idle'
      state.items = { ...state.items, [payload.id]: payload }
      state.currentRequestId = undefined
      state.idList = [payload.id]
    })
    builder.addCase(postTrail.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      state.loading = 'idle'
      state.error = JSON.stringify(error)
      state.currentRequestId = undefined
    })
    builder.addCase(fetchTrails.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchTrails.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        const { items, idList } = payload.reduce(
          (acc: Record<string, any>, item: Record<string, any>) => {
            acc.items[item.id] = item
            acc.idList.push(item.id)
            return acc
          },
          { items: {}, idList: [] }
        )
        state.loading = 'idle'
        state.items = { ...state.items, ...items }
        state.idList = idList
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchTrails.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchTrail.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchTrail.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.items = { ...state.items, [payload.id]: payload }
        state.currentRequestId = undefined
        state.idList = [payload.id]
      }
    })
    builder.addCase(fetchTrail.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(deleteTrail.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(deleteTrail.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        if (state.items) delete state.items[payload.id]
        state.items = { ...state.items }
        state.currentRequestId = undefined
        state.idList = []
      }
    })
    builder.addCase(deleteTrail.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
  },
})

export default appSlice.reducer
