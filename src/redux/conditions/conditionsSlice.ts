import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'

export interface AppState {
  items: Record<string, any> | null | undefined

  byId: {
    idList: string[]
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
    error: null | string | undefined
  }

  all: {
    idList: string[]
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
    error: null | string | undefined
  }
}
export const initialState: AppState = {
  items: null,
  byId: {
    idList: [],
    loading: 'idle',
    currentRequestId: undefined,
    error: null,
  },

  all: {
    idList: [],
    loading: 'idle',
    currentRequestId: undefined,
    error: null,
  },
}
export const postCondition = createAsyncThunk<any, any>(
  'conditions/post',
  async (request, { rejectWithValue }) => {
    try {
      const response = await services.conditionService.createCondition({
        ...request,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchCondition = createAsyncThunk(
  'conditions/get',
  async ({ id }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.conditionService.fetchCondition(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchConditions = createAsyncThunk<any, any>(
  'conditions/getAll',
  async ({ filter, input }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.conditionService.fetchConditions({
        filter,
        input,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

const appSlice = createSlice({
  name: 'conditions',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchCondition.pending, (state, action) => {
      if (state.byId.loading === 'idle') {
        state.byId.loading = 'pending'
        state.byId.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchCondition.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (
        state.byId.loading === 'pending' &&
        state.byId.currentRequestId === requestId
      ) {
        const itemByID = { [payload.id]: payload }
        state.byId.loading = 'idle'
        state.items = { ...state.items, ...itemByID }
        state.byId.idList = [payload.id]
        state.byId.currentRequestId = undefined
      }
    })
    builder.addCase(fetchCondition.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (
        state.byId.loading === 'pending' &&
        state.byId.currentRequestId === requestId
      ) {
        state.byId.loading = 'idle'
        state.byId.error = JSON.stringify(error)
        state.byId.currentRequestId = undefined
      }
    })
    builder.addCase(fetchConditions.pending, (state, action) => {
      state.all.loading = 'pending'
      state.all.currentRequestId = action.meta.requestId
    })
    builder.addCase(fetchConditions.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.all.currentRequestId === requestId) {
        const itemsAll = payload.reduce(
          (acc: Record<string, any>, item: Record<string, any>) => {
            acc[item.id] = item
            return acc
          },
          {}
        )
        state.all.loading = 'idle'
        state.items = { ...state.items, ...itemsAll }
        state.all.idList = payload.reduce(
          (acc: Record<string, any>, item: Record<string, any>) => {
            acc.push(item.id)
            return acc
          },
          []
        )
        state.all.currentRequestId = undefined
      }
    })
    builder.addCase(fetchConditions.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.all.currentRequestId === requestId) {
        state.all.loading = 'idle'
        state.all.error = JSON.stringify(error)
        state.all.currentRequestId = undefined
      }
    })
  },
})

export default appSlice.reducer
