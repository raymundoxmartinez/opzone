import { createSlice } from '@reduxjs/toolkit'

export interface UIState {
  profile: {
    isUpdateUserProfileDialogOpen: boolean
  }
  payments: {
    isPayloadDialogOpen: boolean
    isSmartContractSelectDialogOpen: boolean
    isCreateWalletDialogOpen: boolean
  }
}
export const initialState: UIState = {
  profile: {
    isUpdateUserProfileDialogOpen: false,
  },
  payments: {
    isPayloadDialogOpen: false,
    isSmartContractSelectDialogOpen: false,
    isCreateWalletDialogOpen: false,
  },
}

const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    profile__handleOpenUpdateUserProfileDialog(state) {
      state.profile.isUpdateUserProfileDialogOpen = true
    },
    profile__handleCloseUpdateUserProfileDialog(state) {
      state.profile.isUpdateUserProfileDialogOpen = false
    },
    payments__handleOpenPayloadDialog(state) {
      state.payments.isPayloadDialogOpen = true
    },
    payments__handleClosePayloadDialog(state) {
      state.payments.isPayloadDialogOpen = false
    },
    payments__handleOpenCreateSmartContractSelectDialog(state) {
      state.payments.isSmartContractSelectDialogOpen = true
    },
    payments__handleCloseCreateSmartContractSelectDialog(state) {
      state.payments.isSmartContractSelectDialogOpen = false
    },
    payments__handleOpenCreateWalletDialog(state) {
      state.payments.isCreateWalletDialogOpen = true
    },
    payments__handleCloseCreateWalletDialog(state) {
      state.payments.isCreateWalletDialogOpen = false
    },
  },
})
export const {
  profile__handleOpenUpdateUserProfileDialog,
  profile__handleCloseUpdateUserProfileDialog,
  payments__handleOpenPayloadDialog,
  payments__handleClosePayloadDialog,
  payments__handleOpenCreateSmartContractSelectDialog,
  payments__handleCloseCreateSmartContractSelectDialog,
  payments__handleOpenCreateWalletDialog,
  payments__handleCloseCreateWalletDialog,
} = uiSlice.actions

export default uiSlice.reducer
