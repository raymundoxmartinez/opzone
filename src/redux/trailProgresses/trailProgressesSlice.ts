import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'

export interface AppState {
  error: null | string | undefined
  items: Record<string, any> | null | undefined
  loading: 'idle' | 'pending'
  currentRequestId: string | undefined
  idList: any[] | undefined
}
export const initialState: AppState = {
  error: null,
  items: null,
  loading: 'idle',
  currentRequestId: undefined,
  idList: [],
}

export const postTrailProgress = createAsyncThunk<any, any>(
  'trailProgresses/post',
  async (request, { rejectWithValue }) => {
    try {
      const response =
        await services.trailProgressesService.createTrailProgress({
          ...request,
        })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchTrailProgresses = createAsyncThunk<any, any>(
  'trailProgresses/getAll',
  async ({ filter, input }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response =
        await services.trailProgressesService.fetchTrailProgresses({
          filter,
          input,
        })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchTrailProgress = createAsyncThunk<any, any>(
  'trailProgresses/get',
  async (id: string, { rejectWithValue }) => {
    try {
      const response = await services.trailProgressesService.fetchTrailProgress(
        id
      )
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const deleteTrailProgress = createAsyncThunk<any, any>(
  'trailProgresses/delete',
  async (id: string, { rejectWithValue }) => {
    try {
      const response =
        await services.trailProgressesService.deleteTrailProgress(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
const appSlice = createSlice({
  name: 'trailProgresses',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postTrailProgress.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(postTrailProgress.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.items = { ...state.items, [payload.id]: payload }
        state.currentRequestId = undefined
        state.idList = [payload.id]
      }
    })
    builder.addCase(postTrailProgress.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchTrailProgresses.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      fetchTrailProgresses.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          const { items, idList } = payload.reduce(
            (acc: Record<string, any>, item: Record<string, any>) => {
              acc.items[item.id] = item
              acc.idList.push(item.id)
              return acc
            },
            { items: {}, idList: [] }
          )
          state.loading = 'idle'
          state.items = { ...state.items, ...items }
          state.idList = idList
          state.currentRequestId = undefined
        }
      }
    )
    builder.addCase(fetchTrailProgresses.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchTrailProgress.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      fetchTrailProgress.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          state.items = { ...state.items, [payload.id]: payload }
          state.currentRequestId = undefined
          state.idList = [payload.id]
        }
      }
    )
    builder.addCase(fetchTrailProgress.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(deleteTrailProgress.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      deleteTrailProgress.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          if (state.items) delete state.items[payload.id]
          state.items = { ...state.items }
          state.currentRequestId = undefined
          state.idList = []
        }
      }
    )
    builder.addCase(deleteTrailProgress.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
  },
})

export default appSlice.reducer
