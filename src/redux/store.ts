import { configureStore, getDefaultMiddleware, Action } from '@reduxjs/toolkit'
import thunk, { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { useDispatch } from 'react-redux'
import { createLogger } from 'redux-logger'
import rootReducer, { RootState } from './rootReducer'

const middlewares = [...getDefaultMiddleware()]

//@ts-ignore
if (process.env.NODE_ENV !== 'production' && module.hot) {
  //@ts-ignore
  module.hot.accept('./rootReducer', () => {
    // eslint-disable-next-line
    const newRootReducer = require('./rootReducer').default
    store.replaceReducer(newRootReducer)
  })
  middlewares.push(
    createLogger({
      duration: true,
      collapsed: true,
    })
  )
}
const store = configureStore({
  reducer: rootReducer,
  middleware: middlewares,
})

export type AppState = ReturnType<typeof rootReducer>

export const useAppDispatch = () =>
  useDispatch<ThunkDispatch<AppState, unknown, Action<string>>>()

export type AppDispatch = typeof store.dispatch

export type AppThunk = ThunkAction<void, RootState, null, Action<string>>

export default store
