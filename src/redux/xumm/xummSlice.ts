import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'
import { Transaction, EscrowTransaction } from '@services/xumm/types'

export interface XummState {
  credentials: {
    error: null | string | undefined
    items: Record<string, any> | null | undefined
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
  }

  transactions: {
    error: null | string | undefined
    items: Record<string, any> | null | undefined
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
  }
}
export const initialState: XummState = {
  credentials: {
    error: null,
    items: { xummUserToken: '', xummUserAccount: '' },
    loading: 'idle',
    currentRequestId: undefined,
  },
  transactions: {
    error: null,
    items: null,
    loading: 'idle',
    currentRequestId: undefined,
  },
}

export const postTransaction = createAsyncThunk<any, any>(
  'xumm/transaction/post',
  async (
    { returnUrl, xummUserToken, amount, memo, type, destination }: Transaction,
    { rejectWithValue }
  ) => {
    try {
      const response = await services.xummService.postTransaction({
        destination,
        returnUrl,
        xummUserToken,
        amount,
        memo,
        type,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const postEscrowTransaction = createAsyncThunk<any, any>(
  'xumm/transaction/escrow/post',
  async (
    {
      returnUrl,
      xummUserToken,
      amount,
      memo,
      destination,
      condition,
      cancelAfter,
    }: EscrowTransaction,
    { rejectWithValue }
  ) => {
    try {
      const response = await services.xummService.postEscrowTransaction({
        returnUrl,
        xummUserToken,
        amount,
        memo,
        destination,
        condition,
        cancelAfter,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const postEscrowFinishTransaction = createAsyncThunk<any, any>(
  'xumm/transaction/escrow/finish/post',
  async (
    {
      returnUrl,
      xummUserToken,
      condition,
      fulfillment,
      owner,
      memo,
      fee,
      offerSequence,
    }: any,
    { rejectWithValue }
  ) => {
    try {
      const response = await services.xummService.postEscrowFinishTransaction({
        returnUrl,
        xummUserToken,
        condition,
        fulfillment,
        owner,
        memo,
        fee,
        offerSequence,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchTransaction = createAsyncThunk<any, any>(
  'xumm/transaction/get',
  async (id: string, { rejectWithValue }) => {
    try {
      const response = await services.xummService.getTransaction(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const signIn = createAsyncThunk<any, any>(
  'xumm/signIn',
  async ({ returnUrl }, { rejectWithValue }) => {
    try {
      const response = await services.xummService.signIn({ returnUrl })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const storeXummCredentials = createAsyncThunk<any, any>(
  'xumm/storeXummCredentials',
  async (id: string, { rejectWithValue }) => {
    try {
      const response = await services.xummService.getTransaction(id)
      if (response) {
        const xummUserAccount = response.response.account
        const xummUserToken = response.application.issued_user_token
        //TODO: store in backend on user model

        localStorage.setItem('xummSessionId', id)
        localStorage.setItem('xummUserAccount', xummUserAccount)
        localStorage.setItem('xummUserToken', xummUserToken)

        return { xummUserAccount, xummUserToken }
      }
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const removeXummCredentials = createAsyncThunk<any, void>(
  'xumm/removeXummCredentials',
  async (args, { rejectWithValue }) => {
    localStorage.removeItem('xummSessionId')
    localStorage.removeItem('xummUserAccount')
    localStorage.removeItem('xummUserToken')
    return { xummUserToken: '', xummUserAccount: '' }
  }
)

const appSlice = createSlice({
  name: 'xumm',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchTransaction.pending, (state, action) => {
      if (state.transactions.loading === 'idle') {
        state.transactions.loading = 'pending'
        state.transactions.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchTransaction.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (
        state.transactions.loading === 'pending' &&
        state.transactions.currentRequestId === requestId
      ) {
        state.transactions.loading = 'idle'
        state.transactions.items = payload
        state.transactions.currentRequestId = undefined
      }
    })
    builder.addCase(fetchTransaction.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (
        state.transactions.loading === 'pending' &&
        state.transactions.currentRequestId === requestId
      ) {
        state.transactions.loading = 'idle'
        state.transactions.error = JSON.stringify(error)
        state.transactions.currentRequestId = undefined
      }
    })
    builder.addCase(postTransaction.pending, (state, action) => {
      if (state.transactions.loading === 'idle') {
        state.transactions.loading = 'pending'
        state.transactions.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(postTransaction.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (
        state.transactions.loading === 'pending' &&
        state.transactions.currentRequestId === requestId
      ) {
        state.transactions.loading = 'idle'
        state.transactions.items = {
          ...state.transactions.items,
          [payload.id]: payload,
        }
        state.transactions.currentRequestId = undefined
      }
    })
    builder.addCase(postTransaction.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (
        state.transactions.loading === 'pending' &&
        state.transactions.currentRequestId === requestId
      ) {
        state.transactions.loading = 'idle'
        state.transactions.error = JSON.stringify(error)
        state.transactions.currentRequestId = undefined
      }
    })
    builder.addCase(postEscrowFinishTransaction.pending, (state, action) => {
      if (state.transactions.loading === 'idle') {
        state.transactions.loading = 'pending'
        state.transactions.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      postEscrowFinishTransaction.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.transactions.loading === 'pending' &&
          state.transactions.currentRequestId === requestId
        ) {
          state.transactions.loading = 'idle'
          state.transactions.items = {
            ...state.transactions.items,
            [payload.id]: payload,
          }
          state.transactions.currentRequestId = undefined
        }
      }
    )
    builder.addCase(
      postEscrowFinishTransaction.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (
          state.transactions.loading === 'pending' &&
          state.transactions.currentRequestId === requestId
        ) {
          state.transactions.loading = 'idle'
          state.transactions.error = JSON.stringify(error)
          state.transactions.currentRequestId = undefined
        }
      }
    )
    builder.addCase(postEscrowTransaction.pending, (state, action) => {
      if (state.transactions.loading === 'idle') {
        state.transactions.loading = 'pending'
        state.transactions.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      postEscrowTransaction.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.transactions.loading === 'pending' &&
          state.transactions.currentRequestId === requestId
        ) {
          state.transactions.loading = 'idle'
          state.transactions.items = {
            ...state.transactions.items,
            [payload.id]: payload,
          }
          state.transactions.currentRequestId = undefined
        }
      }
    )
    builder.addCase(
      postEscrowTransaction.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (
          state.transactions.loading === 'pending' &&
          state.transactions.currentRequestId === requestId
        ) {
          state.transactions.loading = 'idle'
          state.transactions.error = JSON.stringify(error)
          state.transactions.currentRequestId = undefined
        }
      }
    )
    builder.addCase(signIn.pending, (state, action) => {
      if (state.transactions.loading === 'idle') {
        state.transactions.loading = 'pending'
        state.transactions.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(signIn.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (
        state.transactions.loading === 'pending' &&
        state.transactions.currentRequestId === requestId
      ) {
        state.transactions.loading = 'idle'
        state.transactions.items = payload
        state.transactions.currentRequestId = undefined
      }
    })
    builder.addCase(signIn.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (
        state.transactions.loading === 'pending' &&
        state.transactions.currentRequestId === requestId
      ) {
        state.transactions.loading = 'idle'
        state.transactions.error = JSON.stringify(error)
        state.transactions.currentRequestId = undefined
      }
    })
    builder.addCase(storeXummCredentials.pending, (state, action) => {
      if (state.credentials.loading === 'idle') {
        state.credentials.loading = 'pending'
        state.credentials.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      storeXummCredentials.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.credentials.loading === 'pending' &&
          state.credentials.currentRequestId === requestId
        ) {
          state.credentials.loading = 'idle'
          state.credentials.items = payload
          state.credentials.currentRequestId = undefined
        }
      }
    )
    builder.addCase(storeXummCredentials.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (
        state.credentials.loading === 'pending' &&
        state.credentials.currentRequestId === requestId
      ) {
        state.credentials.loading = 'idle'
        state.credentials.error = JSON.stringify(error)
        state.credentials.currentRequestId = undefined
      }
    })
    builder.addCase(removeXummCredentials.pending, (state, action) => {
      if (state.credentials.loading === 'idle') {
        state.credentials.loading = 'pending'
        state.credentials.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      removeXummCredentials.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.credentials.loading === 'pending' &&
          state.credentials.currentRequestId === requestId
        ) {
          state.credentials.loading = 'idle'
          state.credentials.items = payload
          state.credentials.currentRequestId = undefined
        }
      }
    )
    builder.addCase(
      removeXummCredentials.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (
          state.credentials.loading === 'pending' &&
          state.credentials.currentRequestId === requestId
        ) {
          state.credentials.loading = 'idle'
          state.credentials.error = JSON.stringify(error)
          state.credentials.currentRequestId = undefined
        }
      }
    )
  },
})

export default appSlice.reducer
