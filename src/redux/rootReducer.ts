import { combineReducers } from '@reduxjs/toolkit'
import usersSlice, { initialState as usersState } from './users/usersSlice'
import appSlice, { initialState as appState } from './app/appSlice'
import uiSlice, { initialState as uiState } from './ui/uiSlice'
import xummSlice, { initialState as xummState } from './xumm/xummSlice'
import conditionsSlice, {
  initialState as conditionsState,
} from './conditions/conditionsSlice'
import walletsSlice, {
  initialState as walletsState,
} from './wallets/walletsSlice'
import transactionsSlice, {
  initialState as transactionsState,
} from './transactions/transactionsSlice'
import assetsSlice, { initialState as assetsState } from './assets/assetsSlice'
import trailsSlice, { initialState as trailsState } from './trails/trailsSlice'
import locationsSlice, {
  initialState as locationsState,
} from './locations/locationsSlice'
import locationViewsSlice, {
  initialState as locationViewsState,
} from './locationViews/locationViewsSlice'
import trailProgressesSlice, {
  initialState as trailProgressesState,
} from './trailProgresses/trailProgressesSlice'
import conditionProgressesSlice, {
  initialState as conditionProgressesState,
} from './conditionProgresses/conditionProgressesSlice'
import rewardsSlice, {
  initialState as rewardsState,
} from './rewards/rewardsSlice'

export const rootInitialState = {
  users: usersState,
  app: appState,
  ui: uiState,
  xumm: xummState,
  transactions: transactionsState,
  wallets: walletsState,
  conditions: conditionsState,
  assets: assetsState,
  trails: trailsState,
  locations: locationsState,
  locationViews: locationViewsState,
  trailProgresses: trailProgressesState,
  conditionProgresses: conditionProgressesState,
  rewards: rewardsState,
}

const combinedReducer = combineReducers({
  users: usersSlice,
  app: appSlice,
  ui: uiSlice,
  xumm: xummSlice,
  transactions: transactionsSlice,
  wallets: walletsSlice,
  conditions: conditionsSlice,
  assets: assetsSlice,
  trails: trailsSlice,
  locations: locationsSlice,
  locationViews: locationViewsSlice,
  trailProgresses: trailProgressesSlice,
  conditionProgresses: conditionProgressesSlice,
  rewards: rewardsSlice,
})

//@ts-ignore
const rootReducer = (state, action) => {
  if (action.type === 'RESET') {
    state = rootInitialState
  }
  return combinedReducer(state, action)
}

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
