import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'

export interface AppState {
  error: null | string | undefined
  items: Record<string, any> | null | undefined
  loading: 'idle' | 'pending'
  currentRequestId: string | undefined
}
export const initialState: AppState = {
  error: null,
  items: null,
  loading: 'idle',
  currentRequestId: undefined,
}

export const postAsset = createAsyncThunk<any, any>(
  'assets/post',
  async (request, { rejectWithValue }) => {
    try {
      const response = await services.assetsService.createAsset({
        ...request,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchAssets = createAsyncThunk<any, any>(
  'assets/getAll',
  async ({ filter, input }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.assetsService.fetchAssets({
        filter,
        input,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchAsset = createAsyncThunk<any, any>(
  'assets/get',
  async (id: string, { rejectWithValue }) => {
    try {
      const response = await services.assetsService.fetchAsset(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const deleteAsset = createAsyncThunk<any, any>(
  'assets/delete',
  async (id: string, { rejectWithValue }) => {
    try {
      const response = await services.assetsService.deleteAsset(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

const appSlice = createSlice({
  name: 'assets',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAssets.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchAssets.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.items = payload
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchAssets.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchAsset.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchAsset.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.items = { ...state.items, [payload.id]: payload }
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchAsset.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(deleteAsset.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(deleteAsset.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        if (state.items) delete state.items[payload.id]
        state.items = { ...state.items }
        state.currentRequestId = undefined
      }
    })
    builder.addCase(deleteAsset.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
  },
})

export default appSlice.reducer
