import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'

export interface AppState {
  error: null | string | undefined
  items: Record<string, any> | null | undefined
  loading: 'idle' | 'pending'
  currentRequestId: string | undefined
  idList: any[] | undefined
}
export const initialState: AppState = {
  error: null,
  items: null,
  loading: 'idle',
  currentRequestId: undefined,
  idList: [],
}

export const postConditionProgress = createAsyncThunk<any, any>(
  'conditionProgresses/post',
  async (request, { rejectWithValue }) => {
    try {
      const response =
        await services.conditionProgressesService.createConditionProgress({
          ...request,
        })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const updateConditionProgress = createAsyncThunk<any, any>(
  'conditionProgresses/update',
  async (request, { rejectWithValue }) => {
    try {
      const response =
        await services.conditionProgressesService.updateConditionProgress({
          ...request,
        })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchConditionProgresses = createAsyncThunk<any, any>(
  'conditionProgresses/getAll',
  async ({ filter, input }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response =
        await services.conditionProgressesService.fetchConditionProgresses({
          filter,
          input,
        })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchConditionProgress = createAsyncThunk<any, any>(
  'conditionProgresses/get',
  async (id: string, { rejectWithValue }) => {
    try {
      const response =
        await services.conditionProgressesService.fetchConditionProgress(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

const appSlice = createSlice({
  name: 'conditionProgresses',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postConditionProgress.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      postConditionProgress.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          state.items = { ...state.items, [payload.conditionID]: payload }
          state.currentRequestId = undefined
          state.idList = [payload.id]
        }
      }
    )
    builder.addCase(
      postConditionProgress.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          state.error = JSON.stringify(error)
          state.currentRequestId = undefined
        }
      }
    )
    builder.addCase(updateConditionProgress.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      updateConditionProgress.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          state.items = { ...state.items, [payload.conditionID]: payload }
          state.currentRequestId = undefined
          state.idList = [payload.id]
        }
      }
    )
    builder.addCase(
      updateConditionProgress.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          state.error = JSON.stringify(error)
          state.currentRequestId = undefined
        }
      }
    )
    builder.addCase(fetchConditionProgresses.pending, (state, action) => {
      state.loading = 'pending'
      state.currentRequestId = action.meta.requestId
    })
    builder.addCase(
      fetchConditionProgresses.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        const { items, idList } = payload.reduce(
          (acc: Record<string, any>, item: Record<string, any>) => {
            acc.items[item.conditionID] = item
            if (!acc.idList.includes(item.conditionID)) {
              acc.idList.push(item.conditionID)
            }
            return acc
          },
          { items: {}, idList: [] }
        )

        state.loading = 'idle'
        state.items = { ...state.items, ...items }
        state.idList = idList
        state.currentRequestId = undefined
      }
    )
    builder.addCase(
      fetchConditionProgresses.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (state.currentRequestId === requestId) {
          state.loading = 'idle'
          state.error = JSON.stringify(error)
          state.currentRequestId = undefined
        }
      }
    )
    builder.addCase(fetchConditionProgress.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      fetchConditionProgress.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          state.items = { ...state.items, [payload.conditionID]: payload }
          state.currentRequestId = undefined
          state.idList = [payload.id]
        }
      }
    )
    builder.addCase(
      fetchConditionProgress.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          state.error = JSON.stringify(error)
          state.currentRequestId = undefined
        }
      }
    )
  },
})

export default appSlice.reducer
