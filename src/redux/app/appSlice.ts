import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'

export interface AppState {
  isLoggedIn: boolean
  user: {
    error: null | string | undefined
    items: Record<string, any> | null
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
  }
}
export const initialState: AppState = {
  isLoggedIn: false,
  user: {
    error: null,
    items: null,
    loading: 'idle',
    currentRequestId: undefined,
  },
}

export const resetReduxState = () => {
  return { type: 'RESET' }
}
export const signOut = createAsyncThunk(
  'app/user/signOut',
  async (args, { rejectWithValue }) => {
    try {
      const response = await services.amplifyService.signOut()
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchCurrentUser = createAsyncThunk(
  'app/user/get',
  async (args, { rejectWithValue }) => {
    try {
      const response = await services.userService.fetchCurrentUser()
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchCurrentUser.pending, (state, action) => {
      if (state.user.loading === 'idle') {
        state.user.loading = 'pending'
        state.user.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchCurrentUser.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (
        state.user.loading === 'pending' &&
        state.user.currentRequestId === requestId
      ) {
        state.user.loading = 'idle'
        state.user.items = payload
        state.isLoggedIn = true
        state.user.currentRequestId = undefined
      }
    })
    builder.addCase(fetchCurrentUser.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (
        state.user.loading === 'pending' &&
        state.user.currentRequestId === requestId
      ) {
        state.user.loading = 'idle'
        state.user.error = JSON.stringify(error)
        state.user.currentRequestId = undefined
      }
    })
    builder.addCase(signOut.pending, (state, action) => {
      if (state.user.loading === 'idle') {
        state.user.loading = 'pending'
        state.user.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(signOut.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (
        state.user.loading === 'pending' &&
        state.user.currentRequestId === requestId
      ) {
        state.user.loading = 'idle'
        state.isLoggedIn = false
        state.user.currentRequestId = undefined
      }
    })
    builder.addCase(signOut.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (
        state.user.loading === 'pending' &&
        state.user.currentRequestId === requestId
      ) {
        state.user.loading = 'idle'
        state.user.error = JSON.stringify(error)
        state.user.currentRequestId = undefined
      }
    })
  },
})

export default appSlice.reducer
