import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'

export interface AppState {
  error: null | string | undefined
  items: Record<string, any> | null | undefined
  loading: 'idle' | 'pending'
  currentRequestId: string | undefined
}
export const initialState: AppState = {
  error: null,
  items: null,
  loading: 'idle',
  currentRequestId: undefined,
}
export const postWallet = createAsyncThunk<any, any>(
  'wallets/post',
  async (request, { rejectWithValue }) => {
    try {
      const response = await services.walletService.createWallet({
        ...request,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchWallet = createAsyncThunk(
  'wallets/get',
  async ({ id }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.walletService.fetchWallet(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchWallets = createAsyncThunk<any, any>(
  'wallets/getAll',
  async ({ filter, input }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.walletService.fetchWallets({
        filter,
        input,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchWalletsByUserID = createAsyncThunk<any, any>(
  'walletsByUserID/getAll',
  async ({ filter, userID }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.walletService.fetchWalletsByUserID({
        filter,
        userID,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchWalletsByAddress = createAsyncThunk<any, any>(
  'walletsByAddress/getAll',
  async ({ filter, address }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.walletService.fetchWalletsByAddress({
        filter,
        address,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

const appSlice = createSlice({
  name: 'wallets',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchWallet.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchWallet.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.items = payload
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchWallet.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchWallets.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchWallets.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.items = payload
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchWallets.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchWalletsByUserID.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      fetchWalletsByUserID.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          state.items = payload
          state.currentRequestId = undefined
        }
      }
    )
    builder.addCase(fetchWalletsByUserID.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
  },
})

export default appSlice.reducer
