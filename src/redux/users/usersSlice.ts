import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'
import { BaseUser } from '@services/users/model'

export interface AppState {
  error: null | string | undefined
  items: Record<string, any> | null | undefined
  loading: 'idle' | 'pending'
  currentRequestId: string | undefined
}
export const initialState: AppState = {
  error: null,
  items: null,
  loading: 'idle',
  currentRequestId: undefined,
}

export const updateUser = createAsyncThunk(
  'users/update',
  async (userDetails: Record<string, any>, { rejectWithValue }) => {
    try {
      const { id, ...restOfDetails } = userDetails
      const response = await services.userService.updateUser(
        id,
        restOfDetails as BaseUser
      )
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchUser = createAsyncThunk(
  'users/get',
  async ({ id }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.userService.fetchUser(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchUsers = createAsyncThunk<any, any>(
  'users/getAll',
  async ({ filter, input }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.userService.fetchUsers({ filter, input })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

const appSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchUser.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchUser.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.items = payload
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchUser.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchUsers.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchUsers.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.items = payload
        state.currentRequestId = undefined
      }
    })
    builder.addCase(fetchUsers.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
    builder.addCase(updateUser.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(updateUser.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.items = payload
        state.currentRequestId = undefined
      }
    })
    builder.addCase(updateUser.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
  },
})

export default appSlice.reducer
