import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'

export interface AppState {
  error: null | string | undefined
  items: Record<string, any> | null | undefined
  loading: 'idle' | 'pending'
  currentRequestId: string | undefined
}
export const initialState: AppState = {
  error: null,
  items: null,
  loading: 'idle',
  currentRequestId: undefined,
}

export const postLocationView = createAsyncThunk<any, any>(
  'locationViews/post',
  async (request, { rejectWithValue }) => {
    try {
      const response = await services.locationViewsService.createLocationView(
        request
      )
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchLocationViews = createAsyncThunk<any, any>(
  'locationViews/getAll',
  async (request: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.locationViewsService.fetchLocationViews(
        request
      )
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

const appSlice = createSlice({
  name: 'locationViews',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchLocationViews.pending, (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      fetchLocationViews.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.loading === 'pending' &&
          state.currentRequestId === requestId
        ) {
          state.loading = 'idle'
          state.items = payload
          state.currentRequestId = undefined
        }
      }
    )
    builder.addCase(fetchLocationViews.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = JSON.stringify(error)
        state.currentRequestId = undefined
      }
    })
  },
})

export default appSlice.reducer
