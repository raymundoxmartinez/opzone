import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import services from '@services/index'

export interface AppState {
  items: Record<string, any> | null | undefined
  byReceiverId: {
    idList: string[]
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
    error: null | string | undefined
  }
  byId: {
    idList: string[]
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
    error: null | string | undefined
  }
  bySenderId: {
    idList: string[]
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
    error: null | string | undefined
  }
  byXummTransactionId: {
    idList: string[]
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
    error: null | string | undefined
  }
  all: {
    idList: string[]
    loading: 'idle' | 'pending'
    currentRequestId: string | undefined
    error: null | string | undefined
  }
}
export const initialState: AppState = {
  items: null,
  byId: {
    idList: [],
    loading: 'idle',
    currentRequestId: undefined,
    error: null,
  },
  byReceiverId: {
    idList: [],
    loading: 'idle',
    currentRequestId: undefined,
    error: null,
  },
  bySenderId: {
    idList: [],
    loading: 'idle',
    currentRequestId: undefined,
    error: null,
  },
  byXummTransactionId: {
    idList: [],
    loading: 'idle',
    currentRequestId: undefined,
    error: null,
  },
  all: {
    idList: [],
    loading: 'idle',
    currentRequestId: undefined,
    error: null,
  },
}
export const postTransaction = createAsyncThunk<any, any>(
  'transactions/post',
  async (request, { rejectWithValue }) => {
    try {
      const response = await services.transactionService.createTransaction({
        ...request,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const updateTransaction = createAsyncThunk<any, any>(
  'transactions/update',
  async (request, { rejectWithValue }) => {
    try {
      const response = await services.transactionService.updateTransaction({
        ...request,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchTransaction = createAsyncThunk(
  'transactions/get',
  async (id: string, { rejectWithValue }) => {
    try {
      const response = await services.transactionService.fetchTransaction(id)
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchTransactions = createAsyncThunk<any, any>(
  'transactions/getAll',
  async ({ filter, input }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response = await services.transactionService.fetchTransactions({
        filter,
        input,
      })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchTransactionsByXummTransactionID = createAsyncThunk<any, any>(
  'transactions/getTransactionsByXummTransactionID',
  async (
    { filter, xummTransactionID }: Record<string, any>,
    { rejectWithValue }
  ) => {
    try {
      const response =
        await services.transactionService.fetchTransactionsByXummTransactionID({
          filter,
          xummTransactionID,
        })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchTransactionsBySenderID = createAsyncThunk<any, any>(
  'transactions/getTransactionsBySenderID',
  async ({ filter, senderID }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response =
        await services.transactionService.fetchTransactionsBySenderID({
          filter,
          senderID,
        })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)
export const fetchTransactionsByReceiverID = createAsyncThunk<any, any>(
  'transactions/getTransactionsByReceiverID',
  async ({ filter, receiverID }: Record<string, any>, { rejectWithValue }) => {
    try {
      const response =
        await services.transactionService.fetchTransactionsByReceiverID({
          filter,
          receiverID,
        })
      return response
    } catch (error) {
      //@ts-ignore
      if (!error.response) {
        throw error
      }
      //@ts-ignore
      return rejectWithValue(error.response.data)
    }
  }
)

const appSlice = createSlice({
  name: 'transactions',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchTransaction.pending, (state, action) => {
      state.byId.loading = 'pending'
      state.byId.currentRequestId = action.meta.requestId
    })
    builder.addCase(fetchTransaction.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta

      const itemByID = { [payload.id]: payload }
      state.byId.loading = 'idle'
      state.items = { ...state.items, ...itemByID }
      state.byId.idList = [payload.id]
      state.byId.currentRequestId = undefined
    })
    builder.addCase(fetchTransaction.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (
        state.byId.loading === 'pending' &&
        state.byId.currentRequestId === requestId
      ) {
        state.byId.loading = 'idle'
        state.byId.error = JSON.stringify(error)
        state.byId.currentRequestId = undefined
      }
    })
    builder.addCase(fetchTransactions.pending, (state, action) => {
      if (state.all.loading === 'idle') {
        state.all.loading = 'pending'
        state.all.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(fetchTransactions.fulfilled, (state, { payload, meta }) => {
      const { requestId } = meta
      if (
        state.all.loading === 'pending' &&
        state.all.currentRequestId === requestId
      ) {
        const itemsAll = payload.reduce(
          (acc: Record<string, any>, item: Record<string, any>) => {
            acc[item.id] = item
            return acc
          },
          {}
        )
        state.all.loading = 'idle'
        state.items = { ...state.items, ...itemsAll }
        state.all.idList = payload.reduce(
          (acc: Record<string, any>, item: Record<string, any>) => {
            acc.push(item.id)
            return acc
          },
          []
        )
        state.all.currentRequestId = undefined
      }
    })
    builder.addCase(fetchTransactions.rejected, (state, { error, meta }) => {
      const { requestId } = meta
      if (
        state.all.loading === 'pending' &&
        state.all.currentRequestId === requestId
      ) {
        state.all.loading = 'idle'
        state.all.error = JSON.stringify(error)
        state.all.currentRequestId = undefined
      }
    })
    builder.addCase(fetchTransactionsBySenderID.pending, (state, action) => {
      if (state.bySenderId.loading === 'idle') {
        state.bySenderId.loading = 'pending'
        state.bySenderId.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      fetchTransactionsBySenderID.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.bySenderId.loading === 'pending' &&
          state.bySenderId.currentRequestId === requestId
        ) {
          const itemsBySenderID = payload.reduce(
            (acc: Record<string, any>, item: Record<string, any>) => {
              acc[item.id] = item
              return acc
            },
            {}
          )
          state.bySenderId.loading = 'idle'
          state.items = { ...state.items, ...itemsBySenderID }
          state.bySenderId.idList = payload.reduce(
            (acc: Record<string, any>, item: Record<string, any>) => {
              acc.push(item.id)
              return acc
            },
            []
          )
          state.bySenderId.currentRequestId = undefined
        }
      }
    )
    builder.addCase(
      fetchTransactionsBySenderID.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (
          state.bySenderId.loading === 'pending' &&
          state.bySenderId.currentRequestId === requestId
        ) {
          state.bySenderId.loading = 'idle'
          state.bySenderId.error = JSON.stringify(error)
          state.bySenderId.currentRequestId = undefined
        }
      }
    )
    builder.addCase(fetchTransactionsByReceiverID.pending, (state, action) => {
      if (state.byReceiverId.loading === 'idle') {
        state.byReceiverId.loading = 'pending'
        state.byReceiverId.currentRequestId = action.meta.requestId
      }
    })
    builder.addCase(
      fetchTransactionsByReceiverID.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.byReceiverId.loading === 'pending' &&
          state.byReceiverId.currentRequestId === requestId
        ) {
          const itemsByReceiverID = payload.reduce(
            (acc: Record<string, any>, item: Record<string, any>) => {
              acc[item.id] = item
              return acc
            },
            {}
          )
          state.byReceiverId.loading = 'idle'
          state.items = { ...state.items, ...itemsByReceiverID }
          state.byReceiverId.idList = payload.reduce(
            (acc: Record<string, any>, item: Record<string, any>) => {
              acc.push(item.id)
              return acc
            },
            []
          )
          state.byReceiverId.currentRequestId = undefined
        }
      }
    )
    builder.addCase(
      fetchTransactionsByReceiverID.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (
          state.byReceiverId.loading === 'pending' &&
          state.byReceiverId.currentRequestId === requestId
        ) {
          state.byReceiverId.loading = 'idle'
          state.byReceiverId.error = JSON.stringify(error)
          state.byReceiverId.currentRequestId = undefined
        }
      }
    )
    builder.addCase(
      fetchTransactionsByXummTransactionID.pending,
      (state, action) => {
        if (state.byXummTransactionId.loading === 'idle') {
          state.byXummTransactionId.loading = 'pending'
          state.byXummTransactionId.currentRequestId = action.meta.requestId
        }
      }
    )
    builder.addCase(
      fetchTransactionsByXummTransactionID.fulfilled,
      (state, { payload, meta }) => {
        const { requestId } = meta
        if (
          state.byXummTransactionId.loading === 'pending' &&
          state.byXummTransactionId.currentRequestId === requestId
        ) {
          const itemsByXummTransactionID = payload.reduce(
            (acc: Record<string, any>, item: Record<string, any>) => {
              acc[item.id] = item
              return acc
            },
            {}
          )
          state.byXummTransactionId.loading = 'idle'
          state.items = { ...state.items, ...itemsByXummTransactionID }
          state.byXummTransactionId.idList = payload.reduce(
            (acc: Record<string, any>, item: Record<string, any>) => {
              acc.push(item.id)
              return acc
            },
            []
          )
          state.byXummTransactionId.currentRequestId = undefined
        }
      }
    )
    builder.addCase(
      fetchTransactionsByXummTransactionID.rejected,
      (state, { error, meta }) => {
        const { requestId } = meta
        if (
          state.byXummTransactionId.loading === 'pending' &&
          state.byXummTransactionId.currentRequestId === requestId
        ) {
          state.byXummTransactionId.loading = 'idle'
          state.byXummTransactionId.error = JSON.stringify(error)
          state.byXummTransactionId.currentRequestId = undefined
        }
      }
    )
  },
})

export default appSlice.reducer
