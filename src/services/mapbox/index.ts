//@ts-ignore
import mapboxgl from '!mapbox-gl'

const MapboxService = function () {
  return {
    getAddressFromCoordinates,
    getStaticMapUrl,
    verifyLongitudeLatitudeBounds,
  }

  async function getAddressFromCoordinates({
    latitude,
    longitude,
  }: {
    latitude: string
    longitude: string
  }) {
    return fetch(
      `https://api.mapbox.com/geocoding/v5/mapbox.places/${longitude},${latitude}.json?access_token=${process.env.NEXT_PUBLIC_MAPBOX_ACCCESS_TOKEN}`
    )
      .then((res) => {
        return res.json()
      })
      .then((res) => {
        const placeName = res.features[0].place_name
        return placeName
      })
      .catch((err) => {
        throw err
      })
  }
  async function getStaticMapUrl({
    latitude,
    longitude,
  }: {
    latitude: string
    longitude: string
  }) {
    return `https://api.mapbox.com/styles/v1/mapbox/light-v10/static/pin-s-l+000(${longitude},${latitude})/${longitude},${latitude},14/500x300?access_token=${process.env.NEXT_PUBLIC_MAPBOX_ACCCESS_TOKEN}`
  }

  function verifyLongitudeLatitudeBounds({
    latitude,
    longitude,
  }: {
    latitude: string
    longitude: string
  }) {
    return new Promise((resolve, reject) => {
      const ll = new mapboxgl.LngLat(longitude, latitude)
      const llb = ll.toBounds(100)
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const currentLocation = new mapboxgl.LngLat(
            position.coords.longitude,
            position.coords.latitude
          )
          resolve(llb.contains(currentLocation))
        },
        (error) => {
          reject(error)
        }
      )
    })
  }
}

export default MapboxService
