import { API, graphqlOperation, Auth } from 'aws-amplify'
import {
  createTrail as createTrailGraphQL,
  createTrailLocationViews as createTrailLocationViewsGraphQL,
  deleteTrail as deleteTrailGraphQL,
} from '@graphql/mutations'
import { getTrail, listTrails } from '@graphql/customQueries'

const TrailsService = function () {
  return {
    fetchTrails,
    fetchTrail,
    createTrail,
    createTrailLocationView,
    deleteTrail,
  }
  async function createTrail({
    name,
    description,
    type,
    images,
    userID,
  }: {
    name: string
    description: string
    type: string
    userID: string
    images: any[]
  }) {
    const { attributes, username, ...rest } =
      await Auth.currentAuthenticatedUser()
    try {
      const {
        data: { createTrail: newTrail },
      } = await (API.graphql(
        graphqlOperation(createTrailGraphQL, {
          input: { name, description, images, userID },
        })
      ) as Promise<any>)
      return newTrail
    } catch (error) {
      throw error
    }
  }

  async function createTrailLocationView({
    trailID,
    locationViewID,
  }: {
    trailID: string
    locationViewID: string
  }) {
    const { attributes, username, ...rest } =
      await Auth.currentAuthenticatedUser()
    try {
      const {
        data: { createTrailLocationViews: trailLocationViews },
      } = await (API.graphql(
        graphqlOperation(createTrailLocationViewsGraphQL, {
          input: { trailID, locationViewID },
        })
      ) as Promise<any>)
      return trailLocationViews
    } catch (error) {
      throw error
    }
  }
  async function fetchTrails({ filter, input }: { filter: any; input: any }) {
    try {
      const {
        data: {
          listTrails: { items },
        },
      } = await (API.graphql(
        graphqlOperation(listTrails, { filter, input })
      ) as Promise<any>)
      return items
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
  async function fetchTrail(id: string) {
    try {
      const {
        data: { getTrail: trail },
      } = await (API.graphql(
        graphqlOperation(getTrail, { id })
      ) as Promise<any>)
      return trail
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }

  async function deleteTrail(id: string) {
    try {
      const {
        data: { deleteTrail: trail },
      } = await (API.graphql(
        graphqlOperation(deleteTrailGraphQL, { input: { id } })
      ) as Promise<any>)
      return trail
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
}

export default TrailsService
