// const { normalize, schema } = require('normalizr')
import { normalize, schema } from 'normalizr'

export const normalizeTrail = (originalData: any) => {
  // Define the image schema
  const imageSchema = new schema.Entity('images', {}, { idAttribute: 'key' })

  // Define the location schema
  const locationSchema = new schema.Entity('locations')

  // Define the trail schema with images and locations relationship
  const trailSchema = new schema.Entity('trails', {
    images: [imageSchema],
    locations: {
      items: [locationSchema],
    },
  })

  // Normalize the data
  const normalizedData = normalize(originalData, trailSchema)
  return normalizedData
}

// const { entities, result } = normalizeTrail({
//   id: 'dc7c6ac3-f8c8-496b-aa1c-1d80747a4cb2',
//   name: 'fgrg',
//   description: '4ger',
//   duration: null,
//   type: null,
//   images: [
//     {
//       bucket: 'omniagorastore124213-dev',
//       region: 'us-east-1',
//       key: '71ff2b768fUntitled.png',
//     },
//     {
//       bucket: 'omniagorastore124213-dev',
//       region: 'us-east-1',
//       key: 'fb8bbbc3b2image-20210610-173829.png',
//     },
//   ],
//   locations: {
//     items: [
//       {
//         id: 'd2c60ea1-dad4-4918-97a4-7e30aa8c6504',
//         trailID: 'dc7c6ac3-f8c8-496b-aa1c-1d80747a4cb2',
//         locationViewID: '9f777ab0-12f1-4de1-961d-3dfbb28cd13a',
//         createdAt: '2023-05-14T22:17:39.102Z',
//         updatedAt: '2023-05-14T22:17:39.102Z',
//       },
//     ],
//     nextToken: null,
//   },
//   order: null,
//   createdAt: '2023-05-14T22:17:38.637Z',
//   updatedAt: '2023-05-14T22:17:38.637Z',
// })
