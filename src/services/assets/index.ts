import { API, graphqlOperation, Auth } from 'aws-amplify'
import {
  createAsset as createAssetGraphQL,
  deleteAsset as deleteAssetGraphQL,
} from '@graphql/mutations'
import { listAssets, getAsset } from '@graphql/queries'

const AssetsService = function () {
  return {
    fetchAssets,
    fetchAsset,
    createAsset,
    deleteAsset,
  }
  async function createAsset({
    name,
    file,
    userID,
  }: {
    name: string
    file: any
    userID: string
  }) {
    const { attributes, username, ...rest } =
      await Auth.currentAuthenticatedUser()
    try {
      const {
        data: { createAsset: newAsset },
      } = await (API.graphql(
        graphqlOperation(createAssetGraphQL, {
          input: { name, file, userID },
        })
      ) as Promise<any>)
      return newAsset
    } catch (error) {
      throw error
    }
  }
  async function fetchAssets({ filter, input }: { filter: any; input: any }) {
    try {
      const {
        data: {
          listAssets: { items },
        },
      } = await (API.graphql(
        graphqlOperation(listAssets, { filter, input })
      ) as Promise<any>)
      return items
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }

  async function fetchAsset(id: string) {
    try {
      const {
        data: { getAsset: asset },
      } = await (API.graphql(
        graphqlOperation(getAsset, { id })
      ) as Promise<any>)
      return asset
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
  async function deleteAsset(id: string) {
    try {
      const {
        data: { deleteAsset: asset },
      } = await (API.graphql(
        graphqlOperation(deleteAssetGraphQL, { input: { id } })
      ) as Promise<any>)
      return asset
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
}

export default AssetsService
