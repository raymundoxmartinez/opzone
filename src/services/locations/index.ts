import { API, graphqlOperation } from 'aws-amplify'
import {
  createLocation as createLocationGraphQL,
  updateLocation as updateLocationGraphQL,
} from '@graphql/mutations'
import {
  getLocation as getLocationGraphQl,
  listLocations,
} from '@graphql/queries'
import {
  CreateLocationInput,
  UpdateLocationInput,
  GetLocationQueryVariables,
  ListLocationsQueryVariables,
} from '../../API'

const LocationService = function () {
  return {
    createLocation,
    updateLocation,
    fetchLocation,
    fetchLocations,
  }

  async function createLocation({
    name,
    address,
    description,
    coordinates,
    userID,
  }: CreateLocationInput) {
    try {
      const {
        data: { createLocation: newLocation },
      } = await (API.graphql(
        graphqlOperation(createLocationGraphQL, {
          input: {
            name,
            address,
            description,
            coordinates,
            userID,
          },
        })
      ) as Promise<any>)
      return newLocation
    } catch (error) {
      throw error
    }
  }

  async function updateLocation({
    id,
    name,
    address,
    description,
    coordinates,
    userID,
  }: UpdateLocationInput) {
    try {
      const {
        data: { updateLocation: updatedLocation },
      } = await (API.graphql(
        graphqlOperation(updateLocationGraphQL, {
          input: {
            id,
            name,
            address,
            description,
            coordinates,
            userID,
          },
        })
      ) as Promise<any>)
      return updatedLocation
    } catch (error) {
      throw error
    }
  }

  async function fetchLocation({
    id,
  }: GetLocationQueryVariables): Promise<any> {
    const {
      data: { getLocation },
    } = await (API.graphql(
      graphqlOperation(getLocationGraphQl, { id })
    ) as Promise<any>)
    return getLocation
  }

  async function fetchLocations({
    filter,
    limit,
    nextToken,
  }: ListLocationsQueryVariables) {
    try {
      const {
        data: {
          listTrails: { items },
        },
      } = await (API.graphql(
        graphqlOperation(listLocations, { filter, limit, nextToken })
      ) as Promise<any>)
      return items
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
}

export default LocationService
