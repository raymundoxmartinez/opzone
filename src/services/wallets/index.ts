import { API, graphqlOperation, Auth } from 'aws-amplify'
import { createWallet as createWalletGraphQL } from '@graphql/mutations'
import {
  searchWallets,
  getWallet as getWalletGraphQl,
  walletsByUser,
  walletsByAddress,
} from '@graphql/queries'
const WalletService = function () {
  return {
    createWallet,
    fetchWallet,
    fetchWallets,
    fetchWalletsByUserID,
    fetchWalletsByAddress,
  }
  async function createWallet({
    title,
    description,
    address,
  }: {
    title: string
    description: string
    address: string
  }) {
    const { attributes, username, ...rest } =
      await Auth.currentAuthenticatedUser()
    try {
      const {
        data: { createWallet: newWallet },
      } = await (API.graphql(
        graphqlOperation(createWalletGraphQL, {
          input: { title, address, description, userID: attributes.sub },
        })
      ) as Promise<any>)
      return newWallet
    } catch (error) {
      throw error
    }
  }

  async function fetchWallet(id: string): Promise<any> {
    const {
      data: { getWallet },
    } = await (API.graphql(
      graphqlOperation(getWalletGraphQl, { id })
    ) as Promise<any>)
    return getWallet
  }

  async function fetchWallets({ filter, input }: { filter: any; input: any }) {
    const {
      data: {
        searchWallets: { items },
      },
    } = await (API.graphql(
      graphqlOperation(searchWallets, { filter, input })
    ) as Promise<any>)
    return items
  }
  async function fetchWalletsByUserID({
    filter,
    userID,
  }: {
    filter: any
    userID: any
  }) {
    const {
      data: {
        walletsByUser: { items },
      },
    } = await (API.graphql(
      graphqlOperation(walletsByUser, { filter, userID })
    ) as Promise<any>)
    return items
  }
  async function fetchWalletsByAddress({
    filter,
    address,
  }: {
    filter: any
    address: string
  }) {
    const {
      data: {
        walletsByAddress: { items },
      },
    } = await (API.graphql(
      graphqlOperation(walletsByAddress, { filter, address })
    ) as Promise<any>)
    return items
  }
}

export default WalletService
