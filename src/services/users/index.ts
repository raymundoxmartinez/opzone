import { API, graphqlOperation, Auth, Storage } from 'aws-amplify'
import { User, BaseUser } from './model'
import { updateUser as updateUserGraphQL } from '@graphql/mutations'
import { getUser as getUserGraphQl, listUsers } from '@graphql/queries'
import { uploadToS3, retrieveFromS3 } from '../../utils'

const UserService = function () {
  return {
    updateUser,
    fetchUser,
    fetchCurrentUser,
    fetchUsers,
  }
  async function updateUser(id: string, userDetails: BaseUser): Promise<User> {
    if (userDetails.images && userDetails.images.length) {
      if (typeof userDetails.images[0] === 'string') {
        delete userDetails.images
      } else {
        const newFiles = userDetails.images.filter(
          (image) => typeof image !== 'string'
        )
        const preppedNewFiles = await uploadToS3(newFiles)
        userDetails.images = preppedNewFiles
      }
    }
    const {
      data: { updateUser },
    } = await (API.graphql(
      graphqlOperation(updateUserGraphQL, {
        input: {
          id,
          ...userDetails,
        },
      })
    ) as Promise<any>)
    let imageUrl, backgroundImageUrl, location
    if (updateUser['images']) {
      imageUrl = await retrieveFromS3(updateUser['images'])
    }
    if (updateUser['backgroundImage']) {
      backgroundImageUrl = await Storage.get(updateUser['backgroundImage'])
    }
    try {
      location = JSON.parse(updateUser['location'])
    } catch (error) {
      location = updateUser['location']
    }
    return {
      ...updateUser,
      ['images']: imageUrl,
      ['backgroundImage']: backgroundImageUrl,
      location,
    }
  }
  async function fetchUser(id: string): Promise<User> {
    const {
      data: { getUser: currentUser },
    } = await (API.graphql(
      graphqlOperation(getUserGraphQl, { id })
    ) as Promise<any>)
    let imageUrl, backgroundImageUrl, location
    if (currentUser['images']) {
      imageUrl = await retrieveFromS3(currentUser['images'])
    }
    if (currentUser['backgroundImage']) {
      backgroundImageUrl = await Storage.get(currentUser['backgroundImage'])
    }
    try {
      location = JSON.parse(currentUser['location'])
    } catch (error) {
      location = currentUser['location']
    }
    return {
      ...currentUser,
      ['images']: imageUrl,
      ['backgroundImage']: backgroundImageUrl,
      location,
    }
  }
  async function fetchCurrentUser(): Promise<User> {
    const { attributes, username } = await Auth.currentAuthenticatedUser()

    const {
      data: { getUser: currentUser },
    } = await (API.graphql(
      graphqlOperation(getUserGraphQl, { id: attributes.sub })
    ) as Promise<any>)
    let imageUrl, backgroundImageUrl, location
    if (currentUser) {
      if (currentUser['images']) {
        imageUrl = await retrieveFromS3(currentUser['images'])
      }
      if (currentUser['backgroundImage']) {
        backgroundImageUrl = await Storage.get(currentUser['backgroundImage'])
      }
      try {
        location = JSON.parse(currentUser['location'])
      } catch (error) {
        location = currentUser['location']
      }
    }
    return {
      ...(currentUser || { ...attributes, id: attributes.sub }),
      ['images']: imageUrl ? imageUrl : [],
      ['backgroundImage']: backgroundImageUrl ? backgroundImageUrl : '',
      location: location ? location : '',
      username,
    }
  }

  async function fetchUsers({ filter, input }: { filter: any; input: any }) {
    try {
      const {
        data: {
          listUsers: { items },
        },
      } = await (API.graphql(
        graphqlOperation(listUsers, { filter, input })
      ) as Promise<any>)
      return items
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
}

export default UserService
