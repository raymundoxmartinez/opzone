export interface BaseUser {
  username: string
  email: string
  bio: string
  location: string
  images?: any[]
  backgroundImage: string
  wallets: string[]
}

export interface User extends BaseUser {
  id: string
}
