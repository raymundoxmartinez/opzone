import { API, graphqlOperation } from 'aws-amplify'
import {
  createLocationView as createLocationViewGraphQL,
  updateLocationView as updateLocationViewGraphQL,
} from '@graphql/mutations'
import {
  getLocationView as getLocationViewGraphQl,
  listLocationViews,
} from '@graphql/queries'
import {
  CreateLocationViewInput,
  UpdateLocationViewInput,
  GetLocationViewQueryVariables,
  ListLocationViewsQueryVariables,
} from '../../API'

const LocationViewService = function () {
  return {
    createLocationView,
    updateLocationView,
    fetchLocationView,
    fetchLocationViews,
  }

  async function createLocationView({
    name,
    address,
    description,
    coordinates,
    userID,
    locationID,
  }: CreateLocationViewInput) {
    try {
      const {
        data: { createLocationView: newLocationView },
      } = await (API.graphql(
        graphqlOperation(createLocationViewGraphQL, {
          input: {
            name,
            address,
            description,
            coordinates,
            userID,
            locationID,
          },
        })
      ) as Promise<any>)
      return newLocationView
    } catch (error) {
      throw error
    }
  }

  async function updateLocationView({
    id,
    name,
    address,
    description,
    coordinates,
    userID,
    locationID,
  }: UpdateLocationViewInput) {
    try {
      const {
        data: { updateLocationView: updatedLocationView },
      } = await (API.graphql(
        graphqlOperation(updateLocationViewGraphQL, {
          input: {
            id,
            name,
            address,
            description,
            coordinates,
            userID,
            locationID,
          },
        })
      ) as Promise<any>)
      return updatedLocationView
    } catch (error) {
      throw error
    }
  }

  async function fetchLocationView({
    id,
  }: GetLocationViewQueryVariables): Promise<any> {
    const {
      data: { getLocationView },
    } = await (API.graphql(
      graphqlOperation(getLocationViewGraphQl, { id })
    ) as Promise<any>)
    return getLocationView
  }

  async function fetchLocationViews({
    filter,
    limit,
    nextToken,
  }: ListLocationViewsQueryVariables) {
    try {
      const {
        data: {
          listTrails: { items },
        },
      } = await (API.graphql(
        graphqlOperation(listLocationViews, { filter, limit, nextToken })
      ) as Promise<any>)
      return items
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
}

export default LocationViewService
