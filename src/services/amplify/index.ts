import { Auth } from 'aws-amplify'

const AmplifyService = function () {
  return {
    signOut,
  }

  async function signOut() {
    return await Auth.signOut()
  }
}

export default AmplifyService
