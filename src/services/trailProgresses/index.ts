import { API, graphqlOperation, Auth } from 'aws-amplify'
import {
  createTrailProgress as createTrailProgressGraphQL,
  deleteTrailProgress as deleteTrailProgressGraphQL,
} from '@graphql/mutations'
import { getTrailProgress, listTrailProgresses } from '@graphql/customQueries'

const TrailProgressesService = function () {
  return {
    fetchTrailProgresses,
    fetchTrailProgress,
    createTrailProgress,
    deleteTrailProgress,
  }
  async function createTrailProgress({
    name,
    status,
    userID,
    trailID,
  }: {
    name?: string
    status: string
    userID: string
    trailID: string
  }) {
    const { attributes, username, ...rest } =
      await Auth.currentAuthenticatedUser()
    try {
      const {
        data: { createTrailProgress: newTrail },
      } = await (API.graphql(
        graphqlOperation(createTrailProgressGraphQL, {
          input: { name, status, userID, trailID },
        })
      ) as Promise<any>)
      return newTrail
    } catch (error) {
      throw error
    }
  }

  async function fetchTrailProgresses({
    filter,
    input,
  }: {
    filter: any
    input: any
  }) {
    try {
      const {
        data: {
          listTrailProgresses: { items },
        },
      } = await (API.graphql(
        graphqlOperation(listTrailProgresses, { filter, input })
      ) as Promise<any>)
      return items
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
  async function fetchTrailProgress(id: string) {
    try {
      const {
        data: { getTrailProgress: trail },
      } = await (API.graphql(
        graphqlOperation(getTrailProgress, { id })
      ) as Promise<any>)
      return trail
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
  async function deleteTrailProgress(id: string) {
    try {
      const {
        data: { deleteTrailProgress: trail },
      } = await (API.graphql(
        graphqlOperation(deleteTrailProgressGraphQL, { input: { id } })
      ) as Promise<any>)
      return trail
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
}

export default TrailProgressesService
