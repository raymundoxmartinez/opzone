import { API, graphqlOperation, Auth } from 'aws-amplify'
import {
  createTransaction as createTransactionGraphQL,
  updateTransaction as updateTransactionGraphQL,
} from '@graphql/mutations'
import {
  getTransaction as getTransactionGraphQl,
  transactionsByXummTransactionID,
  transactionsBySenderID,
  transactionsByReceiverID,
} from '@graphql/queries'

const TransactionService = function () {
  return {
    createTransaction,
    updateTransaction,
    fetchTransaction,
    fetchTransactions,
    fetchTransactionsByXummTransactionID,
    fetchTransactionsBySenderID,
    fetchTransactionsByReceiverID,
  }

  async function createTransaction({
    status = 'pending',
    amount,
    type = 'FILE',
    fromWalletID,
    toWalletID,
    senderID,
    receiverID,
    fulfillment,
    condition,
    expiration,
    xummTransactionID,
    locationViewID,
    rewardID,
    description,
  }: {
    status: string
    amount: string
    type: string
    fromWalletID?: string
    toWalletID?: string
    senderID: string
    receiverID?: string
    fulfillment: string
    condition: string
    expiration: string
    xummTransactionID: string
    locationViewID: string
    rewardID: string
    description?: string
  }) {
    const { attributes, username, ...rest } =
      await Auth.currentAuthenticatedUser()
    try {
      const {
        data: { createTransaction: newTransaction },
      } = await (API.graphql(
        graphqlOperation(createTransactionGraphQL, {
          input: {
            status,
            amount,
            type,
            fromWalletID,
            toWalletID,
            receiverID,
            fulfillment,
            condition,
            expiration,
            senderID,
            xummTransactionID,
            locationViewID,
            rewardID,
            description,
          },
        })
      ) as Promise<any>)
      return newTransaction
    } catch (error) {
      throw error
    }
  }
  async function updateTransaction({
    status,
    id,
    conditions,
  }: {
    status: string
    conditions: any[]
    id: string
  }) {
    try {
      const {
        data: { updateTransaction: updatedTransaction },
      } = await (API.graphql(
        graphqlOperation(updateTransactionGraphQL, {
          input: {
            status,
            conditions,
            id,
          },
        })
      ) as Promise<any>)
      return updatedTransaction
    } catch (error) {
      throw error
    }
  }

  async function fetchTransaction(id: string): Promise<any> {
    const {
      data: { getTransaction },
    } = await (API.graphql(
      graphqlOperation(getTransactionGraphQl, { id })
    ) as Promise<any>)
    return getTransaction
  }

  async function fetchTransactions({
    filter,
    input,
  }: {
    filter: any
    input: any
  }) {
    const {
      data: {
        searchTransactions: { items },
      },
    } = await (API.graphql(
      graphqlOperation(searchTransactions, { filter, input })
    ) as Promise<any>)
    return items
  }

  async function fetchTransactionsByXummTransactionID({
    filter,
    xummTransactionID,
  }: {
    filter: any
    xummTransactionID: any
  }) {
    const {
      data: {
        transactionsByXummTransactionID: { items },
      },
    } = await (API.graphql(
      graphqlOperation(transactionsByXummTransactionID, {
        filter,
        xummTransactionID,
      })
    ) as Promise<any>)
    return items
  }
  async function fetchTransactionsBySenderID({
    filter,
    senderID,
  }: {
    filter: any
    senderID: any
  }) {
    const {
      data: {
        transactionsBySenderID: { items },
      },
    } = await (API.graphql(
      graphqlOperation(transactionsBySenderID, {
        filter,
        senderID,
      })
    ) as Promise<any>)
    return items
  }

  async function fetchTransactionsByReceiverID({
    filter,
    receiverID,
  }: {
    filter: any
    receiverID: any
  }) {
    const {
      data: {
        transactionsByReceiverID: { items },
      },
    } = await (API.graphql(
      graphqlOperation(transactionsByReceiverID, {
        filter,
        receiverID,
      })
    ) as Promise<any>)
    return items
  }
}

export default TransactionService
