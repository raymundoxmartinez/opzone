import UserService from './users'
import AssetsService from './assets'
import XummService from './xumm'
import WalletService from './wallets'
import TransactionService from './transactions'
import ConditionService from './conditions'
import MapboxService from './mapbox'
import AmplifyService from './amplify'
import TrailsService from './trails'
import LocationsService from './locations'
import LocationViewsService from './locationViews'
import TrailProgressesService from './trailProgresses'
import ConditionProgressesService from './conditionProgresses'
import RewardsService from './rewards'

const services = {
  assetsService: AssetsService(),
  rewardsService: RewardsService(),
  userService: UserService(),
  xummService: XummService(),
  walletService: WalletService(),
  transactionService: TransactionService(),
  conditionService: ConditionService(),
  mapboxService: MapboxService(),
  amplifyService: AmplifyService(),
  trailsService: TrailsService(),
  locationsService: LocationsService(),
  locationViewsService: LocationViewsService(),
  trailProgressesService: TrailProgressesService(),
  conditionProgressesService: ConditionProgressesService(),
}
export default services
