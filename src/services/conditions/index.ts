import { API, graphqlOperation, Auth } from 'aws-amplify'
import { createCondition as createConditionGraphQL } from '@graphql/mutations'
import {
  getCondition as getConditionGraphQl,
  listConditions,
} from '@graphql/queries'

const ConditionService = function () {
  return {
    createCondition,
    fetchCondition,
    fetchConditions,
  }

  async function createCondition({
    status = 'false',
    type,
    latitude,
    longitude,
    question,
    choices,
    answerIndex,
    transactionID,
  }: {
    status: string
    type: string
    latitude?: string
    longitude?: string
    question?: string
    choices?: string[]
    answerIndex?: number
    transactionID: string
  }) {
    try {
      const {
        data: { createCondition: newCondition },
      } = await (API.graphql(
        graphqlOperation(createConditionGraphQL, {
          input: {
            status,
            type,
            latitude,
            longitude,
            question,
            choices,
            answerIndex,
            transactionID,
          },
        })
      ) as Promise<any>)
      return newCondition
    } catch (error) {
      throw error
    }
  }

  async function fetchCondition(id: string): Promise<any> {
    const {
      data: { getCondition },
    } = await (API.graphql(
      graphqlOperation(getConditionGraphQl, { id })
    ) as Promise<any>)
    return getCondition
  }

  async function fetchConditions({
    filter,
    input,
  }: {
    filter: any
    input: any
  }) {
    const {
      data: {
        searchConditions: { items },
      },
    } = await (API.graphql(
      graphqlOperation(listConditions, { filter, input })
    ) as Promise<any>)
    return items
  }
}

export default ConditionService
