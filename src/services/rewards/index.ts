import { API, graphqlOperation, Auth } from 'aws-amplify'
import {
  createReward as createRewardGraphQL,
  deleteReward as deleteRewardGraphQL,
} from '@graphql/mutations'
import { listRewards, getReward } from '@graphql/queries'

const RewardsService = function () {
  return {
    fetchRewards,
    fetchReward,
    createReward,
    deleteReward,
  }
  async function createReward({
    name,
    file,
    userID,
  }: {
    name: string
    file: any
    userID: string
  }) {
    const { attributes, username, ...rest } =
      await Auth.currentAuthenticatedUser()
    try {
      const {
        data: { createReward: newReward },
      } = await (API.graphql(
        graphqlOperation(createRewardGraphQL, {
          input: { name, file, userID },
        })
      ) as Promise<any>)
      return newReward
    } catch (error) {
      throw error
    }
  }
  async function fetchRewards({ filter, input }: { filter: any; input: any }) {
    try {
      const {
        data: {
          listRewards: { items },
        },
      } = await (API.graphql(
        graphqlOperation(listRewards, { filter, input })
      ) as Promise<any>)
      return items
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }

  async function fetchReward(id: string) {
    try {
      const {
        data: { getReward: reward },
      } = await (API.graphql(
        graphqlOperation(getReward, { id })
      ) as Promise<any>)
      return reward
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
  async function deleteReward(id: string) {
    try {
      const {
        data: { deleteReward: reward },
      } = await (API.graphql(
        graphqlOperation(deleteRewardGraphQL, { input: { id } })
      ) as Promise<any>)
      return reward
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
}

export default RewardsService
