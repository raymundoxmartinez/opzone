export interface Transaction {
  xummUserToken: string
  destination: string
  amount: number
  memo: string
  returnUrl: string
  type: string
}
export interface EscrowTransaction {
  xummUserToken: string
  destination: string
  amount: number
  memo: string
  returnUrl: string
  condition: string
  cancelAfter: string
}
