import { API } from 'aws-amplify'
import { Transaction } from './types'

const XummService = function () {
  return {
    postTransaction,
    postEscrowTransaction,
    postEscrowFinishTransaction,
    getTransaction,
    signIn,
    getConditionAndFulfillment,
  }
  async function postTransaction({
    returnUrl,
    destination,
    xummUserToken,
    amount,
    memo,
    type,
  }: Transaction) {
    const response = await API.post('xumm', '/payload', {
      body: {
        options: {
          submit: true,
          expire: 240,
          return_url: {
            web: returnUrl,
            app: returnUrl,
          },
        },
        user_token: xummUserToken,
        txjson: {
          TransactionType: 'Payment',
          //@ts-ignore
          Destination: destination,
          Amount: amount,
          // Memo: {
          //   MemoData: memo,
          //   MemoType: 'string',
          // },
        },
      },
    })

    return response
  }
  async function postEscrowTransaction({
    returnUrl,
    destination,
    xummUserToken,
    condition,
    cancelAfter,
    amount,
    memo,
  }: any) {
    const response = await API.post('xumm', '/payload', {
      body: {
        options: {
          submit: true,
          expire: 240,
          return_url: {
            web: returnUrl,
            app: returnUrl,
          },
        },
        user_token: xummUserToken,
        txjson: {
          TransactionType: 'EscrowCreate',
          //@ts-ignore
          Destination: destination,
          Amount: amount.toString(),
          Condition: condition,
          CancelAfter: cancelAfter,
          // Memo: {
          //   MemoData: memo,
          //   MemoType: 'string',
          // },
        },
      },
    })
    return response
  }
  async function postEscrowFinishTransaction({
    returnUrl,
    xummUserToken,
    condition,
    fulfillment,
    owner,
    memo,
    fee,
    offerSequence,
  }: any) {
    const response = await API.post('xumm', '/payload', {
      body: {
        options: {
          submit: true,
          expire: 240,
          return_url: {
            web: returnUrl,
            app: returnUrl,
          },
        },
        user_token: xummUserToken,
        txjson: {
          TransactionType: 'EscrowFinish',
          Owner: owner,
          OfferSequence: offerSequence,
          //@ts-ignore
          Condition: condition,
          Fulfillment: fulfillment,
          // Memo: {
          //   MemoData: memo,
          //   MemoType: 'string',
          // },
        },
      },
    })

    return response
  }

  async function getTransaction(id: string) {
    const response = await API.get('xumm', `/payload`, {
      queryStringParameters: {
        id,
      },
    })
    return response
  }

  async function signIn({ returnUrl }: { returnUrl: string }) {
    const response = await API.post('xumm', '/payload', {
      body: {
        options: {
          submit: false,
          expire: 240,
          return_url: {
            web: returnUrl,
            app: returnUrl,
          },
        },
        txjson: {
          TransactionType: 'SignIn',
        },
      },
    })
    return response
  }

  async function getConditionAndFulfillment() {
    const response = await API.get('xumm', `/fulfillmentAndCondition`, {
      queryStringParameters: {},
    })
    return response
  }
}

export default XummService
