import { API, graphqlOperation, Auth } from 'aws-amplify'
import {
  createConditionProgress as createConditionProgressGraphQL,
  updateConditionProgress as updateConditionProgressGraphQL,
} from '@graphql/mutations'
import { listConditionProgresses } from '@graphql/queries'
import { getConditionProgress } from '@graphql/customQueries'

const ConditionProgressesService = function () {
  return {
    fetchConditionProgresses,
    fetchConditionProgress,
    createConditionProgress,
    updateConditionProgress,
  }
  async function createConditionProgress({
    status,
    userID,
    conditionID,
  }: {
    status: string
    userID: string
    conditionID: string
  }) {
    try {
      const {
        data: { createConditionProgress: newConditionProgress },
      } = await (API.graphql(
        graphqlOperation(createConditionProgressGraphQL, {
          input: { status, userID, conditionID },
        })
      ) as Promise<any>)
      return newConditionProgress
    } catch (error) {
      throw error
    }
  }
  async function updateConditionProgress({
    conditionProgressID,
    status,
    userID,
    conditionID,
  }: {
    conditionProgressID: string
    status: string
    userID: string
    conditionID: string
  }) {
    try {
      const {
        data: { updateConditionProgress: updatedConditionProgress },
      } = await (API.graphql(
        graphqlOperation(updateConditionProgressGraphQL, {
          input: { id: conditionProgressID, status, userID, conditionID },
        })
      ) as Promise<any>)
      return updatedConditionProgress
    } catch (error) {
      throw error
    }
  }

  async function fetchConditionProgresses({
    filter,
    input,
  }: {
    filter: any
    input: any
  }) {
    try {
      const {
        data: {
          listConditionProgresses: { items },
        },
      } = await (API.graphql(
        graphqlOperation(listConditionProgresses, { filter, input })
      ) as Promise<any>)
      return items
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
  async function fetchConditionProgress(id: string) {
    try {
      const {
        data: { getConditionProgress: conditionProgress },
      } = await (API.graphql(
        graphqlOperation(getConditionProgress, { id })
      ) as Promise<any>)
      return conditionProgress
    } catch (error) {
      console.log('THIS IS THE TESTING ERROR', error)
      //@ts-ignore
      if (!error.response) {
        throw error
      }
    }
  }
}

export default ConditionProgressesService
