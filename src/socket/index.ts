import SockJS from 'sockjs-client'

const EventEmitter = require('events')

class Socket extends EventEmitter {
  constructor() {
    super()
    this.socket = null
  }

  subscribe = (url: string) => {
    this.socket = new WebSocket(url)
    //@ts-ignore
    this.socket.onopen = function (e) {
      console.log('open')
      this.emit('connected')
    }
  }
}

export default new Socket()
