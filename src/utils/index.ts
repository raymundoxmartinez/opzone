import { Storage } from 'aws-amplify'
import cryptoRandomString from 'crypto-random-string'

export async function uploadToS3(imageFiles: File[]): Promise<string[]> {
  if (!imageFiles.length) return []
  try {
    const imagePromises: Promise<string>[] = imageFiles.map(async (image) => {
      const name = cryptoRandomString({ length: 10 }) + image.name
      const { key, ...rest } = (await Storage.put(name, image, {
        contentType: image.type,
      })) as { key: string }
      return key
    })
    const results = await Promise.all(imagePromises)
      .then((results: string[]) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        return []
      })
    return results
  } catch (error) {
    //@ts-ignore
    if (!error.response) {
      throw error
    }
    //@ts-ignore
    throw error.response.data
  }
}
export async function retrieveFromS3(imageFiles: string[]): Promise<string[]> {
  if (!imageFiles.length) return []
  try {
    const filePromises: Promise<string>[] = imageFiles.map(async (fileKey) => {
      const fileUrl = await Storage.get(fileKey)
      const results = await Promise.all([fileUrl]).then((results) => {
        return results[0]
      })
      return results
    })

    const results = await Promise.all(filePromises)
      .then((results: string[]) => {
        return results
      })
      .catch((err) => {
        console.log(err)
        return []
      })
    return results
  } catch (error) {
    //@ts-ignore
    if (!error.response) {
      throw error
    }
    //@ts-ignore
    throw error.response.data
  }
}
