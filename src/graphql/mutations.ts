/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
      username
      email
      bio
      location
      images {
        bucket
        region
        key
      }
      backgroundImage
      wallets {
        items {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
      username
      email
      bio
      location
      images {
        bucket
        region
        key
      }
      backgroundImage
      wallets {
        items {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteUser = /* GraphQL */ `
  mutation DeleteUser(
    $input: DeleteUserInput!
    $condition: ModelUserConditionInput
  ) {
    deleteUser(input: $input, condition: $condition) {
      id
      username
      email
      bio
      location
      images {
        bucket
        region
        key
      }
      backgroundImage
      wallets {
        items {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createWallet = /* GraphQL */ `
  mutation CreateWallet(
    $input: CreateWalletInput!
    $condition: ModelWalletConditionInput
  ) {
    createWallet(input: $input, condition: $condition) {
      id
      title
      images
      description
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateWallet = /* GraphQL */ `
  mutation UpdateWallet(
    $input: UpdateWalletInput!
    $condition: ModelWalletConditionInput
  ) {
    updateWallet(input: $input, condition: $condition) {
      id
      title
      images
      description
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteWallet = /* GraphQL */ `
  mutation DeleteWallet(
    $input: DeleteWalletInput!
    $condition: ModelWalletConditionInput
  ) {
    deleteWallet(input: $input, condition: $condition) {
      id
      title
      images
      description
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createProduct = /* GraphQL */ `
  mutation CreateProduct(
    $input: CreateProductInput!
    $condition: ModelProductConditionInput
  ) {
    createProduct(input: $input, condition: $condition) {
      id
      title
      images {
        bucket
        region
        key
      }
      description
      price
      category
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      nft
      createdAt
      updatedAt
    }
  }
`;
export const updateProduct = /* GraphQL */ `
  mutation UpdateProduct(
    $input: UpdateProductInput!
    $condition: ModelProductConditionInput
  ) {
    updateProduct(input: $input, condition: $condition) {
      id
      title
      images {
        bucket
        region
        key
      }
      description
      price
      category
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      nft
      createdAt
      updatedAt
    }
  }
`;
export const deleteProduct = /* GraphQL */ `
  mutation DeleteProduct(
    $input: DeleteProductInput!
    $condition: ModelProductConditionInput
  ) {
    deleteProduct(input: $input, condition: $condition) {
      id
      title
      images {
        bucket
        region
        key
      }
      description
      price
      category
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      nft
      createdAt
      updatedAt
    }
  }
`;
export const createTrail = /* GraphQL */ `
  mutation CreateTrail(
    $input: CreateTrailInput!
    $condition: ModelTrailConditionInput
  ) {
    createTrail(input: $input, condition: $condition) {
      id
      name
      description
      duration
      type
      images {
        bucket
        region
        key
      }
      locations {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      order
      trailProgress {
        items {
          id
          name
          userID
          status
          trailID
          order
          createdAt
          updatedAt
          trailTrailProgressId
        }
        nextToken
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateTrail = /* GraphQL */ `
  mutation UpdateTrail(
    $input: UpdateTrailInput!
    $condition: ModelTrailConditionInput
  ) {
    updateTrail(input: $input, condition: $condition) {
      id
      name
      description
      duration
      type
      images {
        bucket
        region
        key
      }
      locations {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      order
      trailProgress {
        items {
          id
          name
          userID
          status
          trailID
          order
          createdAt
          updatedAt
          trailTrailProgressId
        }
        nextToken
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteTrail = /* GraphQL */ `
  mutation DeleteTrail(
    $input: DeleteTrailInput!
    $condition: ModelTrailConditionInput
  ) {
    deleteTrail(input: $input, condition: $condition) {
      id
      name
      description
      duration
      type
      images {
        bucket
        region
        key
      }
      locations {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      order
      trailProgress {
        items {
          id
          name
          userID
          status
          trailID
          order
          createdAt
          updatedAt
          trailTrailProgressId
        }
        nextToken
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createTrailProgress = /* GraphQL */ `
  mutation CreateTrailProgress(
    $input: CreateTrailProgressInput!
    $condition: ModelTrailProgressConditionInput
  ) {
    createTrailProgress(input: $input, condition: $condition) {
      id
      name
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      trailID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      order
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      trailTrailProgressId
    }
  }
`;
export const updateTrailProgress = /* GraphQL */ `
  mutation UpdateTrailProgress(
    $input: UpdateTrailProgressInput!
    $condition: ModelTrailProgressConditionInput
  ) {
    updateTrailProgress(input: $input, condition: $condition) {
      id
      name
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      trailID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      order
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      trailTrailProgressId
    }
  }
`;
export const deleteTrailProgress = /* GraphQL */ `
  mutation DeleteTrailProgress(
    $input: DeleteTrailProgressInput!
    $condition: ModelTrailProgressConditionInput
  ) {
    deleteTrailProgress(input: $input, condition: $condition) {
      id
      name
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      trailID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      order
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      trailTrailProgressId
    }
  }
`;
export const createLocation = /* GraphQL */ `
  mutation CreateLocation(
    $input: CreateLocationInput!
    $condition: ModelLocationConditionInput
  ) {
    createLocation(input: $input, condition: $condition) {
      id
      name
      address
      description
      coordinates
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLocation = /* GraphQL */ `
  mutation UpdateLocation(
    $input: UpdateLocationInput!
    $condition: ModelLocationConditionInput
  ) {
    updateLocation(input: $input, condition: $condition) {
      id
      name
      address
      description
      coordinates
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLocation = /* GraphQL */ `
  mutation DeleteLocation(
    $input: DeleteLocationInput!
    $condition: ModelLocationConditionInput
  ) {
    deleteLocation(input: $input, condition: $condition) {
      id
      name
      address
      description
      coordinates
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createLocationView = /* GraphQL */ `
  mutation CreateLocationView(
    $input: CreateLocationViewInput!
    $condition: ModelLocationViewConditionInput
  ) {
    createLocationView(input: $input, condition: $condition) {
      id
      name
      address
      description
      coordinates
      locationID
      location {
        id
        name
        address
        description
        coordinates
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transactions {
        items {
          id
          status
          amount
          description
          type
          senderID
          verifierID
          receiverID
          expiration
          fulfillment
          condition
          fromWalletID
          toWalletID
          xummTransactionID
          locationViewID
          rewardID
          createdAt
          updatedAt
        }
        nextToken
      }
      trails {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLocationView = /* GraphQL */ `
  mutation UpdateLocationView(
    $input: UpdateLocationViewInput!
    $condition: ModelLocationViewConditionInput
  ) {
    updateLocationView(input: $input, condition: $condition) {
      id
      name
      address
      description
      coordinates
      locationID
      location {
        id
        name
        address
        description
        coordinates
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transactions {
        items {
          id
          status
          amount
          description
          type
          senderID
          verifierID
          receiverID
          expiration
          fulfillment
          condition
          fromWalletID
          toWalletID
          xummTransactionID
          locationViewID
          rewardID
          createdAt
          updatedAt
        }
        nextToken
      }
      trails {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLocationView = /* GraphQL */ `
  mutation DeleteLocationView(
    $input: DeleteLocationViewInput!
    $condition: ModelLocationViewConditionInput
  ) {
    deleteLocationView(input: $input, condition: $condition) {
      id
      name
      address
      description
      coordinates
      locationID
      location {
        id
        name
        address
        description
        coordinates
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transactions {
        items {
          id
          status
          amount
          description
          type
          senderID
          verifierID
          receiverID
          expiration
          fulfillment
          condition
          fromWalletID
          toWalletID
          xummTransactionID
          locationViewID
          rewardID
          createdAt
          updatedAt
        }
        nextToken
      }
      trails {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createTransaction = /* GraphQL */ `
  mutation CreateTransaction(
    $input: CreateTransactionInput!
    $condition: ModelTransactionConditionInput
  ) {
    createTransaction(input: $input, condition: $condition) {
      id
      status
      amount
      description
      type
      senderID
      verifierID
      receiverID
      conditions {
        items {
          id
          type
          latitude
          longitude
          question
          choices
          answerIndex
          status
          transactionID
          createdAt
          updatedAt
        }
        nextToken
      }
      expiration
      sender {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      verifier {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      receiver {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      fulfillment
      condition
      fromWalletID
      toWalletID
      toWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      fromWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      xummTransactionID
      locationViewID
      rewardID
      reward {
        id
        name
        file {
          bucket
          region
          key
        }
        createdAt
        updatedAt
      }
      transactionProgress {
        items {
          id
          userID
          transactionID
          status
          createdAt
          updatedAt
          transactionTransactionProgressId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateTransaction = /* GraphQL */ `
  mutation UpdateTransaction(
    $input: UpdateTransactionInput!
    $condition: ModelTransactionConditionInput
  ) {
    updateTransaction(input: $input, condition: $condition) {
      id
      status
      amount
      description
      type
      senderID
      verifierID
      receiverID
      conditions {
        items {
          id
          type
          latitude
          longitude
          question
          choices
          answerIndex
          status
          transactionID
          createdAt
          updatedAt
        }
        nextToken
      }
      expiration
      sender {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      verifier {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      receiver {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      fulfillment
      condition
      fromWalletID
      toWalletID
      toWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      fromWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      xummTransactionID
      locationViewID
      rewardID
      reward {
        id
        name
        file {
          bucket
          region
          key
        }
        createdAt
        updatedAt
      }
      transactionProgress {
        items {
          id
          userID
          transactionID
          status
          createdAt
          updatedAt
          transactionTransactionProgressId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteTransaction = /* GraphQL */ `
  mutation DeleteTransaction(
    $input: DeleteTransactionInput!
    $condition: ModelTransactionConditionInput
  ) {
    deleteTransaction(input: $input, condition: $condition) {
      id
      status
      amount
      description
      type
      senderID
      verifierID
      receiverID
      conditions {
        items {
          id
          type
          latitude
          longitude
          question
          choices
          answerIndex
          status
          transactionID
          createdAt
          updatedAt
        }
        nextToken
      }
      expiration
      sender {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      verifier {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      receiver {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      fulfillment
      condition
      fromWalletID
      toWalletID
      toWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      fromWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      xummTransactionID
      locationViewID
      rewardID
      reward {
        id
        name
        file {
          bucket
          region
          key
        }
        createdAt
        updatedAt
      }
      transactionProgress {
        items {
          id
          userID
          transactionID
          status
          createdAt
          updatedAt
          transactionTransactionProgressId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createTransactionProgress = /* GraphQL */ `
  mutation CreateTransactionProgress(
    $input: CreateTransactionProgressInput!
    $condition: ModelTransactionProgressConditionInput
  ) {
    createTransactionProgress(input: $input, condition: $condition) {
      id
      userID
      transactionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transaction {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      transactionTransactionProgressId
    }
  }
`;
export const updateTransactionProgress = /* GraphQL */ `
  mutation UpdateTransactionProgress(
    $input: UpdateTransactionProgressInput!
    $condition: ModelTransactionProgressConditionInput
  ) {
    updateTransactionProgress(input: $input, condition: $condition) {
      id
      userID
      transactionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transaction {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      transactionTransactionProgressId
    }
  }
`;
export const deleteTransactionProgress = /* GraphQL */ `
  mutation DeleteTransactionProgress(
    $input: DeleteTransactionProgressInput!
    $condition: ModelTransactionProgressConditionInput
  ) {
    deleteTransactionProgress(input: $input, condition: $condition) {
      id
      userID
      transactionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transaction {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      transactionTransactionProgressId
    }
  }
`;
export const createCondition = /* GraphQL */ `
  mutation CreateCondition(
    $input: CreateConditionInput!
    $condition: ModelConditionConditionInput
  ) {
    createCondition(input: $input, condition: $condition) {
      id
      type
      latitude
      longitude
      question
      choices
      answerIndex
      status
      transactionID
      createdAt
      updatedAt
    }
  }
`;
export const updateCondition = /* GraphQL */ `
  mutation UpdateCondition(
    $input: UpdateConditionInput!
    $condition: ModelConditionConditionInput
  ) {
    updateCondition(input: $input, condition: $condition) {
      id
      type
      latitude
      longitude
      question
      choices
      answerIndex
      status
      transactionID
      createdAt
      updatedAt
    }
  }
`;
export const deleteCondition = /* GraphQL */ `
  mutation DeleteCondition(
    $input: DeleteConditionInput!
    $condition: ModelConditionConditionInput
  ) {
    deleteCondition(input: $input, condition: $condition) {
      id
      type
      latitude
      longitude
      question
      choices
      answerIndex
      status
      transactionID
      createdAt
      updatedAt
    }
  }
`;
export const createConditionProgress = /* GraphQL */ `
  mutation CreateConditionProgress(
    $input: CreateConditionProgressInput!
    $condition: ModelConditionProgressConditionInput
  ) {
    createConditionProgress(input: $input, condition: $condition) {
      id
      userID
      conditionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      condition {
        id
        type
        latitude
        longitude
        question
        choices
        answerIndex
        status
        transactionID
        createdAt
        updatedAt
      }
      status
      createdAt
      updatedAt
      trailProgressCompletedConditionsId
      transactionProgressCompletedConditionsId
    }
  }
`;
export const updateConditionProgress = /* GraphQL */ `
  mutation UpdateConditionProgress(
    $input: UpdateConditionProgressInput!
    $condition: ModelConditionProgressConditionInput
  ) {
    updateConditionProgress(input: $input, condition: $condition) {
      id
      userID
      conditionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      condition {
        id
        type
        latitude
        longitude
        question
        choices
        answerIndex
        status
        transactionID
        createdAt
        updatedAt
      }
      status
      createdAt
      updatedAt
      trailProgressCompletedConditionsId
      transactionProgressCompletedConditionsId
    }
  }
`;
export const deleteConditionProgress = /* GraphQL */ `
  mutation DeleteConditionProgress(
    $input: DeleteConditionProgressInput!
    $condition: ModelConditionProgressConditionInput
  ) {
    deleteConditionProgress(input: $input, condition: $condition) {
      id
      userID
      conditionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      condition {
        id
        type
        latitude
        longitude
        question
        choices
        answerIndex
        status
        transactionID
        createdAt
        updatedAt
      }
      status
      createdAt
      updatedAt
      trailProgressCompletedConditionsId
      transactionProgressCompletedConditionsId
    }
  }
`;
export const createAsset = /* GraphQL */ `
  mutation CreateAsset(
    $input: CreateAssetInput!
    $condition: ModelAssetConditionInput
  ) {
    createAsset(input: $input, condition: $condition) {
      id
      name
      file {
        bucket
        region
        key
      }
      userID
      owner {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateAsset = /* GraphQL */ `
  mutation UpdateAsset(
    $input: UpdateAssetInput!
    $condition: ModelAssetConditionInput
  ) {
    updateAsset(input: $input, condition: $condition) {
      id
      name
      file {
        bucket
        region
        key
      }
      userID
      owner {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteAsset = /* GraphQL */ `
  mutation DeleteAsset(
    $input: DeleteAssetInput!
    $condition: ModelAssetConditionInput
  ) {
    deleteAsset(input: $input, condition: $condition) {
      id
      name
      file {
        bucket
        region
        key
      }
      userID
      owner {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createReward = /* GraphQL */ `
  mutation CreateReward(
    $input: CreateRewardInput!
    $condition: ModelRewardConditionInput
  ) {
    createReward(input: $input, condition: $condition) {
      id
      name
      file {
        bucket
        region
        key
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateReward = /* GraphQL */ `
  mutation UpdateReward(
    $input: UpdateRewardInput!
    $condition: ModelRewardConditionInput
  ) {
    updateReward(input: $input, condition: $condition) {
      id
      name
      file {
        bucket
        region
        key
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteReward = /* GraphQL */ `
  mutation DeleteReward(
    $input: DeleteRewardInput!
    $condition: ModelRewardConditionInput
  ) {
    deleteReward(input: $input, condition: $condition) {
      id
      name
      file {
        bucket
        region
        key
      }
      createdAt
      updatedAt
    }
  }
`;
export const createTrailLocationViews = /* GraphQL */ `
  mutation CreateTrailLocationViews(
    $input: CreateTrailLocationViewsInput!
    $condition: ModelTrailLocationViewsConditionInput
  ) {
    createTrailLocationViews(input: $input, condition: $condition) {
      id
      trailID
      locationViewID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      locationView {
        id
        name
        address
        description
        coordinates
        locationID
        location {
          id
          name
          address
          description
          coordinates
          userID
          createdAt
          updatedAt
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        transactions {
          nextToken
        }
        trails {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateTrailLocationViews = /* GraphQL */ `
  mutation UpdateTrailLocationViews(
    $input: UpdateTrailLocationViewsInput!
    $condition: ModelTrailLocationViewsConditionInput
  ) {
    updateTrailLocationViews(input: $input, condition: $condition) {
      id
      trailID
      locationViewID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      locationView {
        id
        name
        address
        description
        coordinates
        locationID
        location {
          id
          name
          address
          description
          coordinates
          userID
          createdAt
          updatedAt
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        transactions {
          nextToken
        }
        trails {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteTrailLocationViews = /* GraphQL */ `
  mutation DeleteTrailLocationViews(
    $input: DeleteTrailLocationViewsInput!
    $condition: ModelTrailLocationViewsConditionInput
  ) {
    deleteTrailLocationViews(input: $input, condition: $condition) {
      id
      trailID
      locationViewID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      locationView {
        id
        name
        address
        description
        coordinates
        locationID
        location {
          id
          name
          address
          description
          coordinates
          userID
          createdAt
          updatedAt
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        transactions {
          nextToken
        }
        trails {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
