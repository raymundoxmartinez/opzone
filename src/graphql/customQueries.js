export const searchUsers = /* GraphQL */ `
  query SearchUsers(
    $filter: SearchableUserFilterInput
    $sort: SearchableUserSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchUsers(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        username
        email
        bio
        location
        images
        backgroundImage
        wallets {
          items {
            id
            title
            images
            description
            userID
            address
            user {
              id
              username
              email
              bio
              location
              images
              backgroundImage
              createdAt
              updatedAt
            }
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;

export const getTrail = /* GraphQL */ `
  query GetTrail($id: ID!) {
    getTrail(id: $id) {
      id
      name
      description
      duration
      type
      userID
      images {
        bucket
        region
        key
      }
      locations {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
          locationView {
              id
              name
              address
              coordinates
              description
              transactions {
                items {
                  id
                  description
                  status
                  type
                }
                nextToken
              }
              createdAt
              updatedAt
          }
        }
        nextToken
      }
      order
      createdAt
      updatedAt
    }
  }
`;

export const listTrails = /* GraphQL */ `
  query ListTrails(
    $filter: ModelTrailFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTrails(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          items {
            id
            trailID
            locationViewID
            createdAt
            updatedAt
            locationView {
                id
                name
                address
                coordinates
                description
                transactions {
                  items {
                    id
                    description
                    status
                    type
                  }
                  nextToken
                }
                createdAt
                updatedAt
            }
          }
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTrailProgress = /* GraphQL */ `
  query GetTrailProgress($id: ID!) {
    getTrailProgress(id: $id) {
      id
      name
      description
      duration
      type
      images {
        bucket
        region
        key
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      trailID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          items {
            id
            trailID
            locationViewID
            createdAt
            updatedAt
            locationView {
                id
                name
                address
                coordinates
                description
                transactions {
                  items {
                    id
                    description
                    status
                    type
                  }
                  nextToken
                }
                createdAt
                updatedAt
            }
          }
          nextToken
        }
        order
        createdAt
        updatedAt
      }
      order
      locations {
        items {
          id
          trailProgressID
          locationViewID
          createdAt
          updatedAt
          locationView {
            id
            name
            address
            description
            transactions {
              items {
                id
                description
                status
                type
              }
              nextToken
            }
            createdAt
            updatedAt
        }
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;

export const listTrailProgresses = /* GraphQL */ `
  query ListTrailProgresses(
    $filter: ModelTrailProgressFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTrailProgresses(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        status
        trailID
        trail {
          id
          name
          description
          duration
          type
          order
          userID
          createdAt
          updatedAt
          images {
            bucket
            region
            key
          }
          locations {
            items {
              id
              trailID
              locationViewID
              createdAt
              updatedAt
              locationView {
                  id
                  name
                  address
                  coordinates
                  description
                  transactions {
                    items {
                      id
                      description
                      status
                      type
                    }
                    nextToken
                  }
                  createdAt
                  updatedAt
              }
            }
            nextToken
          }
        }
        order
        completedConditions {
          nextToken
        }
        createdAt
        updatedAt
        trailTrailProgressId
      }
      nextToken
    }
  }
`