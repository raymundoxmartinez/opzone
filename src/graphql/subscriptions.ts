/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateUser = /* GraphQL */ `
  subscription OnCreateUser($filter: ModelSubscriptionUserFilterInput) {
    onCreateUser(filter: $filter) {
      id
      username
      email
      bio
      location
      images {
        bucket
        region
        key
      }
      backgroundImage
      wallets {
        items {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUser = /* GraphQL */ `
  subscription OnUpdateUser($filter: ModelSubscriptionUserFilterInput) {
    onUpdateUser(filter: $filter) {
      id
      username
      email
      bio
      location
      images {
        bucket
        region
        key
      }
      backgroundImage
      wallets {
        items {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUser = /* GraphQL */ `
  subscription OnDeleteUser($filter: ModelSubscriptionUserFilterInput) {
    onDeleteUser(filter: $filter) {
      id
      username
      email
      bio
      location
      images {
        bucket
        region
        key
      }
      backgroundImage
      wallets {
        items {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateWallet = /* GraphQL */ `
  subscription OnCreateWallet($filter: ModelSubscriptionWalletFilterInput) {
    onCreateWallet(filter: $filter) {
      id
      title
      images
      description
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateWallet = /* GraphQL */ `
  subscription OnUpdateWallet($filter: ModelSubscriptionWalletFilterInput) {
    onUpdateWallet(filter: $filter) {
      id
      title
      images
      description
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteWallet = /* GraphQL */ `
  subscription OnDeleteWallet($filter: ModelSubscriptionWalletFilterInput) {
    onDeleteWallet(filter: $filter) {
      id
      title
      images
      description
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateProduct = /* GraphQL */ `
  subscription OnCreateProduct($filter: ModelSubscriptionProductFilterInput) {
    onCreateProduct(filter: $filter) {
      id
      title
      images {
        bucket
        region
        key
      }
      description
      price
      category
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      nft
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateProduct = /* GraphQL */ `
  subscription OnUpdateProduct($filter: ModelSubscriptionProductFilterInput) {
    onUpdateProduct(filter: $filter) {
      id
      title
      images {
        bucket
        region
        key
      }
      description
      price
      category
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      nft
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteProduct = /* GraphQL */ `
  subscription OnDeleteProduct($filter: ModelSubscriptionProductFilterInput) {
    onDeleteProduct(filter: $filter) {
      id
      title
      images {
        bucket
        region
        key
      }
      description
      price
      category
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      nft
      createdAt
      updatedAt
    }
  }
`;
export const onCreateTrail = /* GraphQL */ `
  subscription OnCreateTrail($filter: ModelSubscriptionTrailFilterInput) {
    onCreateTrail(filter: $filter) {
      id
      name
      description
      duration
      type
      images {
        bucket
        region
        key
      }
      locations {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      order
      trailProgress {
        items {
          id
          name
          userID
          status
          trailID
          order
          createdAt
          updatedAt
          trailTrailProgressId
        }
        nextToken
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTrail = /* GraphQL */ `
  subscription OnUpdateTrail($filter: ModelSubscriptionTrailFilterInput) {
    onUpdateTrail(filter: $filter) {
      id
      name
      description
      duration
      type
      images {
        bucket
        region
        key
      }
      locations {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      order
      trailProgress {
        items {
          id
          name
          userID
          status
          trailID
          order
          createdAt
          updatedAt
          trailTrailProgressId
        }
        nextToken
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTrail = /* GraphQL */ `
  subscription OnDeleteTrail($filter: ModelSubscriptionTrailFilterInput) {
    onDeleteTrail(filter: $filter) {
      id
      name
      description
      duration
      type
      images {
        bucket
        region
        key
      }
      locations {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      order
      trailProgress {
        items {
          id
          name
          userID
          status
          trailID
          order
          createdAt
          updatedAt
          trailTrailProgressId
        }
        nextToken
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateTrailProgress = /* GraphQL */ `
  subscription OnCreateTrailProgress(
    $filter: ModelSubscriptionTrailProgressFilterInput
  ) {
    onCreateTrailProgress(filter: $filter) {
      id
      name
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      trailID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      order
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      trailTrailProgressId
    }
  }
`;
export const onUpdateTrailProgress = /* GraphQL */ `
  subscription OnUpdateTrailProgress(
    $filter: ModelSubscriptionTrailProgressFilterInput
  ) {
    onUpdateTrailProgress(filter: $filter) {
      id
      name
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      trailID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      order
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      trailTrailProgressId
    }
  }
`;
export const onDeleteTrailProgress = /* GraphQL */ `
  subscription OnDeleteTrailProgress(
    $filter: ModelSubscriptionTrailProgressFilterInput
  ) {
    onDeleteTrailProgress(filter: $filter) {
      id
      name
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      trailID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      order
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      trailTrailProgressId
    }
  }
`;
export const onCreateLocation = /* GraphQL */ `
  subscription OnCreateLocation($filter: ModelSubscriptionLocationFilterInput) {
    onCreateLocation(filter: $filter) {
      id
      name
      address
      description
      coordinates
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateLocation = /* GraphQL */ `
  subscription OnUpdateLocation($filter: ModelSubscriptionLocationFilterInput) {
    onUpdateLocation(filter: $filter) {
      id
      name
      address
      description
      coordinates
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteLocation = /* GraphQL */ `
  subscription OnDeleteLocation($filter: ModelSubscriptionLocationFilterInput) {
    onDeleteLocation(filter: $filter) {
      id
      name
      address
      description
      coordinates
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateLocationView = /* GraphQL */ `
  subscription OnCreateLocationView(
    $filter: ModelSubscriptionLocationViewFilterInput
  ) {
    onCreateLocationView(filter: $filter) {
      id
      name
      address
      description
      coordinates
      locationID
      location {
        id
        name
        address
        description
        coordinates
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transactions {
        items {
          id
          status
          amount
          description
          type
          senderID
          verifierID
          receiverID
          expiration
          fulfillment
          condition
          fromWalletID
          toWalletID
          xummTransactionID
          locationViewID
          rewardID
          createdAt
          updatedAt
        }
        nextToken
      }
      trails {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateLocationView = /* GraphQL */ `
  subscription OnUpdateLocationView(
    $filter: ModelSubscriptionLocationViewFilterInput
  ) {
    onUpdateLocationView(filter: $filter) {
      id
      name
      address
      description
      coordinates
      locationID
      location {
        id
        name
        address
        description
        coordinates
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transactions {
        items {
          id
          status
          amount
          description
          type
          senderID
          verifierID
          receiverID
          expiration
          fulfillment
          condition
          fromWalletID
          toWalletID
          xummTransactionID
          locationViewID
          rewardID
          createdAt
          updatedAt
        }
        nextToken
      }
      trails {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteLocationView = /* GraphQL */ `
  subscription OnDeleteLocationView(
    $filter: ModelSubscriptionLocationViewFilterInput
  ) {
    onDeleteLocationView(filter: $filter) {
      id
      name
      address
      description
      coordinates
      locationID
      location {
        id
        name
        address
        description
        coordinates
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transactions {
        items {
          id
          status
          amount
          description
          type
          senderID
          verifierID
          receiverID
          expiration
          fulfillment
          condition
          fromWalletID
          toWalletID
          xummTransactionID
          locationViewID
          rewardID
          createdAt
          updatedAt
        }
        nextToken
      }
      trails {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateTransaction = /* GraphQL */ `
  subscription OnCreateTransaction(
    $filter: ModelSubscriptionTransactionFilterInput
  ) {
    onCreateTransaction(filter: $filter) {
      id
      status
      amount
      description
      type
      senderID
      verifierID
      receiverID
      conditions {
        items {
          id
          type
          latitude
          longitude
          question
          choices
          answerIndex
          status
          transactionID
          createdAt
          updatedAt
        }
        nextToken
      }
      expiration
      sender {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      verifier {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      receiver {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      fulfillment
      condition
      fromWalletID
      toWalletID
      toWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      fromWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      xummTransactionID
      locationViewID
      rewardID
      reward {
        id
        name
        file {
          bucket
          region
          key
        }
        createdAt
        updatedAt
      }
      transactionProgress {
        items {
          id
          userID
          transactionID
          status
          createdAt
          updatedAt
          transactionTransactionProgressId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTransaction = /* GraphQL */ `
  subscription OnUpdateTransaction(
    $filter: ModelSubscriptionTransactionFilterInput
  ) {
    onUpdateTransaction(filter: $filter) {
      id
      status
      amount
      description
      type
      senderID
      verifierID
      receiverID
      conditions {
        items {
          id
          type
          latitude
          longitude
          question
          choices
          answerIndex
          status
          transactionID
          createdAt
          updatedAt
        }
        nextToken
      }
      expiration
      sender {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      verifier {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      receiver {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      fulfillment
      condition
      fromWalletID
      toWalletID
      toWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      fromWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      xummTransactionID
      locationViewID
      rewardID
      reward {
        id
        name
        file {
          bucket
          region
          key
        }
        createdAt
        updatedAt
      }
      transactionProgress {
        items {
          id
          userID
          transactionID
          status
          createdAt
          updatedAt
          transactionTransactionProgressId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTransaction = /* GraphQL */ `
  subscription OnDeleteTransaction(
    $filter: ModelSubscriptionTransactionFilterInput
  ) {
    onDeleteTransaction(filter: $filter) {
      id
      status
      amount
      description
      type
      senderID
      verifierID
      receiverID
      conditions {
        items {
          id
          type
          latitude
          longitude
          question
          choices
          answerIndex
          status
          transactionID
          createdAt
          updatedAt
        }
        nextToken
      }
      expiration
      sender {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      verifier {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      receiver {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      fulfillment
      condition
      fromWalletID
      toWalletID
      toWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      fromWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      xummTransactionID
      locationViewID
      rewardID
      reward {
        id
        name
        file {
          bucket
          region
          key
        }
        createdAt
        updatedAt
      }
      transactionProgress {
        items {
          id
          userID
          transactionID
          status
          createdAt
          updatedAt
          transactionTransactionProgressId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateTransactionProgress = /* GraphQL */ `
  subscription OnCreateTransactionProgress(
    $filter: ModelSubscriptionTransactionProgressFilterInput
  ) {
    onCreateTransactionProgress(filter: $filter) {
      id
      userID
      transactionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transaction {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      transactionTransactionProgressId
    }
  }
`;
export const onUpdateTransactionProgress = /* GraphQL */ `
  subscription OnUpdateTransactionProgress(
    $filter: ModelSubscriptionTransactionProgressFilterInput
  ) {
    onUpdateTransactionProgress(filter: $filter) {
      id
      userID
      transactionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transaction {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      transactionTransactionProgressId
    }
  }
`;
export const onDeleteTransactionProgress = /* GraphQL */ `
  subscription OnDeleteTransactionProgress(
    $filter: ModelSubscriptionTransactionProgressFilterInput
  ) {
    onDeleteTransactionProgress(filter: $filter) {
      id
      userID
      transactionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transaction {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      transactionTransactionProgressId
    }
  }
`;
export const onCreateCondition = /* GraphQL */ `
  subscription OnCreateCondition(
    $filter: ModelSubscriptionConditionFilterInput
  ) {
    onCreateCondition(filter: $filter) {
      id
      type
      latitude
      longitude
      question
      choices
      answerIndex
      status
      transactionID
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCondition = /* GraphQL */ `
  subscription OnUpdateCondition(
    $filter: ModelSubscriptionConditionFilterInput
  ) {
    onUpdateCondition(filter: $filter) {
      id
      type
      latitude
      longitude
      question
      choices
      answerIndex
      status
      transactionID
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCondition = /* GraphQL */ `
  subscription OnDeleteCondition(
    $filter: ModelSubscriptionConditionFilterInput
  ) {
    onDeleteCondition(filter: $filter) {
      id
      type
      latitude
      longitude
      question
      choices
      answerIndex
      status
      transactionID
      createdAt
      updatedAt
    }
  }
`;
export const onCreateConditionProgress = /* GraphQL */ `
  subscription OnCreateConditionProgress(
    $filter: ModelSubscriptionConditionProgressFilterInput
  ) {
    onCreateConditionProgress(filter: $filter) {
      id
      userID
      conditionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      condition {
        id
        type
        latitude
        longitude
        question
        choices
        answerIndex
        status
        transactionID
        createdAt
        updatedAt
      }
      status
      createdAt
      updatedAt
      trailProgressCompletedConditionsId
      transactionProgressCompletedConditionsId
    }
  }
`;
export const onUpdateConditionProgress = /* GraphQL */ `
  subscription OnUpdateConditionProgress(
    $filter: ModelSubscriptionConditionProgressFilterInput
  ) {
    onUpdateConditionProgress(filter: $filter) {
      id
      userID
      conditionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      condition {
        id
        type
        latitude
        longitude
        question
        choices
        answerIndex
        status
        transactionID
        createdAt
        updatedAt
      }
      status
      createdAt
      updatedAt
      trailProgressCompletedConditionsId
      transactionProgressCompletedConditionsId
    }
  }
`;
export const onDeleteConditionProgress = /* GraphQL */ `
  subscription OnDeleteConditionProgress(
    $filter: ModelSubscriptionConditionProgressFilterInput
  ) {
    onDeleteConditionProgress(filter: $filter) {
      id
      userID
      conditionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      condition {
        id
        type
        latitude
        longitude
        question
        choices
        answerIndex
        status
        transactionID
        createdAt
        updatedAt
      }
      status
      createdAt
      updatedAt
      trailProgressCompletedConditionsId
      transactionProgressCompletedConditionsId
    }
  }
`;
export const onCreateAsset = /* GraphQL */ `
  subscription OnCreateAsset($filter: ModelSubscriptionAssetFilterInput) {
    onCreateAsset(filter: $filter) {
      id
      name
      file {
        bucket
        region
        key
      }
      userID
      owner {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateAsset = /* GraphQL */ `
  subscription OnUpdateAsset($filter: ModelSubscriptionAssetFilterInput) {
    onUpdateAsset(filter: $filter) {
      id
      name
      file {
        bucket
        region
        key
      }
      userID
      owner {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteAsset = /* GraphQL */ `
  subscription OnDeleteAsset($filter: ModelSubscriptionAssetFilterInput) {
    onDeleteAsset(filter: $filter) {
      id
      name
      file {
        bucket
        region
        key
      }
      userID
      owner {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateReward = /* GraphQL */ `
  subscription OnCreateReward($filter: ModelSubscriptionRewardFilterInput) {
    onCreateReward(filter: $filter) {
      id
      name
      file {
        bucket
        region
        key
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateReward = /* GraphQL */ `
  subscription OnUpdateReward($filter: ModelSubscriptionRewardFilterInput) {
    onUpdateReward(filter: $filter) {
      id
      name
      file {
        bucket
        region
        key
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteReward = /* GraphQL */ `
  subscription OnDeleteReward($filter: ModelSubscriptionRewardFilterInput) {
    onDeleteReward(filter: $filter) {
      id
      name
      file {
        bucket
        region
        key
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateTrailLocationViews = /* GraphQL */ `
  subscription OnCreateTrailLocationViews(
    $filter: ModelSubscriptionTrailLocationViewsFilterInput
  ) {
    onCreateTrailLocationViews(filter: $filter) {
      id
      trailID
      locationViewID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      locationView {
        id
        name
        address
        description
        coordinates
        locationID
        location {
          id
          name
          address
          description
          coordinates
          userID
          createdAt
          updatedAt
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        transactions {
          nextToken
        }
        trails {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTrailLocationViews = /* GraphQL */ `
  subscription OnUpdateTrailLocationViews(
    $filter: ModelSubscriptionTrailLocationViewsFilterInput
  ) {
    onUpdateTrailLocationViews(filter: $filter) {
      id
      trailID
      locationViewID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      locationView {
        id
        name
        address
        description
        coordinates
        locationID
        location {
          id
          name
          address
          description
          coordinates
          userID
          createdAt
          updatedAt
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        transactions {
          nextToken
        }
        trails {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTrailLocationViews = /* GraphQL */ `
  subscription OnDeleteTrailLocationViews(
    $filter: ModelSubscriptionTrailLocationViewsFilterInput
  ) {
    onDeleteTrailLocationViews(filter: $filter) {
      id
      trailID
      locationViewID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      locationView {
        id
        name
        address
        description
        coordinates
        locationID
        location {
          id
          name
          address
          description
          coordinates
          userID
          createdAt
          updatedAt
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        transactions {
          nextToken
        }
        trails {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
