/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      username
      email
      bio
      location
      images {
        bucket
        region
        key
      }
      backgroundImage
      wallets {
        items {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getWallet = /* GraphQL */ `
  query GetWallet($id: ID!) {
    getWallet(id: $id) {
      id
      title
      images
      description
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listWallets = /* GraphQL */ `
  query ListWallets(
    $filter: ModelWalletFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listWallets(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getProduct = /* GraphQL */ `
  query GetProduct($id: ID!) {
    getProduct(id: $id) {
      id
      title
      images {
        bucket
        region
        key
      }
      description
      price
      category
      userID
      address
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      nft
      createdAt
      updatedAt
    }
  }
`;
export const listProducts = /* GraphQL */ `
  query ListProducts(
    $filter: ModelProductFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listProducts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        images {
          bucket
          region
          key
        }
        description
        price
        category
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        nft
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTrail = /* GraphQL */ `
  query GetTrail($id: ID!) {
    getTrail(id: $id) {
      id
      name
      description
      duration
      type
      images {
        bucket
        region
        key
      }
      locations {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      order
      trailProgress {
        items {
          id
          name
          userID
          status
          trailID
          order
          createdAt
          updatedAt
          trailTrailProgressId
        }
        nextToken
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listTrails = /* GraphQL */ `
  query ListTrails(
    $filter: ModelTrailFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTrails(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTrailProgress = /* GraphQL */ `
  query GetTrailProgress($id: ID!) {
    getTrailProgress(id: $id) {
      id
      name
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      trailID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      order
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      trailTrailProgressId
    }
  }
`;
export const listTrailProgresses = /* GraphQL */ `
  query ListTrailProgresses(
    $filter: ModelTrailProgressFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTrailProgresses(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        status
        trailID
        trail {
          id
          name
          description
          duration
          type
          order
          userID
          createdAt
          updatedAt
        }
        order
        completedConditions {
          nextToken
        }
        createdAt
        updatedAt
        trailTrailProgressId
      }
      nextToken
    }
  }
`;
export const getLocation = /* GraphQL */ `
  query GetLocation($id: ID!) {
    getLocation(id: $id) {
      id
      name
      address
      description
      coordinates
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLocations = /* GraphQL */ `
  query ListLocations(
    $filter: ModelLocationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLocations(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        address
        description
        coordinates
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLocationView = /* GraphQL */ `
  query GetLocationView($id: ID!) {
    getLocationView(id: $id) {
      id
      name
      address
      description
      coordinates
      locationID
      location {
        id
        name
        address
        description
        coordinates
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      userID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transactions {
        items {
          id
          status
          amount
          description
          type
          senderID
          verifierID
          receiverID
          expiration
          fulfillment
          condition
          fromWalletID
          toWalletID
          xummTransactionID
          locationViewID
          rewardID
          createdAt
          updatedAt
        }
        nextToken
      }
      trails {
        items {
          id
          trailID
          locationViewID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLocationViews = /* GraphQL */ `
  query ListLocationViews(
    $filter: ModelLocationViewFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLocationViews(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        address
        description
        coordinates
        locationID
        location {
          id
          name
          address
          description
          coordinates
          userID
          createdAt
          updatedAt
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        transactions {
          nextToken
        }
        trails {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTransaction = /* GraphQL */ `
  query GetTransaction($id: ID!) {
    getTransaction(id: $id) {
      id
      status
      amount
      description
      type
      senderID
      verifierID
      receiverID
      conditions {
        items {
          id
          type
          latitude
          longitude
          question
          choices
          answerIndex
          status
          transactionID
          createdAt
          updatedAt
        }
        nextToken
      }
      expiration
      sender {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      verifier {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      receiver {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      fulfillment
      condition
      fromWalletID
      toWalletID
      toWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      fromWallet {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      xummTransactionID
      locationViewID
      rewardID
      reward {
        id
        name
        file {
          bucket
          region
          key
        }
        createdAt
        updatedAt
      }
      transactionProgress {
        items {
          id
          userID
          transactionID
          status
          createdAt
          updatedAt
          transactionTransactionProgressId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listTransactions = /* GraphQL */ `
  query ListTransactions(
    $filter: ModelTransactionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTransactions(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTransactionProgress = /* GraphQL */ `
  query GetTransactionProgress($id: ID!) {
    getTransactionProgress(id: $id) {
      id
      userID
      transactionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      transaction {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      status
      completedConditions {
        items {
          id
          userID
          conditionID
          status
          createdAt
          updatedAt
          trailProgressCompletedConditionsId
          transactionProgressCompletedConditionsId
        }
        nextToken
      }
      createdAt
      updatedAt
      transactionTransactionProgressId
    }
  }
`;
export const listTransactionProgresses = /* GraphQL */ `
  query ListTransactionProgresses(
    $filter: ModelTransactionProgressFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTransactionProgresses(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        userID
        transactionID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        transaction {
          id
          status
          amount
          description
          type
          senderID
          verifierID
          receiverID
          expiration
          fulfillment
          condition
          fromWalletID
          toWalletID
          xummTransactionID
          locationViewID
          rewardID
          createdAt
          updatedAt
        }
        status
        completedConditions {
          nextToken
        }
        createdAt
        updatedAt
        transactionTransactionProgressId
      }
      nextToken
    }
  }
`;
export const getCondition = /* GraphQL */ `
  query GetCondition($id: ID!) {
    getCondition(id: $id) {
      id
      type
      latitude
      longitude
      question
      choices
      answerIndex
      status
      transactionID
      createdAt
      updatedAt
    }
  }
`;
export const listConditions = /* GraphQL */ `
  query ListConditions(
    $filter: ModelConditionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listConditions(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        type
        latitude
        longitude
        question
        choices
        answerIndex
        status
        transactionID
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getConditionProgress = /* GraphQL */ `
  query GetConditionProgress($id: ID!) {
    getConditionProgress(id: $id) {
      id
      userID
      conditionID
      user {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      condition {
        id
        type
        latitude
        longitude
        question
        choices
        answerIndex
        status
        transactionID
        createdAt
        updatedAt
      }
      status
      createdAt
      updatedAt
      trailProgressCompletedConditionsId
      transactionProgressCompletedConditionsId
    }
  }
`;
export const listConditionProgresses = /* GraphQL */ `
  query ListConditionProgresses(
    $filter: ModelConditionProgressFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listConditionProgresses(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        userID
        conditionID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        condition {
          id
          type
          latitude
          longitude
          question
          choices
          answerIndex
          status
          transactionID
          createdAt
          updatedAt
        }
        status
        createdAt
        updatedAt
        trailProgressCompletedConditionsId
        transactionProgressCompletedConditionsId
      }
      nextToken
    }
  }
`;
export const getAsset = /* GraphQL */ `
  query GetAsset($id: ID!) {
    getAsset(id: $id) {
      id
      name
      file {
        bucket
        region
        key
      }
      userID
      owner {
        id
        username
        email
        bio
        location
        images {
          bucket
          region
          key
        }
        backgroundImage
        wallets {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listAssets = /* GraphQL */ `
  query ListAssets(
    $filter: ModelAssetFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listAssets(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        file {
          bucket
          region
          key
        }
        userID
        owner {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getReward = /* GraphQL */ `
  query GetReward($id: ID!) {
    getReward(id: $id) {
      id
      name
      file {
        bucket
        region
        key
      }
      createdAt
      updatedAt
    }
  }
`;
export const listRewards = /* GraphQL */ `
  query ListRewards(
    $filter: ModelRewardFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listRewards(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        file {
          bucket
          region
          key
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTrailLocationViews = /* GraphQL */ `
  query GetTrailLocationViews($id: ID!) {
    getTrailLocationViews(id: $id) {
      id
      trailID
      locationViewID
      trail {
        id
        name
        description
        duration
        type
        images {
          bucket
          region
          key
        }
        locations {
          nextToken
        }
        order
        trailProgress {
          nextToken
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      locationView {
        id
        name
        address
        description
        coordinates
        locationID
        location {
          id
          name
          address
          description
          coordinates
          userID
          createdAt
          updatedAt
        }
        userID
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        transactions {
          nextToken
        }
        trails {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listTrailLocationViews = /* GraphQL */ `
  query ListTrailLocationViews(
    $filter: ModelTrailLocationViewsFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTrailLocationViews(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        trailID
        locationViewID
        trail {
          id
          name
          description
          duration
          type
          order
          userID
          createdAt
          updatedAt
        }
        locationView {
          id
          name
          address
          description
          coordinates
          locationID
          userID
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const walletsByUser = /* GraphQL */ `
  query WalletsByUser(
    $userID: ID!
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelWalletFilterInput
    $limit: Int
    $nextToken: String
  ) {
    walletsByUser(
      userID: $userID
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const walletsByAddress = /* GraphQL */ `
  query WalletsByAddress(
    $address: String!
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelWalletFilterInput
    $limit: Int
    $nextToken: String
  ) {
    walletsByAddress(
      address: $address
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        title
        images
        description
        userID
        address
        user {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const transactionsBySenderID = /* GraphQL */ `
  query TransactionsBySenderID(
    $senderID: ID!
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTransactionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    transactionsBySenderID(
      senderID: $senderID
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const transactionsByVerifierID = /* GraphQL */ `
  query TransactionsByVerifierID(
    $verifierID: ID!
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTransactionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    transactionsByVerifierID(
      verifierID: $verifierID
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const transactionsByReceiverID = /* GraphQL */ `
  query TransactionsByReceiverID(
    $receiverID: ID!
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTransactionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    transactionsByReceiverID(
      receiverID: $receiverID
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const transactionsByFromToWalletID = /* GraphQL */ `
  query TransactionsByFromToWalletID(
    $fromWalletID: ID!
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTransactionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    transactionsByFromToWalletID(
      fromWalletID: $fromWalletID
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const transactionsByToWalletID = /* GraphQL */ `
  query TransactionsByToWalletID(
    $toWalletID: ID!
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTransactionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    transactionsByToWalletID(
      toWalletID: $toWalletID
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const transactionsByXummTransactionID = /* GraphQL */ `
  query TransactionsByXummTransactionID(
    $xummTransactionID: String!
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTransactionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    transactionsByXummTransactionID(
      xummTransactionID: $xummTransactionID
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        status
        amount
        description
        type
        senderID
        verifierID
        receiverID
        conditions {
          nextToken
        }
        expiration
        sender {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        verifier {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        receiver {
          id
          username
          email
          bio
          location
          backgroundImage
          createdAt
          updatedAt
        }
        fulfillment
        condition
        fromWalletID
        toWalletID
        toWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        fromWallet {
          id
          title
          images
          description
          userID
          address
          createdAt
          updatedAt
        }
        xummTransactionID
        locationViewID
        rewardID
        reward {
          id
          name
          createdAt
          updatedAt
        }
        transactionProgress {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
