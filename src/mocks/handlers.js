import { rest } from 'msw'
import products from './products'
import nfts from './nfts'
import assets from './assets'

export const handlers = [
    rest.get('/products', (req, res, ctx) => {
        return res(ctx.status(200), ctx.delay(500), ctx.json(products))
    }),
    rest.get('/nfts', (req, res, ctx) => {
        return res(ctx.status(200), ctx.delay(500), ctx.json(nfts))
    }),
    rest.get('/assets', (req, res, ctx) => {
        return res(ctx.status(200), ctx.delay(500), ctx.json(assets))
    })
]

