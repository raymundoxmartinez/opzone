const projects = [
  {
    id: '1',
    createdAt: 'September 14, 2016',
    title: 'Health Fund',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',
    images: ['/images/healthFund.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'fund',
  },
  {
    id: '2',
    createdAt: 'September 14, 2016',
    title: 'Education Fund',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/educationFund.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'fund',
  },
  {
    id: '3',
    createdAt: 'September 14, 2016',
    title: 'Construction Fund',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/hospital.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'fund',
  },
  {
    id: '4',
    createdAt: 'September 14, 2016',
    title: 'Real Estate Fund',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/realEstateFund.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    category: 'fund',
  },
  {
    id: '5',
    createdAt: 'September 14, 2016',
    title: 'Finance Fund',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/financeFund.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'fund',
  },
  {
    id: '6',
    createdAt: 'September 14, 2016',
    title: 'Agriculture Fund',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',
    images: ['/images/agricultureFund.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'fund',
  },
  {
    id: '7',
    createdAt: 'September 14, 2016',
    title: 'Hospital Renovation',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/hospital.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'asset',
  },
  {
    id: '8',
    createdAt: 'September 14, 2016',
    title: 'School Project',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/educationAsset.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'asset',
  },
  {
    id: '9',
    createdAt: 'September 14, 2016',
    title: 'Investment Group',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/financeAsset.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'asset',
  },
  {
    id: '10',
    createdAt: 'September 14, 2016',
    title: 'Invest in Property',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/realEstateAsset.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'asset',
  },
  {
    id: '11',
    createdAt: 'September 14, 2016',
    title: 'Large Iced Coffee',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/coffeeAsset.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'cg',
  },
  {
    id: '12',
    createdAt: 'September 14, 2016',
    title: 'Chocolate Chip Cookies',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/cookiesAsset.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'cg',
    price: '$1.00',
  },
  {
    id: '13',
    createdAt: 'September 14, 2016',
    title: 'Orange Juice',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/orangeJuiceAsset.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'cg',
    price: '$1.50',
  },
  {
    id: '14',
    createdAt: 'September 14, 2016',
    title: 'My Cafe',
    location: '300 Pine Ridge Dr #APT B17 Morehead, Kentucky(KY), 40351',
    description:
      'Site Information: • ~55,000 sq ft building • ~2.25 acre parking lot • Building was last used forSite',

    images: ['/images/coffeeShopStore.jpeg'],
    goal: '$250,000',
    raised: '$30,000',
    contributions: Array(10).fill(1),
    address: 'rp8cjyExh2LUNqYFU468kPUmKWEMWEWwy2',
    category: 'store',
    price: 'N/A',
  },
]
export default projects
