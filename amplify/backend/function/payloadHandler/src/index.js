/* Amplify Params - DO NOT EDIT
    ENV
    REGION
    XUMM_API_KEY
    XUMM_API_SECRET
Amplify Params - DO NOT EDIT */

const { XummSdk } = require('xumm-sdk')

exports.handler = async (event) => {
    console.log("EVENT\n" + JSON.stringify(event))
    const Sdk = new XummSdk(process.env.XUMM_API_KEY, process.env.XUMM_API_SECRET)
    let xummResponse
    switch (event.httpMethod) {
        case ('POST'):
            xummResponse = await Sdk.payload.create(event.body)
            break
        case ('GET'):
            const payloadId = event.queryStringParameters.id
            xummResponse = await Sdk.payload.get(payloadId)
            break
        default:
            break
    }

    const response = {
        statusCode: 200,
        body: JSON.stringify(xummResponse),
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*"
        }
    };
    return response;
};

