const cc = require('five-bells-condition')
const crypto = require('crypto')

exports.handler = async (event) => {
	let fulfillmentAndCondition
	switch (event.httpMethod) {
		case ('GET'):

			const preimageData = crypto.randomBytes(32)
			const fulfillment = new cc.PreimageSha256()
			fulfillment.setPreimage(preimageData)

			const condition = fulfillment.getConditionBinary().toString('hex').toUpperCase()
			console.log('Condition:', condition)

			// Keep secret until you want to finish the escrow
			const fulfillment_hex = fulfillment.serializeBinary().toString('hex').toUpperCase()
			console.log('Fulfillment:', fulfillment_hex)
			fulfillmentAndCondition = { condition, fulfillment: fulfillment_hex }
			fulfillmentAndCondition = { condition, fulfillment: fulfillment_hex }
			break
		default:
			break
	}
	const response = {
		statusCode: 200,
		//  Uncomment below to enable CORS requests
		headers: {
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Headers": "*"
		},
		body: JSON.stringify(fulfillmentAndCondition),
	};
	return response;
};
